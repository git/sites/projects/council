\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{councilsummary}

\LoadClass[a4paper]{article}

\RequirePackage{mathptmx}
\RequirePackage[scaled=.90]{helvet}
\RequirePackage{courier}

\RequirePackage{datetime}
\RequirePackage{textcomp}
\RequirePackage{color}
\RequirePackage{underscore}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}

\g@addto@macro\@documentclasshook{
  \RequirePackage{hyperref}
}

\newcommand{\summary}[3]{
 \begin{center}\LARGE \bf
  {Council meeting summary\\
  #3\ \protect\monthname[#2]\ #1}
 \end{center}
 \vspace*{1cm}
 \officiallogs{#1\twodigit{#2}\twodigit{#3}}
}

\newcommand{\agendaitem}[1]{
  \section{#1}
}

\newcommand{\gentooifempty}[3]{%
  \sbox0{#1}%
  \ifdim\wd0=0pt
    {#2}% if #1 is empty
  \else
    {#3}% if #1 is not empty
  \fi
}

\newcommand{\gentoobugtitle}[1]{}
\renewcommand{\textrangle}{\ensuremath{>}}
\renewcommand{\textlangle}{\ensuremath{<}}
\InputIfFileExists{decisions.bug}{}

\newcommand{\bug}[1]{\href{https://bugs.gentoo.org/#1}{%
bug {#1}}%
\gentooifempty{\gentoobugtitle{#1}}{}{ (``{\em\gentoobugtitle{#1}}'')}%
}

\newcommand{\gentoomailfrom}[1]{}
\newcommand{\gentoomailsubject}[1]{}
\newcommand{\gentoomaildate}[1]{}
\InputIfFileExists{decisions.mld}{}

\newcommand{\agoref}[2]{%
\gentooifempty{\gentoomailsubject{#2}}{%
\href{https://archives.gentoo.org/#1/message/#2}{%
mailing list {#1}, message {#2}}}{%
\href{https://archives.gentoo.org/#1/message/#2}{%
mailing list message by \gentoomailfrom{#2}} (list #1, 
subject ``{\em\gentoomailsubject{#2}}'', \gentoomaildate{#2})}}

\newcommand{\gleptitle}[1]{}
\InputIfFileExists{decisions.glg}{}

\newcommand{\twodigits}[1]{\ifnum#1<10 0\fi#1}
\newcommand{\threedigits}[1]{\ifnum#1<100 0\fi\twodigits{#1}}%
\newcommand{\fourdigits}[1]{\ifnum#1<1000 0\fi\threedigits{#1}}%
\newcommand{\fivedigits}[1]{\ifnum#1<10000 0\fi\fourdigits{#1}}

\newcommand{\glep}[1]{%
\href{https://www.gentoo.org/glep/glep-\fourdigits{#1}.html} { GLEP 
{#1}}%
\gentooifempty{\gleptitle{#1}}{}{
 (``\gleptitle{#1}'')}}

\newcommand{\devname}[1]{}
\newcommand{\devlink}[1]{}
\InputIfFileExists{decisions.deg}{}

\newcommand{\dev}[1]{%
\gentooifempty{\devname{#1}}{#1}{%
\gentooifempty{\devlink{#1}}{\devname{#1} (#1)}{%
{\devname{#1} (#1)}}%
}}

\newcommand{\wgoref}[1]{\href{https://wiki.gentoo.org/wiki/#1}{wiki page {#1}}}

\newcommand{\eclass}[1]{{#1}.eclass}

\newcommand{\package}[1]{\href{https://packages.gentoo.org/packages/#1}{#1}}

\newcommand{\vote}[2]{
\begin{quote}
{\bf vote:}
{\it #1} --- {#2}
\end{quote}
}

\newcommand{\officiallogs}[1]{
Follow the links here for the
\href{https://projects.gentoo.org/council/meeting-logs/#1.txt}{log of the 
council meeting} and the
\href{https://projects.gentoo.org/council/meeting-logs/#1-summary.txt}{official 
summary}.
}

\definecolor{darkblue}{rgb}{0,0,0.7}
\definecolor{darkred}{rgb}{0.7,0,0}

\g@addto@macro\@documentclasshook{
  \hypersetup{%
    colorlinks=true,
    linkcolor={darkblue},
    citecolor={darkblue},
    urlcolor={darkblue},
    pdftitle={Council meeting summary},
  }
}

\setlength{\parindent}{0cm}
