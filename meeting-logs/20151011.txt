[20:55:55] -*- WilliamH is here
[20:59:29] -*- K_F is here, but FYI a bit sluggish for next few minutes while I get settled
[21:00:05] <jlec> hi there
[21:00:13] <dilfridge> hi all
[21:00:17] <dilfridge> it's time :)
[21:00:31] <dilfridge> I forgot, am I chairing again today?
[21:00:33] <rich0> Oh boy...
[21:00:54] <dilfridge> !expn council
[21:00:55] <willikins> council = jlec,k_f,blueness,dilfridge,rich0,williamh,ulm,
[21:01:01] <dilfridge> roll call :)
[21:01:05] -*- blueness here
[21:01:08] -*- WilliamH here
[21:01:15] -*- jlec here
[21:01:37] -*- rich0 here
[21:02:21] <dilfridge> only ulm missing, let's give him a moment
[21:02:49] <rich0> ulm isn't all that interested in EAPI, is he?  :)
[21:02:54] <dilfridge> hrhr
[21:04:06] <dilfridge> I'm texting him
[21:04:27] <blueness> k
[21:04:34] <blueness> isn't he in germany?
[21:05:12] <dilfridge> done and delivered
[21:05:16] <WilliamH> blueness: sure, but I think dilfridge  is too.
[21:05:21] <rich0> +49 wherever that is
[21:05:24] <dilfridge> exactly
[21:05:41] <dilfridge> ok let's start
[21:05:44] -*- ulm here
[21:05:51] <dilfridge> excellent timing
[21:05:53] <blueness> yay!
[21:06:07] <dilfridge> 2) EAPI=4 deprecation
[21:06:16] <dilfridge> https://archives.gentoo.org/gentoo-project/message/b5b7aa83ddd64fdfa84284c1ceddcec6
[21:06:17] <WilliamH> I see no reason not to.
[21:06:23] <jlec> I am all for it
[21:06:24] <dilfridge> same here
[21:06:39] <K_F> fine for me
[21:06:43] <rich0> ++
[21:06:46] <blueness> i'm okay with it too
[21:06:47] <dilfridge> I see no obvious disadvantages of 5 over 4
[21:06:47] -*- WilliamH yes
[21:06:58] <ulm> fine with me too
[21:07:08] <ulm> 5 is 4 done right :)
[21:07:18] <blueness> we once talked about having a release schedule after a new EAPI.  i just mention this in passing.
[21:07:40] <dilfridge> ok, as far as we can see, that's pretty much unanimous in favour of EAPI=4 deprecation. 
[21:07:48] <dilfridge> I guess we dont need a separate vote.
[21:07:50] <jlec> looks like
[21:08:02] <dilfridge> 2) done,yay!
[21:08:08] <dilfridge> 3) Behaviour of asterisk with = dependency operator [2,3,4]
[21:08:16] <dilfridge> [3] https://bugs.gentoo.org/show_bug.cgi?id=560466
[21:08:16] <dilfridge> [4] 
[21:08:16] <dilfridge> https://gitweb.gentoo.org/proj/portage.git/commit/?id=d4966a381ee4577818bd972946647338046715b1
[21:08:16] <dilfridge> [5] https://archives.gentoo.org/gentoo-project/message/a8b5b499b9dbfdaea57a8f2a158c1fe7
[21:08:57] <dilfridge> the retroactive change is already in ~arch portage, right?
[21:08:59] <ulm> I would be very much in favour of fixing this retroactively
[21:09:14] <ulm> especially since it is in portage already
[21:09:33] <dilfridge> err
[21:09:35] <ulm> and I've seen no major fallout of it
[21:09:43] <dilfridge> [2] https://archives.gentoo.org/gentoo-project/message/65abf95ef1ea05a1aa3bc716be386a64
[21:09:43] <dilfridge> [3] https://bugs.gentoo.org/show_bug.cgi?id=560466
[21:09:43] <dilfridge> [4] 
[21:09:43] <dilfridge> https://gitweb.gentoo.org/proj/portage.git/commit/?id=d4966a381ee4577818bd972946647338046715b1
[21:09:51] <dilfridge> these are the correct links, sorry
[21:09:55] <rich0> So, this would be to make the behavior the same as Paludis is?
[21:10:00] <rich0> Obviously no impact there in that case.
[21:10:14] <dilfridge> did anyone hear about fallout ?
[21:10:23] <jlec> not me
[21:10:24] <ulm> there was one problem in zero_chaos's overlay but afaik that has been fixed
[21:10:30] <dilfridge> the one thing I know is that pentoo profiles broke badly
[21:10:32] <jlec> everything is working fine
[21:10:59] <ulm> rich0: paludis has two behaviours
[21:11:08] <WilliamH> So which behaviour are we shooting =foo-2* means =foo-2.*?
[21:11:16] <WilliamH> s/shooting/shooting for/
[21:11:27] <dilfridge> WilliamH: yes, effectively
[21:11:39] --> keytoaster (~tobias@gentoo/developer/keytoaster) hat #gentoo-council betreten
[21:11:40] --> toralf (~toralf@x50abb077.dyn.telefonica.de) hat #gentoo-council betreten
[21:11:45] <dilfridge> 2* can be fulfilled by 2.1.0, but NOT by 20.1
[21:11:55] <jlec> That is what makes most sense to me
[21:12:33] <dilfridge> ok I suggest we vote a,b,c as in the mail [2]
[21:12:36] <rich0> I'm fine with ulm's proposal
[21:12:50] <dilfridge> a is "retroactively", b is "EAPI6", c is "later than 6"
[21:12:54] -*- dilfridge a
[21:12:56] <jlec> a
[21:12:58] -*- rich0 a
[21:13:12] -*- ulm a
[21:13:15] <ulm> obviously
[21:13:23] <blueness> this owrks -> the asterisk acts as a wildcard for any further components
[21:13:44] <blueness> a
[21:13:49] -*- WilliamH a
[21:13:51] <ulm> blueness: PMS wording is here: https://560466.bugs.gentoo.org/attachment.cgi?id=412594
[21:13:58] <K_F> a
[21:14:08] <ulm> thanks :)
[21:14:10] <dilfridge> that's unanimous, excellent
[21:14:23] <dilfridge> 3) done, yahoo!
[21:14:31] <jlec> that were the easy ones
[21:14:36] <dilfridge> 4) Runtime dependencies and dynamic dependency deprecation [5,6]
[21:14:45] <dilfridge> [5] https://archives.gentoo.org/gentoo-project/message/a8b5b499b9dbfdaea57a8f2a158c1fe7
[21:14:45] <dilfridge> [6] http://article.gmane.org/gmane.linux.gentoo.devel/97742
[21:15:07] <dilfridge> actually [6] is more interesting :)
[21:15:13] <rich0> Ugh, so much for this not coming up today.
[21:15:39] <rich0> 6 has been revised
[21:15:47] <dilfridge> link?
[21:15:49] <rich0> http://thread.gmane.org/gmane.linux.gentoo.devel/97428/focus=97742
[21:16:22] <rich0> That is the latest, but kensington/jmbsvicetto did make some arguments for holding off on IRC.  I'm torn personally.
[21:16:27] -*- WilliamH gets a headache reading the criteria for when to bump or not
[21:16:45] <rich0> The last email is actually the simplest.
[21:16:48] <rich0> So, start with that.
[21:17:01] <dilfridge> I'm all fine with 1 and 2, but have strong reservations about 3 and 4
[21:17:41] <rich0> A possible compromise is to make a much more general policy statement, and turn all the stuff in my email into guidelines subject (to a degree) to maintainer discretion
[21:17:44] <-> kallamej_ heißt jetzt kallamej
[21:18:17] <K_F> that might be a better option than trying to hit all the corner cases
[21:18:21] <rich0> It is likely that devs will think of other exceptions to the rule
[21:18:30] <WilliamH> can you link the archive.g.o message? gmane is a pain for me to read from.
[21:18:41] <ulm> the general guideline should be that the maintainer must bump if the RDEPEND change can break users' systems
[21:18:43] <rich0> WilliamH: that still exists?  :)
[21:18:56] <rich0> ulm: that is the gist of it
[21:18:58] <WilliamH> archives.gentoo.org
[21:18:59] <dilfridge> the eclass case is more where common sense should kick in 
[21:19:08] <mgorny> if i may, i think a simple rule would be to request that dynamic dependencies must not be applied or assumed they apply
[21:19:17] <rich0> mgorny: that was more what I was thinking of
[21:19:28] <rich0> really a combination of hte two
[21:20:19] <K_F> WilliamH: https://archives.gentoo.org/gentoo-dev/message/499a8bbc0513301f517ed14e7ee85d30
[21:20:49] <rich0> "Maintainers must not assume that dynamic dependencies will be applied by the package manager.  When changing dependencies the maintainer should revision the ebuild if the changes are likely to cause problems for end users."  Then we'd endorse the initial guidelines but it woudl be clear that they do not require council approval to be updated.
[21:21:25] <rich0> The real issue is that the problems are subtle and can show up months after the change that caused them.
[21:21:49] <rich0> Changes without bumps cause a user's system to not match what repoman is regularly checking.
[21:21:51] <jlec> That sounds good, still the eclass thing creates problems
[21:22:02] <rich0> jlec: what is your eclass concern?
[21:22:07] <rich0> Obviously it creates work.
[21:22:13] <rich0> I don't see how it creates "problems" otherwise.
[21:22:25] <jlec> How would you handle that? 3 and 4 imply massive work
[21:22:35] <jlec> *implicate
[21:22:38] <dilfridge> rich0: in my opinion 3 and 4 are not realistic
[21:22:46] <dilfridge> as jlec says
[21:22:46] <blueness> rich0:  do we have a case where the problem showed up much later, like a classic case that we could analys?
[21:23:05] <rich0> I know I had one a few weeks ago, but I didn't capture the cause.
[21:23:21] <jlec> Do we have any date on how much the whole thing is really present in eclasses?
[21:23:32] <jlec> *data
[21:23:34] <rich0> I'll probably never see it again since I gave up on the whole thing and am just using --changed-deps, which is what I'll recommend to anybody I can in the meantime :)
[21:23:43] <dilfridge> but then, usually what happens when deps in an eclass are changed is for example that a minimum version requirement is raised
[21:23:43] <blueness> rich0:  i'm okay with asking for the revbump by policy but it would be nice to have portage cathc this for us
[21:24:03] <rich0> blueness: by "catch" do you mean repoman?
[21:24:04] <dilfridge> and that's not so extremely critical since the first rebuilt package will pull it in
[21:24:08] <rich0> Or do you mean dynamic deps?
[21:24:15] <WilliamH> My only concern is revbumps means ~->stable again, so 30 days then hit the arch teams w/ stable requests.
[21:24:23] <dilfridge> nah
[21:24:24] <blueness> rich0:  yes repoman (which is part of portage) but also on the user side
[21:24:29] <mgorny> to be honest, i don't think we really need this precise policies
[21:24:34] <rich0> dilfridge: my proposal 6 already covers min deps being raised
[21:24:41] <mgorny> a good start would be disallowing developers to say 'i won't revbump, dyndeps, i don't care'
[21:24:53] <dilfridge> where's 6?
[21:24:55] <WilliamH> and revbumping eclasses means rewriting all ebuilds to inherit a new eclass.
[21:25:03] <rich0> WilliamH: I wouldn't propose holding the revbumps in ~arch.
[21:25:10] <rich0> That actually leaves stable systems broken for 30 days.
[21:25:11] <blueness> the eclasses are the problems
[21:25:19] <rich0> If we were going to do that then we should revision the ebuilds.
[21:25:19] <dilfridge> oh dear somewhere after 4b and 3a
[21:25:36] <dilfridge> mgorny: yes.
[21:25:37] <blueness> mgorny:  any chance of getting repoman to yell "you should revbump because dyndeps"
[21:25:56] <rich0> dilfridge: use the latest version of my proposal (from this morning)
[21:26:13] <rich0> It is actually the simplest anyway
[21:26:17] <WilliamH> rich0: some devs think stable = untouchable.
[21:26:31] <rich0> WilliamH: then don't change the eclass
[21:26:38] <rich0> If you change the eclass you're already touching stable
[21:26:51] <rich0> The revbump is just avoiding breaking it while you touch it.
[21:26:56] <WilliamH> rich0: agreed, I'm just pointing out what I've seen on -dev.
[21:27:26] <dilfridge> ok how about, 
[21:27:30] <blueness> the only way to avoid touching stable when editing an eclass is by bumping it
[21:27:32] <ulm> WilliamH: I am am not aware of any such absolute policy about not touching stable
[21:27:38] <jlec> Is it really practical to revbump eclasses so often?
[21:27:45] <dilfridge> it's not.
[21:27:48] <ulm> jlec: not really
[21:27:50] <rich0> jlec: that was my concern with that approach
[21:28:05] <dilfridge> also it's not practical to revbump 1000 perl-module.eclass consumers
[21:28:07] <jlec> so the only solution is revbumping the ebuilds including stable
[21:28:11] <WilliamH> Yeah same here, I"m not comfortable with revbumping eclasses that way
[21:28:35] <dilfridge> at least not if it can be avoided somehow. 
[21:28:44] <dilfridge> how about:
[21:28:47] <rich0> dilfridge: in the case of PERL do RDEPEND changes often have that would actually cause breakage without dynamic deps?
[21:28:58] <dilfridge> no
[21:29:04] <dilfridge> i think that's harmless
[21:29:05] <jlec> So again, how likely is such a change in an eclass?
[21:29:13] <blueness> you could write eclasses to respond to changes to some global version variable
[21:29:14] <mgorny> so far perl only broke stuff by not revbumping virtuals...
[21:29:36] <blueness> so you edit the eclass in place and add a version variable which turns on you code when its set at some future date
[21:29:36] <dilfridge> s/break/break for dyndeps disabled/ :)
[21:29:38] <rich0> dilfridge: in that case there is no need to bump anything (if there really is no harm).  And if that isn't already in the proposal 6 exceptions we can of course add it.
[21:29:44] <WilliamH> I would rather see some way to determin if the change will break systems than an absolute policy.
[21:29:49] <WilliamH> determine *
[21:29:58] <dilfridge> erhm
[21:30:00] <dilfridge> ok 
[21:30:02] <dilfridge> how about
[21:30:05] <dilfridge> "Maintainers must not assume that dynamic dependencies will be applied by the package manager.  When changing dependencies the maintainer should revision the ebuild if the changes are likely to cause problems for end users."
[21:30:16] <dilfridge> ^ we vote on this from rich0 as policy, 
[21:30:33] <dilfridge> and then vote on the details as recommendations (s/must/should/)
[21:30:39] <K_F> will we get pushback on that phrasing for build deps?
[21:31:20] <ulm> dilfridge: is this identical with rich0's earlier wording?
[21:31:21] <K_F> or is that covered by "likely to cause problems for end users" already
[21:31:22] <rich0> I'm fine with "runtime dependencies"
[21:31:24] <dilfridge> ulm: yes
[21:31:34] <rich0> but it is also covered by the "likely to cause problems..."
[21:31:44] <jlec> K_F: only if those deps change runtime
[21:31:45] <K_F> I'm fine with either, anyways
[21:31:59] <rich0> In any case, the devmanual doesn't have to be 100% council approved.
[21:32:05] <rich0> The intent is the most important thing.
[21:32:22] <blueness> rich0:  i was just about to ask that this be put in the devmanual
[21:32:36] <rich0> blueness: I'd put the policy and the initial guidelines there
[21:32:44] <rich0> And then as appropriate they can evolve.
[21:32:52] <blueness> rich0:  i'd like to illustrate for them what the problem is
[21:32:52] <rich0> maintainers are already given some discretion here
[21:33:10] <rich0> QA as well, etc.
[21:33:49] <rich0> blueness: I could probably toss together a rough example
[21:34:04] <dilfridge> ok here's the new wording, we can always talk about devmanual later:
[21:34:09] <rich0> Package RDEPENDS on || ( foo-1 bar )
[21:34:18] <dilfridge> "Maintainers must not assume that dynamic dependencies will be applied by the package manager. When changing runtime dependencies the maintainer should revision the ebuild if the changes are likely to cause problems for end users."
[21:34:48] <dilfridge> vote?
[21:34:51] <rich0> dilfridge: sure
[21:34:52] -*- rich0 yes
[21:34:55] -*- blueness yes
[21:34:55] -*- K_F yes
[21:34:57] -*- ulm yes
[21:34:58] -*- dilfridge yes
[21:34:59] -*- jlec yes
[21:35:04] -*- WilliamH yes
[21:35:10] <dilfridge> that's unanimous
[21:35:34] <dilfridge> do you want to vote on single points of rich0's proposals?
[21:35:36] <rich0> Do you want me to s/must/should the proposals as initial guidelines?
[21:35:45] <rich0> We can vote on them if we wish.
[21:36:05] <WilliamH> not necessary I don't think since we aren't making them policies are we? I would be against enstating specific policies for this.
[21:36:11] <rich0> I think the only question is which way to handle eclasses, but I think we're all leaning towards 4.  Nothing is wrong with 3 though.
[21:36:36] <rich0> That could also be discretionary, with 4 being the general recommendation.
[21:36:39] <blueness> i have no idea really the eclasses are simply going to be a problem
[21:37:17] -*- dilfridge is strongly against 3, and would like 4 to be at most a recommendation with some caveat about "if likely to cause problems"
[21:37:18] <WilliamH> What about the --newuse/--changed-use situation?
[21:37:21] <rich0> Do we just want to do a round of email to wordsmith the guidelines and send those out as having endoresment, but making it clear that they're not hard rules.
[21:37:52] <WilliamH> for example if I add foo? ( bar ) to rdepends or drop that...
[21:37:53] <rich0> I think that 4 should happen a LOT more often than it does now.
[21:38:09] <rich0> If my sense is that devs are not bumping for eclass changes in general I'll just keep recommending that people use --changed-deps
[21:38:20] <blueness> (hmm looking at the elcasses there' s like a whole bunch of mozconfig-v5.*.eclass)
[21:38:21] <rich0> Then they'll get a bazillion rebuilds all the time whether they need them or not.
[21:38:31] <jlec> I would go along the lines for ebuilds. Maintainers must take care and should apply either 3 or 4 when ever it has to.
[21:38:37] <dilfridge> rich0: that comes from having many packages using one eclass (say >200)
[21:38:47] <WilliamH> rich0: bumping eclasses requires approval each time on the ml since it is affectively a new eclass.
[21:39:08] <rich0> WilliamH: probably wouldn't hurt to discuss the right way to handle revbumps at that time then
[21:39:13] <K_F> jlec: yeah, a general phrasing to that extent could work
[21:40:01] <dilfridge> I dont think we want to rebuild all python packages whenever a python variant is added or removed.
[21:40:03] <rich0> I suggest we take the exact wording up by email/etc, and not require another vote.  We just publish whatever seems best to get everybody started.
[21:40:05] <dilfridge> Oh wait...
[21:40:29] <rich0> Then QA can own the policy long-term/etc, as with most devmanual issues.
[21:40:47] <dilfridge> ok I'm fine with pushing this to e-mail discussion first. it's only recommendations anyway.
[21:41:00] <dilfridge> i.e. s/must/should/
[21:41:20] <dilfridge> anyone else?
[21:41:21] <rich0> Sure, I'll throw something out there.  I don't think this needs to be private - maybe I'll just reply to the existing thread.
[21:41:25] <dilfridge> ++
[21:41:38] <dilfridge> I think we have the important message out 
[21:41:49] <dilfridge> ('you can't rely on dyndeps')
[21:42:06] <jlec> ++
[21:42:12] <K_F> sounds good to me
[21:42:26] <dilfridge> 4) is done, yippi-ka-....
[21:42:31] <dilfridge> 5) Games policies [7]
[21:42:39] <dilfridge> [7] https://archives.gentoo.org/gentoo-project/message/16fc54d2bced9ff51b71d387eb0fb36b
[21:44:09] <ulm> I'm in favour of 1 but don't have a strong opinion about 2
[21:44:29] <K_F> ditto, that seems like maintainer discretion
[21:44:36] <WilliamH> I think 1 and 2 would bring us more in line with what most linuxes do wouldn't it?
[21:44:56] -*- WilliamH isn't really into gaming on Linux
[21:45:05] <ulm> K_F: we should have some distro wide uniformity though
[21:45:21] <dilfridge> that is the critical point, yes
[21:45:37] <blueness> rich0:  are you going go thorugh all the games ebuilds and fix them in line with your recommendations?
[21:45:51] <WilliamH> Why are games so different from other applications that they need their own directory /usr/games?
[21:46:00] <dilfridge> history
[21:46:01] <K_F> ulm: but is it really something that is specific to games?
[21:46:06] <rich0> blueness: I wasn't planning on it :)
[21:46:19] <dilfridge> WilliamH: imagine an undergraduate computer lab at university :P
[21:46:20] <blueness> rich0:  okay so let me ask, how will be get there?
[21:46:20] <rich0> But, for the most part changing games.eclass would probably do most of the work.
[21:46:35] <ulm> WilliamH: one argument is that root shouldn't have /usr/games/bin in their PATH
[21:46:38] <rich0> Or others could do it, or it could happen over time
[21:46:43] <ulm> for security reasons
[21:46:55] <rich0> ulm: should root also not have /usr/bin in their path, for the same reasons?
[21:47:08] <dilfridge> do our security policies and sec team ... apply to games the same as to the rest?
[21:47:12] <rich0> Are games more susceptible to security risks than, eg, browsers?
[21:47:13] <jlec> ulm's point is the only valid one for me in favout of /usr/games
[21:47:14] -*- dilfridge hopes so
[21:47:29] <jlec> Elsewise I wouldn't distinguish between games and the rest
[21:47:34] -*- WilliamH hopes so too
[21:47:35] <K_F> dilfridge: in theory, yes.
[21:47:38] <blueness> rich0:  it just reduces the risk of injection into a directory that we usually don't watch careully /usr/games/bin
[21:47:57] <K_F> dilfridge: as a lot of binary cruft is involved, and no disclosure of security vulnerabilities through the normal channels. not encessarily, but it depends ..
[21:48:09] <rich0> I get the argument, but if we went that route we'd have /usr/X11/bin and a lot of other stuff like that
[21:48:12] <WilliamH> dilfridge: That's why I wanted to kick proprietary games with known security issues out of the tree a few meetings back, but that's another issue.
[21:48:13] <K_F> s/encessarily/nencessarily/
[21:48:19] <dilfridge> but binary cruft goes to /opt, right?
[21:48:20] <jlec> K_F: binary has to go to /opt
[21:48:20] <ulm> K_F: "binary cruft" should go into /opt though
[21:48:26] <ulm> :)
[21:48:56] <ulm> this point has been made I guess :)
[21:49:01] <K_F> indeed, I was thinking more of the general case of whether it is tracked, it is, but to a lesser extent
[21:49:03] <WilliamH> Actually I wasn't talking about just games back then, anything with known security issues that is abandonware.
[21:49:05] <rich0> Honestly, I don't really care a great deal about it.  The issue has been festering for ages it seems and it keeps coming up.
[21:49:21] <blueness> rich0:  suppose we 1) edit the eclass, 2) remove /usr/games/bin from $PATH in baselayout, 3) get rid of user games, and just push that out, what will happen to user's sytems?
[21:49:45] <dilfridge> did anyone from games team (?) comment?
[21:49:48] <WilliamH> But, yeah, binary stuff should go in /opt; that is fhs
[21:49:53] <rich0> dilfridge: not publicly
[21:50:00] <ulm> blueness: what do you mean by "get rid of user games"?
[21:50:10] --> prometheanfire (~promethea@gentoo/developer/prometheanfire) hat #gentoo-council betreten
[21:50:37] <mgorny> WilliamH: sorry to interrupt but it's actually not, it's just gentoo reuse of /opt, but that's offtopic
[21:50:38] <blueness> ulm:  the unix user and group "games"
[21:50:46] <ulm> ah
[21:50:59] <blueness> yellow eclass # cat /etc/group | grep games
[21:50:59] <blueness> users:x:100:games
[21:51:00] <blueness> games:x:35:blueness
[21:51:05] <WilliamH> Those are standard, but I think they are used differently than we were using them.
[21:51:09] <ulm> may we should concentrate on fixing what is broken?
[21:51:22] <ulm> that is mostly the games user and group
[21:51:30] <dilfridge> ok so
[21:51:34] <ulm> these path issues aren't really breakage
[21:51:38] <rich0> ulm: well, what is most broken is the interactions between games devs
[21:51:51] <dilfridge> point 1 is independent of the sec consideration
[21:51:52] <ulm> that too
[21:51:54] <WilliamH> rich0: true
[21:51:55] <rich0> If you have a wrench that works on that I'll gladly apply it.
[21:52:04] <dilfridge> rich0: yes, sadly we can't fix that by vote
[21:52:25] <mgorny> tbh, i think the correct policy for paths is 'use what upstream uses', it's both simple and clear
[21:52:33] <dilfridge> hmm
[21:52:42] <mgorny> and usually covers stuff like /usr/share/games but not gentoo inventions like /usr/games/lib*
[21:52:43] -*- WilliamH would tend to agree with mgorny there... simple and clear...
[21:53:18] <ulm> mgorny: I won't trust games upstreams on that too much
[21:53:45] <blueness> mgorny:  almost except when it violates fhs
[21:54:00] <K_F> yeah, some form of uniformity is good. I just question whether games warrents any kind of special treatment from other apps
[21:54:01] <blueness> and then we should submit a report upstream
[21:54:02] <mgorny> FHS only says /usr/games/bin, /usr/share/games, /var/games AFAICS
[21:54:24] <ulm> FHS say /usr/games/ for binaries, not /usr/games/bin/
[21:54:25] <mgorny> all of them optional
[21:54:27] <blueness> does fhs actually say /usr/games/bin ???
[21:54:29] -*- WilliamH checks fhs
[21:54:33] <mgorny> ulm: oh right
[21:54:43] <jlec> good to know
[21:54:46] <mgorny> i thought i'm reading it wrong but you are probably correct
[21:54:52] <ulm> but we cannot use /usr/games/ because it is blocked
[21:55:10] <rich0> /usr/games is even more braindead than what we have now
[21:55:17] <dilfridge> indeed
[21:55:24] <jlec> yipp
[21:55:25] <-- prometheanfire (~promethea@gentoo/developer/prometheanfire) hat #gentoo-council verlassen
[21:55:27] <K_F> 2x indeed
[21:55:33] <mgorny> so i'd say we use /usr/bin for sanity and conformity, problem solved!
[21:55:40] <blueness> i don't like /usr/games
[21:55:47] <WilliamH> http://refspecs.linuxfoundation.org/FHS_3.0/fhs-3.0.html
[21:55:59] <WilliamH> YOu folks can read it faster than I can ;-)
[21:56:05] <mgorny>  /usr/games/bin is non-FHS, /usr/games is braindead, we're left with /usr/bin ;-)
[21:56:10] <WilliamH> Where is "games" mentioned?
[21:56:19] <ulm> /var/games/ should be kept for highscore files, though
[21:56:19] <mgorny> WilliamH: 4.3
[21:56:32] <mgorny> and /usr/share/games if upstreams use that
[21:56:35] <WilliamH> mgorny: no, specifically where are "games" directories.
[21:56:45] <WilliamH> mgorny: /usr/games? /usr/share/games? etc?
[21:57:10] <mgorny> WilliamH: well, the section on /usr mentions games        Games and educational binaries (optional)
[21:57:15] <ulm> mgorny: yeah, /usr/share/games is fine
[21:57:16] <jlec> WilliamH: "Similarly, a /usr/lib/games hierarchy may be used in addition to the /usr/share/games hierarchy if the distributor wishes to place some game data there."
[21:57:17] -*- ulm doesn't care about 3rd level path components
[21:57:19] <blueness> 4.9.2
[21:57:52] <mgorny> blueness: we can skip /usr/local
[21:58:00] <blueness> yeah
[21:58:21] <blueness> 4.3 then
[21:58:22] -*- dilfridge reads, blinks, pours port
[21:58:54] <blueness> looks silly to me really
[21:59:16] <blueness> i don't know, i don't have a strong feeling about this one
[21:59:17] <mgorny> blueness: i already said today that FHS-3.0 looks like really poor spec
[21:59:49] <blueness> mgorny:  i don't know about the rest of fhs-3 i was just referring to all the games stuff
[21:59:57] <dilfridge> ok what do we do, should we just vote about the different points?
[21:59:58] <blueness> the rest might make sense
[22:00:05] <jlec>  /var/games is acceptable if no other /var dir is/can be used. Everything else should go to the normal dirs
[22:00:15] <mgorny> are you ready to vote for Rich's first two points? i'd really like to get the games@ bug assignment done today too
[22:00:23] <blueness> i'll be interested in reading their / and /usr separation
[22:00:48] <dilfridge> ok
[22:00:49] <ulm> jlec: /var/games is only for very specific files
[22:01:02] <rich0> The bug assignment seems less controversial to me (imo) - we should try to hit it.
[22:01:25] <ulm> heh, we could end up with /games after the /usr merge :p
[22:01:35] <jlec> ulm: data which normally should be in var/lib or /var/cache
[22:01:49] <dilfridge> ok this is getting a bit unfocussed
[22:01:53] <dilfridge> 1.  Decide that games should not be owned by a games group, and that
[22:01:53] <dilfridge> in the default configuration users should not have to be in the games
[22:01:53] <dilfridge> group to run games.
[22:02:06] <dilfridge> ^ shall we vote on this
[22:02:07] <blueness> ulm:  please no  /games
[22:02:15] <blueness> :)
[22:02:25] <WilliamH>  /games wouldn't happen
[22:02:42] <jlec> dilfridge: I am for that point
[22:02:57] <blueness> so let me ask, what's wrong with the current status quo?  i would think we can live with the paths, just get rid of the games group
[22:02:58] <ulm> dilfridge: are we voting on 1?
[22:03:18] -*- dilfridge thinks so but noone else
[22:03:22] -*- ulm could live with the paths too
[22:03:28] <K_F> blueness: is it even the games group that is a problem, or that it is being forced?
[22:03:37] <dilfridge> please no chaos
[22:03:49] <K_F> (which it naturally must if it is to have any intention, but..)
[22:03:50] <dilfridge> just let's stick with one version
[22:04:00] -*- ulm votes yes on 1
[22:04:07] -*- jlec too
[22:04:12] -*- dilfridge yes on 1
[22:04:21] -*- blueness yes
[22:04:22] -*- K_F yes on 1
[22:04:29] -*- rich0 yes on 1
[22:04:42] -*- WilliamH abstains... there are uses for the games group, but I don't remember what they are in other *nixes
[22:04:59] <dilfridge> ok that's 6 yes and one abstention, motion passed
[22:05:01] <dilfridge> now
[22:05:07] <rich0> WilliamH: often used for bones files/etc - there is a different group for that on Gentoo since games was already used
[22:05:08] <dilfridge> 2.  Games should be installed in /usr and not /usr/games as with most
[22:05:08] <dilfridge> applications
[22:05:26] -*- WilliamH yes treat them lik e other apps
[22:05:30] -*- rich0 yes
[22:05:34] -*- dilfridge no
[22:05:48] -*- K_F abstains
[22:05:50] -*- ulm abstains
[22:05:50] -*- jlec abstains
[22:05:58] -*- blueness no
[22:06:05] <rich0> :)
[22:06:20] <dilfridge> 2 yes, 2 no, 3 abstain, motion not passed
[22:06:43] <dilfridge> do we need to talk about 3 and 4 now?
[22:06:51] <ulm> gah, now we must fix games.eclass
[22:07:01] <mgorny> well, path replacement is 90% of games.eclass code
[22:07:07] <mgorny> plus hacks to make games use custom gentoo paths
[22:07:22] <ulm> yeah, so get rid of the user and group
[22:07:32] <ulm> and of prepgamesdirs() etc
[22:07:32] <WilliamH> This is why we should reconsider our position about installation locations
[22:07:39] <mgorny> plus replacements for all helpers that use custom paths
[22:07:43] <WilliamH> gentoo specific hacks are bad
[22:07:57] <mgorny> i think we should at least treat /usr/games/lib* which is completely gentoo-custom
[22:08:10] <jlec> I would leave it to the package and maintainer
[22:08:12] <K_F> the most important part is that nobody is required to use games eclass, and games project does not have monopoly on games
[22:08:13] <mgorny> and now there's /usr/games/bin vs FHS /usr/games
[22:08:20] <rich0> Honestly, if we're going to touch paths at all I'd rather just drop to /usr like everything else
[22:08:21] <K_F> do we really need to take it further in the council at this point?
[22:08:24] <ulm> WilliamH: in principle you're right, but there's a trade-off with the transition period
[22:08:31] <ulm> whoch could be very long
[22:08:32] <dilfridge> K_F: imho no
[22:08:34] <ulm> *which
[22:08:43] <jlec> especially KDE/GNOME gmaes can then go where ever they should be intendet to go
[22:08:53] <rich0> I suggest we either leave things as they are, or simplify, not change to something else which requires a lot of fussing around.
[22:09:11] <dilfridge> so ... I suggest we stop with this item here
[22:09:12] <mgorny> ulm: well, the alternative to transition period vs having two different solutions 'good'
[22:09:12] <blueness> maybe we can revisit this issue after one change at a time?
[22:09:18] <dilfridge> we can always revisit it again
[22:09:25] <WilliamH> What if joe maintainer decides to maintain a games ebuild and removes the use of the eclass?
[22:09:25] <blueness> let's get rid of the group and then see
[22:09:41] <blueness> also we're not requiring games.eclass so games might start to evolve to /usr/ anyhow
[22:09:57] <dilfridge> ok 
[22:09:59] <K_F> blueness: which is fine..
[22:10:07] <blueness> WilliamH:  i don't think anyone is going to impose use of the games.eclass
[22:10:18] <dilfridge> let's postpone the rest of this agenda item, since we're already overtime
[22:10:34] <dilfridge> should we quickly handle the next one?
[22:10:36] <rich0> We already decided maintainers don't have to use games.eclass last year
[22:10:41] <rich0> dilfridge: ++
[22:10:46] <dilfridge> 6) Games bugzilla component [8]
[22:10:46] <blueness> rich0:  correct
[22:10:53] <dilfridge> [8] https://archives.gentoo.org/gentoo-project/message/2175a9dde8a1fb614ccb75c60c43c8c8
[22:11:08] <mgorny> it's just following of the everyone can maintain games
[22:11:10] <rich0> We already decided people can maintain games outside the games team.
[22:11:18] <dilfridge> I thin the fastest way is to just vote
[22:11:20] <jlec> unnecessary at pointed out by Rich (?) in one of the replies
[22:11:27] -*- K_F aye
[22:11:30] -*- dilfridge yes
[22:11:33] -*- jlec yes
[22:11:34] -*- rich0 yes
[22:11:35] -*- blueness yes
[22:11:45] -*- ulm yes
[22:11:46] -*- WilliamH yes
[22:11:52] <dilfridge> that's unanimous
[22:11:59] <dilfridge> now
[22:12:05] <rich0> In general the bugzilla components could use cleanout anyway :)
[22:12:05] <dilfridge> shall we meet again next week?
[22:12:16] <blueness> looks like we're slowly dismantling games herd/project/eclass which is fine, its a huge component of the tree, and a real mess
[22:12:21] <blueness> slow = good
[22:12:25] <rich0> wfm
[22:12:42] <K_F> dilfridge: I'm fine with continuing now, next saturday I'm in champagne..
[22:12:44] <WilliamH> wfm
[22:12:48] <jlec> me too
[22:12:50] <K_F> sunday too for that matter..
[22:13:03] <WilliamH> continuing now is fine for me if people want to also
[22:13:04] <blueness> dilfridge:  yes to same time next week
[22:13:10] <blueness> i have to go
[22:13:42] <WilliamH> Who is still left -- who could continue?
[22:13:51] <mgorny> you can do a quick vote: now vs next week, and see when more people can attend ;-P
[22:13:55] <dilfridge> heh
[22:14:12] <dilfridge> I like the idea, let's vote "continue now or next week"
[22:14:17] -*- dilfridge next week
[22:14:18] -*- jlec next week
[22:14:21] -*- K_F now
[22:14:22] -*- WilliamH now
[22:14:22] -*- ulm now
[22:14:34] -*- rich0 next week
[22:14:48] <mgorny> and blueness had to go, so next week it is
[22:14:49] <jlec> blueness says next week
[22:14:57] <dilfridge> ok then next week it is
[22:15:00] <blueness> next week
[22:15:10] <dilfridge> thank you all aaaaand
[22:15:12] <dilfridge> bang
[22:15:14] <rich0> besides, this way we only tick off 25% of the dev population at a time
[22:15:14] <dilfridge> meeting closed
