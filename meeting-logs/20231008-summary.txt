Summary of Gentoo Council meeting 2023-10-08

Agenda
======

1. Roll call
2. Status of dissolving the foundation/moving under an umbrella org
3. Acknowledgement of Code of Conduct changes after Proctors project dissolution
4. Open bugs with Council participation
5. Open floor


Roll call
=========

Present: ajak, mgorny, soap, ulm
Absent: dilfridge, mattst88 (slacker), sam


Status of dissolving the Foundation/moving under an umbrella org
================================================================

After the trustees meeting on 20231002 (log [1]), ulm states that our
intention is to approach SPI once again after they did not respond to
our last inquiries. ulm notes that SPI is preferable for us due to its
being a 501c3 organization, while OSC is not.

[1] https://projects.gentoo.org/foundation/2023/20231002.log.txt

Acknowledgement of Code of Conduct changes after Proctors project dissolution
=============================================================================

The Council voted on a motion to "acknowledge ulm's change to the code
of conduct in the wake of proctors@ dissolution", with 4 yes and 3
absences.

Open bugs with Council involvement
==================================

Bug 784713 - Remove old distfile mirror layout
----------------------------------------------

Work is finished and took place in bug 914092 [2]. Bug is now closed.

[2] https://bugs.gentoo.org/914092

Open floor
==========

No topics.
