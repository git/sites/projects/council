--- Log opened Tue Apr 24 00:00:49 2007
01:04 -!- FuzzyRay|work [n=pvarner@gentoo/developer/FuzzyRay] has quit [Remote closed the connection]
02:43 -!- robbat2 is now known as robbat2|na
03:40 -!- mpagano [n=mpagano@pool-70-105-167-17.nwrknj.fios.verizon.net] has joined #gentoo-council
03:55 -!- mpagano [n=mpagano@pool-70-105-167-17.nwrknj.fios.verizon.net] has quit [Client Quit]
07:34 -!- KingTaco [n=kingtaco@gentoo/developer/kingtaco] has quit ["brb"]
07:36 -!- KingTaco [n=kingtaco@gentoo/developer/kingtaco] has joined #gentoo-council
07:36 -!- mode/#gentoo-council [+o KingTaco] by ChanServ
13:19 -!- onox [n=onox@kalfjeslab.demon.nl] has joined #gentoo-council
14:18 -!- fmccor|home [n=fmccor@gentoo/developer/fmccor] has quit [Read error: 110 (Connection timed out)]
14:22 -!- fmccor|home [n=fmccor@209.249.182.18] has joined #gentoo-council
15:52 -!- FuzzyRay|work [n=pvarner@gentoo/developer/FuzzyRay] has joined #gentoo-council
19:51 -!- Calchan|Home [n=Calchan@gentoo/developer/calchan] has joined #gentoo-council
20:14 -!- robbat2|na is now known as robbat2
20:16 <@Kugelfang> rest-of-council: ping
20:18 < Jokey> KingTaco kloeri robbat2 SpanKY wolf31o2 ping
20:18 <@Kugelfang> :-)
20:18 < Jokey> highlight++
20:18 <@Kugelfang> thanks Jokey
20:20 <@kloeri> hiya Kugelfang, Jokey 
20:20 <@Kugelfang> kloeri: heya...
20:21  * Jokey passes kloeri a beer
20:21 <@Kugelfang> kloeri: i'm a bit fed up about the mplayer version problem
20:21 <@kloeri> me too
20:21 <@Kugelfang> kloeri: i was planning to contact lu_zero so he moves it to a proper revision
20:22 <@kloeri> sounds good
20:22 <@Kugelfang> kloeri: as we need at least to council members do decide on that, i'd like a proper "yes" :-)
20:22 <@robbat2> yo
20:23 <@Kugelfang> robbat2: you also think it should be moved?
20:23 <@kloeri> well, is it really neccessary to have a council decision on that issue?
20:23 <@robbat2> i'm not aware of the problem (i don't pay attension to video stuff, and I haven't read my mail for the last 18 hours)
20:23 <@Kugelfang> robbat2: no recent problem... it's in for 4 weeks already
20:23 <@Kugelfang> http://sources.gentoo.org/viewcvs.py/gentoo-x86/media-video/mplayer/
20:23 <@Kugelfang> robbat2: have a look at that last version
20:24 < spb> the short answer is that mplayer is using an invalid version spec
20:24 <@Kugelfang> QA demanded to remove it already
20:24 <@Kugelfang> (iirc)
20:24 < zlin> bug #166522
20:24 < jeeves> zlin: https://bugs.gentoo.org/166522 nor, P2, All, vapier@gentoo.org->dev-portage@gentoo.org, REOPENED, pending, portage should only accept one version suffix
20:25 < Jokey> hrm, wasn't there some announcement that portage added support for it?
20:25 < spb> Jokey: none that i saw
20:25 < spb> and portage gaining support in some version doesn't mean you can use it in the tree
20:25 <@robbat2> make him covert it to "_rc%04d%04d%02d%02d",$RC,$YEAR,$MONTH,$DAY
20:26 <@robbat2> or some useful variation if they want the dang date
20:27 <@kloeri> that would be much better imo
20:28 <@kloeri> _alpha_beta and whatever else is allowed by portage now doesn't make any sense imo (and neither does _rc_alpha or a thousand other combinations)
20:29 <@Kugelfang> so can we conclude that multiple version suffixes are illegal in the tree?
20:29 <@Kugelfang> until council says otherwise?
20:29 < spb> imo yes
20:29 <@Kugelfang> robbat2, kloeri?
20:30 <@robbat2> yeah, i'm good with that
20:30 <@Kugelfang> me too
20:30 <@robbat2> in my above example, i can't recall if portage cares about leading zeros on the rc number or not
20:30 <@Kugelfang> kloeri?
20:30 <@kloeri> yes
20:31 <@Kugelfang> okies
20:31 <@Kugelfang> I'll draft a mail and file bugs
20:34 < solar> alsa-something was doing the same thing a little while ago also.
20:36 < solar> media-video/transcode-1.0.3_rc2_p20070310-r1 <-- there is the other. alsa seems to be ok now.
20:36 <@Kugelfang> solar: nod, thanks for the heads up
20:50 <@Kugelfang> kloeri, robbat2: http://rafb.net/p/1vVpR133.html
20:51 <@Kugelfang> 'council member' -> 'council members' already done
20:52 <@Kugelfang> 'considere' -> '&d' done, too
20:53  * Kugelfang prods kloeri 
20:55 <@kloeri> Kugelfang: looks good
20:55 <@Kugelfang> cool
20:55 <@Kugelfang> sending it
20:55 <@Kugelfang> done
20:56 <@kloeri> k
21:06 -!- weckle247 [n=weckle@p57B1BC7E.dip0.t-ipconnect.de] has joined #gentoo-council
21:15 < tove> Kugelfang: "Upon making such a decision, the Gentoo Council mailing list must be notified." --> CC: gentoo-council@
21:16 <@Kugelfang> sorry, i read alias somehow
21:16 <@robbat2> and s/viode/video/
21:16 <@Kugelfang> will correct that now
21:16 <@Kugelfang> it is sent already
21:16 <@robbat2> yeah, I was afk
21:17 <@Kugelfang> tove: thanks for the heads up!
21:17 <@Kugelfang> tove: i corrected my mistake
21:25 -!- mpagano [n=mpagano@pool-70-105-167-17.nwrknj.fios.verizon.net] has joined #gentoo-council
21:32  * fmccor notices that someone has problems counting ("as few as one council member ...") :)
21:32  * fmccor hides
21:34  * agaffney notices that some people see conspiracies everywhere and will bitch about anything
21:34 < spb> someone has a problem not being a twit
21:34 < agaffney> wtf was up with the paludis conspiracy theory?
21:34 < agaffney> people--
21:34 < agaffney> I'm not a paludis groupie, and *I* think that's completely fscking retarded
21:35 <@Kugelfang> see my response
21:35 < spb> please to reply to him and say so
21:36 <@Kugelfang> please don't
21:41 <@kloeri> the conspiracy theory is a bit much but replying will probably just lead to further silliness and/or flames
21:43 < solar> robbat2: The $RC$YYYY$MM$DD solution you proposed is not exactly ideal either. atom numerics probably will need to be limited to 8 numeric chars in the future.
21:43 -!- weckle247 [n=weckle@p57B1BC7E.dip0.t-ipconnect.de] has quit [Remote closed the connection]
21:44 <@Kugelfang> $RC$YY$MM$DD should be sufficient
21:44 < solar> as long as $RC does not exceed 99
21:45 <@Kugelfang> nod
21:45 <@Kugelfang> and -r$REV doesn't exceed 99999999 :-)
21:46 <@kloeri> heh
21:46 < solar> that is not set in stone yet. But should probably be an item for later discussions to set that in stone
21:47 <@Kugelfang> solar: definitely
21:48  * solar has one offending package right now.
21:48 <@Kugelfang> that is?
21:49 < solar> gradm
21:49 <@Kugelfang> cat?
21:50 < solar> !meta gradm
21:50 < jeeves> solar: Package: sys-apps/gradm Herd: hardened Maintainer: solar@gentoo.org
21:50 <@Kugelfang> nod
21:50 < solar> on a 64bit arch these compare as equals. x-200602141801111111111111111111111111111111111111111111111111111111 == x-20060214180999999999
21:51 < solar> well I guess it's not 64bit per say but rather sizeof int
21:52 -!- zxy_64 [n=ziga@89.212.157.253] has joined #gentoo-council
22:10 <@Kugelfang> nod
22:21 < fmccor> sigh.  We have a new important thread, it seems.
22:21 < agaffney> ugh
22:25 < agaffney> wow, wtf is Cardoe's problem?
22:25 < fmccor> My question exactly.
22:26 < agaffney> did a paludis dev rape his mother?
22:27 < fmccor> He could join this channel and ask directly, rather than providing all the entertainment on -dev :)
22:27 < spb> he likes to cause drama
22:28 < fmccor> Or he could work in gentoo-council@ for that matter (I think).
22:28 < fmccor> Or he could go do something useful. :)
22:39 <@kloeri> heh
22:40 <@Kugelfang> did my mail arrive at gentoo-council@l.g.o?
22:41 <@kloeri> no idea, I kill repeats based on msgid
22:41 <@kloeri> so I only got it on -dev in this case
22:41 <@Kugelfang> i forwarded it later on
22:42 <@kloeri> don't think I've got that yet
22:42 <@kloeri> but MLs can be quite slow sometimes
22:57 -!- onox [n=onox@kalfjeslab.demon.nl] has quit ["baselayout-2 \o/"]
22:57 -!- rbrown` [n=mynamewa@gentoo/developer/paludis.rbrown] has joined #gentoo-council
23:42 -!- FuzzyRay|work [n=pvarner@gentoo/developer/FuzzyRay] has quit [Remote closed the connection]
--- Log closed Wed Apr 25 00:00:51 2007
