Summary of Gentoo council meeting 25 July 2014

Agenda
======
1. Roll call
2. Vote for holding meetings every 2nd Tuesday of the month at
   19:00 UTC
3. Vote for continuing last council's workflow considering sending
   call for agenda items (2 weeks in advance), sending the agenda
   (1 week in advance) and have the meeting focussed, e.g., have major
   discussions on -project ML prior to the meeting
4. Appoint chairman for this term's meetings
5. Open bugs with council involvement
6. Open floor to council members to introduce themselves to the
   others, and/or raise issues they would like to deal with
7. Open floor to community


Roll call
=========
Present:
blueness, dberkholz, dilfridge, radhermit, rich0, ulm, williamh

Vote for schedule of meetings
=============================
Meetings will be every 2nd Tuesday of the month at 19:00 UTC.
- Accepted unanimously.

Vote for continuing last council's workflow
===========================================
We shall send a call for agenda items two weeks in advance and we
shall send the agenda one week in advance. We aim to have the meeting
focussed, e.g., have major discussions on the -project mailing list
prior to the meeting.
- Accepted unanimously.

Appoint chairman for this term's meetings
=========================================
There was consensus on the following provisionary list:
   July, August, September:     ulm
   October, November:           rich0
   December, January, February: dilfridge
   March, April:                radhermit
   May, June:                   blueness

Open bugs with council involvement
==================================
- Bug 424647 "archives.gentoo.org: Broken URLs for e.g.
  gentoo-dev-announce and others" [1]
  No action by the council, for the time being.
- Bug 477030 "Missing summary for 20130611 council meeting" [2]
  No progress.
- Bug 503382 "Missing summaries for 20131210, 20140114, and 20140225
  council meetings" [3]
  Action: dberkholz will commit the missing summaries in August.

Open floor to council members
=============================
See full log.

Open floor
==========
No issues were raised.

Next meeting date
=================
Tuesday, 12 Aug 2014, 19:00 UTC


[1] https://bugs.gentoo.org/show_bug.cgi?id=424647
[2] https://bugs.gentoo.org/show_bug.cgi?id=477030
[3] https://bugs.gentoo.org/show_bug.cgi?id=503382
