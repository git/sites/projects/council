20:56:39 ---	solar has changed the topic to: http://archives.gentoo.org/gentoo-council/msg_9fac75691c9845051a8cef0fb9b24115.xml
21:00:03 <Calchan>	Alright, roll call
21:00:11 *	dertobi123 is here
21:00:14 <Betelgeuse>	yes
21:00:37 <scarabeus>	yep
21:00:38 <leio>	here
21:00:52 <Calchan>	I have booted my pen&paper computer so I'm ready to go ;o)
21:01:04 <ulm>	here
21:01:52 <Calchan>	scarabeus and solar were here a few minutes ago and I'm sure they'll show up at some point
21:02:02 <scarabeus>	Calchan: what?
21:02:09 <scarabeus>	Calchan: i already replied to the roll call
21:02:10 <scarabeus>	:D
21:02:23 <Calchan>	scarabeus, blame age ;o)
21:02:31 <solar>	here
21:02:33 <Calchan>	I mean my age, I'm geting blind
21:02:46 <Calchan>	alright, we have everybody
21:03:11 <Calchan>	Betelgeuse, I know you're logging, who else? just in case Betelgeuse has internet issues
21:03:22 <leio>	as usual..
21:03:27 <Calchan>	good
21:03:39 <Calchan>	who wants to chair this meeting?
21:04:12 <Calchan>	as usual I volunteer but if anybody else wnats I'll be happy to let you do it
21:04:33 <Calchan>	no candidates?
21:04:33 <solar>	Calchan: I think it's assumed you will chair the rest of the council meetings
21:04:38 <dertobi123>	just do it, please :)
21:04:41 <dertobi123>	solar: ack :P
21:04:42 <Calchan>	solar, heh thanks
21:04:43 <scarabeus>	:]
21:04:44 <Betelgeuse>	Calchan: You probably know best what you would like to accomplish with 2
21:05:29 <Calchan>	Betelgeuse, I actually don't assume anything and have no pesonal agenda, what I want is us to talk about it
21:05:32 <Calchan>	ok then
21:05:42 <Calchan>	any comment abou tthe agenda before we go?
21:06:20 <solar>	lets go.
21:06:21 <scarabeus>	as this is the last chance to change agenda, we should add the "Glep 39 change process/approach" to it, because now we have no chance to change it, and it kinda blocks numero uno
21:06:51 <dertobi123>	uhrm, no ... discuss stuff like that on-list please
21:06:53 <Calchan>	scarabeus, agreed but we're not going to vote on anything today, so glep 39 doesn't get in the way yet
21:07:20 <scarabeus>	ok lets keep it on list then, i will sent mail after meeting :]
21:07:23 <Calchan>	and changing glep 39 isn't something we can add to the agenda at the last minute, see my answer to vapier
21:07:51 <Calchan>	scarabeus, since you joined us recently you might not rememebr that I was the one raising this issue at the beginning of our term
21:08:28 <Calchan>	and the council decided that it couldn't change glep 39 on its own, which is silly because it had been done before
21:08:35 <Calchan>	anyway, back to the agenda
21:08:36 <solar>	#2 I'm in favor of allowing ppl to vote by email or use a www based app such as Betelgeuse proposed as long as the majority is present.
21:08:43 <Calchan>	2. Voting by email (20 minutes)
21:08:58 <Betelgeuse>	I agree with solar.
21:09:21 <Calchan>	I think the tool is irrelevant, but I agree voting by email should be allowed
21:09:22 <dertobi123>	i'm in favour of people voting before the meeting by email or to cast *all* votes after a meeting on web-based app
21:09:47 <scarabeus>	i would preffer the web-app after the meeting
21:09:55 <scarabeus>	so they can read last minute notes from meeting too
21:10:21 <Calchan>	the problem for anything done after the meeting is as we tend to slack we need to be strict on deadlines
21:10:22 <scarabeus>	but it should be also limited somehow, so none can vote lets say 20 days after the meeting
21:10:28 <Calchan>	and it delays all processes again
21:10:37 <scarabeus>	it should be 72 hours
21:10:46 <scarabeus>	thats enough to get connection at least somehow
21:10:48 <Calchan>	scarabeus, we should limit it to like 48 hours no more
21:10:58 <scarabeus>	or even 48 :] i just tried to be nice
21:11:01 <scarabeus>	i can go with 2 days :]
21:11:03 <solar>	after the notes are posted?
21:11:16 <scarabeus>	after the log is posted
21:11:23 <Calchan>	solar, that would take too long, how about after the logs are posted
21:11:24 <Calchan>	?
21:11:33 <solar>	I've seen long delays in the notes being posted however
21:11:41 <dertobi123>	48 hours just after the meeting ended
21:12:00 <solar>	and it's the persons own responsiblity to find the notes?
21:12:19 <Betelgeuse>	If we use a web application, deadlines are easy to enforce.
21:12:20 <leio>	you can read the raw log, summaries can't cover everything needed to know
21:12:34 <Calchan>	solar, is there any way we can have the logs posted automatically right at the end of the meeting?
21:12:42 <dertobi123>	solar: as this only affects people which were absent, i see no problem with that
21:12:48 <solar>	script-o-magic maybe
21:13:00 *	solar is ok with that. stamp it/seal it and let move on.
21:13:24 <solar>	no really. So it boils down to email vs web app. Either is fine with me
21:13:24 <leio>	there are meeting bots that can do something like that. Other than that I've been commiting raw logs within hours of meeting end too the past couple meetings
21:14:05 <Betelgeuse>	Calchan: A bot.
21:14:11 <Calchan>	now, we all seem to converge on an agreement on how to vote by email, although it might need a bit of polishing and working on some details, but how do we factor in the fact this is a change of what glep 39 says and that we have decided we can't change glep 39?
21:14:30 <solar>	we are not
21:14:31 <Betelgeuse>	Calchan: 50% still needs to be there.
21:14:42 <Betelgeuse>	If someone-misses they can vote after.
21:14:42 <scarabeus>	50% still needs to attend
21:14:43 <leio>	I think the glep39 doesn't say per se that we can't do vote in other places too
21:14:48 <Calchan>	solar, can you please elaborate?
21:14:58 <Betelgeuse>	GLEP 39 doesn't require us to vote in meetings at all.
21:15:00 <Calchan>	leio, but it doesn't say we can
21:15:07 <Calchan>	Betelgeuse, ok, good point
21:15:15 <ulm>	leio: but it says that only those who show up can vote
21:15:34 <ulm>	"Council decisions are by majority vote of those who show up (or their proxies)."
21:15:40 <Calchan>	another good point
21:16:03 <dertobi123>	well, if i don't vote in a meeting, but sent my votes before i'm somewhat proxying myself
21:16:06 <leio>	yeah, I made and quoted it before, then thought that doesn't cover all votes, but "Council decisions" sounds broad
21:16:11 <dertobi123>	same if we're voting afterwards
21:16:35 <Betelgeuse>	I think the best approach is to move the whole vote after the meeting.
21:16:42 <Betelgeuse>	And do it in 48 hours after it.
21:16:43 <scarabeus>	well we should have the proccess of glep39 update and update the text to fit our needs
21:16:55 <Calchan>	so it seems we first need to decide whether allowing vote by email requires a change of glep 39
21:17:17 <dertobi123>	scarabeus: get the discussion started and submit a proposal on which fellow developers can vote :P
21:17:19 <Betelgeuse>	GLEP 39 should be updated so that someone comes up with a new text.
21:17:37 <Calchan>	scarabeus, again agreed, but it's a hot topic, please the logs and emails around the time of the first council meetings of this term
21:17:37 <Betelgeuse>	Then open it for comments and finally submit it for developer vote.
21:17:44 <Betelgeuse>	Don't have discussion first.
21:17:45 <Calchan>	Betelgeuse, but who does that if council can
21:17:47 <Calchan>	t?
21:18:04 <Betelgeuse>	Calchan: Anyone can.
21:18:19 <scarabeus>	foundation guys
21:18:23 <scarabeus>	woudl be sane choice
21:18:27 <scarabeus>	they should be our coutnerpart
21:18:37 <scarabeus>	or all devs
21:18:38 *	dertobi123 sighs.
21:18:43 <Calchan>	scarabeus, I'm guessing they don't care and don
21:18:47 <Calchan>	t wan tto interfere
21:18:57 <scarabeus>	hey in that case why you voted we cant touch that glep :D
21:19:05 <scarabeus>	if none cares :]
21:19:18 <Betelgeuse>	the council can still come up with the new text
21:19:20 <Calchan>	scarabeus, I voted for us to be able to change that glep, so don't look at me
21:19:23 <Betelgeuse>	we can't just approve it
21:19:38 <scarabeus>	question is, who approves it then
21:19:50 <leio>	condorcet voting by elections team, probably
21:19:56 <Betelgeuse>	scarabeus: developer vote by elections team
21:20:03 <dertobi123>	please take a look at the July 20th 2009 summary
21:20:04 <Betelgeuse>	yes/no
21:20:16 <ulm>	leio: why condorcet? it's just yes or no
21:20:21 <Betelgeuse>	ulm: same results
21:20:25 <Betelgeuse>	ulm: doesn't matter
21:20:31 <Calchan>	ok, so it seems that we need to prepare a change on glep 39, and as there are other things to change in there than just voting by email I suggest we batch them together
21:20:39 <leio>	shrug, tradition. No need to argue then also what voting method it has to be ;p
21:21:10 <ulm>	Calchan: yes, throw out all the outdated cruft
21:21:21 <ulm>	especially in "Rationale" and "Motivation"
21:21:25 <Calchan>	ulm, yes, and also see vapier's mail
21:21:40 <ulm>	and change the title ;)
21:21:52 <Calchan>	so, do we have a volunteer to lead that effort?
21:22:16 <--	Ford_Prefect has quit (Read error: Connection reset by peer)
21:23:04 <Calchan>	mmmkay... so what I propose is I gather all required changes (voting by email, vapier's stuff, ulm's stuff) and submit it to you guys
21:23:38 <Calchan>	and we work on it all together with me as your sexy secretary, and we submit that to a vote of all devs
21:23:50 -->	Ford_Prefect (~ford_pref@gentoo/developer/ford-prefect) has joined #gentoo-council
21:24:21 <scarabeus>	Calchan: and i hope you will wear some nice gentoo t-shirt if you want to present yourself as sexy secretary :D
21:24:21 <ulm>	sounds good
21:24:23 <scarabeus>	sounds sane
21:24:50 <dertobi123>	Calchan: please make separate patches so developers can vote on each of the changes separately
21:25:08 <Calchan>	dertobi123, interesting idea, ok will do
21:25:39 <Betelgeuse>	I think it should be intirely new GLEP
21:25:40 <Calchan>	I will first gather an informal list before making patches though
21:25:45 <Betelgeuse>	and leave 39 as deprecated
21:26:09 <Calchan>	Betelgeuse, we could do that to, it would make things easier
21:26:25 *	solar thinks so as well.
21:26:52 <scarabeus>	we should implement it as approach for everything, expect typo changes all gleps would go to deprecated and presented as new glep
21:26:53 <Calchan>	and on the voting by email subject it looks we all agree and only some details need to be worked out
21:27:14 <Calchan>	scarabeus, that or we version them
21:27:43 <scarabeus>	we never agreed even on versioning eclasses :D how do you want us to version gleps :P
21:27:52 <dertobi123>	well, in general glep 39 shouldn't be a glep, but the council's constitution ... if we're creating a new one, make it the council's constitution, not yet another glep
21:28:01 <Calchan>	scarabeus, we just do it, it isn't that difficult if we decide it
21:28:35 <Calchan>	dertobi123, I've been wanting to do that for a long time but when I talked about it it was met with scepticism
21:28:45 <Calchan>	whatever it's spelled in english
21:28:49 <dertobi123>	heh
21:28:55 <scarabeus>	we can haz constitution
21:29:23 <Calchan>	ok, that can be one of the topics to discuss among all the others and we may have devs vote on that
21:29:54 <Calchan>	anything else to add to that topic before we switch to thwe next one?
21:30:36 <Calchan>	wow, we're right on schedule
21:30:48 <Calchan>	3. Do we want of a policy for changes in metadata.xml? (20 minutes)
21:31:14 <Calchan>	who starts?
21:31:23 <solar>	Not sure about a policy. But shoving that info into metadata.xml is just flat out wrong
21:31:27 <scarabeus>	it looks weird
21:31:31 <Calchan>	how about solar, you had an interesting proposal?
21:31:32 <scarabeus>	and definitely not in metadata
21:31:35 <Calchan>	dammit, slow fingers
21:32:01 <solar>	reason for that being is the end user gets this data and it provides them zero advantage. It bloats the tree.
21:32:12 <Calchan>	agreed here
21:32:28 <Betelgeuse>	solar: Easy to filter out in CVS --> rsync
21:32:31 <ulm>	it would be a technical solution for a social problem
21:32:45 <solar>	Betelgeuse: not if it's in metadata.xml (any other file is fine)
21:32:56 <Calchan>	is there any way we can have such changes help us getting toward less territoriality?
21:33:02 <solar>	antarus suggested owners.xml
21:33:05 <Calchan>	one cookie for ulm :o)
21:33:17 <Betelgeuse>	I sometimes find myself touching some dev-java/ stuff that's not maintained by me so repoman reminding me would be useful in that case
21:33:22 <vapier>	Calchan: you should keep the calendar up-to-date with council meetings
21:33:50 <ulm>	just use common sense, we don't need more complicated rules
21:33:57 <Calchan>	vapier, can we discus that in the open floor session?
21:34:37 <vapier>	yeah
21:34:38 <ulm>	ask the maintainer, and if he doesn't answer within reasonable time then just fix it
21:34:46 <scarabeus>	i think the rule "You dont maintain -> ask first" is quite clear
21:35:05 <Betelgeuse>	I could write a repoman check for checking against metadata.xml and herds.xml disabled if only changing KEYWORDS
21:35:08 <scarabeus>	and only thing we should implement is some repoman warning "I hope you ask since you dont maintain this"
21:35:12 <Betelgeuse>	Should remind people enough
21:35:14 <solar>	ulm: yes. So far it's only one questionable seed in this entire mix. Seeing as devrel/QA or other did not slam the hammer down. it would be nice to make this overcomplex. But Betelgeuse has a good point about repoman.
21:35:16 <Calchan>	scarabeus, indeed and it makes sense, the problem occurs when maintainers are slow to answer due to real life
21:35:27 <dertobi123>	ulm: agreed ... besides having a tard or two every other year screwing up that one did work for years ....
21:35:35 <scarabeus>	Calchan: well i always wait at least 10 days
21:35:44 <scarabeus>	Calchan: expect security fix, nothing need more rush
21:35:55 <Calchan>	scarabeus, and some will think that 2 days is enough, some will think it's one month
21:36:07 <Betelgeuse>	We could also write some common time to devmanual
21:36:07 <scarabeus>	they can always ask us in QA
21:36:08 <ulm>	dertobi123: and for that case metadata information wouldn't help I guess
21:36:13 <Calchan>	so we could always set a time limit but that's one more rule, do we need it?
21:36:15 <dertobi123>	ulm: indeed ...
21:36:15 <Betelgeuse>	about how soon you can commit if there's no response
21:36:17 <scarabeus>	to look over that fix
21:36:18 <Betelgeuse>	currently it's fuzzy
21:37:00 <ulm>	Betelgeuse: one week?
21:37:09 <scarabeus>	sounds like sensible time
21:37:14 <scarabeus>	because it always contains weekend
21:37:15 <scarabeus>	:]
21:37:23 <dertobi123>	this topic is somewhat useless ... there's no problem to solve. if someone screws up -> get their account locked.
21:37:28 <Calchan>	how about letting anybody touching anything as long as it is masked?
21:37:40 <Calchan>	dertobi123, unfortunately it isn't that easy
21:37:50 <dertobi123>	it is ...
21:37:52 <solar>	ok so policy.
21:37:52 <Betelgeuse>	Lot of people are well reachable so allowing touching is not good.
21:37:59 <Calchan>	dertobi123, we have to leave people a certain margin for error, they
21:38:01 <Calchan>	re not robots
21:38:16 <dertobi123>	having loads of policy just for some tards is just plain bullshit
21:38:20 <solar>	policy proposal. No useless data in metadata.xml (that's what #3 is about right?)
21:38:33 <Calchan>	Betelgeuse, ok, (I was raising an hypothetical idea here, not pushing anything)
21:38:43 <Calchan>	solar, right
21:38:45 <scarabeus>	yeah lets stop that bikeshed on -dev about that metadata changes
21:38:45 <solar>	where useless is stuff that does not help the end user in anyway. Ie internal stuff only.
21:38:48 <ulm>	solar: right, it's useless in the users' tree
21:39:54 <solar>	can we keep it that simple then?
21:40:41 <Calchan>	solar, didn't you propose we keep this in another file at some point?
21:41:00 <scarabeus>	that policy is complicated, so i would vote against it anyway
21:41:00 <Betelgeuse>	I propose we vote on the week timeline?
21:41:22 <Calchan>	Betelgeuse, we could do that, yes
21:41:26 <leio>	I bet cvs->rsync can strip the tags with a simple xml tool. Having them in there or not is a different question, which we don't know if we should put our nose in it or not right now
21:41:38 <Calchan>	anybody opposed to us voting on this?
21:41:46 *	dertobi123 
21:42:04 <solar>	Calchan: that was antarus. But I think that is somewhat outside the scope.
21:42:16 <ulm>	leio: it would break the Manifest files
21:42:41 <solar>	repoman in theory could check it from any dir.
21:43:07 <Calchan>	dertobi123, you're opposed to us voting on making it official that after one week of warning one could touch any package?
21:43:40 <Betelgeuse>	Calchan: Just have a vote. If people don't want a policy they vote no.
21:43:45 <Betelgeuse>	To stay in status quo.
21:43:56 <dertobi123>	Calchan: i'm opposed to randomly put things up to vote just somewhen during the meeting
21:44:02 <Calchan>	Betelgeuse, I'd agree with that but I wanted to hear his reasons, it could be intersting
21:44:22 <scarabeus>	Betelgeuse: it is another topic, the change for the devmanual, i would say first we close this topic and then lets vote on open floor
21:44:25 <scarabeus>	we can do that no?
21:44:29 <scarabeus>	sorry i bit disappeared
21:44:50 <Calchan>	dertobi123, good point, so we should think this over a bit more, and if needed put it on the agenda for net time?
21:45:08 <solar>	correct
21:45:12 <Calchan>	scarabeus, open floor isn't for voting
21:45:18 <dertobi123>	Calchan: yeah ...
21:45:23 <scarabeus>	or put it before the open floor
21:45:23 <ulm>	Calchan: right, we shouldn't rush into such a vote. put it on the next meeting's agenda
21:45:36 <Calchan>	scarabeus, it's to let users and devs  interact with us directly
21:45:38 <scarabeus>	but it is 3 minutes to vote, so we can do that next month
21:45:58 <scarabeus>	just one thing, we should really somehow punish people not obeying that rule
21:46:09 <Betelgeuse>	scarabeus: use current process
21:46:26 <Betelgeuse>	scarabeus: devrel will act if evidence is presented
21:46:44 <Calchan>	scarabeus, and bugs filed, and a chance to correct behavior given
21:46:58 <Calchan>	I'm talking yoda style more and more, I'm getting worried
21:47:19 <scarabeus>	force use, you must stop
21:47:27 <Calchan>	heh
21:47:46 <scarabeus>	:D
21:48:11 <Calchan>	ok, so does any one of us volunteer to lead that topic on mailing lists and if needed make a proposition for next time?
21:48:31 <scarabeus>	about the devmanual change of the packages changes?
21:48:34 <scarabeus>	yeah i will do it
21:48:54 <Calchan>	scarabeus, thanks
21:48:56 <scarabeus>	pachages touching
21:48:58 <dertobi123>	you volunteering to do something and don't know what to do? :P
21:49:05 <dertobi123>	you're
21:49:06 <dertobi123>	*
21:49:08 <scarabeus>	nah just making sure
21:49:17 <dertobi123>	heh
21:49:29 <Calchan>	anybody wwants to add something?
21:49:39 <solar>	I'm having a hell of a time following you guys. It sounds like you got way off topic of #3. Has that ended?
21:50:08 <Calchan>	solar, I was making sure everybody had a chance to say whatver they wanted to say
21:50:14 <scarabeus>	solar: you want to vote about it? most of us said no :]
21:50:18 <Calchan>	so go ahead if there's anything you want to add
21:51:20 <leio>	I understood the question is if we want to have policies for changes in metadata.xml at all, not if we support <changespolicy> tag or file
21:51:35 <Calchan>	leio, indeed
21:51:48 <solar>	I've already made my basic proposal related to #3 "No useless data in metadata.xml where useless is stuff that does not help the end user in anyway. Ie internal stuff only."
21:51:48 <Calchan>	scarabeus, ^^^
21:51:49 -->	nirbheek|netboo (~nirbheek@gentoo/developer/nirbheek) has joined #gentoo-council
21:51:52 <leio>	as a random comment, I see nothing wrong with territorialism
21:52:22 <leio>	if exercised by an always available team
21:52:23 <dertobi123>	leio: the question is not even related to metadata.xml strictly - it is more about "do we want to solve a social problem with a technical solution?"
21:52:46 <Betelgeuse>	dertobi123: it's not purely social as I pointed out
21:53:09 <scarabeus>	Betelgeuse: well i think the devmanual update is better than load of new xml tags
21:53:26 <Calchan>	scarabeus, you mean devmanual or dev handbook?
21:53:36 <scarabeus>	demanual/dev handbook
21:53:40 <Calchan>	scarabeus, not the same
21:53:46 <Betelgeuse>	they are two different things
21:53:50 <dertobi123>	Betelgeuse: well, make it 95% social then - doesn't really change my pov
21:54:27 <scarabeus>	ok dev handbook is about policies so i speak about that one :]
21:54:33 <Betelgeuse>	The deadline might be better in CVS part of dev handbook than devmanual.
21:54:54 <scarabeus>	yep
21:55:00 <Betelgeuse>	http://www.gentoo.org/proj/en/devrel/handbook/handbook.xml?part=3&chap=2
21:55:02 <Betelgeuse>	Maybe here
21:55:07 -->	blueness (~hnsctq40@2001:470:1d:170:224:8cff:fe54:4052) has joined #gentoo-council
21:55:09 <Calchan>	scarabeus, anyway don't bother about making a patch for the dev handbook yet, once we have the answer to our question it will be easy to do, let's work on the answer first, and maybe on the question too as douglas adams would say
21:55:25 <scarabeus>	:]
21:55:46 <Calchan>	I'm not kidding, finding out exactly what problem we are trying to solv eis essential here
21:55:55 <scarabeus>	yes i get it
21:56:26 <Calchan>	ok, do we leave it at that or does anybody have anything to add?
21:57:09 <dertobi123>	no, trying to find the problem to solve ... that sums it up quite good
21:57:32 <Calchan>	the current conclusion is scarabeus will look at the thread on ml, identify what the problem is and how we can solve it if there's anything to solve, and report back next month
21:57:53 <Calchan>	and it's not forbidden to help him, whoever you are
21:58:16 <Calchan>	alright, final topic then
21:58:26 <Calchan>	4. Conclusion (10 minutes)
21:58:31 <Calchan>	4.1 Action list. Who does what and when?
21:58:49 <Calchan>	see above for scarabeus, and I'll work on the glep 39 thing
21:58:56 <Calchan>	anything else?
21:59:06 <dertobi123>	no
21:59:22 <Calchan>	4.2 Who takes care of the summary and log for this meeting? When?
21:59:48 <Calchan>	I can do that in the next 2 days, it's no big deal
21:59:53 <leio>	Log I will commit within a couple hours
21:59:57 <Calchan>	unless somebody else wnats to do it
22:00:10 <Calchan>	leio, thanks
22:00:22 <Calchan>	4.3 Next meeting date/time.
22:00:35 <Calchan>	how about april 8th?
22:00:44 <Calchan>	april 1st is risky ;o)
22:00:47 <solar>	no
22:00:55 <Calchan>	sorry, wrong calendar
22:00:57 <leio>	8th seems like a Thursday ;p
22:00:57 <Calchan>	not a monday
22:00:58 <solar>	8th is on the Thursday. How about the 12th?
22:01:03 <leio>	How about 5th
22:01:10 <Calchan>	I was just testing you guys ! ;o)
22:01:13 <ulm>	leio: easter monday ;)
22:01:18 <solar>	is there a rush of topics for next month?
22:01:19 <scarabeus>	5 or 19 are ok with me
22:01:21 <scarabeus>	not 12
22:01:23 <dertobi123>	5th is easter monday, prefer 12th ...
22:01:28 <ulm>	12th ok for me too
22:01:31 <leio>	I need a calendar with comments and special days or something
22:01:32 <ulm>	or 19th
22:01:50 <Calchan>	5th and 12th are ok with me
22:01:54 <dertobi123>	19th might work as well
22:02:30 <Calchan>	it seems it's one of those complicated ones, how about we settle that on the alias?
22:02:57 <dertobi123>	just makes it even more difficult ...
22:03:08 <Calchan>	dertobi123, not untrue
22:03:22 <dertobi123>	what about the 19th? ok for everyone?
22:03:39 <Calchan>	is everybody ok with the 19th?
22:03:42 <Calchan>	I am
22:04:03 <leio>	I am. We shouldn't make the meeting after that another 6 weeks interval then, but 4 or so.
22:04:21 <Betelgeuse>	yes
22:04:29 <Calchan>	leio, but some are not available on the 5th and some others on the 12th
22:04:41 <Calchan>	leio, and as long as we have a meeting amonth we're ok
22:05:13 <leio>	yes, I'm saying hopefully the May meeting can then happen on 17th (4 weeks after 12th April), not a longer interval again
22:05:30 <Calchan>	ah ok, I misunderstood
22:05:36 <dertobi123>	10th might work as well
22:05:42 <leio>	we'll see then, all of 5th, 12th and 19th April are OK for me
22:05:44 <dertobi123>	(10th may that is)
22:06:17 <Calchan>	so let's tentatively settle for the 19th then, those who haven't spoken have say 48 hours to do so
22:06:34 <Calchan>	4.4 Who will follow-up discussions and prepare the agenda for the next meeting?
22:07:03 <ulm>	Calchan: I can prepare the agenda, unless you want to do it again ;)
22:07:14 <Calchan>	ulm, thanks a lot
22:07:47 <Calchan>	anything to add before open floor?
22:08:23 <solar>	thank you
22:08:43 <Calchan>	thanks solar
22:08:44 <Calchan>	the floor is open then
22:09:04 <Calchan>	vapier?
22:09:27 <Calchan>	I'm running to the microwave, I'll be back in a couple minutes
22:09:56 <vapier>	Calchan: you should keep the calendar up-to-date with council meetings
22:10:04 <vapier>	i dont have anything else to add :P
22:10:15 <scarabeus>	:D
22:10:18 <solar>	haha. He waited all that time to repeat himself :)
22:10:35 <leio>	no, meanwhile he watched us dance
22:11:00 <scarabeus>	vapier: you cant do this, now i have to clean up the table with spit coffee, laughter is disaster at some points :D
22:11:34 <Calchan>	vapier, what calender are you talking about?
22:11:50 ---	solar has changed the topic to: Tentative Next Council meeting Apr 19th at 1900UTC, i.e. 2000CET, 1400EST, 1200MST, 1100PST.
22:12:10 <scarabeus>	guys on web page lu_zero is still marked as slacker
22:12:16 <scarabeus>	and elections where i was elected are missing
22:12:21 <scarabeus>	just now spotted
22:12:59 <leio>	scarabeus: what web page?
22:13:06 <scarabeus>	http://www.gentoo.org/proj/en/council/
22:13:38 <leio>	scarabeus: Oh, some weird section that has been added at some point
22:13:42 <vapier>	Calchan: it's linked from gentoo.org frontpage
22:13:46 <vapier>	it's on the left side
22:13:58 <vapier>	if you dont have access, i can add you now ... just tell me your gmail account
22:17:18 <vapier>	same for any other Gentoo dev
22:17:38 >vapier<	mart.raudsepp
22:20:36 <leio>	ok, anyone else for open floor speak?
22:20:42 <Calchan>	vapier: first name . last name at gmail dot com
22:21:10 -->	NeddySeagoon (~NeddySeag@gentoo/developer/NeddySeagoon) has joined #gentoo-council
22:22:00 <vapier>	done
22:22:13 <Calchan>	vapier, thanks
22:22:41 <vapier>	there's an old recurring event on there i guess will need updating/deleting
22:24:16 <--	Ford_Prefect has quit (Quit: Ex-Chat)
22:25:16 <Calchan>	vapier, I'll look into that, also I'm just now think that we'll switch to summer time  and we might want to take that into account
22:28:51 <scarabeus>	i guess we are over meeting right? i am going to disappear if none wants something from me :]
22:29:11 <Calchan>	scarabeus, yes, thanks for your participation
22:29:47 <scarabeus>	we should more thank you for coordination, we just do what others elected us to do (at least try best to do so) :]
