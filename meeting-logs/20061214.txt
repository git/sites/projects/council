20:59 <@robbat2> kloeri, Kugelfang, SpanKY, you here?
20:59 < vapier> moo
20:59 <@FlameBook> kingtaco?
20:59 < vapier> how do you rebind a channel in bx to a window ?
20:59 <@kloeri> yup
21:00 < vapier> ah here we go
21:00 <@FlameBook> somebody knows why kingtaco is not here?
21:00 < vapier> i may pop in and out
21:00 <@wolf31o2|mobile> nope
21:01 <@Kugelfang> heya there
21:02  * Kugelfang is now available, too -)
21:02 <@FlameBook> do we start?
21:02 <@robbat2> just missing kingtaco
21:03 <@FlameBook> yeah pinged him in #-dev
21:03 <@Kugelfang> oh
21:03 <@FlameBook> he was around a few mins ago
21:03 <@Kugelfang> well, he'll pop up eventually i guess :-)
21:04 <@FlameBook> so let's start with an easy one
21:04 -!- mode/#gentoo-council [+m] by FlameBook
21:05 <@FlameBook> did you read my mail about icons we have on the site'
21:05 <@robbat2> what all is on the agenda
21:05 <@FlameBook> ?
21:05 <@wolf31o2|mobile> I did... and I agree that we should probably unlink them from the site until it can be cleared up by the trustees
21:06 <@FlameBook> trustees never cleared it up in more than one year
21:07 <@wolf31o2|mobile> until you said something, I'd not heard a thing about it
21:07 <@FlameBook> the most clear statement we have is that we're not liable unless they demonstrate we broke copyright laws intentionally
21:07 <@robbat2> yes, I saw the email. i don't think trustees are going to help - perhaps better to announce that unless the license issues are cleared up, they will be going away
21:07 <@wolf31o2|mobile> basically, anything that was tasked to the old trustees, the new ones likely know nothing about
21:07 <@FlameBook> wolf31o2|mobile, I mailed last year about that
21:07 <@FlameBook> sigh -_-
21:07 -!- mpagano [n=mpagano@pool-70-105-167-94.nwrknj.fios.verizon.net] has quit [Client Quit]
21:07 <@kloeri> we have new trustees now so I think it'd be worth bringing it up with trustees again
21:08 <@wolf31o2|mobile> right
21:08 <@Kugelfang> remove until they have settled?
21:08 <@wolf31o2|mobile> I would say so
21:08 <@Kugelfang> nod
21:08 <@FlameBook> I'd remove them and ask for new artists
21:08 <@kloeri> they may or may not know about it but a reminder certainly won't hurt a bit
21:08 <@wolf31o2|mobile> better safe than sorry and all that jazz
21:08 <@FlameBook> it's impossible to clear them up anyway
21:08 <@FlameBook> most of them are copied from Windows software
21:08 <@Kugelfang> vote: remove the icons and let the trustees handle the situation afterwards
21:08 <@wolf31o2|mobile> right... unless we simply removed any offending ones
21:08 <@FlameBook> is anybody going to write Microsoft to ask permission to use them? :D
21:08 <@wolf31o2|mobile> Kugelfang: yes
21:08 <@Kugelfang> vote: yes
21:08 <@kloeri> yes
21:09 <@robbat2> vote: yes
21:09 <@FlameBook> Kugelfang, yes, although I'd rather take a definitive approach
21:09 <@Kugelfang> phone
21:09 <@wolf31o2|mobile> FlameBook: the "definitive" approach is they're being removed... if the trustees don't do anything beyond that, they're still removed
21:09 <@robbat2> there's voicemail for kingtaco (thanks to solar), so he should be here soon
21:10 <@FlameBook> wolf31o2|mobile, well, if we wait for trustees, we're "waiting to clear up"
21:10 <@FlameBook> if we're just scratching them we're "asking new artists to contribute a true Gentoo icon set"
21:10 <@robbat2> with all of the license issues clear
21:10 <@FlameBook> right
21:11 <@FlameBook> a new icon set, either original or derived, with a proper license
21:11 <@Kugelfang> so your proposal is to remove them permanently?
21:11 <@FlameBook> I'm not sure how much lila is related to gentoo, but it might as well be asked to made official
21:11 <@FlameBook> Kugelfang, yes
21:11 <@Kugelfang> i can live with that :-)
21:13 <@Kugelfang> so new vote?
21:13 <@wolf31o2|mobile> on what?
21:13 <@Kugelfang> remove them permanently
21:13 <@FlameBook> The Lila theme is a community project, originally created by Daniel G. Taylor and members of the Gentoo Linux community.
21:13 <@FlameBook> http://www.lila-center.info/doku.php?id=about we might as well consider the idea of making these the suggested one or something
21:14 -!- mode/#gentoo-council [+o vapier] by ChanServ
21:14 <@wolf31o2|mobile> I would prefer that if we were to have an "official" icon theme that it at least attempt to match the other themes we have
21:15 <@FlameBook> wolf31o2|mobile, the current one does not really match anything
21:15 <@FlameBook> lila at least match the colour
21:15 <@wolf31o2|mobile> so if the vote is just to drop the current icon set, then I'm giving a yes... if it involves including *any* current icon set as some sort of "official" set then I'd say no
21:15 <@wolf31o2|mobile> I don't care about the current ones other than the fact that they are in violation of other people's IP
21:15 <@vapier> lila isnt part of the icons on the Gentoo website
21:16 <@Kugelfang> phone again
21:16 <@FlameBook> I'd just remove the one in the website, and then lend it over to userrel
21:16 <@kloeri> vapier: lila could be adapted instead of the current icons, that's what FlameBook is aiming at I think
21:16 <@Kugelfang> i'm fine with either removing it permenently or just until the trustees have settled on it
21:16 <@Kugelfang> biab
21:16 <@wolf31o2|mobile> and lila doesn't match our themes... but again, I don't think that needs to have *anything* to do with the discussion, which should be focused 100% on the current set, which is in violation of several trademarks
21:16 <@vapier> what themes
21:16 <@vapier> the only themes we have are on the livecd
21:17 <@wolf31o2|mobile> yes
21:17 <@wolf31o2|mobile> those are the only current "gentoo" themes that I am aware of
21:17 <@vapier> easier to transition livecd to lila than create something compeletely new to match livecd
21:17 <@wolf31o2|mobile> I'm sorry, but I'm not putting that gay ass pastel purple on anything with my name on it
21:17 <@wolf31o2|mobile> ;]
21:17 <@FlameBook> just ignore the lila problem now
21:18 <@FlameBook> consider just the icons on the website
21:18 <@wolf31o2|mobile> thank you
21:18 <@FlameBook> those, IMHO, has to go
21:18 <@wolf31o2|mobile> agreed
21:18 <@kloeri> agreed
21:18 <@FlameBook> after that, we might ask userrel to handle it, or some other project
21:19 <@FlameBook> but the icons on the site has to go quickly
21:19 <@vapier> the purple on the livecd blows and everyone knows it ;p
21:20 <@vapier> well just vote cause the discussion is going nowhere and our sense/ability to put together a decent theme is close to nil
21:20 <@FlameBook> as vapier said
21:20 <@FlameBook> so votes on removing the icons permanently and closing the discussion without asking trustees to clear anything up
21:20 <@FlameBook> me: yes
21:20 <@Kugelfang> yes
21:20 <@robbat2> yes
21:20 <@wolf31o2|mobile> yes
21:20 <@kloeri> yes
21:21 <@robbat2> vapier, your vote?
21:21 <@vapier> whatever
21:22 <@vapier> i imagine the icons will be fetchable via the forums, so it's fine to scrub from www
21:22 <@robbat2> vapier, could you give a definitive yes/no
21:22 <@vapier> sure
21:23 <@FlameBook> so, robbat2 can you handle it as part of infra, or can you tell me who to ask to enact this?
21:23 <@robbat2> 6 votes for, 1 absent
21:23 <@robbat2> FlameBook, one of the web docs folks I think
21:23 <@wolf31o2|mobile> neysx would be the best bet
21:23 <@vapier> licensing is so lame
21:24 -!- amne [n=amne@gentoo/developer/amne] has joined #gentoo-council
21:24 <@FlameBook> okay next point? spf? how's the status on that?
21:25 <@Kugelfang> anybody from infra who can tell us the status of the docs?
21:25 <@Kugelfang> that's the only thing missing, right?
21:25 <@kloeri> yes
21:25 <@robbat2> not my side of infra
21:26 <@kloeri> I was supposed to document some of it but haven't quite finished that yet due to RL constraints
21:26 <@FlameBook> kloeri, do you need help with the Reply-To doc btw?
21:27 <@kloeri> I mostly need to read what I've got one more time, making sure it's actually correct and makes sense + xml'ify it
21:27 <@FlameBook> if you want, I can easily xmlify it
21:27 <@kloeri> I definitely expect to get it done this weekend (I'm behind on a few different things so spending the weekend playing catch up)
21:27 <@kloeri> xmlifying isn't a problem
21:28 <@kloeri> it's not that much text anyway so it shouldn't take long adding a few xml tags
21:28 <@kloeri> I'm more concerned about making sure it's actually correct before committing any junk :)
21:30 <@FlameBook> so, let's just wait a it more about spf doc, hoping that it can actually be addressed?
21:30 <@kloeri> infra was supposed to document some sample configurations of using dev.g.o smtp iirc
21:30 <@Kugelfang> FlameBook: nod
21:30 <@kloeri> yes
21:30 <@FlameBook> kloeri, and change the doc so that does not say that the smtp is only for who can't really do otherwise
21:30 <@kloeri> FlameBook: right
21:31 <@vapier> i'm satisfied
21:31 <@FlameBook> just want to be sure, if next month we'll be still waiting, it would make us a bunch of clowns..
21:31 <@robbat2> yup, thats all fine
21:31 <@FlameBook> [at least waiting without any progress on it]
21:32 <@kloeri> that's not going to happen - at least not my part of it
21:32 <@FlameBook> kloeri, thanks, but I was actually concerned about SPF
21:34 <@kloeri> well, I can't answer on infras behalf - think you need kingtaco in this case
21:34 <@FlameBook> and he's the one missing :/
21:34 <@FlameBook> robbat2, what remains on agenda?
21:34 <@robbat2> FlameBook, not sure, that's why I asked what was on the agenda at the start!
21:34 <@FlameBook> [after two items from me, I'd leave some space to someone else before the last one :P]
21:35 <@wolf31o2|mobile> robbat2: how about an update on bugstest/bugs?
21:35 <@vapier> that'd be nice, bugs.g.o just gets worse everyday
21:36 <@robbat2> bugstest is up, there's two more enhancements that people have asked for, next weekend is a possible for moving
21:36 <@wolf31o2|mobile> nice
21:36 <@FlameBook> next 16/12 or 23/12?
21:37 <@vapier> robbat2: who is taking care of bugs now ?  you ?
21:37 <@vapier> jforman seems to have peaced out
21:37 <@robbat2> 23/12
21:37 <@robbat2> but sooner might happen if bugs.g.o gets really bad
21:38 <@FlameBook> robbat2, is utf-8 fixed or fixable easily?
21:38 <@robbat2> the utf-8 it turns out wasn't an actual bug in bugzilla, just in how we did the test migration
21:38 <@kloeri> bugs.g.o is already really bad imo
21:38 <@Kugelfang> definitely
21:39 <@FlameBook> so anybody has an idea on what is going on with qa? do we have a qa project at all at the moment?
21:40 <@Kugelfang> we do
21:40 <@kloeri> spb is actually working on EAPI docs and eroyf are working on setting up automated QA scans
21:40 <@Kugelfang> sbp just finished his studies and is back on open source work now
21:40 <@FlameBook> and who is addressing the shadowy things for which we still miss a policy?
21:41 <@wolf31o2|mobile> I've been doing weekly builds for both RelEng/QA
21:41  * FlameBook poins to /usr/libexec vs /usr/lib/misc
21:41 <@kloeri> I'm providing boxes etc. for the qa scans and try to help get that part set up
21:41 <@vapier> robbat2: so who is taking care of bugs now ?  you ? ;p
21:41 <@kloeri> I'll be doing weekly alpha builds and probably weekly ia64 builds too from this weekend forward
21:41 <@Kugelfang> FlameBook: remind me, should spb handle that?
21:42 <@FlameBook> Kugelfang, if QA wants to enforce a policy, QA should decide on policies, shouldn't they?
21:42 <@kloeri> and setting up alpha tinderboxing too I guess
21:42 <@robbat2> vapier, for the new boxes, there is no actual plan yet, beyond a rough guess that the bugzilla admin interface is still jforman, but the rest of it I can handle
21:42 <@Kugelfang> FlameBook: iirc that was an open discussion, and if QA doesn't think it's a problem, should they make it one?
21:42 <@FlameBook> this is one of the many things we don't have any clear rule or at least path to follow
21:42 <@robbat2> as I don't have any experience with the bugzilla admin stuff, just mysql on the back
21:42 <@Kugelfang> FlameBook: speaking just avbout the misc thing right now
21:42 <@FlameBook> Kugelfang, well, it _is_ a problem for Gentoo practice
21:42 <@kloeri> FlameBook: I was talking to spb about documentation and policies the other day actually
21:43 <@kloeri> FlameBook: glep 40? (the one about the qa team) mentions that qa should help devrel with updating quizzes etc. so I expect to get started on that soon'ish
21:43 <@FlameBook> Kugelfang, Quality Assurance is not just checking if an ebuild has a misplaced braket
21:43 <@FlameBook> kloeri, when was glep 40 dated, just to know?
21:44 <@kloeri> glep 48 it is
21:44 <@Kugelfang> FlameBook: please, no commonplaces
21:44 <@FlameBook> dated when?
21:44 <@kloeri> 24 april 2006
21:44 -!- spb [i=spb@gentoo/developer/spb] has joined #gentoo-council
21:45 <@FlameBook> Kugelfang, uhm? I was just saying that IMHO even issues like places where we put stuff, so that they won't break in the long run if we need to relocate them, are part of QA concerns
21:45 <@FlameBook> so yes, QA should be handling them, and not only
21:45 <@Kugelfang> FlameBook: i think this was handled on the mailing list sufficiently
21:45 <@vapier> robbat2: i'm interested in helping with the frontend stuff ... jforman never gets back to me
21:45 <@vapier> robbat2: things like all these user interface regressions
21:46 <@Kugelfang> FlameBook: but i guess vapier can give more insight there, as he was one of the most active participants in that discussion
21:46 <@FlameBook> Kugelfang, this as in QA concerns, or the misc thing? because at the moment, it's not handled at all
21:46 <@FlameBook> [the misc thing, that is]
21:46 <@robbat2> vapier, consider yourself hired then, get into #gentoo-infra later on today
21:46 <@Kugelfang> FlameBook: the misc stuff
21:47 <@vapier> k
21:47 <@FlameBook> Kugelfang, last time we talked (me and vapier) the result was that he wants to use /usr/$(get_libdir)/misc, while I know of at least one package that requires a single directory for two ABIs...
21:47 <@FlameBook> and we have stil ccache and distcc using /usr/$PN
21:47 <@FlameBook> so I don't think it was handled in the mailing list, not enough at least...
21:48 <@Kugelfang> ok...
21:48 <@Kugelfang> i will put it on my agenda than to create a proper polcy for it and put it into the devmanual after propser discussion
21:48 <@Kugelfang> FlameBook: ok?
21:49 <@vapier> symlinks across multiple ABI's would address that
21:49 <@vapier> while libexec does not have the ability to handle the cas
21:49 <@vapier> e
21:49 <@Kugelfang> nod
21:49 <@FlameBook> but besides that, what I'd like to ask QA is to commit for a broader involvement of developers in the process, and accept that they have to address multiple faces of QA, not only on correctness of ebuilds or proper code generation
21:49 <@vapier> plus libexec screws up my tab completion
21:49 <@Kugelfang> i have no time this weekend, but starting monday i will be able to work on it
21:49 <@FlameBook> vapier, I don't think that, we'll fill up with a lot of symlinks at the end
21:49 <@Kugelfang> FlameBook: sure... but this needs people to actually contribute
21:50 <@Kugelfang> vapier: *g*
21:51 <@vapier> your mom is a symlink
21:51 <@Kugelfang> is she?
21:52 <@Kugelfang> you surely know
21:52 <@Kugelfang> sticking your pointer into any symlink!
21:52 <@FlameBook> so anything else or we open the floor?
21:53 <@Kugelfang> i have nothing else
21:53 <@kloeri> I don't have anything either
21:53 <@Kugelfang> FlameBook: can you pleas esummarise why the current practive is bad in your eyes (in regard to /misc/?) per mail?
21:53 <@Kugelfang> FlameBook: either to -dev or to me directly
21:54 <@Kugelfang> (if it's not too much an effort :-))
21:54 <@FlameBook> Kugelfang, sure, just give me a bit of time, tonight I'm off sooner than usual
21:55 <@Kugelfang> FlameBook: sure, as i said... i won't work on it before monday
21:56 <@wolf31o2|mobile> so open floor?
21:56 -!- mode/#gentoo-council [-m] by Kugelfang
21:56 -!- kingtaco|work [n=kingtaco@gentoo/developer/kingtaco] has joined #gentoo-council
21:56 -!- mode/#gentoo-council [+o kingtaco|work] by ChanServ
21:56 <@kingtaco|work> doh
21:56 <@Kugelfang> yes :-)
21:56 <@Kugelfang> kingtaco|work: hahaha
21:56 <@Kugelfang> kingtaco|work: the meeting just finished
21:56 <@FlameBook> kingtaco|work, your timing i just the funniest :)
21:56 <@kingtaco|work> sorry, damn time zones messed me up
21:56 <@robbat2> UTC never moves
21:56 <@FlameBook> kingtaco|work, do you want to add something before we open the floor?
21:56 <@Kugelfang> FlameBook: the floor has been opened
21:56  * jokey is curious about gentoo-x86 migration
21:56 <@kingtaco|work> FlameBook, nothing I have thiss month
21:57 <@FlameBook> we discussed about the icons currently on the site (resolution: remove them as soon as possible)
21:57 <@Kugelfang> and kingtaco|work is now officially a slacker!!!!!!
21:57 <@Kugelfang> :-P
21:57 <@robbat2> jokey, to git/svn you mean?
21:57 <@kloeri> heh
21:57 < jokey> robbat2: yep
21:57 <@Kugelfang> kingtaco|work: sorry dude ;-)
21:57 <@kingtaco|work> Kugelfang, tis ok
21:57 <@robbat2> jokey, it was covered previously, but i'll recap
21:57 <@kingtaco|work> I do want to vote on that
21:57 <@robbat2> jokey, while svn would work at the moment, it isn't ideal
21:57 <@vapier> why are we converting
21:58 <@vapier> first i've heard
21:58 <@vapier> cvs works fine
21:58 <@kingtaco|work> vapier, no reason to
21:58 <@robbat2> jokey, git almost fills the requirements
21:58 <@Kugelfang> git was not designed for that
21:58 <@kloeri> cvs is shit but the pain of migrating is probably not worth it imo
21:58 <@robbat2> the final recommendation from antarus' SoC was that until git gets a few more specific features, we should hold off from any migration
21:59 -!- spb- [n=spb@gentoo/developer/spb] has joined #gentoo-council
21:59  * Kugelfang would probably vote yes for a migration to svn, but certainly not for git
21:59 <+g2boojum> FlameBook: Sorry, somehow I completely lack any memory of the icons issue.  It's a non-issue now, but care to fill me in?
21:59 <@robbat2> in specific, git needs history slicing and repo slicing
21:59  * jokey is fully with Kugelfang here
21:59 <@FlameBook> g2boojum, the icons we have on the /dyn/icons.xml page, contributed by the users, most certainly infringe copyrights and licenses
21:59 <@robbat2> upstream git does claim to be working on those issues, but says not to expect  results for several months
22:00 <@kingtaco|work> how is the icons thing our problem?
22:00 <@Kugelfang> robbat2: git is not the proper tool for that in my eyes
22:00 <@FlameBook> we have edited copies of windows's icons, other proprietary software icons, real and other logos, and some crystalsvg icons (from kde) that are licensed under LGPL (the icons on the site have no license)
22:00 <@wolf31o2|mobile> we put it on our page
22:00 <@kingtaco|work> has soneone claimed we're violating something?
22:00 <@kingtaco|work> IMO it's a trustees issue
22:00 <@robbat2> Kugelfang, on what grounds do you make that claim?
22:00 <@FlameBook> kingtaco|work, the previous trustees never answered to the call
22:00 <@kingtaco|work> FlameBook, so?  we have new ones now
22:00 <@wolf31o2|mobile> which is what I said... and FlameBook says the previous trustees didn't do anything about it... so brought it here
22:00 <@kingtaco|work> and it still isn't our place
22:01 <@Kugelfang> git is designed as a tool for distributed development
22:01 <@robbat2> jokey, i'm interested in your objections to git as well
22:01 <@FlameBook> kingtaco|work, right but what can they do? ask windows the permission to use the icons?
22:01 <@kingtaco|work> FlameBook, wait until someone proves we're infringing and then remove the infringing stuff?
22:01 <@Kugelfang> robbat2: gentoo's work is not of distributed nature
22:01 <+g2boojum> FlameBook: Do we know which ones violate copyright?  I'm happy to suggest that all infringing ones should go; I just don't know which those are.
22:01 <@FlameBook> kingtaco|work, it would be bad PR if it happens
22:01 <@kingtaco|work> FlameBook, we have plenty of that already
22:01 <@Kugelfang> kingtaco|work: no, as in that case we would liable for compensation
22:01 <@kingtaco|work> misbehaving devs
22:01 <@FlameBook> g2boojum, I can spot a lot that are infringing, so much that's IMHO not worth the hassle of spotting them
22:01 <@Kugelfang> kingtaco|work: as soon as we know it, we need to remove it
22:01 <@FlameBook> and we might miss some
22:02 <@FlameBook> kingtaco|work, any reason you have to keep em?
22:02 <@robbat2> err, noisy in here, Kugelfang/jokey, would you join #gentoo-cvs-migration to discuss this at length please?
22:02 < jokey> just that gentoo-x86 highly depends on the latest ebuilds everywhere so distributing it doesn't make sense
22:02 <@kingtaco|work> Kugelfang, I don't admit to gentoo putting up any copyrighted material
22:02  * jokey joins
22:02 <@wolf31o2|mobile> FlameBook: uhh... it's the trustees job (not the council's) to deal with *anything* legal... so if they wanted to ask MS, that's their choice... at the same time, they're responsible for making sure we comply with any licenses/laws
22:02 <@kingtaco|work> afaik, it's all free
22:02 <@kingtaco|work> until someone says otherwise
22:02 <+g2boojum> FlameBook: Okay, fair enough.
22:02 <@kingtaco|work> honestly, the council needs to stay out of legal issues
22:02 <@FlameBook> kingtaco|work, the point is that it's risky, we might be liable, and I'd rather avoid it for the icons we have
22:02 <@kingtaco|work> has anyone talked to the new trustees?
22:03 <@kingtaco|work> wolf31o2|mobile, you're one, right?
22:03 <@wolf31o2|mobile> kingtaco|work: I am
22:03 <@FlameBook> kingtaco|work, uhm no, it's not free until said otherwise, windows's icons are for sure copyrighted and thus not usable
22:03 <@vapier> trustees need to stop making legal desicions and talk to the lawyers
22:03 <@wolf31o2|mobile> vapier: we do
22:03 <@kingtaco|work> vapier, agreed, but it's still not our shindig
22:03 <@kingtaco|work> wolf31o2|mobile, tell me that the trustees are handling it
22:04 <@wolf31o2|mobile> anyway... nobody has said anything about it to the trustees since I've been on the alias
22:04 <@wolf31o2|mobile> kingtaco|work: as a trustee, I just say we remove the whole damn lot of them
22:04 <@wolf31o2|mobile> be done with it
22:04 <@kingtaco|work> wolf31o2|mobile, I'd agree with you, but _we_ don't make that decision
22:04 <+g2boojum> wolf31o2|mobile: I'd be happy to go along with that.
22:04 <@wolf31o2|mobile> kingtaco|work: correct... the council has no say on legal matters other than to make suggestions (as anyone can do) to the trustees
22:05 <@wolf31o2|mobile> anyway... bbiab
22:05 <@kingtaco|work> da
22:05 <@FlameBook> kingtaco|work, err why can't we make that decision, considering the icons territory is currently not looked after anyone, technically?
22:05 <@kingtaco|work> FlameBook, because the only ground for removing them is that it might violate some copyright
22:05 <@kingtaco|work> it's not proven either way
22:05 <@kingtaco|work> thus it's a legal issue
22:05 <@kingtaco|work> and we don't touch them
22:05 <@FlameBook> I see it in the opposite logic
22:05 <@FlameBook> we have no standing to leave them there
22:06 <@kingtaco|work> sure we do
22:06 <@kingtaco|work> they already exist
22:06 <@kingtaco|work> presumably people use them
22:06 <@FlameBook> _and_ we know we'll never have a clearance to use most of them
22:06 <@kingtaco|work> FlameBook, I don't admit that
22:06 <@kingtaco|work> and you shouldn't either
22:07 <@kingtaco|work> if M$ gets sand in their panties for us using their icons(if indeed it's theirs) then the trustees will remove it for copyright violations
22:07 <@kingtaco|work> unless you've done the byte by byte comparison of our files with theirs, noone knows
22:07 <@kingtaco|work> you can't assert either way
22:07 <@kingtaco|work> nor I
22:08 <@FlameBook> *shrug* then I will say that the council is pointless, if we can't even decide that we want to avoid issues and get rid of something unmaintained that might make us liable to copyright infridgement
22:08 <@FlameBook> really, I try to be pragmatic, those icons are a risk, they are not even that good IMHO, they are not in an usable format for icon themes, they are a bunch of graphics that is currently unmaintained
22:08 -!- kingtaco|laptop [n=kingtaco@gentoo/developer/kingtaco] has joined #gentoo-council
22:08 -!- mode/#gentoo-council [+o kingtaco|laptop] by ChanServ
22:09 <@kingtaco|laptop> damn video card
22:09 <@FlameBook> I don't find worth the risk and the hassle to leave them on there, if you want to bring it to the trustees, do so then.
22:09 <@kingtaco|laptop> where was I
22:09 <@FlameBook> [22:09]  <FlameBook> *shrug* then I will say that the council is pointless, if we can't even decide that we want to avoid issues and get rid of something unmaintained that might make us liable to copyright infridgement
22:09 <@FlameBook> [22:10]  <FlameBook> really, I try to be pragmatic, those icons are a risk, they are not even that good IMHO, they are not in an usable format for icon themes, they are a bunch of graphics that is currently unmaintained
22:09 <@FlameBook> [22:10]  * kingtaco|laptop (n=kingtaco@gentoo/developer/kingtaco) has joined #gentoo-council
22:09 <@kingtaco|laptop> FlameBook, again, it's not our choice
22:09 <@kingtaco|laptop> moreover, it's a moot point
22:09 <@kingtaco|laptop> 2 of the N trustees agree
22:09 -!- kingtaco|work [n=kingtaco@gentoo/developer/kingtaco] has quit [Read error: 113 (No route to host)]
22:09 <@FlameBook> I can't see why it's not our choice
22:09 <@FlameBook> are we deciding on technical matters, aren't we?
22:09 <@kingtaco|laptop> because we don't make legal decisions
22:10 <@kingtaco|laptop> this isn't remotly technical
22:10 <+g2boojum> FlameBook: We have a quorum in #-trustees now.  Working on the issue.
22:10 <@FlameBook> do whatever you want, if the meeting is finished, I'll work on danny's mail.. and I'll consider twice for next council, really.
22:10 <+g2boojum> rl03 suggests keeping the Gentoo icons (the ones w/ a gentoo icon inside), and dumping the rest.
22:11 <@kingtaco|laptop> FlameBook, that' really silly
22:11 <+g2boojum> FlameBook: Would you please get off your high horse.  You're getting what you wanted, even if it's not quite the way you intended.
22:12 <@FlameBook> kingtaco|laptop, no it is not, it's just that I think we're just losing time here if we have to jump in fire circles on every decision
22:12 -!- kingtaco|work [n=kingtaco@gentoo/developer/kingtaco] has joined #gentoo-council
22:12 -!- mode/#gentoo-council [+o kingtaco|work] by ChanServ
22:12 <+g2boojum> FlameBook: Had you popped into #-trustees, we probably could have handled it there, too.
22:12 <@FlameBook> g2boojum, my point is not the icons by themselves
22:12 <@FlameBook> it's just that it's a bureaucracy I'm no more sure I want to be part of.
22:14 <@kingtaco|laptop> FlameBook, feel free to write a GLEP changing what we can and cannot do
22:14 <@kingtaco|laptop> that's what it boils down to
22:14 <@kingtaco|laptop> we're not without limits
22:15 <@kingtaco|laptop> however, if we vote on it, then it probalby couldn't go into effect until the next set of council people
22:15 <@kingtaco|laptop> if we wanted it this year, then I guess it'd have to be a general vote
22:16 <@FlameBook> kingtaco|laptop, it's not just this particular thing, it's a more general problem
22:16 <@FlameBook> you can compare with my latest rant on blog.
22:18 <@FlameBook> besides, I mailed about the icons thing on Nov 24, if it was clear the issue was not our to decide, I wonder why nobody said it...
22:19 <@kingtaco|work> I would have cought it if I wasn't moving
22:19 <@kingtaco|work> I'm still behind on emails
22:19 <@kingtaco|work> I can't speak for the others
22:19 <@FlameBook> I'm just more used to the technical side, so I'll consider what I'll do before next meeting.
22:20 <@kingtaco|work> I don't understand
22:21 <+g2boojum> FlameBook: I saw your "rant", but that seemed like a different issue (the person who was supposed to write up summaries wasn't).
22:21 <+g2boojum> FlameBook: Or was your complaint that the last two meetings were dull and uninteresting?
22:22 <@FlameBook> g2boojum, no, it's just the same issue, we're losing ourselves in non-issues, and we can't enact anything decided, even if we stated we wanted to be a stronger council
22:22 <@FlameBook> this issue just confirmed my impression, so I'm not sure anymore if I want to have part in this, and I'll have to think about it.
22:22 <@kingtaco|work> FlameBook, the council exists to deside on technical issues, not legal issues
22:23 <@kingtaco|work> there is no technical reason to decide to keep or remove any or all icons from that page
22:23 <@kingtaco|work> there is a possible legal reason
22:23 <@kingtaco|work> even then the jurisdiction matters
22:23 <@FlameBook> kingtaco|work, there's no need to continue really, I said what I had to say, if the meeting is finished, I'll close here
22:23 <@kingtaco|work> frankly, we're not competent to decide on such issues
22:24 <@wolf31o2|mobile> neither are the trustees... we defer legal questions to the lawyers
22:24 <@kingtaco|work> FlameBook, if you're not willing to listen then I'll stop talking
22:25 <@FlameBook> kingtaco|work, I just think you're repeating "it's a legal issue you has to ignore it and leave ti to trustees" which is something I don't agree with, and I won't even if you repeat it 30 times
22:26 <@wolf31o2|mobile> jesus fucking christ on a stick
22:26 <@kingtaco|work> I've said all I can say
22:26 <@wolf31o2|mobile> nobody said you haev to ignore it
22:26 <@kingtaco|work> do what you want
22:26 <@wolf31o2|mobile> they just said you have to take it to the right people
22:26 <@wolf31o2|mobile> it is *that* simple
22:27 <@wolf31o2|mobile> anyway...
22:27  * kloeri agrees
22:27 <@wolf31o2|mobile> I mean... if you wanted... you could hound the trustees daily about it
22:27 <@wolf31o2|mobile> heh
22:27 <@FlameBook> wolf31o2|mobile, I still think that legal or not legal, deciding on removing them should be allowed to us, considering there's no one maintaining that page anymore
22:27 <@FlameBook> wolf31o2|mobile, have to look up the response from the previous trustees?
22:28 <@wolf31o2|mobile> FlameBook: sure, except that your only reasoning for removing them is legal
22:28 <@FlameBook> wolf31o2|mobile, I asked for a reason to keep them, too..
22:28 <@kingtaco|work> and I gave one
22:28 <@kingtaco|work> people use them
22:28 <@wolf31o2|mobile> how about "because they're there already and take 0 maintenance and aren't broken"
22:29 <@wolf31o2|mobile> same as any package in the tree that may or may not be maintained
22:29 <+g2boojum> FlameBook: For what it's worth, the trustees hang out in #-trustees, so if you ask in there we'll do what we can to help.  That's something new w/ the new crop of trustees, but it seems to be working.
22:29 <+g2boojum> FlameBook: (I'm not complaining about you bringing it to the council, I just want to let you know.)
22:30 -!- wolf31o2|mobile [n=wolf31o2@gentoo/developer/wolf31o2] has left #gentoo-council ["Leaving"]
22:30 <@FlameBook> again, I don't think that the idea of ignoring the issue and just saying "it's legal issue it's legal issue I don't want to hear NANANANANNANA" is a wrong turn.
22:30 -!- fmccor is now known as fmccor|away
22:31 <@kingtaco|work> FlameBook, don't take cheap shots at me just because I don't agree with what you want
22:31 <@FlameBook> I can understand for things that might be controverse, but I don't really find much controversy in this... besides, the point that people use them... as mike said they are also on the forum
22:31 <@kloeri> reminds me, I have a somewhat tricky trustees issue but I should probably mail trustees about that :)
22:31 <@kingtaco|work> that's incredably poor taste
22:31 <@FlameBook> kingtaco|work, not taking a cheap shot, that's just how I seen your behaviour in this matter
22:32 <@FlameBook> didn't you want to redirect it to trustees without even considering the issue at all?
22:32 <@kingtaco|work> I AGREED WITH YOU!
22:32 <@kingtaco|work> I disagreed that it was something we should do
22:32 <@kingtaco|work> considering that the trustees didn't look at it
22:32 <@kingtaco|work> fuck the old trustees
22:32 <@kingtaco|work> they don't matter
22:33 <@kingtaco|work> moreover, as soon as you brought it up, they started looking at it
22:36 <@kingtaco|work> anyone have anything else to add to this meeting?
22:37 <@kloeri> nope
22:38 <@kingtaco|laptop> who ran the meeting today?
22:40 <@kloeri> didn't decide that, just jumped to flameeyes issues
22:41 <@kingtaco|laptop> oh
22:41 <@kingtaco|laptop> someone want to close the meeting?
22:41 <@kloeri> I can mail log + summary fwiw
22:41 <@kloeri> meeting closed :)
22:44 <@kingtaco|laptop> ok
22:45 <+g2boojum> FlameBook: If you're still around, I've just sent an e-mail to neysx requesting that the icons page be pulled.
22:45 <@FlameBook> g2boojum, was going to ask you about that
22:46 <+g2boojum> FlameBook: It took a bit of discussion before I knew who could do it.  I'm not sure how the dyn pages work.
22:46 <@FlameBook> g2boojum, the same applies to me, I asked that beforehand just to be sure, /dyn/ should be under infra's look, and robbat2 confirmed before it was doc guys to handle (thus, neysx)
22:55 -!- robbat2 [n=robbat2@gentoo/developer/robbat2] has quit [Remote closed the connection]
22:57 -!- fmccor|home [n=fmccor@gentoo/developer/fmccor] has joined #gentoo-council
23:10 -!- spb [i=spb@gentoo/developer/spb] has quit ["hooray for new machines"]
23:15 -!- FlameBook [n=intrepid@gentoo/developer/Flameeyes] has quit ["Leaving"]
23:16 -!- spb- is now known as spb
23:26 -!- Flameeyes is now known as FlAFKeyes
23:35 -!- robbat2 [n=robbat2@gentoo/developer/robbat2] has joined #gentoo-council
23:35 -!- mode/#gentoo-council [+o robbat2] by ChanServ
23:49 -!- g2boojum [n=grant@gentoo/developer/g2boojum] has left #gentoo-council []
--- Log closed Fri Dec 15 00:00:38 2006
