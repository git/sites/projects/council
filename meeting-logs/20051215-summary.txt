this months meeting wasnt too eventful, kind of quiet ... on the agenda:

- Marius: decision on multi-hash for Manifest1
there was a bit of hearsay about why the council was asked to review/decide 
on this issue since we werent able to locate any portage devs at the time of 
the meeting ... so our decision comes with a slight caveat.  assuming the 
reasons our input was asked for was summarized in the e-mail originally sent 
by Marius [1], then we're for what we dubbed option (2.5.1).  that is, the 
portage team should go ahead with portage 2.0.54 and include support for 
SHA256/RMD160 hashes on top of MD5 hashes.  SHA1 should not be included as 
having both SHA256/SHA1 is pointless.  further more, we hope this is just a 
hold over until Manifest2 is ironed out/approved/implemented/deployed.  it 
was also noted that we should probably omit ChangeLog and metadata.xml files 
from the current Manifest schema as digesting them serves no real purpose.
[1] http://article.gmane.org/gmane.linux.gentoo.devel/33434

- Council: portage signing
shortly after our November meeting, a nice summary was posted by Robin 
Johnson that covered signing issues from top to bottom.  as such, it was felt 
that trying to throw together a GLEP would not be beneficial.  instead we 
will be adding a constant agenda item to future council meetings as to the 
status of portage signing issues to keep the project from slipping into 
obscurity again.
