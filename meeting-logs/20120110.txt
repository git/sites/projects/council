21:00 <@  grobian> okies
21:00 <@  grobian> ping Betelgeuse hwoarang jmbsvicetto ulm ulm_ dberkholz blueness 
21:00 <  blueness> here
21:01 <@ hwoarang> here
21:01 <@      ulm> here
21:01 <+dberkholz> hi
21:02 <@  grobian> hmmm, jmbsvicetto and Betelgeuse are still missing
21:05 <@  grobian> jmbsvicetto said last time he'd by swamped in work at this time, so that may explain
21:05 <  blueness> long seen time on Betelgeuse, over a day
21:05 <@  grobian> agenda is supershort, which makes it easy to deal with, it's on http://dev.gentoo.org/~grobian/agenda-20120110
21:06 <@  grobian> yeah, he usually pops in, though
21:06 <@  grobian> if everybody agrees, in open actions, I "closed" my action, given my post to -project on the matter
21:07 <@ hwoarang> i dont follow
21:07 <@ hwoarang> you mean [1] ?
21:07 <@ hwoarang> re eclass API
21:08 <@  grobian> yeah
21:08 <@  grobian> it's an open action on the agenda
21:09 <@Betelgeus> grobian: hello
21:09 <@Betelgeus> sorry about being late
21:09 <@  grobian> ah, Betelgeuse 
21:09 <@  grobian> anyone got contact details for jmbsvicetto 
21:09 <@Betelgeus> grobian: I can phone him
21:09 <@  grobian> I searched my cards
21:10 <@  grobian> do we all agree that the agenda is very short?  That is, if there's no open floor questions, we're done if jorge just says he's ok with the agenda
21:11 <@ hwoarang> fine with me
21:11 <  blueness> Chainsaw gave me no issues to raise, so sure
21:11 <@  grobian> yeah
21:11 <@  grobian> any open floor issues?
21:11 <@Betelgeus> grobian: jmbsvicetto is coming
21:12 <@  grobian> Betelgeuse: thanks a lot
21:12 <@jmbsvicet> sorry :(
21:12 <@jmbsvicet> Petteri thanks for calling me
21:13 <@  grobian> ok, we all agree on the agenda?
21:13 <@  grobian> I take that as a yes
21:13 <  blueness> yes
21:13 <@  grobian> Anyone who wants to raise issues in the open floor?
21:13 <@jmbsvicet> quickly reading backlog and agenda
21:14 <@  grobian> jmbsvicetto: open actions, I assume no update from your part?
21:14 <@jmbsvicet> grobian: sorry, no
21:14 <@  grobian> ok, updated
21:14 <@jmbsvicet> I looked at the elections page this weekend, but wasn't able to complete the move
21:15 <@jmbsvicet> grobian: agenda is ok for me
21:15 <@  grobian> well, then, if there's no open floor issues, thank you all for being here, and thanks for this productive meeting ;)
