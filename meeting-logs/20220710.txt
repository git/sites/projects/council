[12:00:22 pm] <willikins> (council@gentoo.org) ajak, dilfridge, gyakovlev, mattst88, mgorny, sam, ulm
[12:00:40 pm] <@ulm> who wants to chair?
[12:00:57 pm] <@gyakovlev> I can, agenda looks easy =)
[12:01:07 pm] <@ulm> k :)
[12:01:40 pm] <@gyakovlev> ok, let's start then.
[12:01:40 pm] <@gyakovlev> Agenda for today's meeting:
[12:01:40 pm] <@gyakovlev> https://archives.gentoo.org/gentoo-project/message/8c2a6a547d4dc5b883e3c957cfd9b398
[12:01:49 pm] <@gyakovlev> 1. Roll call
[12:01:53 pm] -*- ulm here
[12:01:53 pm] -*- gyakovlev here
[12:01:54 pm] -*- ajak here
[12:01:54 pm] -*- sam_ here
[12:01:57 pm] -*- mattst88 here
[12:02:02 pm] -*- mgorny here
[12:02:24 pm] <@gyakovlev> dilfridge: ping
[12:02:54 pm] <@gyakovlev> let's pausefor  2-3 minutes and go on.
[12:03:09 pm] <+robbat2> hi, I have to run; but I do have comments
[12:03:17 pm] <@ulm> gyakovlev: I'll text him
[12:03:24 pm] <+robbat2> the new servers are online; all existing VMs are migrated
[12:03:46 pm] <+robbat2> the old servers are powered down to save some power in the rack for now, and to make sure nobody finds anything missing from the migration
[12:04:03 pm] <+robbat2> and i'm also moving house; medium distance
[12:04:20 pm] <@gyakovlev> robbat2: and by the looks of ssd ticket it has been reimbursed, right?
[12:04:22 pm] <+robbat2> next steps when i'm back online at the new house: migrate dipper.g.o from physical harder into new VM hosts
[12:04:39 pm] <+robbat2> then wipe & reinstall the old servers; to bring up more dev/build capacity
[12:04:51 pm] <+robbat2> yes, as treasurer, the reimbursement was issued
[12:04:58 pm] <@gyakovlev> ty
[12:05:05 pm] <+robbat2> do not close the ticket until I close the ledger/financial books on the fiscal year
[12:05:44 pm] <@gyakovlev> ok thanks for updates.
[12:05:44 pm] <@gyakovlev> we'll continue with next agenda item then, hope dilfridge catches up later in time.
[12:05:55 pm] <@gyakovlev> next item:
[12:05:55 pm] <@gyakovlev> 2. Constitute the new council
[12:06:00 pm] <+robbat2> are there any other bugs for infra you need me for?
[12:06:33 pm] <@gyakovlev> file hosting, but idk
[12:06:33 pm] <@gyakovlev> https://bugs.gentoo.org/176186
[12:07:24 pm] <@ajak> the file hosting one and the pkgcore mirroring bugs are both "infra" bugs, so maybe
[12:07:34 pm] <+robbat2> the only request I have is for council to increase policy around about what should be done for developers who retire/are-retired, w.r.t: 1. live votes; should they be counted if cast before deadline; 2. overlays converting from developer overlays to user overlays
[12:07:48 pm] <+robbat2> on #1; I feel yes; on #2 I feel also yes
[12:08:16 pm] <+robbat2> file hosting is a maybe project for working on my laptop when i'm between having a home (staying in a friend's apartment for 1.5 weeks)
[12:08:17 pm] <@mgorny> these are not items for this meeting
[12:08:43 pm] <@ulm> yes, let's decide on that later (next month?)
[12:08:52 pm] <@gyakovlev> yeah those are agebda-worthy
[12:08:54 pm] <+robbat2> i'm going to go back to packing boxes into a moving van then
[12:08:57 pm] <@gyakovlev> agenda*
[12:08:57 pm] <@sam_> thanks!
[12:09:23 pm] <@gyakovlev> ok we had a small open floor, let's continue with 
[12:09:23 pm] <@gyakovlev> 2. Constitute the new council
[12:09:56 pm] <@gyakovlev> all council members khow our worflow, but we have a fresh member: welcome ajak!
[12:09:57 pm] <@ulm> gyakovlev: so much about an easy summary :p
[12:10:23 pm] <@gyakovlev> - Decide on time of meetings. The previous council had its meetings
[12:10:23 pm] <@gyakovlev> on the 2nd Sunday of every month at 19:00 UTC.
[12:10:36 pm] <@ajak> thanks :)
[12:10:41 pm] <@gyakovlev> any1 got any other suggestions?
[12:11:01 pm] <@gyakovlev> or we just keep the schedule (note this meeting is on time with old schedule)
[12:11:09 pm] <@mattst88> mgorny: didn't you suggest having an earlier time?
[12:11:29 pm] <@mgorny> not really, no
[12:11:30 pm] <@gyakovlev> or was it 1 time thing?
[12:11:36 pm] <@mattst88> I'm totally fine with 19:00 UTC, 2nd Sunday, but an earlier time would be fine with me too
[12:11:55 pm] <@mgorny> let's not move overcomplicate things with changes ;-)
[12:11:59 pm] <@mattst88> wfm
[12:12:15 pm] <@gyakovlev> wfm too
[12:12:15 pm] <@gyakovlev> ajak are you comfortable with those dates/times?
[12:12:19 pm] <@ajak> yes
[12:13:11 pm] <@gyakovlev> ulm: I assume you are ok with those too? or should we do a quick vote?
[12:13:17 pm] <@ulm> wfm
[12:13:33 pm] <@gyakovlev> ok let's move on then. dilfridge missed his chance to speak up =)
[12:14:34 pm] <@gyakovlev> next item: general workflow.
[12:14:34 pm] <@gyakovlev> call for agenda is due 2 weeks before the meeting.
[12:14:34 pm] <@gyakovlev> final agenda is due 1 week before meeting.
[12:14:34 pm] <@gyakovlev> discussions are encouraged to happen on project ml.
[12:15:01 pm] <@gyakovlev> any comments, additions from anyone on this?
[12:15:46 pm] <@ulm> this workflow is well established
[12:16:01 pm] <@mattst88> nope, but we should probably make a bot or a calendar reminder or something so we don't forget as often as in the past :)
[12:16:06 pm] <@sam_> heh
[12:16:27 pm] <@ulm> it's in the /topic here, to start with
[12:16:34 pm] -*- ajak was already planning on making lots of reminds in calendar
[12:17:17 pm] <@gyakovlev> bot is optional, calendar is personal.
[12:17:54 pm] <@gyakovlev> I opt in for cal =) but if someone wants to teach willikins or other bot new tricks - feel free too.
[12:19:09 pm] <@mattst88> okay, what's next?
[12:19:12 pm] <@gyakovlev> ok since the agenda item says vote, let's cast a quick vote.
[12:19:12 pm] <@gyakovlev> council's workflow:
[12:19:12 pm] <@gyakovlev> sending a call for agenda items (two weeks in advance), sending the
[12:19:12 pm] <@gyakovlev> agenda (one week in advance) and have the meeting focussed, i.e.
[12:19:12 pm] <@gyakovlev> have major discussions on the gentoo-project mailing list prior
[12:19:12 pm] <@gyakovlev> to the meeting
[12:19:18 pm] -*- gyakovlev yes
[12:19:21 pm] -*- ajak yes
[12:19:22 pm] -*- sam_ yes
[12:19:22 pm] -*- ulm yes
[12:19:25 pm] -*- mgorny yes
[12:19:40 pm] -*- mattst88 yes
[12:20:03 pm] <@gyakovlev> ok that's 6 yes, 1 missing
[12:20:03 pm] <@gyakovlev> motion passes.
[12:20:10 pm] <@gyakovlev> moving on
[12:20:10 pm] <@gyakovlev> - Appoint chairmen for this term's meetings
[12:20:19 pm] <@ulm> I can take January/February
[12:20:27 pm] <@gyakovlev> Since I took Jul, I can take Aug too.
[12:20:34 pm] <@mattst88> I'll take whatever and I'll be happy to trade
[12:20:45 pm] <@ajak> ^ same here
[12:20:56 pm] <@mattst88> I'll take Nov/Dec for now
[12:21:11 pm] <@sam_> i'll do sept/oct
[12:21:28 pm] <@gyakovlev> ok ulm sam_ and mattst88 added (I'll paste full later)
[12:21:34 pm] <@mattst88> got Mar/Apr and May/June left
[12:21:51 pm] <@ajak> i'll do mar/apr
[12:21:52 pm] <@mattst88> I think we should assign all four to dilfridge :)
[12:21:56 pm] <@ajak> lol
[12:22:32 pm] <@mgorny> i'll take May and let's give June to dilfridge
[12:22:33 pm] <@gyakovlev> mgorny: I'm assigning May+June to you, ok?
[12:22:48 pm] <@gyakovlev> we can change it later
[12:22:51 pm] <@mgorny> sure
[12:23:13 pm] <@gyakovlev> feel free to pressure dilfridge to taking one, this I'm not comfortable on forcing on him
[12:23:18 pm] <@gyakovlev> without his ack
[12:23:26 pm] <@mgorny> he'll take every other one as a punishment
[12:23:41 pm] <@gyakovlev> ok, here's what I have
[12:23:41 pm] <@gyakovlev> July - gyakovlev
[12:23:41 pm] <@gyakovlev> August - gyakovlev
[12:23:41 pm] <@gyakovlev> September - sam
[12:23:41 pm] <@gyakovlev> October - sam
[12:23:41 pm] <@gyakovlev> November - mattst88
[12:23:41 pm] <@gyakovlev> December - mattst88
[12:23:41 pm] <@gyakovlev> January - ulm
[12:23:41 pm] <@gyakovlev> Feruary - ulm
[12:23:41 pm] <@gyakovlev> March - ajak
[12:23:41 pm] <@gyakovlev> April - ajak
[12:23:41 pm] <@gyakovlev> May - mgorny
[12:23:41 pm] <@gyakovlev> June - mgorny
[12:24:05 pm] <@gyakovlev> (can be changed/exchanged as we go)
[12:24:38 pm] <@gyakovlev> ok, we are done with new council stuff
[12:24:38 pm] <@gyakovlev> let's move to bugs
[12:24:40 pm] <@gyakovlev> 3. Open bugs with council involvement
[12:24:51 pm] <@gyakovlev> bug 835152
[12:24:52 pm] <willikins> gyakovlev: https://bugs.gentoo.org/835152 "Mirror pkgcore/* repos from github"; Gentoo Infrastructure, Git; CONF; sam:infra-bugs
[12:25:11 pm] <@sam_> I feel like we may have agreed to drop ourselves from that and I forgot to do it?
[12:26:02 pm] <@gyakovlev> at least on 2022-04-10  we agreed to monitor
[12:26:10 pm] <@gyakovlev> checking 05-08 log (no summary yet)
[12:26:11 pm] <@sam_> oh ok
[12:26:37 pm] <@mattst88> nothing for us to do at the moment, AFAIK
[12:26:45 pm] <@gyakovlev> [2022-05-08T20:11:35+0100] <willikins> sam_: https://bugs.gentoo.org/835152 "Mirror pkgcore/* repos from github"; Gentoo Infrastructure, Git; CONF; sam:infra-bugs
[12:26:45 pm] <@gyakovlev> [2022-05-08T20:11:40+0100] <@sam_> I think we're making progress on gitlab, nothing more to say there really
[12:26:45 pm] <@gyakovlev> [2022-05-08T20:11:53+0100] <@sam_> I think the intention is to host everything for pkgcore on gitlab if possible
[12:26:59 pm] <@gyakovlev> so continue monitoring it is then?
[12:27:08 pm] <@mattst88> sure
[12:27:09 pm] <@sam_> it needs to be done so yeah, sure
[12:27:38 pm] <@gyakovlev> moving on
[12:27:38 pm] <@gyakovlev> bug 176186
[12:27:42 pm] <willikins> gyakovlev: https://bugs.gentoo.org/176186 "Gentoo projects file hosting"; Gentoo Infrastructure, Other; IN_P; dsd:infra-bugs
[12:28:24 pm] <@mattst88> sounds like progress is being made
[12:28:33 pm] <@mattst88> don't think there's anything for us to do at the moment
[12:28:42 pm] <@mattst88> except to continue monitoring
[12:28:50 pm] <@gyakovlev> I see patch is posted on the bug, Robin will continue after personal matters handled.
[12:28:57 pm] *** Mode #gentoo-council +v ConiKost by ChanServ
[12:29:03 pm] <@gyakovlev> yeah agreed, let's keep keeping eye on it =)
[12:29:16 pm] <@gyakovlev> bug 835165
[12:29:18 pm] <willikins> gyakovlev: https://bugs.gentoo.org/835165 "Reimbursement for SSD purchase for Infra 3/14/2022"; Gentoo Foundation, Reimbursements; CONF; antarus:trustees
[12:29:38 pm] <@gyakovlev> update from robbat2 received, payment sent, not closing bug yet.
[12:29:50 pm] <@mgorny> un-cc us?
[12:30:03 pm] <@ajak> does council need to stay cc'd now that voting is done (for a while)?
[12:30:13 pm] <@gyakovlev> yeah, agreed. un-cc
[12:30:44 pm] <@gyakovlev> we just stayed on cc to monitor situation, but assuming it's actually done and bug is only needed for accounting
[12:31:00 pm] <@gyakovlev> I'll un-CC council right now unless someone has strong counter argument
[12:31:04 pm] <@sam_> nah, wfm
[12:31:38 pm] <@gyakovlev> done
[12:32:20 pm] <@gyakovlev> and I don't see server purchase bug in bugzie search
[12:32:29 pm] <@gyakovlev> but we have a nice update from infra
[12:32:37 pm] <@gyakovlev> I'll add it to the meeting summary.
[12:33:06 pm] <@gyakovlev> ok moving on
[12:33:06 pm] <@gyakovlev> 4. Open floor
[12:33:09 pm] <@gyakovlev> anyone?
[12:33:33 pm] <@ulm> I was working on a GLEP for EAPI deprecation: https://github.com/ulm/glep/blob/glep-eapi-deprecation/glep-0083.rst
[12:33:47 pm] <@mgorny> we should probably think of pushing for umbrella migration, esp. given the trustee election fiasco
[12:33:47 pm] <@ulm> presumably I'll post it to -dev sometime next week
[12:35:37 pm] <@mattst88> mgorny: do you have time to reach out again to the potential umbrellas that would take us?
[12:35:41 pm] <@gyakovlev> ulm: sounds good, thanks for heads up. we had a very heated discussion before about upgrade paths and timelines, this one fits that bill
[12:35:51 pm] <@mgorny> mattst88: no, it's totally out of my capabilities right now
[12:35:56 pm] <@mgorny> somebody else needs to take over
[12:36:15 pm] <@mattst88> okay, I can totally understand that
[12:36:57 pm] <@ulm> gyakovlev: the obvious parameters in GLEP 83 are the two time constants which may require some fine tuning
[12:37:09 pm] <@gyakovlev> ulm: 24mo and 5%?
[12:37:16 pm] <@ulm> but let's see what will come up in the discussion
[12:37:26 pm] <@ulm> 24 months and 24 months
[12:37:44 pm] <@gyakovlev> I quickly skimmed the doc, but I need to read it properly, will do once it's posted.
[12:39:43 pm] <@mattst88> no other topics?
[12:40:13 pm] <@gyakovlev> I also agree council needs do something about foundation  struggles, but I personally am unable to commit to pushing it.
[12:41:33 pm] <@mattst88> I'm also moving house (across the country), so if there are no other topics, I'm going to take off and continue packing
[12:41:54 pm] <@gyakovlev> yeah looks like open floor is concluded.
[12:42:33 pm] <@mgorny> thanks, everyone
[12:42:37 pm] -*- gyakovlev bangs the gong
[12:42:37 pm] <@gyakovlev> meeting closed, thanks everyone for participation.
