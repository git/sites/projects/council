Summary of Gentoo council meeting 2020-09-13

Agenda
======
1. Roll call
2. Open bugs with council participation
3. Open floor

Roll call
=========
Present: dilfridge, gyakovlev, mattst88, slyfox, ulm, whissi, williamh

Open bugs with council participation
====================================
- Bug 662982 "[TRACKER] New default locations for the Gentoo
  repository, distfiles, and binary packages":
  Tabled until next month.

- Bug 687938 "QA lead approval 2020: soap edition":
  The council has approved soap as QA lead.

- Bug 688876 "Comrel webpage does not document expectations of privacy":
  No council action, for the time being.

- Bug 729062 "Services and Software which is critical for Gentoo
  should be developed/run in the Gentoo namespace":
  RFC posted to dev mailing list.

- Bug 736760 "Application to Software Freedom Conservancy":
  No council action, for the time being.

Open floor
==========
No items were raised.
