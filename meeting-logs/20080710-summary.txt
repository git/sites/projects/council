Quick summary
=============

GLEP 54: There were numerous questions that apparently were not brought 
         up on the mailing list in advance or were not addressed.

GLEP 55: On hold pending a concrete requirement for it. GLEP 54 may be, 
         but that's unclear until it's been revised.

GLEP 56: Approved. Cardoe will get repoman changes made, followed by a 
         server-side script to generate use.local.desc from 
         metadata.xml.


The meeting wrapped up in under 1 hour again. We still need to work 
harder to push more discussion and questions to the mailing list, 
though.


Topics
======

GLEP 54
-------
Preparation: Post your opinion to the -dev thread "A few questions to 
our nominees" 4+ hours before the meeting.

Last month: 
	dberkholz: http://archives.gentoo.org/gentoo-dev/msg_c6e4ba8293f50c1e0444e67d59cf85ea.xml
	lu_zero: http://archives.gentoo.org/gentoo-dev/msg_05614741b3942bfdfb21fd8ebb7955e0.xml

Goal: Status check at meeting to see who's ready to vote. Vote on-list 
no later than July 17.

<Betelgeuse@> dberkholz: with GLEP 55 EAPI X can add the support for scm
<Betelgeuse@> dberkholz: and older Portage versions work just fine

<Betelgeuse@> dberkholz: In general I oppose adding things to EAPI 0

<   lu_zero@> dberkholz problem: if you have -scm installed
<   lu_zero@> and then switch to a pm not knowing it
<   lu_zero@> you have a nice recipe for inconsistency

<   Halcy0n@> I would really like to see a list of features that we would 
                    end up having after implementing this GLEP.  The GLEP 
                    mentions possible enhancements, but I'd like to see what we 
                    would have planned if we go forward with this change.
<   Halcy0n@> Well, it only mentions one enhancement, I'd like to see 
                    what else we could do to judge if it is worth it.
Halcy0n@> Betelgeuse: yes, I know there are some things we could do,
                    but I'd like to see a more extensive list of possibilities, 
                    what are other possible ways of doing this (like a metadata 
                    tag for the ebuild), and why those other methods aren't 
                    sufficient.

< dberkholz@> i think the point here is that the glep should address what 
                    made its implementation superior to other possible ones, 
                    which it also describes

< dberkholz@> ok, i've noted the issues raised here
< dberkholz@> once they're address, the glep can be revised and we'll 
                    consider it again

Summary: Specific questions and requests are above.


GLEP 55
-------
Preparation: Post your opinion to the -dev thread "GLEP 55" 4+ hours 
before the meeting.

Last month:
	dberkholz: http://archives.gentoo.org/gentoo-dev/msg_c6e4ba8293f50c1e0444e67d59cf85ea.xml

Goal: Status check at meeting to see who's ready to vote. Vote on-list 
once we're ready.

<Betelgeuse@> But I don't see the use of accepting it before we a) 
                    Portage has something that would make use of it b) some 
                    other pkg manager is made official
<   Halcy0n@> So, can we vote on postponing a GLEP of this nature until 
                    another glep requires such changes?

Summary: On hold pending a concrete requirement for it. GLEP 54 may be, 
but that's unclear until it's been revised.


GLEP 56
-------
Preparation: Post your opinion to the -dev thread "[GLEP56] USE flag 
descriptions in metadata" 4+ hours before the meeting. (Cardoe: Did the 
requested updates ever get made?)

Last month:
	dberkholz: http://archives.gentoo.org/gentoo-dev/msg_54ee20d2b1d8122370afdd4b3d7aafc9.xml

Goal: Status check at meeting to see who's ready to vote. Vote on-list 
no later than July 17, if requested changes are made.

Requested changes were made: 
http://sources.gentoo.org/viewcvs.py/gentoo/xml/htdocs/proj/en/glep/glep-0056.txt?r1=1.1&r2=1.2

<    Cardoe > Well the first step of making that portion happen is going 
                    to be to add a check to repoman that if use.local.desc is 
                    not present in the repo, do new QA check.
<    Cardoe > Once that's in place that developers can use, then the 
                    infra script will happen.
<    Cardoe > I've already discussed it with the Portage folks and the 
                    infra folks.

Summary: Approved.


Roll call
=========

(here, proxy [by whom] or slacker?)

betelgeuse  here
dberkholz   here
dertobi123  here
flameeyes   here
halcy0n     here
jokey       here
lu_zero     here
