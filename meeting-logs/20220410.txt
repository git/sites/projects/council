[12:00:02 pm] <@gyakovlev> !proj council
[12:00:03 pm] <willikins> (council@gentoo.org) dilfridge, gyakovlev, marecki, mattst88, mgorny, sam, ulm
[12:00:08 pm] <@gyakovlev> meeting time!
[12:00:23 pm] <@gyakovlev> agenda for today: https://archives.gentoo.org/gentoo-project/message/43aa9b678f7108d3269548d87c895e59
[12:00:33 pm] <@gyakovlev> 1) Roll call
[12:00:37 pm] -*- mattst88 here
[12:00:37 pm] -*- gyakovlev here
[12:00:41 pm] -*- sam_ here
[12:00:43 pm] -*- ulm here
[12:00:45 pm] -*- mgorny here
[12:01:27 pm] -*- Marecki here
[12:01:58 pm] <@gyakovlev> dilfridge: ping
[12:02:17 pm] <@dilfridge> here
[12:02:27 pm] <@gyakovlev> nice, everyone is here. moving on.
[12:02:35 pm] <@gyakovlev> 2) Utilizing GH functionality that Gentoo infra does not provide
[12:03:08 pm] <@gyakovlev> here has been a discussion on #g-project
[12:03:19 pm] <@dilfridge> (or, "vapier wants an extrawurst")
[12:04:02 pm] <@mattst88> I don't understand the requirement to be hosted on gentoo infra, tbh
[12:04:13 pm] <@mattst88> e.g. why is it okay for OpenRC to be hosted on Github?
[12:04:52 pm] <@dilfridge> shrug, because we can run away to Gretna Green with systemd at any moment?
[12:05:06 pm] <@dilfridge> dunno either
[12:05:19 pm] <@sam_> OpenRC isn't a gentoo project
[12:05:24 pm] <@mattst88> and what horrible thing would happen if we started using Github releases for sandbox? (sandbox is an example because it's something that vapier releases)
[12:05:30 pm] <@mgorny> mattst88: we've abandoned openrc fwir
[12:05:48 pm] <@mattst88> sam_: I think that's a lazy argument -- it certainly started as a Gentoo project
[12:05:49 pm] <@sam_> openrc is very much not a gentoo project, and william is quite clear on that, _but_ there is a valid point about "gentoo depending on things we don't host ourselves"
[12:05:55 pm] <@sam_> yes, but it's beside the point
[12:06:01 pm] <@sam_> it's not relevant to comparison with sandbox/pax-utils
[12:06:09 pm] <@sam_> william is very clear it's disconnected with gentoo and they host bugs on github too
[12:07:14 pm] <@gyakovlev> we already talked about this one before, I still don't get what exactly is needed? =)
[12:07:14 pm] <@gyakovlev> anyone can commit .github directory with some yaml voodoo for CI.
[12:07:14 pm] <@gyakovlev> I'm  against on relying on GH tarball hosting for pure gentoo projects though.
[12:07:14 pm] <@gyakovlev> it's ok to grab a tag, and mark it as release on github - sure.
[12:07:14 pm] <@gyakovlev> but primary SRC_URI should be gitweb tarball imo.
[12:07:14 pm] <@gyakovlev> mirror system syncs tags already.
[12:07:42 pm] <@mattst88> I wish vapier had replied to me with info on what projects he would like to change
[12:08:30 pm] <@mattst88> gyakovlev: but why? in what case can you foresee a problem relying on a github tarball for SRC_URI?
[12:08:31 pm] <@Marecki> With GitHub, I would be VERY careful about vendor lock-in. Much easier to migrate to GH than off it.
[12:09:05 pm] <@Marecki> Unlike Gitlab, which you can basically take with you.
[12:09:12 pm] <@mgorny> my main concern is long-term reliability
[12:09:21 pm] <@mattst88> I mean, anything can happen. we've had plenty of source sites shut down, like berlios.de
[12:09:30 pm] <@mgorny> we've been hit by this before
[12:09:32 pm] <@dilfridge> mgorny: that is a two-edged sword
[12:09:34 pm] <@sam_> free CI is nice and I don't mind that, I just don't really get the benefit or need to make official releases on GH or anything
[12:09:51 pm] <@gyakovlev> mattst88: not a problem per-se, just feels wrong long term.
[12:09:51 pm] <@gyakovlev> what's the difference specifying infra SRC_URI instead of github?
[12:09:51 pm] <@mgorny> if we host stuff on gentoo infra, we at least can have backups that we can acces
[12:10:05 pm] <@dilfridge> but yes, what nobody has mentioned yet is, there is a reason why gentoo github was essentially locked down
[12:10:06 pm] <@mattst88> sam_: agreed. maybe we just table this today and let more discussion happen
[12:10:13 pm] <@gyakovlev> there's 0 difference if you specify tagged tarball
[12:10:23 pm] <@gyakovlev> both systems support tags
[12:10:25 pm] <@dilfridge> and that reason is still valid
[12:10:36 pm] <@mgorny> i don't have problem with people running CI on github or filing pull requests (though i would prefer our own hosted system for that)
[12:10:44 pm] <@mgorny> because if we lose that, it's not critical
[12:10:45 pm] <@mattst88> (FWIW, if gitlab.gentoo.org was totally ready, I'd just say use it)
[12:10:47 pm] <@sam_> the thing about CI on github + PRs is that it's additive; we're not relying on it
[12:10:49 pm] <@sam_> yes, exactly
[12:11:00 pm] <@mgorny> but i don't want to be recovering release tarballs from mirrors because backing github up is hard
[12:11:15 pm] <@sam_> mattst88: yeah, the person driving the discussion needs to input more I think; I'm not sure any of us really get what's desired here (or why, anyway)
[12:11:18 pm] <@sam_> feels a bit X-Y problemish
[12:11:28 pm] <@dilfridge> so, how about we just tell everyone who wants to use additional github features, "focus your energy on getting our gitlab running"?
[12:11:29 pm] <@mattst88> and I kind of share vapier's skepticism of our infra. remember the forums migration?
[12:12:06 pm] <@dilfridge> infra_2 
[12:12:39 pm] <@mattst88> dilfridge: sounds good to me.
[12:13:11 pm] <@sam_> what i do accept is the frustration and issue with e.g. devs retiring and tarballs they hosted going away, for projects we host
[12:13:21 pm] <@ulm> another aspect is that any discussion happening in github pull requests and issues may be lost at some time, and we don't have control
[12:13:24 pm] <@sam_> and I think even an imperfect solution to bug 176186 would be great
[12:13:25 pm] <willikins> sam_: https://bugs.gentoo.org/176186 "Gentoo projects file hosting"; Gentoo Infrastructure, Other; IN_P; dsd:infra-bugs
[12:13:36 pm] <@mattst88> sam_: that's really freaking ridiculous, tbh. 
[12:13:47 pm] <@mgorny> yes, i still wish we had something like 90s ftp server
[12:13:56 pm] <@sam_> mattst88: it looks awful too because pax-utils is used by external projects, and then when slyfox retires, 404!
[12:14:04 pm] <@mgorny> but infra doesn't bother with such non-enterprise solutions
[12:14:15 pm] <@sam_> or they have to know that they have to try both vapier's and my devspace or something
[12:14:30 pm] <@sam_> yes, an FTP server would be better than nothing
[12:14:38 pm] <@sam_> i'm very close to just trying out kup myself and seeing what happens
[12:14:47 pm] <@dilfridge> anything would be better than current state
[12:15:04 pm] <@dilfridge> public git-lfs instance
[12:15:29 pm] <@mattst88> alright, so we're okay kicking this back to the gentoo-project@ thread for more discussion?
[12:15:38 pm] <@dilfridge> no we kill it outright
[12:15:39 pm] <@sam_> i hate that i actually like the ftp idea
[12:15:50 pm] <@sam_> it shows the situation we're in is poor
[12:15:57 pm] <@Marecki> I feel tempted to suggest xrootd, it's a thing from HEP which aside from its native protocol supports WebDAV. With authentication, TLS, the whole works.
[12:16:11 pm] <@dilfridge> subversion
[12:16:22 pm] <@dilfridge> does webdav too
[12:17:28 pm] -*- mgorny checks if he accidentally ended up on overcomplex idea contest
[12:18:54 pm] <@Marecki> mgorny: My main point here is that one really doesn't have to fall back to FTP nowadays. A WebDAV server isn't THAT hard to deploy any more.
[12:19:19 pm] <@mgorny> i wasn't serious about using FTP protocol ;-)
[12:19:20 pm] <@mattst88> Marecki: I think he was just saying that even a plain old FTP would be better than what we have now
[12:19:25 pm] <@mgorny> just a shared dir available over SSH
[12:19:58 pm] <@dilfridge> or a solution for ~/public_html that is backed up 
[12:20:05 pm] <@dilfridge> and archived
[12:20:13 pm] <@mgorny> dilfridge: we need to move it outta woodpecker
[12:20:14 pm] <@gyakovlev> and without username in the URI
[12:20:34 pm] <@mgorny> i also need some file hosting for bin kernels
[12:20:38 pm] <@mgorny> but github won't work there
[12:20:54 pm] <@dilfridge> a kernel bin, so to say :P
[12:21:22 pm] <@mgorny> i'm pretty sure they wouldn't be happy about a weekly dose of ~1 GiB
[12:21:32 pm] <@sam_> right
[12:21:35 pm] <@mattst88> (do we have more topics?)
[12:21:49 pm] <@sam_> i think we probably need to ask infra what the blocker is on that bug, and if we can get a something > nothing solution, even if it's a PoC to get feedback
[12:21:55 pm] <@gyakovlev> mattst88: just generic ones, like bugs and open floor
[12:21:55 pm] <@sam_> and i think we also need to ask vapier for more information
[12:22:29 pm] <@Marecki> What, again? And hope we'll actually get it this time?
[12:22:36 pm] <@dilfridge> that is pointless
[12:22:47 pm] <@gyakovlev> so kicking it back to ML? does not look actionable right now, but has some valid points without solutions.
[12:22:47 pm] <@sam_> i'm fine with moving on too given we've done it before
[12:23:08 pm] <@sam_> i think the projects.gentoo.org/distfiles hosting is a real thing we should figure out, but that's not really related to approving doing more on github
[12:23:20 pm] <@sam_> so yes i'm fine with just saying no given this is the second time we've tried 
[12:23:26 pm] *** Mode #gentoo-council +v xgqt by ChanServ
[12:23:46 pm] <@mgorny> did vapier finally say which projects he wants to move to github?
[12:23:53 pm] <@sam_> honestly, it borderline feels like a discussion about nothing
[12:23:56 pm] <@sam_> there's insufficient substance 
[12:24:07 pm] <@mgorny> i don't really mind people hosting one-man projects on github (like i do)
[12:24:21 pm] <@mgorny> but if we have a major project that's coop between many devs, it should stay within gentoo infra
[12:24:34 pm] <@dilfridge> the point that vapier tries to make is very unspecific, my guess would be he objects that github is locked down so much
[12:24:37 pm] <@mgorny> with bugs on bugzilla and not github issues
[12:24:47 pm] <@dilfridge> but there's a reason for that, and it's very valid
[12:24:54 pm] <@mattst88> yeah, agreed
[12:25:12 pm] <@mgorny> next thing you know, people can't join project X because they refuse to use github
[12:25:24 pm] <@dilfridge> mgorny: yes
[12:27:56 pm] <@gyakovlev> ok, motion ( though I still feel it's not actionable in current state.
[12:27:56 pm] <@gyakovlev> Utilizing GH functionality that Gentoo infra does not provide
[12:27:56 pm] <@gyakovlev> 1) yes
[12:27:56 pm] <@gyakovlev> 2) no
[12:28:23 pm] <@mgorny> i think that's too generic
[12:28:36 pm] <@gyakovlev> that's the problem with the whole thing
[12:28:43 pm] <@gyakovlev> it's not actionable as is
[12:28:49 pm] <@mgorny> if we're to allow anything, we need to make it clear what things can be used and under what circumstances
[12:28:50 pm] <@ulm> it's not really actionable at this point
[12:28:52 pm] <@gyakovlev> and I don't think we can make it one.
[12:29:01 pm] <@gyakovlev> with current info
[12:29:25 pm] <@sam_> all I'd like is to not have to talk about this again in 3 months if no more information is given, like last time
[12:29:36 pm] <@mattst88> ++
[12:29:39 pm] <@sam_> i don't think we're any clearer than when we asked for more info last time
[12:30:33 pm] <@gyakovlev> ok kicking it back for more details and clear actionable definition.
[12:30:33 pm] <@gyakovlev> if it's 3 month away - another council will have to deal with it ahahah.
[12:31:14 pm] <@mgorny> yeah, exact details what is required and with example projects that will use it and how
[12:31:30 pm] <@gyakovlev> eveyone ok with moving on? say if no, otherwise I'll me moving to next agenda item in a min.
[12:31:41 pm] <@sam_> yes
[12:31:56 pm] <@ulm> move on please
[12:32:03 pm] <@gyakovlev> 3) Open bugs with Council participation
[12:32:03 pm] <@gyakovlev> https://bugs.gentoo.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=IN_PROGRESS&email2=council%40gentoo.org&emailassigned_to2=1&emailcc2=1&emailreporter2=1&emailtype2=substring&list_id=6102359&query_format=advanced
[12:32:06 pm] <@gyakovlev> bug 786105
[12:32:07 pm] <willikins> gyakovlev: https://bugs.gentoo.org/786105 "access to manage projects on GH"; Gentoo Infrastructure, GitHub; CONF; vapier:github
[12:32:10 pm] <@sam_> hahahaha
[12:32:11 pm] <@gyakovlev> this is what we just discussed
[12:32:12 pm] <@sam_> no!!
[12:32:20 pm] <@mattst88> lol
[12:32:22 pm] <@gyakovlev> next one =)
[12:32:23 pm] <@gyakovlev> bug 835152
[12:32:24 pm] <willikins> gyakovlev: https://bugs.gentoo.org/835152 "Mirror pkgcore/* repos from github"; Gentoo Infrastructure, Git; CONF; sam:infra-bugs
[12:32:47 pm] <@gyakovlev> sam_: any updates on it? seems related =)
[12:32:53 pm] <@mattst88> I think we're working on that in the context of gitlab.gentoo.org
[12:32:55 pm] <@mgorny> i think we've pretty much agreed we can move the primary repos to gentoo infra
[12:33:03 pm] <@sam_> arthurzam: ^^
[12:33:08 pm] <@mgorny> but tbh i don't know what to do about existing bugs
[12:33:12 pm] <@mgorny> on github issues
[12:33:31 pm] <@mattst88> gitlab can import all of that from a github repo
[12:33:37 pm] <@sam_> oh nice
[12:33:54 pm] <@mgorny> hmm, though i think we're currently relying on repo access in github actions to make releases
[12:34:42 pm] <@gyakovlev> please just update bug and we can move on , looks like things are happening
[12:34:43 pm] <@mattst88> dunno. I don't know what council has to contribute in that regard though
[12:34:52 pm] <@sam_> ack
[12:34:54 pm] <@gyakovlev> just monitoring?
[12:34:57 pm] <@gyakovlev> anyway,
[12:34:58 pm] <@mattst88> yes
[12:34:59 pm] <@gyakovlev> bug 834997
[12:35:00 pm] <willikins> gyakovlev: https://bugs.gentoo.org/834997 "Missing summaries for 20211114 and 20211212 council meetings"; Gentoo Council, unspecified; CONF; ulm:dilfridge
[12:35:08 pm] <@sam_> it's important for us to monitor but not actionable 
[12:35:26 pm] <@gyakovlev> dilfridge: looks like your meetings? sorry if I'm worng, memory is hazy on meeting hosts
[12:35:31 pm] <@dilfridge> sorry
[12:35:32 pm] <@dilfridge> wip
[12:35:51 pm] <@gyakovlev> that's it for council bugs.
[12:36:01 pm] <@gyakovlev> 4) Open floor
[12:36:40 pm] -*- gyakovlev will wait 4 minutes until 19:40 UTC
[12:36:54 pm] <@sam_> only thing from me is I think we should ask infra to look into the projects hosting via kup, even if it's a PoC
[12:36:55 pm] <@sam_> [20:21:48]  <@sam_> i think we probably need to ask infra what the blocker is on that bug, and if we can get a something > nothing solution, even if it's a PoC to get feedback
[12:37:04 pm] *** Mode #gentoo-council +v antarus by ChanServ
[12:37:21 pm] <@mgorny> let's put in the summary that sam_ will look into distfile hosting problem
[12:37:26 pm] <@sam_> sure
[12:37:30 pm] <@sam_> (already looking into kup bits)
[12:37:41 pm] <@gyakovlev> I think what you've mentioned before is a good action plan?
[12:37:41 pm] <@gyakovlev> investigate first and take it to infra if it looks promising.
[12:37:47 pm] <@sam_> sounds good
[12:37:52 pm] <@dilfridge> looks more complicated than ftp
[12:37:52 pm] <@gyakovlev> nice
[12:38:02 pm] <@mgorny> mgorny will continue his uneven fight with python
[12:38:07 pm] <@sam_> dilfridge: i have some gitolite experience which helps a bit
[12:38:09 pm] <@sam_> we'll see
[12:38:10 pm] <@mgorny> in the world that has moved on
[12:38:38 pm] <@dilfridge> perl 5.36 will be out soon
[12:38:43 pm] <@gyakovlev> ftp seems completely dead tbh
[12:38:56 pm] <@gyakovlev> even browsers dropped support for it =)
[12:39:04 pm] <@sam_> ftp would actually work with https serving the directories
[12:39:07 pm] <@dilfridge> hey, at least then it's secure
[12:39:07 pm] <@sam_> but eh
[12:39:08 pm] <@ulm> what's wrong with it for hosting distfiles?
[12:39:22 pm] <@ulm> that are validated after download
[12:39:24 pm] <@mgorny> well, i'd be perfectly happy with being able to scp into some https server's shared directory
[12:39:48 pm] <@gyakovlev> I think kup does that + verification + signatures
[12:39:53 pm] <@gyakovlev> sam_: ^ ?
[12:39:56 pm] <@sam_> yes
[12:40:30 pm] <@gyakovlev> ok any other items?
[12:40:33 pm] <@ulm> we could use gopher :)
[12:40:52 pm] <@mgorny> what about cleanup of old distfiles?
[12:41:16 pm] <@mgorny> kernels would some way of removing old distfiles
[12:41:18 pm] <@ulm> mgorny: just don't?
[12:41:41 pm] <@mgorny> ulm: well, not sure if infra is willing to sacrifice that much disk space
[12:41:59 pm] <@ulm> most upstream projects keep their distfiles forever
[12:42:23 pm] <@mgorny> one kernel version is ~250M of binpackages
[12:42:35 pm] <@mgorny> right now we build 4 versions per each release set
[12:42:39 pm] <@dilfridge> one libreoffice-bin is 10Gbyte
[12:42:41 pm] <@mgorny> i.e. 1G per release set
[12:42:46 pm] <@ulm> that's huge of course
[12:42:58 pm] <@mgorny> lately it's gotten rarer but it used to be twice a week
[12:43:28 pm] <@gyakovlev> so we are looking at least 50G per year? more like 70?
[12:43:29 pm] <@mgorny> that's roughly ~100G per year
[12:44:08 pm] <@dilfridge> well, I just bought an 18T disk, it's right here on my desk... something like 300€
[12:44:13 pm] <@mgorny> i mean, i suppose we can buy a few TiB every few years
[12:44:28 pm] <@mgorny> but i'm not really convinced that there's much value in keeping binpkgs of vulnerable kernels
[12:44:45 pm] <@mgorny> especially that they can be rebuilt from source if someone really cares
[12:44:50 pm] <@gyakovlev> anyway, this can be discussed later in the scope of distfile hosting.
[12:45:03 pm] -*- gyakovlev bangs the gong.
[12:45:13 pm] <@gyakovlev> meeting colsed, thanks for participation!
