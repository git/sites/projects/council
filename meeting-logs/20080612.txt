20:00 < dberkholz@> ok, it's 2000
20:00 <NeddySeago > antarus but not real time iteration
20:00 < dberkholz@> council members, speak up if you're here
20:00 <Betelgeuse@> \o/
20:01 < dberkholz@> i've seen amne, Betelgeuse, dberkholz, cardoe [flameeyes], lu_zero. waiting on vapier and jokey 
20:01 < dberkholz@> SpanKY: around?
20:02 <     jokey@> evening
20:02 <   edit_21 > \o
20:02 <   lu_zero@> o/
20:02 <     jokey@> spanky just sent notice to be a slacker
20:02 <Betelgeuse@> dberkholz: just got a mail from vapier saying he is slacking because of company emergency
20:03 <   lu_zero@> =/
20:03 <   astinus > if he worked at Google, he could have dispatched the tea boy^W^W^Wantarus to fix that ;)
20:03  *  astinus hides
20:03 < dberkholz@> ok.
20:04 <   astinus > "Houston, we are *go* for liftoff!"
20:04 < dberkholz@> first off, do we want to try the channel modes i suggested?
20:04 <    Cardoe > I liked that idea.
20:04 < dberkholz@> see /topic if you want to read them
20:04 <NeddySeago > only if chatter gets out of hand
20:05 <     jokey@> time to start, isn't it?
20:05 <    Cardoe > the official log can be taken by someone with +o, so that people comments will be included due to the +z
20:05 <      amne@> dberkholz: yes on channel mode
20:05 -!- mode/#gentoo-council [+o lu_zero] by ChanServ
20:06 <NeddySeago > comments that are not public should not be in the log
20:06 <     nadio > I doubt there will be no need for it, kind of loses its point if the users have common sense.
20:06 < dberkholz@> one reason we moderated in the past is because that isn't the case. people kept interjecting random offtopic comments and questions
20:07 <Ford_Prefe > Might be worth giving a shot to see how it works
20:07 <    TFKyle > guessing ops would have to restate anything that they answer from an unvoiced user? or will the +z mostly be to see voice requests?
20:07 <   edit_21 > dberkholz, well people like that need gagging
20:07 <   lu_zero@> grr
20:07 <   lu_zero@> network issues
20:07 < dberkholz@> i don't want to spend much time dealing with this, so let's do as NeddySeagoon suggests and hold it in reserve and try to get through our topics
20:08 <      amne@> ok
20:08 < dberkholz@> next small thing is a 2-hour time limit. we cut off at 2200 and move to the lists with anything we didn't get to. that ok with people?
20:08 <     jokey@> yup, works for me
20:08 <      amne@> ++
20:09 <     jokey@> Betelgeuse?
20:10 <    Cardoe > dberkholz: works for Diego
20:10 -!- mode/#gentoo-council [+o lu_zero] by ChanServ
20:10 <Betelgeuse@> yeah fine
20:10 < dberkholz@> alright, we're ending at 2200.
20:10 <Betelgeuse@> I have the worst time zone any way
20:10 < dberkholz@> first topic that we might have enough info for is >8-digit versions.
20:11 -!- mode/#gentoo-council [+o lu_zero_] by ChanServ
20:11 <  lu_zero_@> sigh
20:12 < dberkholz@> lu_zero_: use screen or something, you're going to be missing info
20:12 <  lu_zero_@> dberkholz hopefully from there I should be fine
20:12 < dberkholz@> a nonzero number of packages do have upstream versioning with yyyymmddhhmm or similar.
20:12 < dberkholz@> as far as i'm aware, nobody with interest in the topic did the testing in versionator.eclass
20:12 <     jokey@> yep we see a number of packages also on the agenda
20:13 <     jokey@> I saw that topic floating around in #-portage.. assumed one of us took it up with people there
20:14 < dberkholz@> we didn't get responses from all of those maintainers. ulm replied that some emacs packages would be able to use it because their upstream versioning was >8
20:14 -!- mode/#gentoo-council [+o lu_zero__] by ChanServ
20:14 -!- lu_zero__ is now known as lu_zero
20:14 <   antarus > I would prefer versionator eclass die
20:15 <   antarus > and instead get some sort of hook that is PM dependent
20:15 <   astinus > antarus: do you have a replacement?
20:15 <     jokey@> antarus: that would be something for eapi-2 then
20:15 <   antarus > jokey: yes
20:15 < dberkholz@> are you prepared to vote one way or the other, or do you have a specific problem/question that needs to be answered first?
20:16 <     jokey@> clear to vote here
20:16 <     ferdy > antarus: *nod* that's better done on the PM side
20:16 <    Cardoe > good to go here
20:16 <   lu_zero@> let me reread
20:16 <      zlin > that would delay eapi 2. I'm all for getting it in a future eapi though.
20:17 <     jokey@> other option would be disallowing it until eapi2 is there but I highly dislike that option
20:17 <   antarus > zlin: I stand corrected, s/2/futureEAPI/ ;)
20:17 < dberkholz@> i'm still waiting for enough council members to answer my question to move on..
20:18 <   lu_zero@> dberkholz ok
20:18 <Betelgeuse@> ok
20:18 < dberkholz@> these other issues with replacing the eclass are probably better discussed over in #-dev
20:18 <     jokey@> right, not subject here now
20:19 <      amne@> uhm i may have trouble reading, what exactly is the question we're voting on now?
20:19 <    Cardoe > amne: to allow >8 versioning or not
20:19 < dberkholz@> amne: whether we're ready to vote or still need more info. are you ready to vote?
20:20 < dberkholz@> the vote will be to allow versions >8 digits.
20:20 <      amne@> as proposed in the glep, not the other stuff (future eapis etc as just been said)?
20:20 <      amne@> in any case, i'm ready to vote
20:20 < dberkholz@> as proposed for pms
20:21 < dberkholz@> ok, vote: should >8-digit versions be allowed? yes or no.
20:21 < dberkholz@> yes
20:21 <   lu_zero@> yes
20:21 <     jokey@> yes
20:21 <    Cardoe > yes
20:21 <Betelgeuse@> heh that's enough
20:21 <      amne@> yes
20:21 <Betelgeuse@> +1
20:22 < dberkholz@> good. the next topic is appeals.
20:22 <     ferdy > [ remember, portage-utils and versionator.eclass need to be fixed with respect to that decision ]
20:22 < dberkholz@> do you like my proposal for handling them, or do you want to propose another one on the list?
20:22 < dberkholz@> as a reminder, it's on the -council list
20:23 <     jokey@> dberkholz: sounds reasonable to me
20:23 <   lu_zero@> I'm fine with it
20:23 <    Cardoe > dberkholz: you proposal sounded the most reasonable
20:24 <      amne@> as already posted on the list, ++
20:24 <     jokey@> I doubt rush there would help now
20:24 <     jokey@> hence all for it
20:24 < dberkholz@> ok. the appeals will proceed as described in http://archives.gentoo.org/gentoo-council/msg_d7c402fb577a3d5b1707e2bdf4b0a264.xml
20:24 <      zlin > did this mean max 18, 19 or unlimited number of digits?
20:24 <Betelgeuse@> dberkholz: sounds good
20:25 < dberkholz@> there is currently no limitation. if people want one, that should be a separate request
20:25 < dberkholz@> the next topic is turning on --as-needed by default
20:26 <jmbsvicett > dberkholz: I understand you not wanting to make a decision on this, but you have the authority to do it and I had hoped you would do it
20:26 < dberkholz@> jmbsvicetto: let's talk in #-dev
20:26 <jmbsvicett > dberkholz: ok
20:27 <     jokey@> dberkholz: if I read it correctly, it would be suggesting that as a default in make.conf.example, right?
20:27 < dberkholz@> antarus: describe. =)
20:28 <    Cardoe > a short summary would be good here since the agenda only said read the thread.
20:29 <    Cardoe > the thread also bounced between profiles and make.conf{,.example}
20:29 <   antarus > I believe the intent was to use profiles
20:30 <   antarus > to set LDFLAGS
20:30 <     jokey@> as genone said it, we only suggest stuff in make.conf.examples as people override it either way
20:30 < dberkholz@> my interpretation was setting it as a default just like our default CFLAGS
20:30  *  antarus is open to either way
20:31 <Betelgeuse@> Does tinderbox for example have as-needed?
20:31 <Betelgeuse@> Would rather see the whole tree built with it before putting it on by default
20:31 < dberkholz@> `grep CFLAGS /usr/portage/profiles/arch -r` to get a feel for what the default CFLAGS look like
20:31 <    Cardoe > right. This is the question. Since if it's in profiles it'll be turned on for a lot more people then how we currently suggest CFLAGS to users
20:32 <   antarus > Cardoe: As stated, I'll go either way
20:32 <   antarus > I'm more concerned with the challenges regarding --as-needed itself
20:32 <   antarus > not how it is deployed
20:32 < dberkholz@> Betelgeuse: that sounds pretty reasonable.
20:33 <  ColdWind > Betelgeuse: if you want to know the current status of the tree, it's that it doesn't work fully with as-needed right now
20:33 < dberkholz@> not necessarily fixing every package, but having a bug for every package that needs to not build with it
20:33 <Betelgeuse@> ColdWind: most likely
20:33 < dberkholz@> i think flameeyes posted that you could append -Wl,--no-as-needed or so
20:33 <   lu_zero@> there is an open bug about it iirc
20:34 < dberkholz@> to do that means someone would need to build every stable and ~arch package in the tree
20:34 <Betelgeuse@> ~arch doesn't matter
20:34 <   antarus > http://bugs.gentoo.org/show_bug.cgi?id=129413
20:34 <   antarus > is the tracker, for your reference
20:34 <    Cardoe > I think getting the data Betelgeuse suggested would be something very useful. That being said, I know a good portion of the tree does build with --as-needed
20:34 < dberkholz@> what i'm seeing here is that this is still being discussed and we're not really sure what should happen
20:35 < dberkholz@> so i suggest we refer this to the lists for hashing out the details
20:35 <Betelgeuse@> Cardoe: Yeah I run with it too.
20:35 <    Cardoe > I build my corporate tinderbox with it.
20:36 <   antarus > dberkholz: I believe the only argument is: 'is the savings for rebuilding packages worth the risk of some packages being broken' ?
20:36 <   antarus > dberkholz: do you feel we are not doing enough to mitigate breakage?
20:36 <     jokey@> *subtle broken at worst
20:36 <   antarus > or what questions do you have that prevent you from voting?
20:37 < dberkholz@> there's the question of what the prerequisites are for doing this
20:37 <  ColdWind > note that enabling it would mean that maintainers and AT should always test --as-needed, otherwise this will do a lot of harm
20:37 <   antarus > so you want some kind of deployment plan?
20:37 < dberkholz@> pretty much, yes
20:37 <   antarus > ColdWind: a valid point
20:37 <   antarus > dberkholz: ok
20:37 <     ferdy > jokey: subtly broken is worse than outright fucked up :)
20:38 <     jokey@> ferdy: depends but yeah ;)
20:38 <jmbsvicett > fwiw, I have 947 packages on this laptopt built with --as-needed
20:38 < dberkholz@> ok, let's just get a feel for this
20:38 <   antarus > jmbsvicetto: you are a poor sample set, sorry ;)
20:38 <    Cardoe > antarus: can you quickly sum up a deployment plan?
20:38 <   antarus > Cardoe: no
20:38 <   antarus > Cardoe: I will come back next month with some concrete stuff
20:38 <   antarus > move on
20:38 < dberkholz@> let's not wait till next month
20:38 < dberkholz@> let's make it happen on the lists
20:38 <     jokey@> +1
20:39 < dberkholz@> there's no reason we can't vote on this within the next week on-list
20:39 <   antarus > dberkholz: i will put a plan together on-list then
20:39 <     jokey@> signed mails on -council ML should be enough
20:39 <    Cardoe > this needs very little work to make it happen. Much less then a month.
20:39 <   antarus > better? :)
20:39 <   lu_zero@> yup
20:39 < dberkholz@> sounds good to me. just post it to -dev for discussion
20:40 < dberkholz@> next topic is glep 54
20:41 < dberkholz@> lu posted an alternate plan about 12 hours ago
20:42 <Betelgeuse@> dberkholz: have a link handy?
20:42 < dberkholz@> yep
20:42 <Ford_Prefe > http://dev.gentoo.org/~lu_zero/glep/liveebuild.rst
20:42 < dberkholz@> http://archives.gentoo.org/gentoo-dev/msg_05614741b3942bfdfb21fd8ebb7955e0.xml
20:42 <    Cardoe > Can I just say as an aside, it'd be nice if someone would summarize the final discussion on the dev ML and post that finalized item as a complete deployment plan (as we just requested of antarus) to the council ML to be the official agenda item instead of "read this thread."
20:43 <   antarus > Cardoe: I think the idea of 'throwing something to the council' should perhaps be slightly defined
20:43 <   antarus > Cardoe: and if the guy running the meeting gets teh feeling that somethnig isn't well specified (as my proposal was not) it would just not get put on the agenda
20:44 <    Cardoe > precisely
20:44 < dberkholz@> since we now have two competing plans, one of which hasn't been around long enough to get much feedback, it doesn't seem like the best time to approve one
20:44 <     jokey@> right, I at least wouldn't vote for one
20:45 < dberkholz@> i suggest that we let them develop and see if they somehow merge
20:45 <      amne@> agreed
20:45 <    Cardoe > I'd say this is a July agenda item.
20:45 <    TFKyle > lu_zero: sorry if it's slightly OT, but how would the revision be communicated to the PM from the eclass? or do they do that currently?
20:45 < dberkholz@> could you guys talk about that in #-dev?
20:45 <     ferdy > careful, it is very easy to DoS the council this way.....
20:45 <   lu_zero@> TFKyle better move on -dev
20:45 <    TFKyle > ah, sorry
20:45 <   antarus > Cardoe: I think they can vote earlier
20:45 <     ferdy > you can propose someting 'alternative' a couple of hours before something is going to be discussed
20:46 <    Cardoe > ferdy: let's remain constructive. Thank you.
20:46 <Betelgeuse@> ferdy: Well we don't have a rule saying we need to postpone it with alternatives
20:46 <     ferdy > Cardoe: this IS constructive
20:46 <      amne@> let's move on please
20:46 < dberkholz@> again, i want to emphasize that once they finalize somewhat, the council can vote without waiting around for a meeting
20:46 <     ferdy > Betelgeuse: which is why I said 'careful'
20:47 <    Cardoe > I think the council members agree that lu's proposal has some merit and should be discussed further as a potential path to take
20:47 <    Cardoe > If they looked at it and were able to quickly dismiss it as not being viable, we would have had a vote now.
20:47 <    Cardoe > moving on
20:47 < dberkholz@> the next two topics are gleps 55 and 56. i suggested that both of them still need further discussion on -dev.
20:48 <   lu_zero@> agreed
20:48 < dberkholz@> in my opinion, the council's next action should be all of us posting our feedback to both gleps
20:48 <    Cardoe > What are the outstanding discussions on glep 56?
20:49 <     jokey@> 56 doesn't have some I think, given there hasn't been feedback for some days now
20:49 <Betelgeuse@> glep 56 makes PMS look like something valid
20:50 < dberkholz@> i outlined my reasons in http://archives.gentoo.org/gentoo-dev/msg_54ee20d2b1d8122370afdd4b3d7aafc9.xml and they basically agree with what genone was saying
20:50 <  ColdWind > lu_zero: you may want to extend your proposal, with the 'Backwards compatibility' section if we are going to discuss it in the list
20:50 <   lu_zero@> ColdWind sure
20:50 <    Cardoe > dberkholz: that doesn't disregard the GLEP from being voted on
20:50 <    Cardoe > dberkholz: the council can say approve this GLEP to deprecate X
20:51 <    Cardoe > I left that up to the council to decide
20:51 <    Cardoe > as my e-mail to genone stated
20:52 <    Cardoe > obviously I abstain from voting but if everyone would like to kick it back to the list I'll create a new summary.
20:52 <    Cardoe > I agree Betelgeuse did raise a valid point.
20:54 < dberkholz@> i didn't quite follow the reasoning
20:54 < dberkholz@> maybe i need more coffee
20:54 <   lu_zero@> Cardoe could you please summarize?
20:55 <NeddySeago > 5 mins left
20:55 < dberkholz@> till 1 hour =)
20:55 <    Cardoe > lu_zero: surely
20:55 <NeddySeago > Oops - sorry
20:55 <    Cardoe > GLEP 56 aims to fill a gap that currently exists in Gentoo's USE flag documentation
20:56 <    Cardoe > Essentially use.local.desc can not be used to override our description of a use.desc flag as per our QA Project's policy
20:56 <    Cardoe > All those overlaps were removed from the tree a few months back
20:56 <    Cardoe > This has resulted in a loss of information for users (that includes devs) about how certain USE flags affect their system.
20:57 <    Cardoe > The goal is to allow these global and even non-global USE flags to be documented with clarity. Additionally, it also allows the lang attribute so that in the future we may internationalize our tree better to support non-English speakers with this information.
20:58 <    Cardoe > The information is wrapped in XML using a simple tag structure that tools can take an advantage of. Tools like packages.gentoo.org and other command line utilities and provides additional measures to cross-reference to other packages when describing a USE flag.
20:58 <  dev-zero > Cardoe: sorry to interrupt, did you get my mail wrt changes for GLEP 56
20:58 <     jokey@> as said, feedback was quite silent, though nothing negative and I personally don't see anything questionable either
20:58 <  dev-zero > ?
20:59 <    Cardoe > dev-zero: yes.
20:59 <    Cardoe > For every one else, the feedback that dev-zero provided was to remove the following: "* The default ``'lang'`` attribute value is "C", which is equivilent to "en"."
21:00 <    Cardoe > Which is currently defined in the devmanual's section on metadata.xml
21:00 < dberkholz@> Cardoe: is there a way to say which flags are local vs global?
21:00 <    Cardoe > So putting that into the GLEP is redundant, so dev-zero would like it removed.
21:00 <  dev-zero > Cardoe: then you missed a bigger part of it
21:00 <    Cardoe > dberkholz: Not in the current version of the GLEP
21:01 <    Cardoe > dberkholz: it merely defines what USE flags affect this package
21:01 < dberkholz@> if you wanted to be able to build up a use.local.desc from this, would you need such a thing?
21:01 <   lu_zero@> dev-zero could you point us a link or a quick summary?
21:01 <    Cardoe > dberkholz: I discussed it with infra. Effectively the process would be to build up a use.local.desc and subtract use.desc from it.
21:01 < dberkholz@> right, that's the other way i was thinking.
21:01 <  dev-zero > lu_zero: http://rafb.net/p/FI6bdj29.html
21:02 < dberkholz@> there may be a danger in allowing local descriptions of globals, which diverge over time until they no longer mean the same thing
21:03 < dberkholz@> but they again, the meaning within a package generally doesn't change much
21:03 < dberkholz@> then*
21:03 <Ford_Prefe > dberkholz: besides, that would be a QA violation and as such should be discouraged/caught
21:03 <     jokey@> should we send it back to ml for discussion?
21:03 <     jokey@> as we agreed to vote on ML, it should be safe for this too
21:04 <     ferdy > Cardoe: sorry for not bringing this earlier (I probably missed the thread). The fact that a USE flag *might* change meaning in the lifetime of a package has been discussed and covered?
21:04 < dberkholz@> The 'restrict' atttribute can limit to specific versions of the package
21:05 <Ford_Prefe > ferdy: I'd assume the USE flag itself would change then, as it would be expecteed to today
21:05 <    Cardoe > what dberkholz said
21:05 <     ferdy > thanks. Sorry for the noise.
21:05 < dberkholz@> good question. i had to check the glep for it
21:05 < dberkholz@> there's 3 t's in attribute, btw.
21:06 <    Cardoe > dberkholz: thank you
21:06 < dberkholz@> i think there is still room for improvement in this glep, not so much in its technical aspects but in the way it promotes itself, the possible generation of legacy files, and the tools to use it. 
21:08 <    Cardoe > Well I abstain from voting on it due to conflict of interest, but as I said before. I'm game for kicking it to the ML and I'll address all the concerns and provide a follow up e-mail to the list.
21:08 < dberkholz@> we already agreed to allow the flags without a glep a few months back <http://www.gentoo.org/proj/en/council/meeting-logs/20071213-summary.txt>, so that's not the purpose of this one ...
21:09 < dberkholz@> if the point isn't to allow them, the point should be something else beyond saying they're possible
21:10 <    Cardoe > I just wanted to write a GLEP to address some council member's concerns about a lack of a GLEP.
21:10 < dberkholz@> i can certainly see the value of a specification
21:10 < dberkholz@> does anyone want to vote on glep 56 now?
21:11 <   lu_zero@> I'd look forward improvements
21:13 < dberkholz@> could i actually get a yes or no response to that question?
21:13 <     jokey@> no
21:13 <   lu_zero@> no
21:13 <      amne@> no
21:13 < dberkholz@> great, thanks.
21:13 <     jokey@> ML vote++
21:14 < dberkholz@> i'm going to talk with cardoe more about my concerns. i encourage you all to do the same, on-list or off
21:14 <   lu_zero@> (sorry but I keep reloggin on d.g.o every 40s average...)
21:14 <     jokey@> screen ftw ;)
21:14 < dberkholz@> the last topic is PMS status
21:15 < dberkholz@> i really think you all need to post your opinions to that mailing-list thread i mentioned in the agenda
21:15 <     jokey@> I raised my concerns today on the ML already
21:16 <   lu_zero@> I voiced them already as well
21:16 <      amne@> i don't think i sent an email, but jokey's really summed up my concerns
21:17 < dberkholz@> what i'd like to do is wrap up this meeting now and since we're under the 2-hour cutoff, see all of us with email access spend the remaining time posting to the list about the topics we sent there
21:17 <     jokey@> sounds good to me
21:17 < dberkholz@> in particular glep 54, glep 56, pms
21:18 < dberkholz@> those should all be resolvable quickly. glep 55 as well, if you have any ideas
21:18 <     jokey@> then we don't have a topic left, do we?
21:19 < dberkholz@> nope, that's it if everyone agrees with my suggestion
21:19 <     jokey@> k' then
21:19 < dberkholz@> amne, Betelgeuse, lu_zero, Cardoe ?
21:19 <      amne@> wfm
21:19 <     jokey@> note to dberkholz: nice meeting style idea, feels good
21:19 <    Cardoe > dberkholz: I agree with your suggestion
21:19 <Betelgeuse@> dberkholz: I might write something but as it gets late.
21:20 <   lu_zero@> dberkholz just back, let me sync back
21:20 < dberkholz@> Betelgeuse: right, understood. it would be good if everyone could post within the next day, 2 tops
21:20 <Betelgeuse@> For the next council I will try to get the meetings earlier.
21:20 <    Cardoe > I've summarized my feelings on the ML. Which really was an extension of jokey's.
21:20 <Betelgeuse@> Winter is fine but DST is a bit late.
21:22 <jmbsvicett > dberkholz: As you've covered all the topics for the meeting, you might want to make some comments about CoC and the tone of mails to the -dev ml
21:23 < dberkholz@> the meeting's officially over. thanks for playing, everyone, and i'll see your posts on the lists in the next day or two. anyone who would like to talk about conduct on the lists can stick around for a little bit =)

--- Meeting ends. Brief discussion of recent on-list behavior and CoC follows. ---

21:24 < dberkholz@> jmbsvicetto: what in particular did you have in mind?
21:25 <jmbsvicett > dberkholz: I saw your comment in the morning, but I didn't had the time to reply to the list then
21:25 <jmbsvicett > dberkholz: First, do you still think there's any interest, purporse or need for CoC?
21:25 < dberkholz@> to catch everyone up here...
21:25 < dberkholz@> 23:00 <jmbsvicett > No. But it wouldn't hurt if people were able to express  their views or points in 2 mails instead of 20
21:25 < dberkholz@> Day changed to 12 Jun 2008
21:25 < dberkholz@> 06:22 < dberkholz@> jmbsvicetto: since you remarked upon the problem, perhaps  you'd like to post to the list. =)
21:25 < dberkholz@> 06:22 < dberkholz@> the whole idea is getting people outside the council to  participate ... peer pressure to maintain a good working  environment
21:25 <jmbsvicett > Second, do you think any of the mails to the -dev ml in the last 2 days violates the CoC?
21:26 <jmbsvicett > dberkholz: You missed the first comment
21:26 <      rane > they probably didn't even read them
21:26 < dberkholz@> you basically just repeated it, didn't you?
21:26 <      rane > i know cause i didn't myself
21:26 <jmbsvicett > I think so
21:26 < dberkholz@> personally, i wasn't terribly impressed with some of the recent back-and-forth threads in the past few days
21:27 < dberkholz@> on either end, not just one person or the other
21:27 <jmbsvicett > dberkholz: I think there were at least 2 messages that should not be tolerated
21:27  * NeddySea would like to thank the outgoing council for their efforts
21:27 <jmbsvicett > The one with the link to the "idiot" definition on wikipedia
21:28  * Ford_Pre echoes NeddySeagoon 
21:28 <jmbsvicett > I also want to thank the council members for their work
21:28 < dberkholz@> jmbsvicetto: bheekling did an outstanding job of stepping in on that thread and one or two others. he's setting a great role model for what the rest of us should do
21:30 <jmbsvicett > let me read the mails again.
21:30 <Ford_Prefe > I guess peer-directed intolerance for bad behaviour is really the ideal solution for this
21:30 <jmbsvicett > skim*
21:33 <jmbsvicett > dberkholz: hmm, I can't find any mesage from bheekling on the -dev ml. Different name on the from address?
21:33 <Ford_Prefe > http://archives.gentoo.org/gentoo-dev/msg_4211dc4054de30f2ff52f6f8a2e2466e.xml might be it
21:33 <      rane > Nirbheek
21:34 < dberkholz@> yeah, that's him.
21:34 <Ford_Prefe > (name is right, this might be the thread dberkholz is referring to)
21:34 <      rane > or sth like that
21:34 <      rane > no idea if it's his real name
21:34 <jmbsvicett > thanks
21:34 < dberkholz@> the specific post i had in mind was a wikipedia reference to flames and personal attacks
21:35 <      rane > yeah, this new idea of people telling others they are behaving like jerks
21:35 <      rane > it looks like it worked
21:35 <Ford_Prefe > Indeed
21:40 <      rane > silent majority stepping in and kicking ass
21:40 <      rane > a great idea indeed
21:47 <Ford_Prefe > Maybe we can have a won't-tolerate-bad-behaviour pledge. :P
