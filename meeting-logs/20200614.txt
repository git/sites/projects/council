[21:00:13] <Whissi> !proj council
[21:00:13] <willikins> Whissi: (council@gentoo.org) dilfridge, gyakovlev, patrick, slyfox, ulm, whissi, williamh
[21:00:26] <Whissi> Welcome to the last Gentoo council meeting in the current legislative period, agenda: https://archives.gentoo.org/gentoo-project/message/dce336a6fa4d4264f0e6b602300242a5
[21:00:36] <dilfridge> jawohl!
[21:00:46] <Whissi> Let's start with topic #1, roll call!
[21:00:50] * slyfox here
[21:00:52] * gyakovlev here
[21:00:52] * xiaomiao present
[21:00:52] * Whissi here
[21:00:53] <dilfridge> looks like another exciting meeting
[21:00:54] * WilliamH here
[21:00:55] * dilfridge here
[21:01:34] <slyfox> ulm: ^
[21:03:31] * Whissi has sent a SMS
[21:03:36] <Whissi> Let's wait a few more minutes
[21:06:31] <Whissi> OK, let's move on: Next topic will be "2. Open bugs with council involvement"
[21:06:40] <Whissi> bug 662982
[21:06:42] <willikins> Whissi: https://bugs.gentoo.org/662982 "[TRACKER] New default locations for the Gentoo repository, distfiles, and binary packages"; Gentoo Linux, Current packages; CONF; zmedico:dev-portage
[21:06:54] <Whissi> Sadly, no progress. Looks like I need to bring Zac and Robin together.
[21:07:07] <slyfox> sounds like a plan
[21:07:47] <Whissi> OK, so I'll try to do that later...
[21:08:23] <Whissi> Last topic for today, "3. Open floor"
[21:08:39] * dilfridge watches the floor open
[21:09:27] <dilfridge> so general remark, it's still nomination time, and whoever is nominated and wants to run has to accept the nomination
[21:09:49] <xiaomiao> it's been a rather uneventful council term, which I think is a very good thing
[21:09:53] <slyfox> looks like we have 7 already accepted nominations :)
[21:09:58] <dilfridge> this probably wasnt the most exciting council year
[21:10:00] <slyfox> https://wiki.gentoo.org/wiki/Project:Elections/Council/202006
[21:10:02] <dilfridge> yep
[21:10:36] <gyakovlev> we should add "the floor is lava" after "open floor" agenda item to get next council term more exciting
[21:10:43] <dilfridge> hrhr
[21:10:47] <Whissi> ;)
[21:10:51] <WilliamH> gyakovlev: lol
[21:12:06] <Whissi> I hereby declare the meeting concluded!
