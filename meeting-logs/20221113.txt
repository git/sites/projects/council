11:00 <@mattst88> okay, meeting time
11:01 <@mattst88> 1. Roll call
11:01              * | ulm here
11:01              * | sam_ here
11:01 <@mattst88> dilfridge, gyakovlev, mgorny, ajak
11:01              * | mattst88 here obviously
11:01              * | ajak here
11:01              * | dilfridge here
11:01              * | gyakovlev here
11:02              * | mgorny here
11:02 <@mattst88> great, everyone's here
11:02 <@mattst88> 2. GLEP78 update (https://archives.gentoo.org/gentoo-dev/message/fa090f351778f1644964d096c20933dd)
11:02 <@mattst88> mgorny: want to explain what we're doing here?
11:02 <@mgorny> the mail says it all
11:03 <@mgorny> - replacing "basename" with more specific explanation
11:03 <@mgorny> - linking formats to the respective standards
11:03 <@mgorny> - deferring compressed file formats to GLEP 74
11:03 <@mgorny> - clarifying the situation around Manifest signing and verifying binpkg authenticity
11:03              * | dilfridge yes
11:03 <@mattst88> okay, so we're just approving the changes here?
11:03 <@mgorny> unless someone has questiosn
11:03 <@mgorny> (sorry, i keep making typos today)
11:03 <@gyakovlev> https://archives.gentoo.org/gentoo-project/message/ef20321c60472ac4c91653236fb8a346
11:03 <@gyakovlev> ^ linking agenda for log purposes
11:03 <@mattst88> thanks :)
11:04 <@sam_> my only question is if portage is currently compliant with all of this or if we need syu to work on any of it
11:04 <@mgorny> i think so
11:05 <@mgorny> though i've never really verified the verification part, though syu is pretty competent there, so i don't think he'd fail
11:05 <@sam_> yeah, I've started using a bunch of the pgp stuff and it seems to work well
11:05 <@mattst88> presumably we have some ack from syu that the glep changes are what he expects?
11:05 <@sam_> good question
11:06 <@mgorny> i don't really recall, this was years ago xP
11:06 <@mgorny> (time is moving really fast for /me)
11:06 <@mattst88> heh, fair. the patches have been up since Sept 21, so I'll assume he's aware :)
11:06 <@mattst88> if he's not, we can revisit :)
11:07              * | dilfridge yes
11:07 <@sam_> yeah, no problem with that
11:07 <@mattst88> okay, motion to approve mgorny's 6 patches to update GLEP78
11:07              * | mattst88 yes
11:07              * | dilfridge yes
11:07              * | sam_ yes
11:07              * | ajak yes
11:07              * | gyakovlev yes
11:07              * | ulm yes
11:07              * | mgorny yes
11:07 <@mattst88> perfect, thanks
11:08 <@ulm> mgorny: this isn't in the repo yet, right?
11:08 <@mgorny> ulm: could you clarify which repo?
11:08 <@mgorny> glep.git?
11:08 <@ulm> glep.git
11:08 <@mgorny> ulm: don't think it is
11:09 <@mattst88> I don't see a glep78 branch, FWIW (but I do see a glep76 branch, which is the next item)
11:09 <@mattst88> 3. GLEP76 update (https://gitweb.gentoo.org/data/glep.git/commit/?h=glep76&id=139198d2e8560f8dfb32c8f4c34a3e49d628b184), discussion at https://archives.gentoo.org/gentoo-project/message/c85b78ca69802522534ee8ab0804f665)
11:11 <@sam_> robbat2: around?
11:11 <@mattst88> I think again we're being asked to approve the changes to GLEP76, which consist of new wording around contributor's names
11:11 <@dilfridge> sigh
11:11 <@ulm> this will somewhat weaken our position, as far as it comes to due diligence. so I'm not a huge fan of it
11:11 <@dilfridge> does a twitter blue tick fulfil requirements of a records search? https://twitter.com/jesus
11:11 <@ajak> but does it actually change the reality that we're accepting people with non-legal names?
11:11 <@mgorny> it's obviously very important, it got far more attention than any of the patches that were sent for review recently
11:11 <        ulm> | mattst88: it's two-step approval I think, first us then trustees
11:12 <@sam_> what mgorny said, and I think it got fleshed out pretty fully on the ML
11:12 <@dilfridge> trustees for any practical purpose dont exist anymore
11:12 <+robbat2> i'm here
11:12 <@dilfridge> or if they do, we have to plan for the moment when they dont
11:12 <@ajak> yeah i'm not sure why trustees would need to review this, given they delegate to us in general anyway
11:12 <@sam_> hi robbat2, we're talking about the glep 76 update (real name stuff)
11:12 <@mattst88> a meta-question: what is the objective in requiring real names for contributors? that is, what problem do we hope to avoid?
11:13 <@mgorny> i suppose it falls to "due diligence"
11:14 <+robbat2> the real name requirement was semi-inherited from the Kernel DCO when that policy was adopted for Gentoo
11:14 <@mgorny> if we do our part in "due diligence" in accepting contributions, our users can assume "due diligence" when they get in trouble because we f* up
11:14 <@mattst88> the kernel was the subject of a many-years-long lawsuit over allegations of it containing code from UNIX
11:14 <@ulm> mgorny: not sure if I understand this
11:15 <@mattst88> what would the analogous situation be for Gentoo?
11:15 <@mattst88> contributors copy a few lines of a PKGBUILD file into an ebuild?
11:15 <+robbat2> records search in the context of the GLEP means to me searching government records, not public records like (twitter.com/jesus)
11:15 <@ajak> what are we getting at here? is there a will to drop the name requirement entirely?
11:15 <@mattst88> ajak: who is that question for?
11:15 <@mgorny> robbat2: then perhaps that should have been worded explicitly because i thought it means "any records"
11:15 <@ajak> robbat2: ie, "attestable in a written statement with a witnessed signature before a notary"
11:16 <@       sam_> | on the one hand I don't really want to reopen the whole point of the GLEP, on the other hand, I can get why mattst88 is asking as it affects the level of standard we want
11:16 <@mgorny> like, "if someone established an identity online, i.e. used the same name for reasonably long time"
11:16 < CatSwarm> https://lwn.net/Articles/200222/
11:16 <       ajak> | mattst88: everyone in general i guess
11:16 < CatSwarm> this was the commit in question ~ [re: DCO]
11:16 <@ulm> robbat2: that's an important clarification, so probably we should add the word "government"
11:17 <@dilfridge> "attestable in a written statement, with a witnessed signature as before a notary"
11:17 <@dilfridge> ^ 1) attestable by whom? 2) what is a notary, definitions vary wildly between countries...   I would drop the half-sentence after "and/or"
11:18 <@ulm> "... can be verified by simple search of government records." full stop
11:18 <@ajak> fine by me
11:18 < CatSwarm> these are major change in tone form the one I was credited with contributing to.
11:18 <@ulm> or rather "... can be verified by a simple search of government records."
11:18 <+robbat2> not all governments have searchable records :-(
11:18 <@dilfridge> that is fine by me too, but changes what was attempted to achieve
11:19 < CatSwarm> *from the original which was discussed, this is a total change in intent and methodology.
11:19 <@dilfridge> (just pointing that out)
11:19 <@mgorny> robbat2: honestly, i'm surprised at all that governments provide searchable records of people
11:19 <@mgorny> also, what would the search prove, in the first place...
11:20 <@mgorny> that at least one person having a particular name exists?
11:20 < CatSwarm> it seems dubious to think that government records are easily accessible when GLEP-76 doesn't require contributors to specify their jurisdiction of residence.
11:20 <@ajak> how about we punt it back to gentoo-project for more review seeing as there are new, previously undiscussed problems brought up here?
11:20 <+robbat2> yes, that a person with a given name exists only (so somebody can't say your name is made up)
11:20 <+robbat2> why didn't anybody raise these concerns in the previous months of discussion?
11:20 <@sam_> yes, I think this should've been brought up before
11:20 <@sam_> yes
11:20 <+robbat2> you had months to raise them
11:21 <@mgorny> robbat2: so, technically speaking, if we all suddenly start committing as, say, Michał Górny? ;-)
11:21 <@sam_> i don't think it's fair to the people who've raised this change to only bring up things like this now, when they've been apparent for months
11:21 <@mgorny> because such a person exists (actually, it's a very popular name in PL)
11:21 < CatSwarm> these new problems were last minute, so please punt it back to -project ML if there's going to be discussion of a major rewrite to the language I proposed to minimize impact to privacy and dignity of queer / trans / nonbinary / refugee & other folx who might be underdocumented.
11:22 <@ulm> CatSwarm: what percentage of contributors would those be?
11:22 <@ajak> it doesn't matter.
11:22 <@ulm> do you have any numbers?
11:22 <@sam_> doesn't matter & it's obviously not something CatSwarm would have stats on anyway
11:22 <@ajak> and there's no sense spending more time on this here
11:22 <@sam_> plus not a matter for right now
11:22 <       ajak> | mattst88: motion plz?
11:22 <@ulm> send it back to the drawing board
11:22 <@ulm> i.e. -project ML
11:22 <@mattst88> ajak: I'd at least like my question to not be totally ignored...
11:22 <@sam_> why didn't you raise any of this on the ML earlier then?
11:23 <@mgorny> ulm: bircoph mentioned there are a lot of people who would have committed to gentoo for decades, if onyl we hadn't created glep76 ;-)
11:23 <       ajak> | mattst88: sorry, which question?
11:23 <+robbat2> does the proposed text make the present situation worse?
11:23 <@sam_> i don't think it does
11:23 <@dilfridge> sam_: because I do not intend to get involved into any public discussions touching identity politics. ever.
11:23 <@ulm> robbat2: it depends what "simple records search" means
11:23 <@sam_> dilfridge: you're in the council and you're asking the questions now
11:23 <+robbat2> can we accept an incremental improvement at least?
11:23 <@sam_> dilfridge: that's the same as asking them on the ML
11:24 <@sam_> the difference is it's not productive to raise it last minute
11:24 <@mattst88> ajak: I asked what the analogous situation to Linux/SCO (that brought about their DCO and real-name requirement) would be for Gentoo; e.g. what problem are we hoping to avoid?
11:24 <@ulm> with the clarification s/simple records search/a simple search of government records/ it would be fine with me
11:25 <@ulm> even with keeping the following phrase
11:25 <@mattst88> I'm fine with the changes as they are, but I suspect we're generating lots of discussion over a problem that doesn't need to be solved
11:25 <        ulm> | mattst88: same problem as linux?
11:25 <@mattst88> ulm: you're worried people will contribute SCO-owned code to gentoo.git?
11:26 <@ulm> AFAIU the american legal systems, anyone could sue us at any time
11:26 <@mgorny> i would still remove the "notary" part, i don't want to suggest people spend their money on a notary
11:26 < CatSwarm> ulm: [reference]
11:26 < CatSwarm> Contributor Name @ line 183
11:26 < CatSwarm> https://github.com/kuzetsa/glep/blame/3aa57dd942a58da2c9b50f973fdba184c72d29b4/glep-0076.rst
11:26 <@mattst88> > contributors copy a few lines of a PKGBUILD file into an ebuild?
11:26 <     mgorny> | mattst88: remember that glep applies to *all* gentoo projects and repos
11:26 < CatSwarm> https://github.com/kuzetsa/glep/blame/0dcde7c19a93fbc13180e1a0708c83557f3de4e7/glep-0076.rst
11:26 <@mgorny> including projects carrying code
11:26 <@mattst88> mgorny: sure
11:26 < CatSwarm> sorry, duplicate paste.
11:26 < CatSwarm> https://github.com/kuzetsa/glep/blob/GLEP-76.SignOff_kuzetsaCatSwarm/glep-0076.rst
11:27 < CatSwarm> that's the version I signed off on, per blame @ line 183 above.
11:27 <@mattst88> ulm: sure. what's your point?
11:27 <@ajak> surely we've established that changes are needed here? i still don't think discussing the changes forever is a productive use of meeting time
11:27 <@ajak> especially when this is exactly what the mailing lists are for
11:28 <@ajak> producing a new patch isn't what we should be doing here
11:28 <+robbat2> yes, changes are needed here, because prospective developers with unusual names that are legal are being turned down
11:28 <@mattst88> robbat2: I assume I have to email my questions to the discussion thread to get a response from you?
11:28 <@ulm> robbat2: are you referring to that south-indian guy?
11:28 <@ulm> with a single letter name?
11:28 < CatSwarm> I'd like to request if changes are being proposed from the revision I signed off on, please send back to -project ML for discussion, because I don't wish to have the wording attributed to me for things I never said or intended.
11:28 <       ajak> | mattst88: yes i think its best to discuss changes to the GLEP on the mailing where they're posed instead of during council meeting
11:28 <@mgorny> robbat2: are they?
11:29 <@ajak> ulm: nobody's referring to anybody in particular, what does it matter?
11:29 <@mgorny> robbat2: if there is such a major problem, why hasn't it been brought up with more specific details?
11:29 <@mattst88> motion to table this and continue discussion on the mailing list?
11:29 <@ulm> ajak: that's the only example I'm aware of
11:29              * | ajak yes
11:29              * | mattst88 yes
11:29 <+robbat2> mgorny: you turned down Catswarn: https://bugs.gentoo.org/606562#c5
11:29 <+robbat2> *CatSwarm
11:29 <+robbat2> despite their ID
11:29 <@mgorny> robbat2: we all know that's not a legal name
11:29 <@ajak> robbat2: mgorny: let's please continue the discussion elsewhere
11:29 <@sam_> i htink it is
11:30 <@sam_> CatSwarm has been quite clear it's a legalname
11:30 <@ulm> mgorny: they have previous commits in the gentoo repo with a different name, so it's public anyway
11:30 <@mgorny> well, it wasn't last time i've interacted with the topic, which was years ago
11:30 <@sam_> that's besides the point too
11:31 <@mgorny> (and i don't think "CatSwarm" part was there at the time)
11:31 <@sam_> anyway, I'm okay with continuing more discussion on the ML, but this time I thought everything was fine, and now we're debating it all at length because nobody raised their concerns there. what stops that happening again?
11:31 <@ulm> sam_: it's an ill-defined problem, so what do you expect?
11:31              * | dilfridge yes to table it
11:31 <@sam_> ulm: the motion has not changed from the thing raised on the ML?
11:31 <@mattst88> sam_: I think a desire to not do /this/ again :)
11:31              * | ulm yes back to ML
11:31 <@sam_> ulm: so any concerns should've been raised then
11:32 < CatSwarm> mgorny: the legality and uncommon spelling of my name is so rare that even without including a surname, there aren't any other folx named kuzetsa. it's extremely easy to verify that I'm who I say. that nitpick is not constructive or relevant, beyond suggeting bias on your part about what names should look like.
11:32 <@ulm> sam_: I had understood the "records search" part differently
11:32 <@ulm> i.e. meaning any records
11:32 <@sam_> ulm: that's a fair point, but others' comments aren't explained by a misunderstanding
11:33 <@sam_> anyway
11:33              * | sam_ yes (back to ML)
11:33              * | mgorny yes
11:33 <@ajak> gyakovlev: ?
11:33 < CatSwarm> thank you. I don't mind taking my time to respond to any concerns in detail, but if a major rewrite is being considered, please don't edit during a council meeting.
11:33              * | gyakovlev yes (back to ml)
11:33 <@gyakovlev> sorry for delay
11:34 <@ulm> CatSwarm: we cannot do such last-minute changes without posting to the ML again
11:34 <@ulm> by glep workflow
11:34 <@mattst88> I think we have yes votes from mattst88, ajak, dilfridge, gyakovlev
11:35 <@mattst88> moving on...
11:35              * | ajak counts yes's for everyone
11:35 <@ulm> CatSwarm: but nothing prevents us from discussing them
11:35 <@mattst88> Open bugs with council participation
11:35 < CatSwarm> [acknowledged] ~ thank you. I prefer there to be open discussion with time for review / reply / transparency before edits are made. it's easier to have more eyes on it with ML.
11:35 <@mattst88> bug 729062
11:35 <  willikins> | mattst88: https://bugs.gentoo.org/729062 "Services and Software which is critical for Gentoo should be developed/run in the Gentoo namespace"; Gentoo Council, unspecified; IN_P; jstein:council
11:35 <@mattst88> just recently reassigned to council@ due to Whissi's retirement
11:35 <@mattst88> not actionable, skipping
11:35 <@sam_> yeah that's fine
11:36              * | ajak nods
11:36 <@mattst88> bug 876921
11:36 <  willikins> | mattst88: https://bugs.gentoo.org/876921 "Vote on GLEP 68 update to 1.3"; Gentoo Council, unspecified; CONF; mgorny:council
11:36 <@dilfridge> we could cc ago
11:36 <@sam_> i'm sorry, i missed that bug
11:36 <@mattst88> I see we have 5 yes votes in this bug^
11:36 <@sam_> i'll vote on that later unless you need me to now
11:36 <@ajak> dilfridge: table that until open floor i think?
11:36 <@mattst88> dilfridge: want to vote on bugzilla^?
11:37 <@mgorny> if anyone has any questions wrt that update, feel free to ask
11:37 <@dilfridge> done
11:37 <@ajak> sam_ too
11:37 <@ulm> can we close that vote now please, so it'll be in the summary
11:37 <@sam_> ok
11:37 <@mattst88> only other bug is bug 878827 -- which we've addressed already
11:37 <  willikins> | mattst88: https://bugs.gentoo.org/878827 "GLEP 78 "Clarifications" update"; Gentoo Council, unspecified; CONF; mgorny:council
11:38 <@mattst88> so I think we're onto Open Floor
11:38 <@ulm> #876921 approved unanimously AFAICS
11:38 <+arthurzam> I'm not sure if this is an existing policy, but can I request Council to mention in every next meeting logs if there was an open vote in a bug that was decided on?
11:39 <@dilfridge> technically we have that policy already, but I'm not sure people remember
11:39 <@ulm> arthurzam: don't we normally do that?
11:39 <@mgorny> arthurzam: i.e. votes since last meeting?
11:39 <+arthurzam> I don't have any evidence it was missed, but please just remember it for future
11:40 <@mattst88> arthurzam: that's a good idea
11:40 <+arthurzam> mgorny: if vote was done in bug and done, just mention in the next available meeting
11:40 <@ulm> and keep the bug open until the next meeting
11:40 <@ulm> so it won't be missed
11:41 <+arthurzam> So dilfridge can document it in the history latex, and we can see it in meeting logs txt
11:41 <+arthurzam> Of course, only mention if it wasn't a hidden vote
11:41 <@mgorny> hmm, i'm actually wondering if something opposite wouldn't be beneficial too -- having a bug for every agenda item
11:41 <@mgorny> rather than a mix of e-mails and meeting logs/summaries
11:41 <@mattst88> honestly, probably so
11:41 <@dilfridge> well, 
11:42 <@ajak> and then trackers for each meeting!
11:42 <@dilfridge> e-mails are archived perfectly, so what is there is fixed
11:42 <@mattst88> ajak: perfect :P
11:42 <@mgorny> ajak: and a global tracker for all trackers
11:42 <@dilfridge> just avoid pastebins, attachments, and files on d.g.o public_html
11:42 <+robbat2> we're on open floor now yes?
11:42 <@mgorny> this would be beautiful in bugzilla's dependency tree view
11:42 <@mattst88> robbat2: yes
11:42 <@dilfridge> because after a while thesea re just gone
11:43 <    robbat2> | mattst88 had asked: <@mattst88> what would the analogous situation be for Gentoo?
11:43 <+robbat2> and I wanted to be sure an answer was on record
11:43 <@mgorny> ulm: do you want me to push glep updates to some branch, so you'd merge them, or should i push straight to master?
11:44 <@ulm> mgorny: push to branch please, so we follow the four eyes principle
11:44 <+robbat2> the analogous situation for Gentoo exists around *novel* code that might be taken from a propietary source
11:44 <@ulm> although I'm sure it's fine
11:44 <@ulm> mgorny: for both 68 and 78
11:45 <@mattst88> robbat2: thanks :)
11:45 <+robbat2> and introduced either as patches for ebuilds, or contributions to non-ebuild repos: e.g. portage itself, various patch bundles like kernel, mysql
11:45 <@ulm> mgorny: unfortunately it's hard to extract patches from archives.g.o
11:45 <@mattst88> any other open floor items?
11:46 <@dilfridge> fwiw, the stand proposal for fosdem 2023 is submitted
11:46 <@ajak> dilfridge: do we want an ago discussion
11:46 <@dilfridge> well, we can discuss it but it won't lead anywhere imho
11:46              * | ajak not really sure about the use of such a discussion
11:46 <@ajak> yeah
11:46 <@mgorny> this reminds me, we'd probably have to run glep76 updates through devs hired by certain companies
11:46 <@dilfridge> we've asked ago long ago to opensource his tooling, and he just refuses
11:47 <@mgorny> who may lose ability to contribute while their legal departments take care of proofreading the changed sentence
11:47 <@dilfridge> but I dont know what the current state is
11:47 <@ulm> mgorny: nah, only if we change the certificate of origin
11:47 <@mattst88> do we have any understanding why ago is so obstinate about this?
11:47 <@dilfridge> not me
11:47 <@mattst88> I've only heard "the code is messy"
11:47 <@ajak> ago has recently deleted the github repository where he let people file issues against his "CI", which is a signal to me that he's outright not interested in feedback
11:47 <       sam_> | mattst88: I don't think so. what makes it weirder is that he specifically said he wrote his own scripts because he got frustrated by someone years ago not sharing theirs
11:48 <@mattst88> yes, that is how I interpret that as well
11:48 <  gyakovlev> | mattst88: no real answer has been given to this q ever.
11:48 <@mattst88> sam_: /o\
11:48 <@ajak> https://archives.gentoo.org/gentoo-dev/message/b9a014a18091cac95368e15233fe02d2 there's some discussion under this
11:48 <@mgorny> ulm: pushed glep78-update now
11:48 <@mattst88> if there are no other topics, let's close the meeting :)
11:48 <@ajak> https://archives.gentoo.org/gentoo-dev/message/4041549d5af8b6118b1422d59bb2d224 says "To make John Helmert III happy, I just switched to tatt; so my actual workflow is tatt + nattka and there is nothing more." but that's his arch testing stuff, and not the "CI"
11:49 <@ajak> but yeah we can discuss ago later and see if there's anything we can do
11:49              * | mattst88 bangs gavel
11:49 <@sam_> thanks!
11:49 <@mattst88> thanks all
11:49 <@ajak> thank you
11:49 <+robbat2> mgorny, ulm
11:49 <@dilfridge> thanks everyone
11:49 <+robbat2> can we confirm the list of parts you want to change in the glep76?
11:49 <@gyakovlev> thanks, have a nice rest of the Sunday everyone.
11:49 <+robbat2> ulm wants records search to say government
11:50 <@mgorny> ulm: 68 was pushed already (i guess when we had a majority of "yes" votes, since it was blocking media type request)
11:50 <+robbat2> and it sounded like mgorny wanted a revision for the definition of notary being uneven around the world
11:50 <@ulm> robbat2: you said that this was the intended meaning
11:50 <@mgorny> robbat2: not me
11:50 <@ajak> i really think we should move the discussion to the ML
11:50 < CatSwarm> I'm not sure "government records search" even has a universal procedure, unless you're implementing full KYC and ave access to the relevant tool(s) for such things?
11:50 <@ulm> ajak: and we will
11:50 <@ajak> so, please, everyone with concerns about the GLEP, please take concerns to gentoo-project where we've been having this discussion since july
11:51              * | ajak is unlikely to follow discussion here due to lack of structure
11:51 <@dilfridge> it's probably easier to follow this intent by dropping the real name requirement entirely
11:51 <+robbat2> i want to make sure we have at least a partial list of the concerns to discuss
11:51 <+robbat2> i'd love to drop the entire requirement
11:51 <+robbat2> if you'll let us do that
11:51 <@ajak> me too
11:51 <@ulm> I'm strongly opposed against that
11:52 <@ajak> shocker (and this is why i have pushed for the amendment rather than dropping the requirement entirely)
11:52 <@ulm> because I predict that we'll get contributions under completely silly names
11:52 < pietinger> Does anyone know how Linux does the verify of real names ?
11:52 < CatSwarm> dropping it and leaving it to the diligence of whoever does the commits and sign-offs from an @gentoo associated email means the reputation risk, and burden of understanding the situation of the contributor isn't made overly intrusive.
11:53 <+sultan> ulm: We already have silly names ;)
11:53 < CatSwarm> pietinger: yes, I've discussed this with both multiple activists and linux foundation members over the years, as well as policymakers in other orgs with a contributor name policy.
11:54 < pietinger> CatSwarm: Thanks ... and ... How do they do it ?
11:54 <+robbat2> pietinger: obvious fakes rejected, but no other changes rejected that i've seen on public lists
11:54 <@ulm> robbat2: in principle it's all fine with me, with one added word for clarification
11:54 < CatSwarm> pietinger: I'd need to write a formal paper with a bibliography / set of references and footnotes, and can't concisely summarize it in a line or two. sorry.
11:55 <@ulm> because I think it should be made obvious when this is the intention
11:55 < pietinger> CatSwarm: Ok. Thanks.
11:55 < CatSwarm> robbat2: correct ~ it usually has a low bar to clear, being to reject obvious pseudonyms and assume everyone else contributed in good faith.
11:55 <+robbat2> are there other specific concerns to add to the discussion for the mailing list?
11:56 < CatSwarm> the details usually vary by which orgs do full KYC, or signed documentation / affidavit-style documents, etc.
11:56 <+robbat2> so far: 1. records search, 2. notary meaning, 3. dropping the entire requirement
11:56 < CatSwarm> (plus there's a custodian of records in that case)
11:57 <@ulm> 4. people supply their names to the foundation / its successor
11:57 <@ulm> and then they can commit under some handle
11:57 <@dilfridge> let's try to get along with 1 and 2 first
11:57 <+robbat2> CatSwarm: it was discussed much further in the past that we don't want to do full KYC because of the regulatory burden it places on the organization (e.g. securing copies of ID)
11:57 < CatSwarm> ulm: hmm? is there going to be a custodian of documents for that for personally identifiable info?
11:57 <+robbat2> i sure as hell don't want to keep anybody else's ID anywhere at all
11:58 <@dilfridge> ++
11:58 <@ulm> robbat2: we do that for developers, don't we?
11:58 <@dilfridge> no
11:58 <@ulm> no? pretty sure my name is in ldap
11:58 <@dilfridge> (if with ID you mean a copy of government docs)
11:59 < CatSwarm> I'm strongly in favor of dropping [the name requirement], and leaving it to the diligence of whoever does the commits and sign-offs from an @gentoo associated email means the reputation risk, and burden of understanding the situation of the contributor ~ [author is the term used by git] ~ isn't made overly intrusive.
11:59 <@ulm> only over my dead commit access
11:59 <@dilfridge> " leaving it to the diligence of whoever does the commits and sign-offs from an @gentoo associated email "
11:59 <@dilfridge> ^ why do you assume people will be willing to do that?
11:59 <@ajak> eh, what over your dead commit access?
11:59 < CatSwarm> dilfridge: I'm proposing this as a compromise to full KYC
11:59 <@ulm> ajak: famous quote from an ex-dev :)
12:00 <@ulm> flameeyes, that is :)
12:00 <@dilfridge> CatSwarm: in that case you're only shifting the load to someone else instead of taking responsibility yourself
12:01              * | dilfridge thinks that contributing should also mean taking responsibility
12:01 < CatSwarm> refuting comment: gentoo needs to be responsible for KYC or openly shift the burden in a way that leads to opening themselves up for disparate impact / discriminiation claims, or just come up with a compromise if retaining and safely handling and storing documents isn't being done.
12:03 < CatSwarm> there are ways to eliminate the burden on minorities and vulnerable populations who would be impacted by an overly strict name policy, and they have legal rights internationally, and in more jurisdictions than I could easily count
12:03 < CatSwarm> failure to take a less-intrusive approaching knowing they will be impacted is called disparate impact, legally-speaking.
12:04 <@ulm> CatSwarm: do you agree that the intention of "records search" was to mean "government records search"?
12:04 < CatSwarm> ulm: I didn't want to namedrop google, honestly.
12:04 <@ulm> that doesn't answer the question
12:05 < CatSwarm> if someone's contribs / reputation shows up with a quick google check and stuff like keybase and/or multiple profiles which show a clear history of beign a real identity, that's more than sufficient to confirme that someone is reachable if the presumably copyrightable contribution really was theirs and not stollen.
12:05 <@ulm> I just say that we should clarify it one way or the other
12:05 < CatSwarm> if the intent isn't to be able to confirm the chain of custody for what's being committed, why is it in a copyright-specific GLEP?
12:06 <@ulm> so if the intention is "government" then it should say so
12:06 <@ulm> and if it's google/twitter/whatever then it should say so as well
12:06 < CatSwarm> I never intentionally tried to, or considered that I had unintentionally implied government, so no.
12:07 < CatSwarm> this is really complicated enough that I don't think it falls under the original scope / intendd rationalle behind GLEP-76
12:07              * | mgorny needs to leave now
12:07 <@mgorny> thanks for the meeting and gn
12:07 <@ulm> CatSwarm: robbat2 apparently understood it differently
12:07 <@ulm> <+robbat2> records search in the context of the GLEP means to me searching government records, not public records like (twitter.com/jesus)
12:07 < CatSwarm> provably chain of custody is sufficient for copyright diligence, at any rate.
12:08 <@sam_> gn
12:08 <@ulm> maybe "public records search" then?
12:09 <@ulm> I mean, we can discuss it for another 6 months, but I guess it won't get us much further
12:10              * | ulm really expected approval today
12:10 < CatSwarm> I'd rather discuss this on ML if there's a specific rationale instead of random gusses for how to implement not-quite-KYC
12:10 <+robbat2> to discuss: is the point of the GLEP: proving chain of custody (e.g. the real X person), or simply that it a real person exists
12:10 <+robbat2> (on the ML)
12:11 < CatSwarm> robbat2: yes please, with a clear intent in the thread title.
12:11 <+robbat2> the DCO itself does NOT provide chain of custody proof
12:14 < CatSwarm> ulm: same ~ I figure perfect is the enemy of good, but if there's rationalles and needs / motives / concerns / expectations which weren't part of the original GLEP-76, I don't feel like this belongs in a discussion about making GLEP-76 more inclusive, as originally proposed:
12:14 < CatSwarm> bug #805575
12:14 < willikins> CatSwarm: https://bugs.gentoo.org/805575 "GLEP 76 wording: "legal name" requirement is unfriendly to transgender people"; Documentation, GLEP Changes; RESO, CANT; cyber+gentoo:glep
12:14 < CatSwarm> if it went straight to RESOLVED CANTFIX, that's worrisome.
12:15 <@ulm> purely procedural, it should have gone to the ML first
12:15 <@sam_> (stated clearly on the bug too)
12:15 <@ulm> as I've explained in comment #1
12:15 <@ulm> yes
12:16 < CatSwarm> ah, so RESOLVED CANTFIX only means that it's still an open discussion and being put to discussion, and "can fix" is still the status?
12:17 < CatSwarm> that's a confusing tag. I apologize for my misunderstanding.
12:17 <+robbat2> (maybe NEEDINFO would have been better, info being discussion on the ML)
12:19 < CatSwarm> mmm, NEEDINFO without the RESOLVED [perhaps an in-progress tag] would've been less of a rejection vibe, I think. okay.
12:20 <+robbat2> i have to go for now, and i'll try to raise threads for the 5? things to get clarification on [or finding consensus that the proposed text was good enough]
12:20 < CatSwarm> either way I found it intimidating.
12:20 <@sam_> CatSwarm: to be fair it was very clear if you read the bug 
12:20 < CatSwarm> robbat2: acknowledged. I'll be on the lookout for ML
12:20 <+robbat2> bugzilla: you can't use resolutions unless status==resolved
12:21 < CatSwarm> sam_ ~ RESOLVED means something to me other than in-progress / NEEDINFO, and implies rejection regardless of the intent of whoever closed the bug.
12:21 <@sam_> CatSwarm: i think you're reading too much int o the resolution, bugzilla is just very rigid. if you read the bug then you'll see why.
12:21 <@sam_> it's a technical thing based on the fact that we don't make actual edits to gleps based on bugs
12:21 <@sam_> the component on bz is for editorial stuff
12:21 <@sam_> you cannot really analyse things based on the bugzilla resolution in isolation
12:22 <@sam_> you do have to read the actual bug
12:22 < CatSwarm> either way, the one who posted the bug did post to ML and there's already been a previous council meeting on this topic. I think I just need to do a better job watching ML for now if it's being pushed away from bugzilla entirely.
12:22 <@ulm> acually CANTFIX says that it's a valid issue
12:23 <@ulm> otherwise it would have been RESP INVALID
12:23 <@ulm> *RESO
12:23 <@ulm> https://bugs.gentoo.org/page.cgi?id=fields.html#bug_status
12:24 < CatSwarm> dialect / grammar / linguistic understanding of implying something is unfixable is a fault on my end. I recognize that designing the bugzilla workflow wasn't done internally. either way, I apologize for the confusion.
12:24 <+robbat2> i have to run, clean house and cook a roast
12:24 < CatSwarm> good luck with tasty meal, etc.
12:24 < CatSwarm> similar ~ I have non-GLEP stuff ot attend to.
