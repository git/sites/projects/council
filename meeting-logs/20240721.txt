[2024-07-21T22:00:47+03:00] <@ulm> meeting time
[2024-07-21T22:00:51+03:00] <@ulm> !proj council
[2024-07-21T22:00:51+03:00] <willikins> (council@gentoo.org) arthurzam, dilfridge, mgorny, robbat2, sam, soap, ulm
[2024-07-21T22:00:57+03:00] <@robbat2> present
[2024-07-21T22:00:58+03:00] <@ulm> who wants the chair?
[2024-07-21T22:01:01+03:00] -*- dilfridge here
[2024-07-21T22:01:06+03:00] -*- soap here
[2024-07-21T22:01:11+03:00] <@dilfridge> always who asks
[2024-07-21T22:01:26+03:00] <@ulm> ok, I can take it
[2024-07-21T22:01:27+03:00] <@arthurzam> I can chair
[2024-07-21T22:01:37+03:00] <@ulm> arthurzam: go ahead :)
[2024-07-21T22:02:01+03:00] <@arthurzam> Agenda for today: https://marc.info/?l=gentoo-project&m=172094483522305&w=2
[2024-07-21T22:02:01+03:00] <@arthurzam> (since our mail archives are down)
[2024-07-21T22:02:06+03:00] <@arthurzam> 1. Roll Call
[2024-07-21T22:02:10+03:00] -*- arthurzam here
[2024-07-21T22:02:12+03:00] -*- ulm here
[2024-07-21T22:02:13+03:00] -*- dilfridge here
[2024-07-21T22:02:15+03:00] -*- sam_ here
[2024-07-21T22:02:15+03:00] -*- mgorny here
[2024-07-21T22:02:18+03:00] <@robbat2> here
[2024-07-21T22:02:24+03:00] -*- soap here
[2024-07-21T22:02:31+03:00] <@arthurzam> 7 yes, all here
[2024-07-21T22:02:44+03:00] <@arthurzam> 2. Constitute the new council
[2024-07-21T22:02:57+03:00] <@arthurzam>    - Decide on time of meetings. The previous council had its meetings
[2024-07-21T22:02:57+03:00] <@arthurzam>      on the 2nd Sunday of every month at 19:00 UTC
[2024-07-21T22:03:04+03:00] <@arthurzam> does this match everyone?
[2024-07-21T22:03:13+03:00] <@robbat2> works for me
[2024-07-21T22:03:14+03:00] <@dilfridge> fine for me
[2024-07-21T22:03:17+03:00] <@soap> fine for me
[2024-07-21T22:03:17+03:00] <@ulm> fine
[2024-07-21T22:03:20+03:00] <@sam_> sure
[2024-07-21T22:03:21+03:00] <@mgorny> likewise
[2024-07-21T22:03:32+03:00] <@arthurzam> nice, good for all, so let's continue it
[2024-07-21T22:03:41+03:00] <@arthurzam>    - Vote for continuing last council's workflow considering sending
[2024-07-21T22:03:41+03:00] <@arthurzam>      a call for agenda items (two weeks in advance), sending the
[2024-07-21T22:03:41+03:00] <@arthurzam>      agenda (one week in advance) and have the meeting focussed, i.e.
[2024-07-21T22:03:41+03:00] <@arthurzam>      have major discussions on the gentoo-project mailing list prior
[2024-07-21T22:03:41+03:00] <@arthurzam>      to the meeting
[2024-07-21T22:03:54+03:00] <@arthurzam> I don't think anyone here has issues with it
[2024-07-21T22:04:01+03:00] <@dilfridge> I think nobody wants to change that
[2024-07-21T22:04:07+03:00] <@arthurzam> And now to the hard part:
[2024-07-21T22:04:07+03:00] <@arthurzam>    - Appoint chairmen for this term's meetings
[2024-07-21T22:04:13+03:00] <@soap> fine with me
[2024-07-21T22:04:17+03:00] <@ulm> I can take Jan and Feb
[2024-07-21T22:04:27+03:00] <@arthurzam> I can take Jul & Aug
[2024-07-21T22:04:27+03:00] <@dilfridge> nov and dec for me
[2024-07-21T22:04:30+03:00] <@sam_> I'll do dec/jan
[2024-07-21T22:04:34+03:00] <@soap> I'd suggest replacing all the old with new members 1:1?
[2024-07-21T22:04:35+03:00] <@sam_> uh
[2024-07-21T22:04:46+03:00] <@robbat2> i'm going to abstain on chair role, because I expect a lot in shutting down the foundation
[2024-07-21T22:04:58+03:00] <@dilfridge> sam_: I take oct you take dec
[2024-07-21T22:05:01+03:00] <@sam_> ok
[2024-07-21T22:05:04+03:00] <@arthurzam> I'm quite easy to move if needed :)
[2024-07-21T22:05:30+03:00] <@ulm> sam_: I could take Mar if you want Jan
[2024-07-21T22:05:43+03:00] <@mgorny> err, could we do it keeping adjacent months to one person?
[2024-07-21T22:05:53+03:00] <@dilfridge> we do
[2024-07-21T22:06:07+03:00] -*- dilfridge hands mgorny some coffee
[2024-07-21T22:06:16+03:00] <@mgorny> so who took Sep?
[2024-07-21T22:06:21+03:00] <@dilfridge> noone yet
[2024-07-21T22:06:29+03:00] <@soap> I was august last time
[2024-07-21T22:06:34+03:00] <@soap> IIRC
[2024-07-21T22:06:34+03:00] <@mgorny> yet Aug and Oct are taken!
[2024-07-21T22:06:41+03:00] <@mgorny> hence, "we do" is false
[2024-07-21T22:06:58+03:00] <@arthurzam> soap: I can give you aug, I don't really care
[2024-07-21T22:06:58+03:00] <@sam_> shall we just do soap's suggestion? I don't really care when as long as it's not october or feb
[2024-07-21T22:07:13+03:00] <@dilfridge> please no january for me, otherwise I'm fine
[2024-07-21T22:07:16+03:00] <@soap> I will do aug/sep then
[2024-07-21T22:07:32+03:00] <@dilfridge> and july not either
[2024-07-21T22:07:36+03:00] -*- mgorny will take any two adjacent months
[2024-07-21T22:07:46+03:00] <@arthurzam> mgorny: may/june?
[2024-07-21T22:07:51+03:00] <@mgorny> sure, wfm
[2024-07-21T22:08:13+03:00] <@arthurzam> sam_: dilfridge what have you selected between you 2?
[2024-07-21T22:08:30+03:00] <@dilfridge> last was, I do oct & nov
[2024-07-21T22:08:52+03:00] <@sam_> then I'll do dec/jan, yeah?
[2024-07-21T22:08:56+03:00] <@dilfridge> ++
[2024-07-21T22:09:01+03:00] <@sam_> if ulm is fine with that too
[2024-07-21T22:09:01+03:00] <@ulm> feb/mar for me then
[2024-07-21T22:09:04+03:00] <@sam_> ok
[2024-07-21T22:09:16+03:00] <@arthurzam> ok, so in summary:
[2024-07-21T22:09:45+03:00] <@arthurzam> arthurzam, soap, soap, dilfridge, dilfridge, sam_, sam_, ulm, ulm, arthurzam, mgorny, mgorny
[2024-07-21T22:10:17+03:00] <@arthurzam> So let's put official vote for it:
[2024-07-21T22:10:17+03:00] <@arthurzam> 2. Constitute the new council
[2024-07-21T22:10:20+03:00] -*- arthurzam yes
[2024-07-21T22:10:25+03:00] -*- sam_ yes
[2024-07-21T22:10:26+03:00] -*- soap yes
[2024-07-21T22:10:32+03:00] -*- robbat2 aye
[2024-07-21T22:10:34+03:00] -*- dilfridge yes
[2024-07-21T22:10:35+03:00] -*- ulm yes
[2024-07-21T22:10:42+03:00] -*- mgorny yes
[2024-07-21T22:10:48+03:00] <@arthurzam> 7 yes, passed
[2024-07-21T22:11:04+03:00] <@arthurzam> 3. Change GLEP 84 status to Final
[2024-07-21T22:11:04+03:00] <@arthurzam> bug 914982
[2024-07-21T22:11:05+03:00] <willikins> https://bugs.gentoo.org/914982 "GLEP 84: Standard format for package.mask files"; Documentation, New GLEP submissions; IN_P; arthurzam:glep
[2024-07-21T22:11:26+03:00] <@arthurzam> So I forgot there is next step after Accepted status, can we make it FINAL now?
[2024-07-21T22:11:54+03:00] <@arthurzam> soko (p.g.o) already uses the new format to render, and pkgdev mask uses the new format to create masks
[2024-07-21T22:12:00+03:00] <@robbat2> the last message seemed to be a concern that it wasn't fully implemented?
[2024-07-21T22:12:15+03:00] <@sam_> are all entries in profiles/ compliant?
[2024-07-21T22:12:42+03:00] <@arthurzam> sam_: not all, only in profiles/packages.mask - you want all of them to comply?
[2024-07-21T22:12:50+03:00] <@arthurzam> robbat2: only pkgcheck is missing to hard enforce it
[2024-07-21T22:13:01+03:00] <@sam_> recursive package.mask for now ideally
[2024-07-21T22:13:28+03:00] <@arthurzam> ok I see my homework until next council meeting
[2024-07-21T22:13:30+03:00] <@robbat2> should we defer or just mark final and give pkgcheck authors a bug to enable enforcement
[2024-07-21T22:13:41+03:00] <@sam_> I think the latter is ok
[2024-07-21T22:14:07+03:00] <@ulm> either way wfm, slight preference for the former
[2024-07-21T22:14:18+03:00] <@robbat2> i think final & bug is good enough, that's our intent, and the tooling can start to enforce it more over time
[2024-07-21T22:14:24+03:00] <@robbat2> eventual consistency
[2024-07-21T22:14:53+03:00] <@arthurzam> So a motion:
[2024-07-21T22:14:53+03:00] <@arthurzam> 3. Mark GLEP 84 as final, open bug for pkgcheck to enforce the GLEP, and open bug to convert all profiles/**/package.mask
[2024-07-21T22:15:03+03:00] <@robbat2> seconded
[2024-07-21T22:15:09+03:00] -*- mgorny yes
[2024-07-21T22:15:14+03:00] -*- robbat2 aye
[2024-07-21T22:15:16+03:00] -*- sam_ yes
[2024-07-21T22:15:17+03:00] -*- ulm yes
[2024-07-21T22:15:24+03:00] -*- arthurzam yes
[2024-07-21T22:15:33+03:00] -*- soap yes
[2024-07-21T22:15:58+03:00] <@robbat2> dilfridge: ?
[2024-07-21T22:16:08+03:00] -*- dilfridge yes
[2024-07-21T22:16:13+03:00] <@arthurzam> 7 yes, passed
[2024-07-21T22:16:17+03:00] <@ulm> so action item for GLEP editors to mark it as final
[2024-07-21T22:16:28+03:00] <@arthurzam> I'll open the bugs after meeting, ulm could you handle the GLEP side?
[2024-07-21T22:16:32+03:00] <@ulm> sure
[2024-07-21T22:16:43+03:00] <@arthurzam> So next one:
[2024-07-21T22:16:51+03:00] <@arthurzam> 4. Status of architectures https://marc.info/?l=gentoo-project&m=171995035718027&w=2
[2024-07-21T22:17:08+03:00] <@arthurzam> Here there would be multiple votes, let's start from first one:
[2024-07-21T22:17:16+03:00] <@arthurzam> 4a. alpha - make profile stable
[2024-07-21T22:17:43+03:00] -*- robbat2 aye
[2024-07-21T22:17:45+03:00] <@arthurzam> matoro handles alpha very well, is responsive, and tree of deps is nearly done (breaks because of us continuing to add dependencies)
[2024-07-21T22:17:58+03:00] <@dilfridge> yes please
[2024-07-21T22:18:06+03:00] -*- arthurzam yes
[2024-07-21T22:18:10+03:00] -*- dilfridge yes
[2024-07-21T22:18:17+03:00] -*- sam_ yes
[2024-07-21T22:18:21+03:00] <@mgorny> arthurzam: just to clarify: we are talking about "stable" profile and not "stable" keywords
[2024-07-21T22:18:23+03:00] -*- ulm yes
[2024-07-21T22:18:30+03:00] <@dilfridge> exactly
[2024-07-21T22:18:33+03:00] -*- mgorny yes (provided it passes pkgcheck)
[2024-07-21T22:18:34+03:00] <@arthurzam> mgorny: yes, ~alpha but consistent dep tree
[2024-07-21T22:18:38+03:00] -*- soap yes
[2024-07-21T22:18:45+03:00] <@arthurzam> We will do it only after full tree is done
[2024-07-21T22:18:57+03:00] <@arthurzam> 7 yes, passed
[2024-07-21T22:19:08+03:00] <@arthurzam> I'll contact matoro with the action items for that
[2024-07-21T22:19:18+03:00] <@arthurzam> 4b. ia64 - deprecate arch
[2024-07-21T22:19:28+03:00] <@dilfridge> unrelated and unreleased:
[2024-07-21T22:19:28+03:00] <@dilfridge> >>> Completed testing sys-libs/glibc-2.40
[2024-07-21T22:19:40+03:00] <@arthurzam> for ia64, I think deprecation period of 1 year
[2024-07-21T22:19:44+03:00] <@arthurzam> does is sound fine?
[2024-07-21T22:19:51+03:00] <@dilfridge> yes
[2024-07-21T22:19:53+03:00] <@mgorny> it's dead, jim
[2024-07-21T22:20:06+03:00] <@ulm> this is an ex-parrot
[2024-07-21T22:20:12+03:00] <@robbat2> i think it could be shorter - given the devbox problems
[2024-07-21T22:20:18+03:00] <@robbat2> stop dragging it out
[2024-07-21T22:20:41+03:00] <@arthurzam> ulm: dilfridge: any thoughts against it? I don't care and will gladly even do it one day
[2024-07-21T22:20:42+03:00] <@dilfridge> the last stages are from april
[2024-07-21T22:21:15+03:00] <@sam_> just kill it, sorry
[2024-07-21T22:21:15+03:00] <@ulm> maybe not tomorrow, give it a month or so
[2024-07-21T22:21:25+03:00] <@sam_> give it a month, do a news item
[2024-07-21T22:21:34+03:00] <@dilfridge> shrug, there are no real reasons to do shorter - if there is still anyone using it it would be nicer to use the normal timing
[2024-07-21T22:21:53+03:00] <@dilfridge> but I'm fine with it either way
[2024-07-21T22:21:57+03:00] <@arthurzam> ok, so the motion: "ia64 - deprecate arch, one month period after news-item"
[2024-07-21T22:22:05+03:00] -*- arthurzam yes
[2024-07-21T22:22:08+03:00] <@robbat2> does killing it earlier make any processes easier?
[2024-07-21T22:22:16+03:00] <@sam_> it means devs stop wasting time on kwreqs and such
[2024-07-21T22:22:22+03:00] <@arthurzam> Less keywording bugs
[2024-07-21T22:22:34+03:00] <@sam_> and imo that's more value add than keeping it for a year
[2024-07-21T22:22:47+03:00] <@robbat2> agreed
[2024-07-21T22:23:10+03:00] <@ulm> we could do the wikipedia thing, everyone states a time period and we take the median
[2024-07-21T22:23:22+03:00] <@soap> ok 1 -year
[2024-07-21T22:23:27+03:00] <@soap> ok -1 year :D
[2024-07-21T22:23:35+03:00] <@arthurzam> nice
[2024-07-21T22:23:49+03:00] <@arthurzam> let's go with 1 month - this is dead
[2024-07-21T22:23:51+03:00] <@robbat2> 1 month
[2024-07-21T22:23:55+03:00] <@soap> 1 month, yup
[2024-07-21T22:24:03+03:00] <@ulm> 3 months
[2024-07-21T22:24:12+03:00] <@mgorny> 3 months
[2024-07-21T22:24:27+03:00] <@dilfridge> EDONTCARE
[2024-07-21T22:24:47+03:00] <@sam_> 1 month
[2024-07-21T22:24:48+03:00] <@arthurzam> median results in 1 month?
[2024-07-21T22:25:13+03:00] <@arthurzam> yep, python confirms it
[2024-07-21T22:25:15+03:00] <@arthurzam> motion: "ia64 - deprecate arch, one month period after news-item"
[2024-07-21T22:25:17+03:00] <@ulm> one vote missing?
[2024-07-21T22:25:21+03:00] <@mgorny> median falls between 1 and 3, so we ought to take avg? xP
[2024-07-21T22:25:33+03:00] <@mgorny> ah, sorry, didn't notice sam
[2024-07-21T22:25:36+03:00] -*- dilfridge abstain
[2024-07-21T22:25:46+03:00] -*- arthurzam yes
[2024-07-21T22:25:50+03:00] -*- robbat2 aye
[2024-07-21T22:25:56+03:00] -*- mgorny yes
[2024-07-21T22:25:57+03:00] -*- soap yes
[2024-07-21T22:25:57+03:00] <@ulm> yeah, median is 1 month
[2024-07-21T22:25:59+03:00] -*- ulm yes
[2024-07-21T22:26:26+03:00] <@arthurzam> sam_?
[2024-07-21T22:26:32+03:00] -*- sam_ yes
[2024-07-21T22:26:36+03:00] <@arthurzam> 6 yes, 1 abstain => motion passed
[2024-07-21T22:26:48+03:00] <@arthurzam> I'll work with dilfridge and sam_ to deprecate it correctly
[2024-07-21T22:26:52+03:00] <@sam_> :)
[2024-07-21T22:27:06+03:00] <@arthurzam> 4c. ppc64/32ul - shorten deprecation period
[2024-07-21T22:27:33+03:00] <@arthurzam> I learned not long ago that we have only 17.* profiles which are deprecated for 2 years - can we finally cleanup them?
[2024-07-21T22:27:48+03:00] <@robbat2> arthurzam: let's hold that for new business?
[2024-07-21T22:27:52+03:00] <@arthurzam> This would really help cleanup of ppc64 profiles internal
[2024-07-21T22:27:54+03:00] <@robbat2> and stick to these votes first
[2024-07-21T22:28:20+03:00] <@arthurzam> robbat2: I meant for ppc64/32-bit userland ?
[2024-07-21T22:28:24+03:00] <@arthurzam> robbat2: what do you mean?
[2024-07-21T22:28:49+03:00] <@robbat2> "we have only 17.* profiles which are deprecated for 2 years - can we finally cleanup them?" => that should be new business
[2024-07-21T22:28:58+03:00] <@robbat2> and keep on track w/ 4.c.
[2024-07-21T22:29:26+03:00] <@arthurzam> help
[2024-07-21T22:29:29+03:00] <@dilfridge> arthurzam:  you mean these here? default/linux/powerpc/ppc64/17.0/32bit-userland
[2024-07-21T22:29:34+03:00] <@arthurzam> dilfridge: yes
[2024-07-21T22:29:46+03:00] <@dilfridge> they dont exist in 23.0, correct
[2024-07-21T22:30:21+03:00] <@dilfridge> and the whole default/linux/powerpc tree should be long gone
[2024-07-21T22:30:31+03:00] <@arthurzam> So the deprecation of those profiles isn't exactly defined - can we shorten it to cleanup or 1 month?
[2024-07-21T22:30:36+03:00] <@sam_> I was just writing that out
[2024-07-21T22:30:48+03:00] <@sam_> they've been deprecated forever, and in 4.c, arthur is asking what to do about it
[2024-07-21T22:30:52+03:00] <@dilfridge> in favour of default/linux/{ppc,ppc64,ppc64le}
[2024-07-21T22:31:21+03:00] <@sam_> they should be fine to yank
[2024-07-21T22:31:31+03:00] <@arthurzam> ok, so no need to vote, sorry :)
[2024-07-21T22:31:54+03:00] <@arthurzam> next one:
[2024-07-21T22:31:55+03:00] <@ulm> why is it ppc64 when it's 32 bit userland? we don't do that for x86/amd64, correct?
[2024-07-21T22:32:17+03:00] <@soap> because these are all weird arches that had weird initial userlands
[2024-07-21T22:32:32+03:00] <@arthurzam> 4d. sparc32 - deprecate profiles
[2024-07-21T22:32:32+03:00] <@arthurzam> I want to request 3 month deprecation period
[2024-07-21T22:32:32+03:00] <@dilfridge> ulm: that's why it should be removed
[2024-07-21T22:32:54+03:00] <@sam_> for sparc, 3 months or maybe 6 months, yeah, as people will need time to reinstall and there's i think at least 2 installs i know f
[2024-07-21T22:33:04+03:00] <@dilfridge> so about 4c still
[2024-07-21T22:33:16+03:00] <@arthurzam> yeah, let's still be on 4c, to not complicate
[2024-07-21T22:33:20+03:00] <@dilfridge> the profiles are only deprecated since 1/June
[2024-07-21T22:34:21+03:00] <@dilfridge> however, even for 17.0 there should be an alternative
[2024-07-21T22:34:23+03:00] <@dilfridge> ?
[2024-07-21T22:34:28+03:00] <@sam_> right -- 4c, I don't think there's anything to do, it's all arch team prorogative and it's been deprecated since years ago by gyak
[2024-07-21T22:34:35+03:00] <@sam_> it's not killing an arch entirely or anything
[2024-07-21T22:34:40+03:00] <@arthurzam> dilfridge: https://gitweb.gentoo.org/repo/gentoo.git/commit/?id=7587ed10a9cd74ed4d8c6ac7e283dcee3aa20967
[2024-07-21T22:34:43+03:00] <@dilfridge> ok wfm
[2024-07-21T22:35:10+03:00] <@arthurzam> ok, back to track:
[2024-07-21T22:35:10+03:00] <@arthurzam> 4d. sparc32 - deprecate profiles
[2024-07-21T22:35:10+03:00] <@arthurzam> I want to request 3 month deprecation period
[2024-07-21T22:35:14+03:00] <@dilfridge> also, arch team can decide that
[2024-07-21T22:35:17+03:00] <@sam_> yeah
[2024-07-21T22:35:31+03:00] <@dilfridge> arthurzam: imagine someone is out there using it :P
[2024-07-21T22:35:45+03:00] <@arthurzam> So 6 month?
[2024-07-21T22:35:50+03:00] <@dilfridge> in the land of fairies and unicorns
[2024-07-21T22:35:57+03:00] <@sam_> let's do 6 please
[2024-07-21T22:35:57+03:00] <@dilfridge> yeah
[2024-07-21T22:36:00+03:00] <@sam_> these machines are a PITA to handle
[2024-07-21T22:36:10+03:00] <@sam_> nobody will be eager to reinstall in a hurry
[2024-07-21T22:36:21+03:00] <@arthurzam> ok, so motion: " sparc32 - deprecate profiles, 6 month period after new item"
[2024-07-21T22:36:28+03:00] -*- arthurzam yes
[2024-07-21T22:36:31+03:00] -*- sam_ yes
[2024-07-21T22:36:34+03:00] -*- soap yes
[2024-07-21T22:36:37+03:00] -*- dilfridge yes
[2024-07-21T22:36:38+03:00] -*- ulm yes
[2024-07-21T22:36:43+03:00] -*- robbat2 aye
[2024-07-21T22:37:07+03:00] <@arthurzam> mgorny?
[2024-07-21T22:37:20+03:00] -*- mgorny yes
[2024-07-21T22:37:21+03:00] <@mgorny> sorry
[2024-07-21T22:37:37+03:00] <@arthurzam> 7 yes => motion passed
[2024-07-21T22:37:37+03:00] <@arthurzam> Will handle with dilfridge and sam
[2024-07-21T22:37:51+03:00] <@dilfridge> easy
[2024-07-21T22:37:52+03:00] <@arthurzam> 4e. s390 not s390x - deprecate profiles
[2024-07-21T22:38:05+03:00] <@arthurzam> I think a short notice here is fine?
[2024-07-21T22:38:16+03:00] <@sam_> even IBM seem to think literally 0 users exist
[2024-07-21T22:38:17+03:00] <@dilfridge> so the only reason for the existence of s390 31bit is the support of legacy software
[2024-07-21T22:38:26+03:00] <@dilfridge> kernel has been 64bit only for ages
[2024-07-21T22:38:38+03:00] <@arthurzam> so 1 month just for protocol?
[2024-07-21T22:38:46+03:00] <@dilfridge> I'd do the same 6 months, to be nice
[2024-07-21T22:38:50+03:00] <@arthurzam> ok
[2024-07-21T22:38:54+03:00] <@sam_> yeah it costs us nothing there
[2024-07-21T22:39:02+03:00] <@sam_> (same keyword too so not a problem)
[2024-07-21T22:39:09+03:00] <@arthurzam> sam_: it does cost - some ugly profiles between s390x unmask s390
[2024-07-21T22:39:20+03:00] <@sam_> ok, but very little :)
[2024-07-21T22:39:31+03:00] <@sam_> it's not the same as ia64 where you're dealing with rekeywording and such for the most part
[2024-07-21T22:39:42+03:00] <@arthurzam> motion 4e: "s390 not s390x - deprecate profiles, 6 month period after news-item"
[2024-07-21T22:39:42+03:00] <@sam_> also very little seems to break on s390
[2024-07-21T22:39:45+03:00] -*- sam_ yes
[2024-07-21T22:39:46+03:00] -*- arthurzam yes
[2024-07-21T22:39:49+03:00] -*- mgorny yes
[2024-07-21T22:39:50+03:00] -*- dilfridge yes
[2024-07-21T22:39:51+03:00] -*- robbat2 aye
[2024-07-21T22:39:52+03:00] -*- soap yes
[2024-07-21T22:39:57+03:00] -*- ulm yes
[2024-07-21T22:40:07+03:00] <@arthurzam> 7 yes => passed
[2024-07-21T22:40:07+03:00] <@arthurzam> I will handle with dilfridge and sam
[2024-07-21T22:40:17+03:00] <@arthurzam> finally (4) is done
[2024-07-21T22:40:28+03:00] <@arthurzam> 5. Foundation dissolution status update
[2024-07-21T22:40:32+03:00] <@arthurzam> anything new?
[2024-07-21T22:40:33+03:00] <@dilfridge> we can make a front page post for ia64 sparc32 and s390
[2024-07-21T22:40:53+03:00] <@ulm> we have a tracker bug for the various tasks
[2024-07-21T22:40:57+03:00] <@robbat2> ulm has put together a tracker bug - it needs many more entries still
[2024-07-21T22:41:08+03:00] <@arthurzam> link please?
[2024-07-21T22:41:20+03:00] <@robbat2> i am chasing SPI's treasurer about the paypal integration pieces needed to do donations to them directly from our page
[2024-07-21T22:41:29+03:00] <@ulm> arthurzam: it's private ATM, will make it public later
[2024-07-21T22:41:36+03:00] <@arthurzam> no problem
[2024-07-21T22:41:39+03:00] <@robbat2> https://bugs.gentoo.org/936211 is the tracker
[2024-07-21T22:41:44+03:00] <@ulm> after we have sorted out what can be public
[2024-07-21T22:41:58+03:00] <@robbat2> I think the tracker should be public - and decide individual items being public
[2024-07-21T22:42:21+03:00] <@ulm> any objections against public tracker?
[2024-07-21T22:42:29+03:00] <@dilfridge> none here
[2024-07-21T22:42:37+03:00] <@arthurzam> ok from me
[2024-07-21T22:42:40+03:00] <@sam_> ok
[2024-07-21T22:43:30+03:00] <@ulm> seems not
[2024-07-21T22:43:42+03:00] <@ulm> public then
[2024-07-21T22:43:42+03:00] <@arthurzam> anything more? I do want to thank everyone working on it :)
[2024-07-21T22:44:22+03:00] <@arthurzam> ok, let's advance
[2024-07-21T22:44:26+03:00] <@arthurzam> 6. Missing meeting logs and summaries
[2024-07-21T22:44:31+03:00] <@dilfridge> ...
[2024-07-21T22:44:42+03:00] <@arthurzam> Hey, even raw logs are missing :(
[2024-07-21T22:44:50+03:00] <@sam_> I've got a draft for my summaries open, I'll finish it today/tomorrow, promise
[2024-07-21T22:45:02+03:00] <@sam_> it just involved the huge AI discussion and I was putting it off
[2024-07-21T22:45:08+03:00] <@dilfridge> soon
[2024-07-21T22:45:41+03:00] <@arthurzam> Can we at least put raw logs soon, as well as participants?
[2024-07-21T22:46:09+03:00] <@dilfridge> middle of the week
[2024-07-21T22:46:18+03:00] <@arthurzam> ok, thank you
[2024-07-21T22:46:34+03:00] <@arthurzam> 7. Open bugs with council involvement
[2024-07-21T22:47:00+03:00] <@arthurzam> will skip that foundation tracker as discussed above
[2024-07-21T22:47:06+03:00] <@arthurzam> bug 925014
[2024-07-21T22:47:06+03:00] <willikins> arthurzam: https://bugs.gentoo.org/925014 "PR services lacking developer redundancy"; Community Relations, User Relations; CONF; ajak:pr
[2024-07-21T22:47:37+03:00] <@arthurzam> Any update?
[2024-07-21T22:47:50+03:00] <@arthurzam> Or, what are we missing to close it?
[2024-07-21T22:48:38+03:00] <@ulm> the XMPP channel still has only one responsible person, I think?
[2024-07-21T22:48:50+03:00] <@ulm> who doens't reply in a timely manner
[2024-07-21T22:49:02+03:00] <@ulm> *doesn't
[2024-07-21T22:49:13+03:00] <@dilfridge> this is an eternal tracker, let's just revisit it next month
[2024-07-21T22:49:46+03:00] <@arthurzam> ok, just note that I've little knowledge on the PR side of gentoo, so will follow you
[2024-07-21T22:49:50+03:00] <@ulm> yes, too early to close IMHO
[2024-07-21T22:50:06+03:00] <@ulm> things are still not in a perfect shape
[2024-07-21T22:50:07+03:00] <@arthurzam> great, deferred to next meeting
[2024-07-21T22:50:13+03:00] <@arthurzam> next one, bug 801499
[2024-07-21T22:50:14+03:00] <willikins> arthurzam: https://bugs.gentoo.org/801499 "Approach Nitrokey for Nitrokey 3 upgrade"; Gentoo Foundation, Proposals; CONF; sam:trustees
[2024-07-21T22:50:29+03:00] <@robbat2> nitrokey: no response from their CEO
[2024-07-21T22:50:32+03:00] <@soap> I think we've buried NK3
[2024-07-21T22:50:35+03:00] <@arthurzam> have we tried to contact yubikey?
[2024-07-21T22:50:42+03:00] <@soap> the UX is terrible
[2024-07-21T22:51:04+03:00] <@arthurzam> soap: talking about YK or NK3?
[2024-07-21T22:51:13+03:00] <@soap> arthurzam: NK3, YK is fine
[2024-07-21T22:51:40+03:00] <@robbat2> contacting yubikey was proposed by the council before I was a member; I don't have any reason to oppose it from the Foundation side, but I do want to focus on dissolution
[2024-07-21T22:51:45+03:00] <@robbat2> so I ask that somebody else take that on
[2024-07-21T22:52:11+03:00] <@arthurzam> is it a EU or US firm?
[2024-07-21T22:52:20+03:00] <@soap> YK?
[2024-07-21T22:52:23+03:00] <@arthurzam> yes
[2024-07-21T22:52:23+03:00] <@soap> its a mixed one
[2024-07-21T22:52:26+03:00] <@soap> SE/US
[2024-07-21T22:52:50+03:00] <@robbat2> Swedish originally
[2024-07-21T22:53:27+03:00] <@arthurzam> soap: ulm: could one of you do it and contact them?
[2024-07-21T22:53:54+03:00] <@soap> ok, what exactly do we want? like a semi sponsorship or what?
[2024-07-21T22:53:57+03:00] <@robbat2> https://www.yubico.com/products/yubikey-as-a-service/ min buyin is 500 units for the public program
[2024-07-21T22:54:10+03:00] <@robbat2> but I don't know about their non-profit side
[2024-07-21T22:54:35+03:00] <@soap> robbat2: what was the gain for us from NK? I mean, we still paid for them?
[2024-07-21T22:55:07+03:00] <@robbat2> roughly the same as the yubikey as a service => bulk discounts, fufillment by nitrokey, billed to foundation
[2024-07-21T22:55:20+03:00] <@arthurzam> I've found https://www.yubico.com/why-yubico/secure-it-forward/
[2024-07-21T22:56:25+03:00] <@robbat2> you can decide if that fits or if they have some other program
[2024-07-21T22:56:58+03:00] <@robbat2> to return to the original question: should we give nitrokey a formal "no", or wait for yubikey?
[2024-07-21T22:56:59+03:00] <@sam_> I think we can do this out of the meeting?
[2024-07-21T22:57:13+03:00] <@sam_> wait for yubikey, I guess
[2024-07-21T22:57:23+03:00] <@mgorny> let's not burn the bridges
[2024-07-21T22:57:28+03:00] <@sam_> right
[2024-07-21T22:57:44+03:00] <@arthurzam> yes, I agree with sam_. We should wait for YK response, but not close NK yet. Also, we can continue in this channel/in bug outside meeting
[2024-07-21T22:58:01+03:00] <@ulm> +1
[2024-07-21T22:58:17+03:00] <@arthurzam> so, no more open council bugs
[2024-07-21T22:58:20+03:00] <@robbat2> https://www.yubico.com/wp-content/uploads/2024/01/1617-graphic-YED-Map-updates-2024-r3@2x-100-1-1024x893.jpg <-- this is where yubikey can ship btw
[2024-07-21T22:58:48+03:00] -*- arthurzam is barely in, that small strip
[2024-07-21T22:59:01+03:00] <@arthurzam> 8. Open floor
[2024-07-21T22:59:32+03:00] <@arthurzam> if someone has anything, please put a simple "dot" message and we will wait while you write
[2024-07-21T22:59:37+03:00] <@robbat2> arthurzam: are there *additional* profiles you wish to see deprecated?
[2024-07-21T23:00:16+03:00] <@robbat2> (i have two other open floor questions as well)
[2024-07-21T23:00:54+03:00] <@arthurzam> robbat2: I want ppc and x86 to loose stable arch status, but it needs discussion on ML
[2024-07-21T23:00:54+03:00] <@arthurzam> I also want to split ppc64 ==> ppc64 & ppc64le, and riscv => riscv & riscv32
[2024-07-21T23:01:06+03:00] <@sam_> I've really got to run shortly
[2024-07-21T23:01:12+03:00] <@arthurzam> But all of this for next meeting, too much for this one already
[2024-07-21T23:01:31+03:00] <@robbat2> thanks; those can to the ML
[2024-07-21T23:01:35+03:00] <@soap> arthurzam: stable keywords or stable profile?
[2024-07-21T23:01:45+03:00] <@arthurzam> soap: stable keywords
[2024-07-21T23:01:52+03:00] <@soap> yeah, I'm fine with that
[2024-07-21T23:02:18+03:00] <@robbat2> 2nd question: previously the Foundation had non-trustee  Liaison roles, like jmbsvicetto representing infra to reduce conflict of interest in votes
[2024-07-21T23:02:35+03:00] <@robbat2> with the dissolution of the Foundation, would the council take a similar process?
[2024-07-21T23:03:12+03:00] <@ulm> robbat2: what exactly are you asking for? a council infra liaison?
[2024-07-21T23:03:12+03:00] <@robbat2> (so that as the foundation treasurer & infra lead: i didn't both propose spending money, authorize it as a trustee, and action it as treasurer)
[2024-07-21T23:03:28+03:00] <@robbat2> to start a clear question: does the Council see a need for a similar process?
[2024-07-21T23:03:29+03:00] <@sam_> that's a pretty big topic which warrants thought on the ML, not a quick thing, I think
[2024-07-21T23:03:32+03:00] <@mgorny> that doesn't sound like a open floor topic
[2024-07-21T23:03:34+03:00] <@sam_> yeah
[2024-07-21T23:03:42+03:00] <@robbat2> just do you want that as a process, or not
[2024-07-21T23:03:46+03:00] <@robbat2> so that I open the threads
[2024-07-21T23:04:06+03:00] <@arthurzam> I think we should do it. I think we are to close to Council = QA = Infra
[2024-07-21T23:04:09+03:00] <@mgorny> i don't think it's necessary, given that there are 7 people here, and worst case you can abstain
[2024-07-21T23:04:21+03:00] <@sam_> arthurzam: ultimately there's only so many people interesed
[2024-07-21T23:04:23+03:00] <@sam_> please bring it up on the ML
[2024-07-21T23:04:30+03:00] <@sam_> i can't possibly make a judgement this quickly
[2024-07-21T23:04:58+03:00] <@arthurzam> robbat2: when you bring it up in ML, please explain some stuff in simpler words, you sentence above was too English living specific words
[2024-07-21T23:05:25+03:00] <@robbat2> thanks, I will start the discussion on the ML
[2024-07-21T23:05:33+03:00] <@sam_> thx
[2024-07-21T23:05:51+03:00] <@arthurzam> I didn't see any "dot" from anyone else, so I think we are done with open floor
[2024-07-21T23:05:56+03:00] <@robbat2> i'll do simialr for the last item as well
[2024-07-21T23:06:12+03:00] -*- arthurzam bangs the gravel
