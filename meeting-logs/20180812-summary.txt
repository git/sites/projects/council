Summary of Gentoo council meeting 12 August 2018

Agenda
======
1. Roll call
2. Appoint chairmen for remaining meetings
3. Open bugs with council involvement
4. Open floor


Roll call
=========
Present: dilfridge, k_f, leio, slyfox, ulm, whissi, williamh

Appoint chairmen for remaining meetings
=======================================
The remaining meeting chairs have been assigned.

Open bugs with council involvement
==================================
- Bug 637328 "GLEP 14 needs to be updated":
  This is moving forwards internally in the security project.

- Bug 642072 "Joint venture to deal with copyright issues":
  Work on GLEP 76 is ongoing, new version to be committed soon.

Open floor
==========
- Council members will exchange their phone numbers by e-mail.

- K_F wants to encourage more public relations work. In particular,
  activity in the U.S. should be increased. Examples are participation
  at conferences and in upstream communities. Other ideas would be
  promotional videos or podcasts.

- leio suggests that council members have informal voice chats. In the
  discussion it is pointed out that finding a platform that is free
  software and also fulfills accessibility requirements may be
  difficult. It is also suggested that council members could meet in
  person, e.g., in combination with a conference.
