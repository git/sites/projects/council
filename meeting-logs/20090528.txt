16:00 <@dev-zero> ok, let's start the roll-call
16:00  * dertobi123 his here and grabs another beer
16:00  * lu_zero is present
16:00  * Naib joins Sput. 
16:00 <@dertobi123> is*
16:00 < Naib> I has beer Sput
16:00  * lu_zero takes some water
16:01 < zmedico> dev-zero: yeah
16:01 <@dev-zero> zmedico: cool :)
16:01  * ulm is here
16:01 <@leio> here
16:01 <@dertobi123> so, we're waiting for Cardoe and Betelgeuse, right?
16:02 <@dev-zero> yes
16:02 <@dev-zero> Cardoe: ping?
16:02 <@dev-zero> Betelgeuse: ping?
16:02 <@Cardoe> pong
16:02 <@dertobi123> heya Cardoe
16:03 <@dev-zero> just updated the agenda again to match the one I posted
16:03 <@dev-zero> Betelgeuse: please call in asap
16:03 <@dev-zero> let's get started
16:03 -!- Ron_157 [n=Matt@DSL01.212.114.237.250.ip-pool.NEFkom.net] has joined #gentoo-council
16:03 <@dev-zero> Donnie decided to resign
16:04 <@Cardoe> dertobi123: hey
16:04 <@Betelgeuse> dev-zero: \o/
16:04 <@dev-zero> the next person on the list are: ssuominen and ulm
16:04 <@dev-zero> Betelgeuse: heya :)
16:04 <@dev-zero> ssuominen decided to leave the spot to ulm
16:05 <@dev-zero> so, unanonymous vote please
16:05 <@Cardoe> ulm: are you present?
16:05 <@dertobi123> he is
16:05 < ulm> Cardoe: yep
16:05 <@lu_zero> ulm =)
16:05 <@Cardoe> yes for ulm
16:05 <@dev-zero> yes for ulm
16:05 <@dertobi123> welcome ulm ;)
16:05 <@lu_zero> ulm welcome =)
16:06 < ulm> thanks :)
16:06 <@leio> yes for ulm
16:06 <@Betelgeuse> yes
16:06 -!- mode/#gentoo-council [+o ulm] by dev-zero
16:06 <@dev-zero> good, that was quick :)
16:07 <@dev-zero> can we move to the next topic?
16:07 <@dertobi123> yes
16:07 <@dev-zero> zmedico: what's the progress on eapi-3 implementation in portage?
16:08 <+tanderson> I'm here
16:08 < zmedico> dev-zero: almost nothing so far. only stuff is what you've given me patches for. however, I hope to make time to get it all done pretty soon.
16:08 <+tanderson> dev-zero: yeah, I've been logging the whole thing
16:09 -!- aknm [n=aknm@5acabe73.bb.sky.com] has joined #gentoo-council
16:09 -!- spatz [n=spatz@unaffiliated/spatz] has joined #gentoo-council
16:09 <@dev-zero> zmedico: ok, thanks
16:10 <@dev-zero> I guess there's nothing more we could do besides start coding to speed up the process, is there?
16:10 <@Betelgeuse> zmedico: Jugding from past experience that's anything from days to a year :)
16:10 <@Betelgeuse> dev-zero: Writing code is recommended for everyone.
16:10 < zmedico> Betelgeuse: yeah, but I don't expect the eapi3 stuff to be much work. just need to make time for it
16:11 <@dev-zero> Betelgeuse: maybe we should point out that many features are not that hard to implement, just need a bit of bash and/or python knowledge
16:11 <@lu_zero> basically patches welcome?
16:11 <@Betelgeuse> dev-zero: Indeed.
16:11 <@leio> lets rewrite portage in C, then I can help *g
16:11 <@Betelgeuse> dev-zero: The barrier of entry for some of them shouldn't be that high and maybe it would encourage further contributions.
16:11 -!- xake [n=xake@liten.csbnet.se] has joined #gentoo-council
16:11 <@lu_zero> leio ok =P
16:11 <+tanderson> I could take a look at making some patch for the bash things
16:11 <+tanderson> *patches
16:11 < Calchan> zmedico, how about asking your recruit to help you ?
16:12 -!- marens [n=marens@unaffiliated/marens] has joined #gentoo-council
16:12 < zmedico> Calchan: good idea, will do
16:12 <+tanderson> zmedico: I never did get around to rewriting ebuild.sh to be more eapi-friendly,did I :/
16:13 < zmedico> well, it works fine as is
16:13 <+tanderson> zmedico: yeah, but it's a tad messy
16:13 -!- spaceinvader [n=desktop@unaffiliated/spaceinvader] has joined #gentoo-council
16:13 < zmedico> yeah, could use some cleanup
16:14 <@lu_zero> zmedico you may try to point some tasks you'd like to see patches in your blog
16:14 <@Betelgeuse> dev-zero: Next topic time?
16:14 <@dev-zero> Betelgeuse: I think so
16:14 <@dev-zero> zmedico: jup, that would be great :)
16:14 < zmedico> lu_zero: good idea, will do
16:14 <@dev-zero> zmedico: thanks for reporting :)
16:14 <@dev-zero> ok, next topic
16:15 < zmedico> you're welcome
16:15 <@dev-zero> removing old eclasses
16:16 <@dev-zero> as Betelgeuse pointed out it would only be a problem for packages where the vdb entry has been generated with a very old portage version
16:16 <@dev-zero> or when using a portage version older than 2.1.4
16:16 <@dev-zero> is that correct?
16:16 <@leio> do I get it right that if user has packages using eclasses that have been removed, all he has to do is upgrade portage before upgrading or removing other stuff to get the environment.bz2 used that has been saved for lots more than since a year?
16:16 <@ulm> zmedico: when exactly was Portage 2.1.4 stabilised?
16:16 <@dev-zero> leio: I did understand it that way as well, yes
16:17  * dertobi123 too
16:18 <@dev-zero> it seems something around a year ago
16:18 < peper> and how horribly <2.1.4 fails at uninstallation? i.e. is it just a case of upgrading and trying again or something is screwed up in the process?
16:18 < zmedico> ulm: around march, 2008
16:20 <@dertobi123> it was mid-february last year according to the changelog
16:20 <@leio> I think it would be fine to remove eclasses once we map out which ones are used by portage and its deps and limit any compatibility breaking impact there
16:20 <@ulm> dertobi123: 2008-02-18 according to bug 210031
16:20 < Willikins> ulm: https://bugs.gentoo.org/210031 "sys-apps/portage-2.1.4.4 stable request"; Portage Development, Conceptual/Abstract Ideas; RESO, FIXE; zmedico@g.o:dev-portage@g.o
16:20 <@Cardoe> The packages that don't know what repo they were generated with are generated with which Portage?
16:20 -!- lavajoe [n=joe@mail.boulder.swri.edu] has joined #gentoo-council
16:20 <@Cardoe> the ones that say "[?=>0]"
16:21 <@dertobi123> ulm: yep, that was the one i found, too
16:21 <@Cardoe> cause if that's 2.1.4, then I still have packages that were recently upgraded with that version
16:21 <@dev-zero> zmedico: ^^ ?
16:21 -!- marens [n=marens@unaffiliated/marens] has left #gentoo-council []
16:22 <@Betelgeuse> Yeah many people never run emerge -e world
16:23 <@Betelgeuse> I don't really see what's the big benefit of being able to rm a couple files?
16:23 <@dertobi123> i don't, too
16:23 <@dertobi123> mark them as deprecated and we're done
16:24 < zmedico> Cardoe: repo labels in /var/db/pkg are in >=portage-2.1.5 iirc
16:24 <@dev-zero> maybe make anything not needed for uninstall unusable? (by using die in src_install/src_compile/etc)
16:24 <@Cardoe> zmedico: and what file is that stored in?
16:24 <+tanderson> dertobi123: you could even put a die in pkg_setup or something 
16:24 <@Betelgeuse> dev-zero: That's been the practise
16:24 <@dev-zero> Betelgeuse: ah, ok
16:24 <@Betelgeuse> dev-zero: see debug.eclass
16:25 < zmedico> Cardoe: /var/db/pkg/*/*/repository
16:25 <@dertobi123> tanderson: exactly ...
16:26 <@dev-zero> well, we already drop enough bugs on our users, I don't like risking even more
16:26 <@dertobi123> agreed
16:26 <@Cardoe> so we can see what directories are missing /var/db/pkg/*/*/repository
16:26 <@dev-zero> Cardoe: but that isn't related to environment.bz2, is it?
16:27 <@Cardoe> and that'll give you a guess at seeing that that some packages are made with an old portage
16:27 <@Cardoe> dev-zero: I thought we were talking about when eclasses were included in environment.bz2
16:27 <@leio> by my understanding that's a lot before 2.1.4
16:28 <@dev-zero> Cardoe: yes, but repository is included with portage >=2.1.5 and environment.bz2 got generated by even older portage but not used
16:28 <@dev-zero> ok, can we come to a conclusion here?
16:28 <@dertobi123> guess we can quickly vote on that
16:28 <@Cardoe> dev-zero: right so my check tells you what vdb's were created with 2.1.4 and older
16:28 <@dev-zero> leio, lu_zero, ulm, Cardoe: what's your opinion on this?
16:28 <@leio> repeating from before: I think it would be fine to remove eclasses once we map out which ones are used by portage and its deps and limit any compatibility breaking impact there
16:29 <@ulm> if it's only a little more that a year ago, then IMO it's too risky to allow removing
16:29 <@lu_zero> as a general rule I'd keep 6 month of deprecation, make sure an upgrade path is available as leio said
16:29 <@ulm> after all, the benefit is very small
16:29 <@lu_zero> and do the cleanup if is deemed useful
16:29 <@Cardoe> I'm with ulm on this one
16:30 <@dev-zero> that leaves us with "wait a bit longer to remove eclasses"?
16:30 <@leio> does that have to be mean about all eclasses?
16:31 <@dev-zero> well, graph theory is non-trivial, so I don't consider myself smart enough to ensure that a given eclass doesn't show up in a dep-tree when updating portage
16:31 <@leio> there's a difference between eclasses that portage and its deps depend on and eclasses that only a limited set of packages used, and all those revisions of such packages were removed two years ago
16:31 <@dertobi123> leio: eclasses which were introduced after portage-2.1.4 went stable might be an exception (though the number of those eclasses isn't that large, i guess)
16:32 <@ulm> dertobi123: that makes sense
16:32 <@leio> maybe the community can bring example of which eclasses they'd like to remove
16:33 <@dev-zero> kde has been mentioned
16:33 <@Betelgeuse> GLEP 55 allows us to use a new eclass approach etc
16:33 <@Betelgeuse> so the old ones can just be left to rot
16:33 <@dertobi123> unused eclasses are marked as deprecated and it's functionality except uninstall-related stuff removed, and that for a minimum of 2 years before final removal
16:34 <@dertobi123> what about that? (and the exception i stated above)
16:34 <@ulm> dertobi123: +1
16:34 <@dev-zero> dertobi123: sounds good
16:34 <@lu_zero> dertobi123 fine
16:34 <@dertobi123> so we have 4 yes on that :)
16:34 <@dertobi123> any "no"s?
16:35 <@Betelgeuse> I don't really see a need for a time limit like that
16:35 <@leio> I take it as something that can be relaxed once more time has passed
16:35 <@dev-zero> where do we write that down?
16:35 <@Betelgeuse> I would just leave the empty ones there
16:35 <@dertobi123> dev-zero: meeting agenda, dev-manual, anywhere else?
16:35 <@dev-zero> dev-manual mainly I think
16:36 <@Betelgeuse> Otherwise if we keep the flat namespace someone might accidentally resurrect an eclass with different behavior
16:36 <@dev-zero> tanderson: can you do the update perhaps?
16:36 <@Cardoe> dev-zero: dev-manual for sure
16:36 <@Cardoe> dev-zero: I'd file a ticket for that
16:37 <@Cardoe> alright let's get moving on the agenda items
16:37 <@Cardoe> got about 20 min left
16:37 <@dertobi123> yeah
16:38 <@dev-zero> good
16:38 <@leio> instead of keeping empty ones, there could be a cvs or git hook to disallow resurrection
16:39 <+tanderson> dev-zero: for eapi 3?
16:39 <@leio> however initially I thought the intention of keeping empty ones around idea was to keep the inherit calls from old package revisions being uninstalled to not error out on missing eclasses
16:39 <+tanderson> dev-zero: or deprecating unused eclasses?
16:39 -!- codejunky [i=jan@nerdig.org] has joined #gentoo-council
16:39 <@dev-zero> tanderson: deprecating unused eclasses
16:39 <+tanderson> right.
16:40 <+tanderson> can do. Probably after my summary is finished though :)
16:40 -!- codejunky [i=jan@nerdig.org] has left #gentoo-council []
16:40 <+tanderson> leio dislikes waiting :p
16:40 <@dev-zero> tanderson: otherwise please open a bug for it
16:40 <+tanderson> ok
16:40 <@dev-zero> leio: can we discuss it on-list or later on?
16:40 <@leio> sure.
16:40 <@dev-zero> ok
16:40 <@leio> (I'll be gone till Monday after meeting though, fyi)
16:41 <@dev-zero> let's move on: Handling EAPI versioning in a forwards-compatible way
16:41 <+tanderson> If I don't get it done this weekend I'll file a bug. Usually I just poke halcy0n with patches
16:41 <@dev-zero> ok, Ferris proposed a good way to maybe shed some light on that issue
16:42 <@dev-zero> are we all clear what the problems are GLEP55 tries to solve?
16:42 <@leio> Not completely.
16:42 <@lu_zero> dev-zero some are still disputing them
16:42 -!- windzor [i=windzor@goatse.baconsvin.dk] has quit [SendQ exceeded]
16:43 <+tanderson> mmm
16:43 <+tanderson> what specifically is not understood about the purpose?
16:43 <@leio> some things it tries to solve seem like things that are not a problem that needs solving
16:43 <@ulm> dev-zero: the glep in its current wording doesn't make clear what the problem is
16:44 < ciaranm> what's unclear about the four bullet points describing the problems?
16:44 <+tanderson> leio: -scm?
16:44 <@lu_zero> tanderson from what I see from a cursory read the items listed as "problems" aren't believed to be problems at all
16:44 < NeddySeagoon> GLEP 55 could use some editorial work.  There is more useful info in emails than in the glep
16:44 <@leio> -scm does not need an extension change.
16:44 <@dertobi123> ulm: right
16:44 < ciaranm> please point out which of the four bullet points listing the problem needs clarifying
16:44 <@leio> this discussion will go downhill from this point.
16:44 <@dertobi123> as usual
16:44  * dertobi123 sighs
16:45 <+tanderson> leio: we do not want per-package eclasses at all? I do certainly
16:45 < ciaranm> if someone could say which of the bullet points they think isn't clear enough, i'm sure the glep could be updated to address that
16:45 < bonsaikitten> ciaranm: they do not list the problem but a potential solution
16:45 <@leio> tanderson: good for you. There are other ways to achieve that.
16:45 <@Betelgeuse> Who wants to us VAR+="addition" in global scope?
16:45 <@Betelgeuse> s/us/use/
16:45 -!- alip [i=alip@unaffiliated/alip] has joined #gentoo-council
16:46 < ciaranm> bonsaikitten: the four bullet points under the Problem title, please
16:46 <@dev-zero> bonsaikitten: no, they don't. Please read "current behaviour" in the glep
16:46 < bonsaikitten> which version are we talking about btw?
16:46 <@Betelgeuse> http://www.gentoo.org/proj/en/glep/glep-0055.html
16:47 < bonsaikitten> Betelgeuse: that's not the current version
16:47 <@lu_zero> ciaranm I'd say
16:47 <@lu_zero> all.
16:47 <@lu_zero> either by their vague definition
16:47 <@lu_zero> or their circularity
16:47 < ciaranm> lu_zero: ok, let's start with "Change the behaviour of inherit in any way (for example, to extend or change eclass functionality)". what's the problem with that one?
16:47 <@lu_zero> ciaranm case 1: vague definition
16:47 < NeddySeagoon> lets not waste the meetings time - lets just fix the glep
16:47 < ciaranm> lu_zero: what specifically is vague, that you don't understand?
16:48 <@Betelgeuse> NeddySeagoon: Do you have a patch in mind?
16:48 < ciaranm> NeddySeagoon: that's what we're doing. we're finally getting someone to say specifically what they think needs fixing.
16:48 <@Betelgeuse> NeddySeagoon: Feel free to submit one.
16:48 < ciaranm> NeddySeagoon: if lu_zero could explain exactly what he thinks is wrong with the four bullet points, we can fix them
16:48 <@dev-zero> jup, that's the point of it
16:48 <@lu_zero> ciaranm "in any way" doesn't explain which way
16:49 <@lu_zero> and could be merged with the point 2
16:49 <@Cardoe> This is a constant struggle.
16:49 < NeddySeagoon> Betelgeuse,  as I suggested on on -ml it needs a rewrite to include much of the missing information, that is available in the emails
16:49 <@lu_zero> and there "sane" isn't defined
16:49 <@Cardoe> People are confused about some wording
16:49 < fmccor> bonsaikitten, It's dated 17 May 2009.  You have something more recent?
16:49 <@Cardoe> and they just want clarification
16:49 < ciaranm> lu_zero: so if we add in links to the pms "future eapi request" bugs that give examples of ways that people want inherit to be changed, you'd be happy with that?
16:49 <@Cardoe> there's no need to be defensive and get in a fight
16:49 < bonsaikitten> fmccor: it shows ver. 1.4, cvs has a 1.5
16:49 <@Cardoe> explain it to the person
16:49 <@Cardoe> if more then 1 person is confused.
16:49 < ciaranm> Cardoe: i'm trying to establish exactly what people don't understand or want changed
16:50 < ssuominen> ulm: to old msg, good :)
16:50 <@lu_zero> ciaranm if that manages to get less people going berserk when you proposed that in ml probably.
16:50 <@Betelgeuse> Let's go this way. Who thinks being able to use VAR+="addition" in global scope is useless?
16:50 < ciaranm> lu_zero: ok, second bullet point. "Add new global scope functions in any sane way.". what is your problem with that statement?
16:50 < NeddySeagoon> ciaranm, references to other docs are only useful if the reference docs will not change
16:50 <@ulm> Betelgeuse: nobody needs this
16:50 <@leio> "Easily fetchable EAPI inside the ebuild" sounds like a good way to start with solving the thing that is the agenda topic - "Handling EAPI versioning in a forwards-compatible way"
16:50 < bonsaikitten> Betelgeuse: nicely loaded question
16:50 < peper> bonsaikitten: it's the current version. probably some cvs/web-sync screw-up
16:50 <@leio> because it merely makes it a rule something that already is a QA policy
16:51 <@lu_zero> ciaranm define "Sane"
16:51 < ciaranm> lu_zero: would it help if we add in links to bugs where people are requesting new global scope functions?
16:51 < bonsaikitten> peper: oh fun ... so which version is authorative?
16:51 <@Betelgeuse> ulm: why?
16:51 <@lu_zero> inherit is a global scope function or not btw?
16:51 < peper> bonsaikitten: it's the same version
16:51 < ciaranm> lu_zero: inherit's an existing global scope funtion, not a new one
16:51 < drantin> it's outside a stage, so it's global
16:51 <@ulm> Betelgeuse: because you can do it in a different way?
16:51 <@lu_zero> ciaranm inherit2 would be
16:51 < ciaranm> lu_zero: and inherit has its own special problems unrelated to adding new functions
16:52 <@lu_zero> or inherit with optional parameters
16:52 <@lu_zero> anyway it's digressing
16:52 <@dev-zero> NeddySeagoon: would it be sufficient to add examples for stuff where "in any way" is written?
16:52 <@Betelgeuse> ulm: We should write longer code on purpose when we could get away with new syntax? I don't see any benefit to sticking with old bash syntax.
16:52 < ciaranm> lu_zero: bullet point 4. what don't you understand about "use newer bash features."?
16:52 < ciaranm> lu_zero: would you like a list of newer bash features that we could use?
16:52 <@lu_zero> ciaranm I think it would help a lot
16:53 < ciaranm> lu_zero: ok, easily done. and bullet point three: "Extend versioning rules in an EAPI - for example, addition of the scm suffix - GLEP54 [1] or allowing more sensible version formats like 1-rc1, 1-alpha etc. to match upstream more closely.". what's wrong with that?
16:53 <@ulm> Betelgeuse: it's no so much longer, probably on the few percent level
16:53 <@lu_zero> it's circular.
16:53 < ciaranm> lu_zero: how is it circular?
16:53 < NeddySeagoon> dev-zero, that will help.  I understand the reason for the glep and support it aims.
16:53 <@lu_zero> glep54 isn't accepted
16:53 < ciaranm> lu_zero: glep 54 is merely an example
16:53 <@dev-zero> NeddySeagoon: good, will happen then
16:54 < ciaranm> lu_zero: it also lists other examples of things we might want to do
16:54 < bonsaikitten> ciaranm: we might want to keep the status quo
16:54 <@ulm> ciaranm: who is "we"?
16:54 <@Betelgeuse> ulm: Well bash syntax evolves constantly and there's other things too. But as said a couple lines up there will be a list.
16:54 <@dev-zero> lu_zero: even your .live extension depends on it
16:54 < ciaranm> ulm: people who submit feature requests for future eapis
16:54 < NeddySeagoon> dev-zero, I can volunteer some editorial time, if that helps.
16:54 <@lu_zero> dev-zero I know
16:54 < ciaranm> lu_zero: would you like us to remove the specific examples from the third bullet point whilst we're adding them in to the other three?
16:55 <@lu_zero> ciaranm as I said 3 are vague, one is circular
16:55 < ciaranm> lu_zero: please explain the circular thing
16:55 < ciaranm> lu_zero: glep 54 is merely one example of a thing we'd like to do
16:55 < Sput> isn't the real problem g55 should be solving "how to figure out EAPI prior to sourcing", and shouldn't the discussion center around how to achieve that best?
16:55 <@dev-zero> lu_zero: ... and your glep is another example
16:55 <@Betelgeuse> I guess he means that go hand in hand but I don't see how it makes GLEP 55 worse.
16:56 <@lu_zero> dev-zero none of them are accepted
16:56 < ciaranm> lu_zero: if that bullet point instead said "Extend versioning rules in an EAPI, for example to allow version formats like 1-rc1, 1-alpha etc. to match upstream more closely", would you be happy?
16:56 < NeddySeagoon> ciaranm, do you already have glep 54 and 55 implemented in paludis ?
16:56 < ciaranm> lu_zero: they can't be accepted until glep 55 is
16:56 <@dev-zero> NeddySeagoon: yes
16:56 < ciaranm> NeddySeagoon: 54, yes. 55, more or less, although not for the specific suffixes we're asking for (it's used by exheres and kdebuild)
16:56 <@lu_zero> so you don't need glep 55 if people agree live ebuild or scm version rules are useful
16:57 -!- alip [i=alip@unaffiliated/alip] has left #gentoo-council []
16:57 <@Betelgeuse> how do you do -scm without 55?
16:57 < ciaranm> lu_zero: you need glep 55 if you want any of the bullet points, or if you want -rc support or so on
16:57 <@Betelgeuse> And no user visible breakage.
16:57 < lack> Betelgeuse: Easy -> Wait a year
16:57 <@Betelgeuse> or year wait
16:57 < NeddySeagoon> ciaranm, maybe that explains why glep55 needs to be read from the bottom up.  You are too close to the problem and perceived solution to document it well
16:57 <@leio> there is no need to wait for a year
16:58 <@leio> to introduce -scm or -live
16:58 <@dev-zero> leio: can you explain that please?
16:58 <@Betelgeuse> dev-zero: I propose having a vote on if the council thinks the problems as worthy.
16:58 < ciaranm> NeddySeagoon: stop thinking of the abstract and title as part of the main body
16:58 < peper> NeddySeagoon: just fyi, I wrote the glep :) as can be easily seen by english quality probably :)
16:58 <@dev-zero> Betelgeuse: and then update them according to what we discussed today?
16:58 < ciaranm> lu_zero: so if we update those four bullet points the ways we discussed, you'll be happy?
16:59 <@Betelgeuse> dev-zero: I only see clarifications coming out of this.
16:59 < NeddySeagoon> ciaranm I'm not. The body is very thin too
16:59 -!- xake [n=xake@liten.csbnet.se] has quit [Client Quit]
16:59 < ciaranm> NeddySeagoon: please point out specific areas where you'd like it fattened up
16:59 <@lu_zero> ciaranm I see people complaining about that so I hope that will make the thing more acceptable to that point
16:59 <@lu_zero> then there is the rest
16:59 <@leio> dev-zero: you simply have portage warn about an unrecognized atom for people with old portage. One line warning per package seen that has a -scm revision, nagging them even further to finally upgrade their package manager for doing, duh, package management. Nothing catastrophic to my knowledge.
16:59 <@Betelgeuse> dev-zero: And if you understand the problem domain you don't need any concrete wording for voting
16:59 < ciaranm> leio: did you see the mess last time that happened?
17:00 <@leio> No.
17:00 <@lu_zero> I don't see benchmarks yet I have most of the alternate proposals ruled as resource heavy
17:00 <@dev-zero> Betelgeuse: ok, agreed for the voting
17:00 <@dev-zero> ok, people
17:00 < NeddySeagoon> ciaranm, I would like objective, quantitive measurements of the main potential solutions included in the GLEP.  Some have been posted to the -ml, so they are probebly available
17:00 <+tanderson> This is a vote on the 'problem' not the solution, right?
17:00 < Calchan> what the glep really needs is summarizing what was said on the list, and I know that ciaranm is going to say that very little to none of it is valuable but many will disagree with that
17:00 <@ulm> leio: do you remember if there were any serious problems when multiple _pre, _rc, etc. were allowed?
17:00 <@dertobi123> NeddySeagoon, Calchan: agreed
17:01 <@lu_zero> and possibly reproducible by third party easily
17:01 <@dev-zero> tanderson: yes
17:01 <@leio> ulm: I don't remember, but that's a reason why I don't see it reasonable to change versioning rules to track upstream better.
17:01 <+tanderson> Calchan: I could clarify some points of the problem part. I just need concrete emails on which parts are confusing and why they are confusing
17:01 < NeddySeagoon> lu_zero, auditable anyway
17:01 <@dev-zero> ok, can we please get votes for whether the problems mentioned in the glep are worth to get solved?
17:01 <@ulm> leio: I also see no need for things like "-rc" instead of "_rc"
17:01 <@Betelgeuse> I vote yes.
17:02 < Calchan> tanderson, that's the hard part, digging the interesting stuff from all these useless flamewars
17:02 <@dertobi123> in general, yes
17:02 <@dev-zero> I vote yes.
17:02 <@lu_zero> I asked them to be clarified so count me as a yes with reserve
17:02 <+tanderson> Calchan: indeed. I'm actually considering asking for _private_ emails so I don't have all the back-and-forth
17:02 <@dev-zero> good, that makes 4
17:03 <@lu_zero> dev-zero that makes me a no if they aren't clarified
17:03 <@dev-zero> so, glep authors: please update the glep as discussed here
17:03 <@dev-zero> lu_zero: yes, I understand that
17:03  * ulm doesn't see a basis to vote about the glep in its current wording
17:03 <@Betelgeuse> ulm: We are not voting about the GLEP at all
17:03 <@dev-zero> ulm: it's not about the glep, only about the problems mentioned
17:03 <@leio> I agree there are things to solve for bullet points 1, 2 and 4, but do not agree with glep55 being the best way to do that.
17:03 <@dertobi123> ulm: we're not voting about the glep, but the problems described
17:03 <@dertobi123> i'd like to vote the next meeting on whether to approve on of the mentioned solutions
17:03 <@dertobi123> and only to vote.
17:03 <@dev-zero> leio: ok, that's the only thing we wanted to know :)
17:04 <@dertobi123> otherwise this will become a neverending story ...
17:04 <@dev-zero> dertobi123: but this discussion was probably one of the most productive ones we had about it
17:04 <@leio> I am heavily against voting on stuff that is not clear only to "get it under the carpet"
17:04 < Calchan> I'd like everybody to note that there seemed to have been an informal consensus on the third solution in the list of three that peper proposed in http://archives.gentoo.org/gentoo-dev/msg_959451961983a52129e3f41cd87eac0e.xml
17:04 -!- Thargor [n=quassel@unaffiliated/thargor] has joined #gentoo-council
17:05 <@dertobi123> leio: there are again 14 days to get those things sorted
17:05 < NeddySeagoon> ciaranm, if the council pick an in ebuild solution, how much work do you have to redo ?
17:05 <@ulm> Calchan: that would be "Easily fetchable EAPI inside the ebuild"?
17:05 < ciaranm> NeddySeagoon: we can implement anything we want in paludis. it's not portage, you know :P
17:06 <@dertobi123> ulm: plus extention change, "easily fetchable eapi inside the ebuild" wasn't mentioned in peper's mail
17:06 < NeddySeagoon> dev-zero, thats good design, break the problem into small pieces and address each piece.  Recursively as needed
17:06 < Calchan> ulm, yes with for example a .eb extension and eapi in she-bang
17:06 <@dertobi123> and that's another thing i dislike
17:06 <@dertobi123> extension* even
17:06 <@dev-zero> ok, I think that topic is over for today
17:06 < NeddySeagoon> ciaranm, I was thinking wasted effort
17:06 < peper> dertobi123: hmm?
17:06 < ciaranm> NeddySeagoon: eh, exherbo's using it anyway
17:06 <@dev-zero> reserve solutions to the problems for the next 14 days and the next meeting, please :)
17:07 < peper> dertobi123: it wasn't cause I hate it. you are free to like it :)
17:07 < NeddySeagoon> ciaranm, I think you went with eapi in filename ?
17:07 <@Betelgeuse> coding and changin wording is nothing compared to what people seem to be using for mailing on lists
17:07 <@dev-zero> is someone on the run or can we peek into the next topic?
17:07 <@dertobi123> peper: if the glep mentiones 4 solutions i'd expect to put all of them up for discussion on the ml and not to hide a solution you personally dislike.
17:07 < ciaranm> NeddySeagoon: oh, you mean, you want someone to code in-ebuild-eapi support? can do that, if the council decides it doesn't want to take the best solution
17:07 < peper> dertobi123: err, the list was labeled: "Just FYI, my order of preference of solutions is:"
17:07  * dertobi123 needs to go to bed somewhat soonish
17:08 < fmccor> dertobi123, It's in GLEP55 (alternative 3 or idea 4, depending on where you start counting)
17:08 < peper> i listed 2. and 3. just to show I can live with them
17:08 <@dertobi123> fmccor: sure
17:08 < NeddySeagoon> ciaranm, the objective evidence for 'best solution' is still missing
17:08 <@dev-zero> ok, the next topic:
17:08 <@Betelgeuse> dev-zero: I need to finish a task for tomorrow.
17:08 <@Betelgeuse> dev-zero: I will glance every once in a while
17:08 <@dev-zero> Betelgeuse: fine by me
17:09 <@Cardoe> dev-zero: we're over time.. have we decided to extend?
17:09 <@dev-zero> ok, next topic (just to get an idea).
17:09 < ciaranm> NeddySeagoon: objectively, you have to either not change version format rules ever, or force the package manager to open() every ebuild rather than no ebuilds before it can find the best version of something
17:09 < peper> NeddySeagoon: re: your editorial time, I would appreciate it and welcome patches
17:09 <@dev-zero> Cardoe: I asked, see above
17:09 <@dertobi123> we did agree on a one-hour meeting and i don't think the next topic is that important to extend the meeting
17:09 <@leio> ciaranm: incorrect, but this is not the place to discuss right now indeed.
17:09 < NeddySeagoon> peper, I can't start until Wednesday ... but ok
17:09  * antarus chuckles
17:09 <@dev-zero> dertobi123: then please say just "no" the next time I ask :)
17:10 <@dev-zero> ok then, meeting is over
