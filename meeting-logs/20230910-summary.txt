Summary of Gentoo Council meeting 2023-09-10

Agenda
======

1. Roll call
2. Status of dissolving the Foundation/moving under an umbrella org
3. Open bugs with Council participation
4. Open floor


Roll call
=========

Present: ajak, dilfridge, mgorny, sam, soap, ulm
Absent: mattst88


Status of dissolving the Foundation/moving under an umbrella org
================================================================

dilfridge notes that thanks to ulm's work, the Gentoo Foundation
trustee set is now fully selected and disambiguated, that being:

dilfridge, prometheanfire, robbat2, soap, ulm

ulm will sift through trustees@ mail archives for history of contact
with umbrellas. It was informally decided to work towards deciding on
a suitable umbrella and reaching out to them by the next meeting. ulm
expressed a preference for this happening only after the next trustees
meeting, but a date for that meeting is yet to be fixed.

dilfridge notes that the principal factor in finding an umbrella is
the status of 501c3 vs 501c6.

Open bugs with Council involvement
==================================

No bugs!

Open floor
==========

turret expresses a preference for a 501c3 umbrella, and pietinger
questions the process of merging a 501c3 corporation into a 501c6
corporation, but it is noted that the Foundation is neither status.
