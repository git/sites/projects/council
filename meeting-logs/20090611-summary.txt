Roll Call:
===========
Betelgeuse: here
Cardoe: here
dertobi123: here 
dev-zero: here
leio: here
lu_zero: here
tanderson(secretary): here
ulm: here

Topics:
===========

     - Short discussion of EAPI 3 progress.
        Zac Medico(zmedico) commented that while no progress had been made, a
        tracker bug had been made[1] for those interested in providing patches
        for and tracking the progress of the EAPI 3 implementation. Ciaran
        McCreesh noted that paludis is ready for EAPI 3 whenever the portage
        implementation is finished.

    - Default contents of ACCEPT_LICENSE(license filtering).
        GLEP23[2] provided a method for users to select what licenses they are
        willing to accept based on a ACCEPT_LICENSE configuration variable. In
        addition it provided for 'license groups' so users could accept or
        decline to use software of a certain license type. What GLEP 23 did not
        specify was the default value of ACCEPT_LICENSE.

        Conclusion:
            The council unanimously voted to have the default ACCEPT_LICENSE
            value as ACCEPT_LICENSE="* -@EULA".

    - BASH 4 in EAPI 3.
        There were three parts to this topic:
            1) Unlocking of feature requests for EAPI 3.
            2) Allowing BASH 4 features in EAPI 3 ebuilds.
            3) Allowing BASH 4 features in all ebuilds with EAPIs >= 3 after a 
               fixed amount of time in gentoo-x86(Overlays could begin use
               immediately).

        Conclusion:
            By a 4-3 decision the council voted not to open the feature list for
            EAPI 3.

    - The banning of igli(Steve Long) from #gentoo-council.
        Tiziano Muller(dev-zero) banned igli from #-council for what he called
        repeated trolling after private warnings. The ban was later reversed by
        Doug Goldstein(Cardoe) because it had not been put to a council vote as
        all bans in #-council are.

        Conclusion:
            No decision yet, the council decided to discuss this issue privately
            on the council@ alias so that precious meeting time is not spent.

    - Define EAPI development/deployment cycles.
        Various Council members expressed support for Ciaran McCreesh's EAPI
        development guidelines as outlined in [3]. However, the discussion
        reached no conclusion and quickly spiraled into a discussion of the
        removal of Ciaran McCreesh's bugzilla privileges.

    - Removal of Ciaran McCreesh's(ciaranm) bugzilla permissions.
        At some point in the last year ciaranm's bugzilla permissions were
        removed. He filed a bug about the issue(#273759) and was talking about
        moving PMS off of Gentoo Infrastructure, a move that some council
        members were strongly opposed to. When asked about the permissions,
        Ciaran had no objections to waiting a few days for the infra to complete
        an investigation into who removed the access and for what reason.

        Conclusion:
            The council voted to reinstate Ciaran's editbugs privileges. Ned
            Ludd (solar) noted that infra will investigate who removed the privileges
            in the first place, and asked for not changing bugzilla privileges before
            this is completed.

[1]: https://bugs.gentoo.org/show_bug.cgi?id=273620
[2]: http://www.gentoo.org/proj/en/glep/glep-0023.html
[3]: http://archives.gentoo.org/gentoo-dev/msg_d3a4758c455fded00608e891f525d3cc.xml
