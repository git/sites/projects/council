Summary of Gentoo council meeting 13 November 2022

Agenda
======

1. Roll call
2. GLEP78 update
3. GLEP76 update
4. Open bugs with council participation
5. Open floor

Roll Call
=========

Present: ajak, dilfridge, gyakovlev, mattst88, mgorny, sam, ulm

GLEP78 Update
=============
mgorny proposed some changes to GLEP78. Most are clarifications.


Motion: Approve mgorny's 6 patches to update GLEP78 [1]
Accepted 7-0

[1] https://archives.gentoo.org/gentoo-dev/message/fa090f351778f1644964d096c20933dd

GLEP76 update
=============
robbat2 proposed changes to GLEP76's "name policy" [1].

Various concerns were raised during the Council meeting:

  - mattst88 asked what is the objective in requiring "real names" for
    contributors. In other words, what problem are we hoping to avoid by
    implementing the proposed changes?

    robbat2 replies that we semi-inherited the "real name requirement" from
    the Kernel DCO when Gentoo adopted it, and mattst88 questions what the
    analogous situation for Gentoo would be, given that the Kernel DCO was
    created in response to a lawsuit over allegations that SCO-owned UNIX code
    had been included in Linux, and it seems unlikely that e.g. a few lines
    of a PKGBUILD file copied into an ebuild would lead to a similar
    situation.

    robbat2 replies later during open floor to say that "analogous situation
    for Gentoo exists around *novel* code that might be taken from a
    propietary source". (In other words, the focus is not on ebuilds, but
    Gentoo-hosted software projects and large patchsets)

  - ulm, in reply to a response from robbat2 about the meaning of "records"
    suggests that we should clarify that we mean "government records". robbat2
    says that not all governments have searchable records. mgorny asks what
    would this search prove. CatSwarm (a contributor to the GLEP) says it's
    dubious to think that government records are easily accessible when GLEP76
    doesn't require a contributor to specify their residence.

  - CatSwarm says that the changes being proposed are a major change in tone
    to what they contributed to.

  - ajak and CatSwarm suggest that if the discussion should move back to the
    mailing list.

Motion: Table the issue and continue discussion on the mailing list
Accepted 7-0

    After the meeting closes, there appears to be agreement that those with
    concerns will take them to the mailing list discussion before the next
    council meeting.

    11:50 <@ajak> i really think we should move the discussion to the ML
    11:50 <@ulm> ajak: and we will
    11:50 <@ajak> so, please, everyone with concerns about the GLEP, please
                  take concerns to gentoo-project where we've been having this
                  discussion since july

[1] https://gitweb.gentoo.org/data/glep.git/commit/?h=glep76&id=139198d2e8560f8dfb32c8f4c34a3e49d628b184

Open bugs with council involvement
==================================
- Bug 729062 "Services and Software which is critical for Gentoo should be developed/run in the Gentoo namespace"
  This was recently reassigned to council@ due to Whissi's retirement.

  Whissi assigned the bug to himself 2021-05-09 in order to remove it from
  future Council discussions until he followed up with jstein@.

  No updates.

- Bug 876921 "Vote on GLEP 68 update to 1.3"
  Votes cast on bugzilla. Accepted 7-0.

Open floor
==========
arthurzam requests that the Council meeting logs contain a mention of any bugs
the Council voted on between meetings. We think we do that, and agree to keep
bugs open until the next council meeting to ensure they're documented.

More discussion of GLEP76 during open floor, and after the meeting ends.

This work is licensed under the CC BY-SA 4.0 License.
