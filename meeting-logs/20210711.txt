<@ulm> everyone o.k. with me chairing?
<+soap> yup
<@mgorny> sue
<@mgorny> sure*
<@ulm> agenda is here:
       https://archives.gentoo.org/gentoo-project/message/c05eb35e7048e2f0f8ee160e3af38c89
<@ulm> so, welcome everyone, especially the new members                 [21:01]
<@ulm> !proj council
<willikins> (council@gentoo.org) dilfridge, gyakovlev, marecki, mattst88,
            mgorny, sam, ulm
<@ulm> 1. Roll call
* mgorny here
* gyakovlev here
* ulm here
* dilfridge here
* Marecki here
* soap here (proxy for mattst88)
<@ulm> sam_ is still missing                                            [21:02]
<+soap> give it another minute
<@ulm> sure
* sam_ here
<@sam_> (here now)
<@ulm> everyone here then
<@ulm> 2. Constitute the new council
<@ulm> - Decide on time of meetings. The previous council had its meetings on
       the 2nd Sunday of every month at 19:00 UTC                       [21:03]
<@ulm> does that work for everyone?
<+soap> shall we vote on it?
<@dilfridge> works for me
<@gyakovlev> wfm
<@ulm> yeah, let's have a quick vote                                    [21:04]
* dilfridge yes
* soap yes
* ulm yes
* gyakovlev yes
* sam_ yes
* Marecki yes
* mgorny yes
<@ulm> unanimous
<@ulm> - Vote for continuing last council's workflow considering sending a
       call for agenda items (two weeks in advance), sending the agenda (one
       week in advance) and have the meeting focussed, i.e. have major
       discussions on the gentoo-project mailing list prior to the meeting
* dilfridge yes
<@ulm> anyone wants to discuss this?
* Marecki yes
* mgorny yes
* soap yes
* gyakovlev yes
* sam_ yes
* ulm yes
<@ulm> assuming the vote is for the item, not for having a discussion on it :)
                                                                        [21:05]
<@ulm> unanimous
<@ulm> - Appoint chairmen for this term's meetings
<@ulm> I could take August as well
<@mgorny> i'll take Sep+Oct
<@dilfridge> I can do NovDc
<@sam_> it doesn't particularly bother me other than ideally not being first
        to find my feet as a new member                                 [21:06]
<@Marecki> Jan and Feb work for me
<@gyakovlev> marchapr - me
<+soap> ok, matt will do the remainder
<+soap> wait, sam hasnt said yet                                        [21:07]
<@gyakovlev> that's the price of soaproxy  =)
<+soap> :D
<@gyakovlev> doing 4 chairs
<+soap> sam: may/june, matt: july?
<@ulm> june is last
<@sam_> that wfm, and we can speak with matt and figure it out if needed
<@mgorny> Marecki and sam_ haven't been on the council yet, so maybe one month
          for each of them?
<@ulm> but you could split
<@sam_> mgorny: that would be nice                                      [21:08]
<@gyakovlev> it can be changed if needed, it's not set in stone.
<@ulm> sam_: adding you for may/june now, and we update it later
<@sam_> ok, fine with me, thanks
<@Marecki> I'm used to doing this sort of work, I don't mind.
<@ulm> 3. GLEP 82 update
<@ulm>
       https://archives.gentoo.org/gentoo-dev/message/625505e15adfb532bf961076ad5f79ab
<@gyakovlev> +eapis-testing = <space-separated EAPI names>              [21:09]
<@gyakovlev> + Specifies one or more EAPIs that must not (yet) be used in
             ebuilds
<@ulm> basically a new "eapis-testing" key in layout.conf
<@mgorny> i don't think anyone has raised any concerns, so straight to vote?
<@ulm> yeah, unless anyone wants to discuss it?
<@gyakovlev> not this one imo.
<@sam_> yes, i don't think anyone has been bothered by this at all, created
        out of necessity (even if it's a rare occasion)
<@Marecki> Feels like a reasonable thing to do, let's go straight to voting
<+soap> yup, lets vote                                                  [21:10]
<@dilfridge> it wasnt that rare before you started stabilizing :P
<@ulm> motion: reapprove GLEP 82
* dilfridge yes
* mgorny yes
* ulm yes
* gyakovlev yes
* soap yes
* Marecki yes
* sam_ yes
<@ulm> unanimous
<@ulm> 4. Old EAPIs
<@ulm> - Deprecate EAPI 6
<@ulm>
       https://archives.gentoo.org/gentoo-project/message/4a504a0b7aa9199bac3ebcaf54064841
<@ulm> do we need a discussion?
<@dilfridge> sounds like a good idea (but will be a load of work)
<@ulm> let's vote then, the motion is to deprecate EAPI 6               [21:11]
<@mgorny> i don't think there are any blockers for it
* soap yes
<@sam_> kernel-2 was the last major eclass
* dilfridge yes
* sam_ yes
* gyakovlev yes
* ulm yes                                                               [21:12]
<@Marecki> All that effectively means that we must not have any new ebuilds
           using it adding to the tree, doesn't it?
<@ulm> basically, yes
* mgorny yes
* Marecki yes
<@dilfridge> you should reasonably avoid it unless absolutely necessary
<@gyakovlev> Marecki: but minor revbumps is ok though
<@gyakovlev> like really minor
<@ulm> that's 7 yes votes, if I've counted correctly
<@ulm> unanimous again
<@dilfridge> basically also voted yes, so 8
<@dilfridge> sorry
<@ulm> - Update policy for banning EAPIs / ban EAPI 5
<@ulm> I had proposed to change the rule                                [21:13]
<@ulm> namely, "A deprecated EAPI is considered banned when the Gentoo
       repository no longer contains any ebuilds using it."
<@ulm> but bman has raised objections and asked to ban EAPI 5 instead   [21:14]
<@Marecki> I would say it's a reasonable change. I mean, what's the point of
           "banning" EAPI 5 if we still have all those ebuilds using said EAPI
           in the tree?
<@mgorny> just to be clear, 'ban' in the old meaning of 'developers must not
          add new ebuilds using it unless absolutely necessary'
<@dilfridge> sounds ok to me... I really see no reasonable scenario why
             someone would have to readd an ebuild in the last minute
<@gyakovlev> what about experimental eapis what we have no ebuilds in
             ::gentoo?
<@sam_> mgorny: thanks, I was going to ask the distinction
<@gyakovlev> just asking, but seems like reasonable suggestion          [21:15]
<@dilfridge> do any still exist? well outside arfrever?
<@Marecki> mgorny: Isn't that what we intend to mean by deprecating an EAPI?
<@sam_> honestly, I think this isn't really a problem either way (nobody was
        adding new EAPI=4 ebuilds towards the end)
<@gyakovlev> dilfridge: idk, hence the question =)
<@sam_> so I'm fine with the change if it simplifies the picture
<@mgorny> i'm for keeping the old split of 1) deprecate (i.e. light 'please do
          not add'), 2) ban (i.e. strong 'do not add unless you have to'), and
          3) ban in layout.conf (i.e. breakage if added)
<@ulm> maybe we just vote on the policy change? if it fails, we can deprecate
       EAPI 5 the old way
<@dilfridge> so essentially                                             [21:16]
<@dilfridge> the policy change means, after the last ebuild is removed, 
<@dilfridge> the eapi can be added to the banned list in metadata 
<@sam_> mgorny: I feel like by the time we're at 2), we've got big problems
        and there shouldn't be any reasons to anyway
<@mgorny> dilfridge: imo the policy change eliminates step 2
<@dilfridge> err                                                        [21:17]
<@sam_> that's how i understood it
<@mgorny> i.e. Council formally banning EAPI for use in new ebuilds (except
          for important bumps)
<@dilfridge> so we'd still have to vote to add it to metadata? that's kinda
             pointless then
<@mgorny> i think adding to metadata can be decided separately
<@ulm> no vote to add it to layout.conf
<@ulm> that would be automatic
<@ulm> when the last ebuild is gone
<@mgorny> ulm: let's split this into two votes                          [21:18]
<@mgorny> one for removing step 2, another for automatically adding when last
          ebuild is gone
<@ulm> ok
<@Marecki> mgorny: Seconded.
<@ulm> motion: "A deprecated EAPI is considered banned when the Gentoo
       repository no longer contains any ebuilds using it."
<@mgorny> ulm: which one is that?
<@Marecki> ulm: Motion seconded.
<@ulm> mgorny: first part of motion?                                    [21:19]
<@ulm> or what was your idea there? please reword then
<@mgorny> sec
<@Marecki> Looks like the second one to me, actually.
<@Marecki> Guess it does need a rewording.                              [21:20]
<@sam_> none of us are English or Italian AFAIK so we can be patient ;)
<@mgorny> maybe "Council will no longer declare EAPIs banned for new ebuilds
          before the last ebuild using the EAPI is gone"                [21:21]
<@dilfridge> uh?
<@mgorny> it's hard, man
<@mgorny> or maybe...
<@sam_> do we want to postpone this for now, given we're not close to banning
        EAPI 5 anyway?                                                  [21:22]
<@mgorny> "the EAPI deprecation workflow is simplified so that there is no
          intermediate ban between deprecation and complete removal of EAPI"
<@mgorny> or s/ban/step/
<@sam_> ok
<@ulm> let's go for that, with s/ban/step/
<@dilfridge> somehow I feel this just gets more confusing
<@ulm> please vote
* mgorny no                                                             [21:23]
<@sam_> I think this is essentially how we've treated the process, fwiw
* ulm yes
<@sam_> it aligns the rule more with reality
* Marecki yes
* dilfridge yes
* Marecki  <
  https://libera.ems.host/_matrix/media/r0/download/libera.chat/7a3b13a1932f82274af14cc7c6975d75eccc7594/message.txt
  >
<@dilfridge> ugh
<@sam_> let me paste Marecki's message for the benefit of the log       [21:24]
<@sam_> "BTW. Here is how I have thought about this to date. Consider EAPI n,
        then:
<@sam_>  - EAPI n+1 comes out - not much change
<@sam_>  - EAPI n+2 comes out - "please don't use EAPI n"
<@sam_>  - EAPI n deprecated - "don't use it unless absolutely necessary",
        active clean-up
<@sam_>  - EAPI n banned - all remaining content in tree dropped, possible
        loss of support at PM level"
<@sam_> (it got Matrix'd into a link)
<@dilfridge> Marecki: what you call deprecated is basically what we called
             banned so far
<@ulm> yeah, but the n+2 step has a time component as well
<@Marecki> I mean seriously, what's the point of an intermediate step if it
           doesn't actually enforce anything                            [21:25]
<@ulm> for the upgrade path
<@sam_> so does an EAPI become unbanned if I restore an ebuild?
<@mgorny> Marecki: technically, the intermediate step is when we could
          actually take action when people deliberately add old EAPIs
<@mgorny> i.e. deprecated = kindly please
<@mgorny> banned = you must not
<@mgorny> removed = you can't or repoman/pkgcheck will complain         [21:26]
<+soap> which hasnt ever happened yet?
<@sam_> that's the problem for me
<+soap> (removed, that is)
<@sam_> i don't think we've fully utilised the old system anyway
<@ulm> so, we were in the middle of a vote
<@Marecki> Well, seeing as how many EAPI 5 ebuilds we have still got in the
           tree, I am not entirely convinced this old approach has been
           effective. This is probably not the time to discuss this, though.
<@ulm> should we abort it?                                              [21:27]
<@mgorny> Marecki: eapi 5 hasn't been banned yet
<@ulm> gyakovlev: soap: sam_: ^^
* soap yes
<@Marecki> mgorny: I know - and it basically means that "kindly please" hasn't
           had much effect.
<@ulm> 4 yes, 1 no so far                                               [21:28]
<@gyakovlev> I can vote ofc but it seems poorly discussed/formulated tbh/
<@ulm> we'll have a second motion on this
<@sam_> I feel a bit reluctant to keep the status quo given we've been banning
        at 0 ebuilds
<@sam_> anyway
* sam_ yes
<@mgorny> sam_: we've been banning earlier
<@mgorny> basically when the count has been small enough and we didn't want
          people increasing it last minute                              [21:29]
<@sam_> oh I see
<@gyakovlev> that's how I remember it and someone had to witch-hunt remains
             till count=0
<@sam_> sorry, I got confused
<@dilfridge> ok here's a motion for a vote: "after the last ebuild of a
             deprecated EAPI has been removed from ::gentoo, the EAPI can be
             immediately banned in layout.conf"                         [21:30]
<@sam_> I think I'm inclined to vote no, actually, but I'm wondering if we
        should actually defer this for a bit because this seems.. muddy
<@gyakovlev> ^ that looks more sensible tbh
<@ulm> gyakovlev: you abstain then?
<@mgorny> dilfridge: that was intended to be the second part, yes
<@dilfridge> excellent
* gyakovlev yes
<@gyakovlev> ulm: ^                                                     [21:31]
<@mgorny> ulm: maybe we should retract the motion as it's unclear
<@mgorny> and i feel like some people don't really knw what they've voted for
<@sam_> I think we've got several texts floating around
<@ulm> so, 6 yes, 1 no, motion passes
<@ulm> *sigh*
<@gyakovlev> we should have posted 2 items but voted separately..
<@sam_> let's rewind for a moment, state both motions, then vote --- or we
        postpone this for the next meeting
<@mgorny> either way, it doesn't really matter unless we actually ban an EAPI
<@mgorny> ok, so maybe let's vote OLD/NEW                               [21:32]
<@sam_> I don't think this is going to be easy to follow in the log at all
<@ulm> the previous motion was: "the EAPI deprecation workflow is simplified
       so that there is no intermediate step between deprecation and complete
       removal of EAPI"
<@ulm> accepted with 6 yes votes and 1 no vote
<@sam_> (I think I did actually change to no)
<@ulm> 5 yes votes and 2 no votes (from mgorny and sam_)                [21:33]
<@sam_> thanks!
<@mgorny> OLD: 3 steps: 1) deprecate via layout.conf; 2) ban via policy; 3)
          ban via layout.conf (when last ebuild is gone)
<@ulm> just a moment please
<@mgorny> NEW: 2 steps: 1) deprecate via layout.conf; 2) ban via layout.conf
          (when last ebuild is gone)
<@sam_> can I suggest we adjourn for 10 minutes, come back with fresh heads,
        vote on the OLD/NEW?
<@mgorny> i can try to quickly grep for dates on past EAPIs             [21:34]
<@gyakovlev> if mgorny's explanation with OLD NEW is correct change my vote to
             no please.
<@gyakovlev> ulm: ^
<@ulm> how about this: "if less than 2% of ebuild in the gentoo repository are
       using a deprecated EAPI, no more ebuilds of that EAPI must be added
       unless absolutely necessary"
<@sam_> mgorny: dates should be in layout.conf but numbers would be useful on
        EAPI #s I suppose
<@Marecki> sam_: Umm, we've just voted on NEW haven''t we.
<@ulm> 2% would be about 600 ebuilds currently
<@sam_> Marecki: given it was supposed to be two motions..              [21:35]
<@mgorny> actaully, there's also a third motion for banning EAPI 5 (if we stay
          with OLD workflow)                                            [21:36]
<@Marecki> sam_: The second vote was on whether we'd have to vote on banning
           an EAPI at count=0 or whether it would happen automatically.
<@ulm> it would clarify that adding ebuilds when almost all of that EAPI are
       gone is a big no-no
<@sam_> Marecki: I see, thanks
<@ulm> except security bumps and other urgent matters
<@sam_> honestly, it gets to the point where if we're security bumping
        something on EAPI (then 4), we consider last-riting it instead if it's
        too hard to port                                                [21:37]
<@sam_> because it shows nobody cares
<@mgorny> so EAPI 4 example:
<@sam_> so I think we shouldn't worry too much
<@mgorny> 2018-04-08 council meeting banned EAPI 4
<@mgorny> 2020-11-26 last ebuild gone (and added to layout.conf)
<@mgorny> (2015-10-11 deprecated)
<@ulm> yes, so what was the point of banning it?                        [21:38]
<@mgorny> so there's rouhgly 2.5 yr period when we were cleaning it after the
          ban
*** tiotags (~tiotags@user/tiotags) has joined channel #gentoo-council
<@Marecki> Point of order, have we got official, formal definition of
           "deprecating" and "banning" an EAPI anywhere?
<@Marecki> ulm: My point exactly.
<@ulm> Marecki: it's in some old log
<@ulm> 2011-ish I guess                                                 [21:39]
<@sam_> In reality, I feel like banning EAPI 5 now (move from "politely,
        please stop" to "really, don't") would fix any woes I have with the
        current tree state.
<@gyakovlev> I think it's clear at this point this item needs more
             discussion/formulation/definition.
<@Marecki> ulm: So nothing we could easily point users to, then.
<@sam_> yes
<@mgorny> i'll crunch ebuild nos in a minute
<@sam_> I'm still happy to adjourn if it'd let us sort it out this week.
                                                                        [21:40]
<@mgorny> i'd suggest we defer this and discuss on the ml
<@sam_> Give mgorny some time to find numbers, ulm to possibly rephrase, ...
<@dilfridge> can we at least vote on EAPI=6 ?
<@mgorny> i'll just paste the numbers for completeness
<@dilfridge> oh we did already 
<@ulm> well, we've just accepted dropping step 2
<@Marecki> It really feels like we need to work on an official EAPI life-cycle
           policy.
<@ulm> are we going to ignote that vote?
<@ulm> *ignore
<@mgorny> EAPI 4 @ 2018-04-08 -> 1656 ebuilds                           [21:41]
<@dilfridge> well, we voted on two things, and both can take effect
<@dilfridge> but that doesnt keep us from tasking Marecki with the writeup of
             an official EAPI life-cycle policy
<@sam_> right
<@dilfridge> :)
<@Marecki> I can do that.
<@mgorny> EAPI 4 @ 2015-10-11 -> 4926 ebuilds
<@mgorny> but i suppose making some month-by-month plot will tell us better
          when 'ban' actually changed anything                          [21:42]
<@ulm> ok, anything else on this topic?
<@ulm> e.g. anyone supporting an EAPI 5 ban right now?
<@sam_> yes, I'm fine with banning EAPI 5 now, and would like us to, given IMO
        we're at the point where people should not be using it          [21:43]
<@ulm> mgorny: there's https://www.akhuettel.de/~huettel/plots/eapi.php
<@mgorny> given the numbers from EAPI 4 ban, i suppose EAPI 5 is fine being
          banned today wrt old policy
<@ulm> and no, you don't see any effect of a ban
<@ulm> nor of deprecation :/
<@sam_> I think it's useful for people to know when they really shouldn't be
        doing it vs "I'll get around to it now it's deprecated"
<@ulm> mgorny: won't harm to postpone it to next month                  [21:44]
<@Marecki> ulm: My personal, cynical view here is that people who do give a
           flying fuck will have already away from an old EAPI long before
           even deprecation and the rest does not care about either.
<@ulm> so let's move on, unless someone would second the motion to ban EAPI 5
<@mgorny> ulm: not sure, i find it hard to read log plot
<@sam_> Marecki: part of the thing here is about having a soft foam pool
        noodle to hit the people who don't care with stronger terminology  
<@dilfridge> scroll down                                                [21:45]
<@ulm> there's linear plots below then
<@sam_> Marecki: deprecated means "oh, ok, I really need to learn the new
        thing" vs banned for "please stop"
<@mgorny> ulm: scale is too small near the ban ;-)
<@sam_> let's postpone this and move on 
<@sam_> we can discuss on ML / outside of a meeting                     [21:46]
<@ulm> we have 3 more items, which I fear won't be faster
<@ulm> moving on
<@ulm> 5. IRC nicknames
<@ulm>
       https://archives.gentoo.org/gentoo-project/message/38fa6888e595165c8521e62becb1daf0
<@mgorny> i don't think any of the proposed solutions are feasible
<@mgorny> but we can kindly ask
<+soap> I'd go for option 1
<@dilfridge> I'd go for none of the above                               [21:47]
<@sam_> I'd like to: 1) kindly request developers update their gentooIM
        entries in LDAP with their regular nicks; 2) we ask infra to
        investigate exposing gentooIM on this page:
        https://www.gentoo.org/inside-gentoo/developers/.
<@sam_> No mandatory action.
<@dilfridge> who are we, should we prescribe the color of ties people wear at
             the council meeting?
<@mgorny> soap: why are you punishing sam for having his nick taken? ;-P
                                                                        [21:48]
<@gyakovlev> I think it's reasonable to ask to maintain user cloack with exact
             developer nickname and update ldap. and encourage usage of dev
             nicknames on IRC.
<@ulm> sam_: I guess you'd have to ask everyone, because of GDPR
<@sam_> gyakovlev: exactly
<@gyakovlev> afaik cloaks can contain arbitrary strings with exact nickname
<@dilfridge> secret piece of information: if you write "dilfridge" you ping me
             even if my current nick is different
<@sam_> ulm: yeah, we might need e.g. a new field or something (e.g.
        gentooIMPublic)
<@gyakovlev> so that works for people with stolen nicks
<@sam_> ulm: but in principle, we could ask infra to investigate this and they
        might conclude that
<+soap> mgorny: you'd obviously have some collision option, but I know
        multiple people who have wondered whether bman is on IRC only to
        learn...
<@mgorny> dilfridge: except i won't write dilfridge if i don't see you around
<@dilfridge> maybe I dont want to be found then :P
<@sam_> i'd like to request a vote on my above motion if that's ok      [21:49]
<@Marecki> I am not happy with exposing my gentooIM to the public. What I've
           put in there is for other Gentoo devs only.
<@sam_> it doesn't rule out other action
<@sam_> (2) could include a new field for public)
<@sam_> Marecki: understandable
<@ulm> sam_: please post the exact wording of your motion then          [21:50]
<@gyakovlev> using valid cloaks is the way to go imo.
<@mgorny> maybe we should start with a vote whether do we want to enforce any
          of the "strong" actions as suggested on the ml? to cut this short if
          others agree with me that we shouldn't
<@gyakovlev> verifiable via simple whois
<@sam_> motion: without ruling out any further decisions, 1) kindly request
        developers update their gentooIM entries in LDAP with their regular
        nicks; 2) we ask infra to investigate exposing gentooIM (or a new
        field e.g. gentooIMpublic) while exploring privacy on this page:
        https://www.gentoo.org/inside-gentoo/developers/. No mandatory action.
                                                                        [21:51]
<@sam_> s/exploring/preserving/
<@Marecki> And I must say I find this thing a bit silly. Will establishing a
           policy here really make people who are fond of silly nicks any less
           silly? IMHO no, it will just redirect them to other, possibly less
           insignificant channels of expression.
<@sam_> I'm not seeking to ban anything or endorse the proposals on the ML,
        just to solve the problem of finding folks.
<@ulm> sam_: I'd vote against this if 2) is included
<@Marecki> But yeah, we could ASK.
<@Marecki> sam_: Can we split this into two?                            [21:52]
<@sam_> ulm: even if we have a way to only include information devs want
        public?
<+soap> I'm not convinced asking nicely solves the problem, but ok
<@sam_> i'm ok with requesting a new gentooIMpublic or something
<@ulm> sam_: well, then next we'll include uids on github, gitlab, and whatnot
<@ulm> where do we stop?
<@sam_> i mean, people could leave it blank                             [21:53]
<@ulm> it's an external service and IMHO doesn't belong on the devs page
<@sam_> but gentooIM(public) is supposed to be generic anyway, a list of
        multiple services
<@sam_> alright
<@sam_> well, I'll call for a vote anyway, because the nature of the format
        allows multiple entries, and we already do it on the wiki but not
        through LDAP
<@sam_> let me give two motions
<@sam_> MOTION 1: 1) kindly request developers update their gentooIM entries
        in LDAP with their regular nicks;                               [21:54]
<@ulm> but let's vote on 1
<@ulm> motion: "kindly request  developers update their gentooIM entries in
       LDAP with their regular nicks"
<@Marecki> Seconded.
<@sam_> MOTION 2: We ask infra to investigate exposing gentooIM (or a new
        field e.g. gentooIMpublic) while exploring privacy on this page:
        https://www.gentoo.org/inside-gentoo/developers/. No mandatory action.
<@ulm> yep, please vote
<@sam_> (we're voting on 1)
<@ulm> on 1
* mgorny yes
* dilfridge yes
* sam_ yes
* Marecki yes
* ulm yes
* soap yes
* gyakovlev yes
<@ulm> unanimous
<@ulm> ok, vote on 2 then                                               [21:55]
* ulm no
* dilfridge no
* Marecki no
* gyakovlev no
* sam_ yes
* soap yes
* mgorny yes (i don't see why Council should block infra on this)
<@sam_> can I better understand the objections here?
<@ulm> 3 yes, 4 no. motion doesn't pass
<@sam_> I'm ok with the result, I just want to understand why
<@dilfridge> if you click on the dev name, you'll end up on the wiki page
                                                                        [21:56]
<@Marecki> I feel Infra has got more important things to do
<@mgorny> ulm: which doesn't mean it can't happen anyway ;-)
<@ulm> sam_: see above
<@dilfridge> everyone can add as much info there as s/he wants
<@ulm> mainly GDPR concerns
<@mgorny> do we want an extra motion to kindly ask devs not to use stu... i
          mean, to use nicknames resembling their dev nicknames? ;-)
<@sam_> infra are free to say no, but again, this would involve a new field or
        some way of e.g. only making it public beyond changes on a date (infra
        would consider our GDPR obligations)
<@sam_> thanks for the explanations anyhow
<@ulm> mgorny: some nicks are taken, so what could people do?           [21:57]
<@ulm> define "resembling"
<@sam_> "resembling"
<@mgorny> "sam_" is close enough to "sam" to be easy to find
<@mgorny> "FreedomBear" is not someone you could think of finding       [21:58]
<+soap> this
<@mgorny> it's just 'kindly asking', so it doesn't have to be precise
<@mgorny> we don't have to form a special group to investigate nickname
          similarity
<@gyakovlev> sam_: as for motion2, I don't really think it matters the way
             it's defined tbh, so no big deal. still can ask/inverstigate
                                                                        [21:59]
<@Marecki> Maybe we should just have the aforementioned Web page explicitly
           say that contact information for individual information might be
           found on their respective Wiki pages, as well as possibly not to
           mention that people's IRC and Gentoo nicks are aligned (yes, I know
           it says SOME developers can be found on IRC).
<@sam_> gyakovlev: but you voted no!
<@gyakovlev> ok what do you think of this possible motion (gimem a sec)
<@ulm> we could vote on item 4 in rich0's mail :)
<@gyakovlev> possible motion: ask groupcontacts to update developer host
             cloaks with official developer nicknames
             gentoo/developer/$officialdevhandle                        [22:00]
<@gyakovlev> ?
<@ulm> what does that mean exactly?
<@sam_> as far as I know, that's what we do, modulo special characters (_)
<@dilfridge> that just feels confused
<@mgorny> gyakovlev: isn't that already the case?
<@dilfridge> the cloak normally has the gentoo uid
<@sam_> i don't recall ever giving a cloak which didn't align
<@dilfridge> except for very rare cases (like peope in more than one project)
                                                                        [22:01]
<@sam_> yes
<@mgorny> (sorry, got a bad lag here)
<@ulm> ^^ this
<+soap> dilfridge: the cloak doesnt help for the lookup
<@gyakovlev> ok nvm then
<@ulm> people in more than one project are a good reason for not enforcing
       anything
<@dilfridge> soap: if you have your uid registered as *one* of your nicknames,
             you'll be found by whois                                   [22:02]
<@gyakovlev> I thought I've seen someone with misalighed one. but maybe I'm
             just confused.
<@mgorny> ulm: as a formality, i think we should vote on rich's motions
<@ulm> all of them?
<@sam_> gyakovlev: if you do, let me know and we'll fix anyway
<@sam_> it's not impossible
<@mgorny> ulm: all-in-one maybe?
<@mgorny> i.e. whether we want to proceed with any of them              [22:03]
<@ulm> no, let's do it like this:
<@ulm> does anyone support one or more of the motions in rich0's mail?
<@dilfridge> no
<@gyakovlev> no                                                         [22:04]
<@sam_> no
* mgorny no
<@ulm> let's move on
* soap no
<@ulm> this isn't a formal vote, so I'm not counting :)
<@ulm> 6. Format of metadata/AUTHORS file
<@ulm>
       https://archives.gentoo.org/gentoo-project/message/2c6dc0d82147e8e7b572e3390b051b4d
<@ulm> my impression is that this is not ready for a vote yet, because there
       was no feedback from the entity listed in the AUTHORS file       [22:05]
<@dilfridge> this proposal is somewhat pointless if we do not have any
             feedback from $CORPORATION people first
<@ulm> yes :)
<@gyakovlev> I have a feeling not enough discussion happened yet.
<@sam_> agreed
<@gyakovlev> especially with copyright holders in that file
<@ulm> let's postpone then                                              [22:06]
<@mgorny> do we want to assign someone from the Council to work on it?
<@dilfridge> so I suggest we ask robbat2 to obtain that feedback in detail
             first 
<@ulm> mgorny: I could take it
<@ulm> and report back next month
<@mgorny> ulm: thanks
<@sam_> thanks
<@ulm> moving on
<@ulm> 7. Open bugs with council participation                          [22:07]
<@ulm> bug 736760
<willikins> ulm: https://bugs.gentoo.org/736760 "Application to Software
            Freedom Conservancy"; Gentoo Foundation, Proposals; CONF;
            mgorny:trustees
<@ulm> no visible progress, at least not in the bug
<@mgorny> this is primarily trustee business but probably won't happen
<@mgorny> they don't want us, we don't really want them anymore
<@gyakovlev> there was other org that was ok with us though.            [22:08]
<@mgorny> probably won't be closed until we decide on a specific umbrella or
          reject all of them
<+robbat2> the progress is tracked here:
           https://wiki.gentoo.org/wiki/Foundation:FoundationFutureState#Status:_SFC
<@mgorny> the two main options are OSC and LF
<@ulm> I must say that after the freenode disaster, I'm a little sceptical
       about giving control to a third party
<@gyakovlev> and I think last thing was a question about moving physical
             assets, which they did not do before.
<@mgorny> gyakovlev: OSC already started supporting ownership of physical
          assets                                                        [22:09]
<@gyakovlev> so maybe let's close SFC bug? it's clear they are not an option.
<@dilfridge> ulm: hilariously the freenode server banner now says "welcome to
             the Freenode AUTONOMOUS ZONE"
<@dilfridge> feels like it's run by antifa :D
<@ulm> who cares?
<@sam_> gyakovlev: formally not for us to do but i'd like that          [22:10]
<+FreedomBear> mgorny: you rang?
<@dilfridge> not me really, I just noticed when I finally logged out
<@gyakovlev> or at least remove council from CC
<@mgorny> ulm: err, freenode sit sounds more like gentoo foundation than
          umbrella to me
<@mgorny> after all, their own corpo took them
<@mgorny> FreedomBear: too late, already past the topic
<+robbat2> (afk, will review logs)
<@Marecki> dilfridge: Well, as an old Polish saying goes "it was written ARSE
           on a fence, someone believe it and all they got for it was
           splinters"
<@mgorny> ulm: in any case, let's move on                               [22:11]
<@Marecki> +d
<@dilfridge> hrhr
<@ulm> remove us from cc then, or ask trustees to close the bug?
<@sam_> why not both?
<@sam_> let's do the latter actually                                    [22:12]
<@mgorny> ulm: i think we should remain CC-ed on the topic since it's
          important
<@ulm> sam_: can you take core of it?
<@ulm> asking, that is?
<@sam_> sure
<@ulm> bug 784710
<willikins> ulm: https://bugs.gentoo.org/784710 "Remove SHA512 hash from
            Manifests"; Gentoo Council, unspecified; CONF; mgorny:council
<@mgorny> wasn't it rejected?
<@ulm> that was discussed back in june
<@dilfridge> mathom
<@ulm> let's close it then                                              [22:13]
<@ulm> WONTFIX for now, I guess
<@mgorny> hmm, or deferred for the new Council that doesn't include members
          loving debating it to death
<@sam_> you're free to raise it again if you wish
* mgorny shrugs                                                         [22:14]
<@ulm> actually, the june summary says "The council has no strong opinion
       either way about this bug"
<@mgorny> do you feel that i should provide some more data?
<@ulm> what would we need data for?
<@mgorny> that's what i'm asking ;-)                                    [22:15]
<+soap> ok I need to go now
<@ulm> mgorny: can you raise it on the ML please                        [22:16]
<@ulm> and we reiterate next month with more input?
<@mgorny> ulm: sure, can do
<@ulm> k
<@ulm> bug 793164
<willikins> ulm: https://bugs.gentoo.org/793164 "GLEP 82: Repository
            configuration file (layout.conf)"; Documentation, New GLEP
            submissions; CONF; mgorny:glep
<@ulm> we had that one as topic before                                  [22:17]
<@Marecki> Taken care of, innit.
<@mgorny> we've handled it via earlier vote today
<@mgorny> (it was blocked on eapis-testing)
<@ulm> 8. Open floor
<@ulm> anything?                                                        [22:18]
<@gyakovlev> sam had an item I think, maybe still typing
<@sam_> I'd like to suggest we just scrap the previous EAPI banning vote and
        revisit this next time. I don't think it was necessarily clear which
        motion was being used / its implications, and as a point of order, we
        ended up saying we'd vote on 2 motions, but only voting on 1.
<@sam_> I think it'd be cleaner for e.g. the log and such if we just revisited
        it after more ML discussion/thought
<@mgorny> i can take care of opening more discussion around it          [22:19]
<@ulm> ok, let's vote on that
<@gyakovlev> I second that request
<@ulm> motion: retract the previous vote on "the EAPI deprecation workflow is
       simplified so that there is no intermediate step between deprecation
       and complete removal of EAPI"
* gyakovlev yes
* dilfridge yes
* sam_ yes
* ulm yes                                                               [22:20]
* soap yes
* mgorny yes
<+FreedomBear> aww damn. did I miss the EAPI convo
* Marecki abstain
<@ulm> passes with 6 yes, 0 no, 1 abstention
<@gyakovlev> FreedomBear: you'll have more time since it's retracted now.
                                                                        [22:21]
<@sam_> thanks
<@gyakovlev> thanks!
<@ulm> gah, writing the summary for this will be a pain :)
<+FreedomBear> It seems so simple. I am not sure how else to approach it
<@ulm> any other item for open floor?
<+FreedomBear> It is just a formality should anyone be silly and keep
               committing EAPI's after deprecation. 
*** msavoritias_ (~msavoriti@91-158-103-66.elisa-laajakaista.fi) has joined
    channel #gentoo-council                                             [22:22]
<@Marecki> Something from me but only if it's something we can cover quickly.
           Gimme a sec.
<+FreedomBear> No need for all the data stuff to try and "prove" a 1 minute
               vote "works"
*** msavoritias (~msavoriti@91-158-103-66.elisa-laajakaista.fi) has quit: Ping
    timeout: 258 seconds
<+FreedomBear> Spent more time considering data instead of just saying "don't
               do that"
* FreedomBear goes back to RStudio                                      [22:23]
<@Marecki> Okay, so it's been stated on the ML lately that "if the arch isn't
           stable, we can't guarantee anything". I would like to ask, if
           possible, for an official confirmation of that statement.
                                                                        [22:24]
<@sam_> oh, yes, I plan on going back to contest that
<@sam_> I didn't really see it as a good argument for dropping a version
                                                                        [22:25]
<@ulm> Marecki: that doesn't look like it will be a quick item
<@Marecki> The context being here that the ~arch status could be used as
           justification for mass dekeywording.
<@sam_> we do have GLEP 72 but i think it will need more discussion
<@dilfridge> mailing list please
<@ulm> and we cannot vote on it in open floor
<@sam_> yeah
<@Marecki> ulm: OK, so nothing already discussed. Let's leave till another
           time, then.
<@mgorny> Marecki: more info, please
<@mgorny> (on the ml)
<@sam_> Marecki: (we can always adjust GLEP 72 if required, but let's see)
                                                                        [22:26]
<@ulm> Marecki: discuss on ML, and we can address it in august
<@sam_> yep
<@ulm> I see nothing else
<@Marecki> mgorny: WilliamH's response in the "dropping dev-lang/lua:5.2"
           thread but since it's not a quick thing to discuss, I won't bother
           digging up an archive link.
* ulm bangs the gavel                                                   [22:27]
<@ulm> meeting is closed
<@ulm> thanks everyone
<@sam_> thanks everyone for the marathon and ulm for chairing
<@mgorny> thanks!
<@Marecki> Cheers!
<@sam_> first meeting, we're allowed to be a little bit rough ;)
*** ulm (~ulm@gentoo/developer/ulm) has set the topic for #gentoo-council:
    "216th meeting: 2021-08-08 19:00 UTC |
    https://www.timeanddate.com/worldclock/fixedtime.html?iso=20210808T19 |
    https://wiki.gentoo.org/wiki/Project:Council |
    https://dev.gentoo.org/~dilfridge/decisions.html"
