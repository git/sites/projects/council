Roll Call:
===========
Betelgeuse: here
Cardoe: here
dberkholz: here
dertobi123: here
dev-zero: here
leio: here
lu_zero: here
tanderson(secretary): here

Topics:
===========

Open Council Spot:
    - Should leio fill the empty council spot?
        Since Mark(halcy0n) resigned from the council there is an empty spot.
        Since Mart Raudsepp(leio) is ranked next from the last election, he is
        eligible to fill the spot.

        Conclusion:
            Mart Raudsepp is unanimously approved for the council.

Technical Issues:
    - GLEP 55
        There had been quite a bit of discussion on this topic recently.
        Within hours of the council meeting new proposals were being proposed
        and discussion was ongoing. 

        Conclusion:
            No decision as of yet. Ciaran Mccreesh(ciaranm) and Zac
            Medico(zmedico) volunteered to benchmark the various proposals on
            the package managers they maintain(paludis and portage
            respectively. Petteri(Betelgeuse) will assist with the portage
	    benchmarks. Tiziano(dev-zero) and Alec Warner(antarus) will write
            up a comparison of the various proposals and their various
            advantages and disadvantages within a week.

    - GLEP 54
        There had been some discussion on gentoo-dev since last meeting,
        though no consensus or agreement had been reached(surprise!)

        Conclusion:
            Thomas Anderson(tanderson) and Luca Barbato(lu_zero) will write up
            a comparison of the advantages and disadvantages of the two
            proposals(-scm and _live). This will be completed within a week.

    - Overlay Masking in Repositories.
        Brian Harring(ferringb) asked for discussion for when overlays
        attempted to unmask packages provided by the master 
        repository(gentoo-x86). Because this is only available in portage
        (it is contrary to PMS), Brian thought it should not be allowed.
        
        Numerous suggestions were made to the effect that if a standardized
        set format was agreed upon for repositories and package.unmask was
        allowed to contain sets, then this problem would be fixed.

        Conclusion:
            No decision, as only discussion was requested. Mart Raudsepp(leio)
	    will follow up on this with discussion on gentoo-dev
