11:52 <@ulm> !proj council
11:52 <+willikins> (council@gentoo.org) dilfridge, k_f, mgorny, slyfox, tamiko, ulm, williamh
11:53 <@dilfridge> wot
11:53 <@ulm> ^^ Soap__ will proxy me in the meeting later today
12:04 -!- rich0 [~quassel@gentoo/developer/rich0] has quit [Remote host closed the connection]
12:07 -!- rich0 [~quassel@gentoo/developer/rich0] has joined #gentoo-council
12:07 -!- mode/#gentoo-council [+v rich0] by ChanServ
12:13 <@dilfridge> !proj council
12:13 <+willikins> (council@gentoo.org) dilfridge, k_f, mgorny, slyfox, tamiko, ulm, williamh
12:13 <@dilfridge> asturm will proxy me today, but I'll check in briefly if we're enough council members... in an emergency I should be able to attend
13:18 -!- Amynka [~frozen@gentoo/developer/amynka] has joined #gentoo-council
13:18 -!- mode/#gentoo-council [+v Amynka] by ChanServ
16:01 -!- Amynka [~frozen@gentoo/developer/amynka] has left #gentoo-council []
16:01 -!- Amynka [~frozen@gentoo/developer/amynka] has joined #gentoo-council
16:01 -!- mode/#gentoo-council [+v Amynka] by ChanServ
16:05 <+Soap__> ulm: Amynka will proxy, sorry, have an emergency too
16:05 <+Soap__> if you disagree, let me know
16:57 <@WilliamH> meeting in a little over an hour
17:04 <@ulm> ok, change of plan
17:05 <@ulm> Amynka will proxy me, rather than Soap__
17:05 <@ulm> !proj council
17:05 <+willikins> (council@gentoo.org) dilfridge, k_f, mgorny, slyfox, tamiko, ulm, williamh
17:05 <@ulm> ^^
17:05 <@ulm> and afk
17:10 < dwfreed> so, based on what's been posted here, there are 3 proxies? tamiko -> Whissi, ulm -> Amynka, and dilfridge -> asturm ?
17:10 < veremitz> are there any council members present, or just proxies?
17:10 < veremitz> I think without a quorum, you might as well postpone ..
17:11 < dwfreed> well, hopefully slyfox will be present, as he's going to chair; WilliamH seems to be here as well
17:11 < veremitz> idk how that works with proxies
17:12 < veremitz> I thought slyfox has an issue, but perhaps I crossed wires ..
17:14 < dwfreed> GLEP 39 makes no mention of needing quorum of real council members rather than proxies
17:14 < veremitz> cool
17:15 < veremitz> or not cool, depending on your persuasion LOL
17:32 <+NeddySeagoon> I think its OK to have 7 proxies.  7 no shows, triggers an election.
17:33 <@slyfox> i'll chair, yes. Today's agenda (spoiler, not much there): https://archives.gentoo.org/gentoo-project/message/ceacb23e8e6f0b5168987b37396b7dcb
17:34 <@slyfox> 25 minutes before meeting starts
17:34 <@slyfox> you still have time to prepare :)
17:34 < veremitz> :)
17:37 -!- jmorgan [~jack@gentoo/developer/jmorgan] has quit [Quit: leaving]
17:38 -!- jmorgan [~jack@gentoo/developer/jmorgan] has joined #gentoo-council
17:38 -!- mode/#gentoo-council [+v jmorgan] by ChanServ
17:47 <@K_F> I'm around, but seems I wont be home before a bit late.. delayed flight and a traffic accident ahead holding up traffic
17:57  * dilfridge is sitting in a talk by Ranga Yogeshwar on the AI apocalypse
17:58 <+Whissi> Greetings! :)
17:58 -!- raynold [uid201163@gateway/web/irccloud.com/x-aylematumucaotqa] has joined #gentoo-council
17:59  * mgorny around
17:59 <@slyfox> !council
17:59 <@slyfox> !proj council
17:59 <+willikins> (council@gentoo.org) dilfridge, k_f, mgorny, slyfox, tamiko, ulm, williamh
18:00 -!- asturm [~andreas@gentoo/developer/asturm] has joined #gentoo-council
18:00 -!- mode/#gentoo-council [+v asturm] by ChanServ
18:00  * K_F here
18:00  * WilliamH here
18:00 <@slyfox> Our today's agenda is: https://archives.gentoo.org/gentoo-project/message/ceacb23e8e6f0b5168987b37396b7dcb
18:00  * Amynka here
18:00  * mgorny here
18:00  * Whissi proxying tamiko here
18:00  * slyfox here
18:01 <+asturm> proxying dilfridge here
18:01 <@slyfox> *nod*
18:01 <@dilfridge> also here
18:01 <@slyfox> 2. Bugs with council involvement:
18:01 <@dilfridge> but 5sec quassel lag
18:01 <@slyfox> GLEP 68: https://archives.gentoo.org/gentoo-dev/message/ae4f89a9697f623df500b7d44d66c215
18:02 <@slyfox> https://bugs.gentoo.org/649740
18:02 <@slyfox> Tl;DR: "Please approve GLEP 68 changes: Add <stabilize-allarches/> element to metadata.xml"
18:02 <@slyfox> Worth a few words on discussion?
18:02 <@mgorny> i think it's mostly self-explanatory but feel free to ask anything
18:03 <@slyfox> Let's give a minute people to skim through again and come up with possible issues.
18:03 <@mgorny> the problem being solved is that currently whoever files stabilization bug needs to check whether ALLARCHES is applicable which can be a hassle
18:03 <@mgorny> esp. if you have e.g. a number of python packages and you need to determine whether they're pure python or C+python
18:04 <+Whissi> My only question would be why it is "zero or more". I cannot imagine purpose of multiple "stabilize-allarches".
18:04 <+asturm> description could be slightly improved. How is it restricted to version e.g.?
18:04 <@WilliamH> Whissi: that allows the stabilize-allarches to be restricted to specific versions
18:05 <@WilliamH> asturm: the restrict= attribute
18:05 <@mgorny> Whissi, asturm: it's consistent with other elements, and other sections describe how to apply restriction
18:06 <@slyfox> Ok, let's move on to voting:
18:06 <@slyfox> vote: "Please approve GLEP 68 changes: Add <stabilize-allarches/> element to metadata.xml"
18:06  * WilliamH yes
18:07  * mgorny yes
18:07  * Amynka yes
18:07  * slyfox yes
18:07  * Whissi yes
18:07  * K_F yes
18:07 <+asturm> yes
18:07 <@slyfox> unanimous
18:08 <@slyfox> Next item: GLEP 74 https://gitweb.gentoo.org/data/glep.git/commit/?id=30fef42d2186d4742c80526a748c40adc5c8953c
18:08 <@slyfox> https://bugs.gentoo.org/648638: "please review update removing one filesystem restriction"
18:09 <@mgorny> long story short, this restriction doesn't have any real advantage and breaks overlayfs
18:09 <@mgorny> it's been acked by co-authors, patches are waiting for 1.5 mo
18:10 <@slyfox> -ETIMEOUT, moving to voting:
18:10 <@slyfox> vote: "please review update removing one filesystem restriction"
18:10 <@K_F> well, it does protect against some linking attacks etc, which is why it was included to begin with.. that said, I'm not sure if we need to enforce it
18:10 <@mgorny> K_F: rsync already protects against out-of-tree symlinks
18:11 <@slyfox> worth asking explicit security@ approval before moving on?
18:12  * WilliamH votes yes
18:12 <@K_F> nah, voting should be good
18:12  * K_F yes
18:12 <+Amynka> not worth asking i would say
18:12 <+asturm> yes
18:12 <@slyfox> Let's do it then!
18:12  * slyfox yes
18:12  * mgorny yes
18:13 <+Amynka> yes
18:13  * Whissi yes
18:13 <@slyfox> yay \o/ approved unanimously
18:13 <@slyfox> Joint venture to deal with copyright issues: https://bugs.gentoo.org/642072
18:14 <@slyfox> I think we'll need a Tl;DR here and explicit action asked from council
18:14 <@mgorny> it's joint venture's business™
18:14 <@mgorny> so for joint meetings
18:15 <@slyfox> skipping then?
18:15 <@K_F> yeah, lets do it in one of the joing meetings, making it somewhat useful to begin with
18:15 <@slyfox> *nod*
18:15 <@slyfox> Moving on to GLEP 14: https://bugs.gentoo.org/637328 "GLEP 14 needs to be updated"
18:16 <+Whissi> Security team is working on this. :)
18:16 <@K_F> we're working on it
18:16 <@slyfox> good!
18:16 <@slyfox> 3. Open floor
18:17 <@slyfox> Everything you were afraid to ask goes here :)
18:17  * mgorny notes the sound of fans
18:18  * Whissi has no additional topics/questions.
18:19 <@slyfox> Allright. Let's call this short meeting done then!
18:19 <@K_F> have a nice evening!
18:19 <@mgorny> EAPI 7 is progressing nicely
18:19 <@mgorny> (stupid lags)
18:20 <+asturm> yay
18:20 <@slyfox> woohoo!
18:20 <@mgorny> ulm has noted he'd like to withdraw 'sandbox path removal functions'
18:20 <@mgorny> as they're not really needed right now and it's probably better to do a single 'esandbox' command in EAPI 8 instead of 8 separate functions
18:21 <@mgorny> i guess that'll be voted as part of the final implementation
18:21 <@mgorny> IUSE_RUNTIME and ||= will probably not be implemented
18:21 <@mgorny> and i'd really appreciate if someone took a look at dostrip
18:22 <@slyfox> do you have bug links to include to meeting notes for everyone who will that offline?
18:23 <@mgorny> ya, sec
18:23 <@mgorny> https://wiki.gentoo.org/wiki/Future_EAPI/EAPI_7_tentative_features
18:23 <@mgorny> this has links to all of it
18:24 <@slyfox> nice! thank you!
18:24  * slyfox declares the meeting done
18:25 <@slyfox> thanks all!
18:25 <+Whissi> Yup, thanks all, bye.
18:26 <@mgorny> thanks
18:26 <+Amynka> thanks all
