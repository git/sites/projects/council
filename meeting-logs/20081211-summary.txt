Roll Call:
==========
Betelgeuse: here
Cardoe: absent, 25 minutes late
dertobi123: here
dev-zero: here
dberkholz: here
Halcy0n: here
lu_zero: here

Meeting Topics:
===============

Technical Issues:

    - Label profiles with EAPI for compatibility checks:
        Should there be labels in the profiles telling package managers what
        EAPI the profile uses. This proposal raised some concerns that
        developers would modify current profiles and bump the EAPI which would
        harm users' systems.

	    Conclusion:
	        Profile EAPI files are approved for use in gentoo-x86 profiles.
            The file for use in profiles is 'eapi'. All current profiles
            are EAPI="0" and only new profiles can be marked with the profile
            EAPI markers. Any developer profiles can be marked with a new
            EAPI.

    - Retroactive EAPI change: Call ebuild functions from trusted working
      directory:
        Donnie(dberkholz) commented that it may not be needed to add something
        to EAPI=0 that is in all the Package Managers.

        Conclusion:
            Approved. This change applies to all current EAPIs(0,1,2).

    - Metadata variable for DEFINED_PHASES
    	Should a metadata variable be added containing the list of all phases
	defined in the ebuild or eclasses?

        Conclusion:
            Approved. Infra will do a full regen of the metadata cache once
            portage has support for it.
