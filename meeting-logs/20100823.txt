[21:59:59] <wired> i guess its about time
[22:00:04] *** Joins: lavajoe (~joe@gentoo/developer/lavajoe)
[22:00:09] <Chainsaw> Ready here :)
[22:00:11] <wired> rollcall! Betelgeuse Chainsaw ferringb Halcy0n jmbsvicetto scarabeus ( ssuominen )
[22:00:15] <Betelgeuse> wired: around
[22:00:20] <jmbsvicetto> around
[22:00:38] <Halcy0n> here
[22:00:54] <Chainsaw> wired: Present.
[22:01:14] <wired> great
[22:01:16] <wired> ferringb?
[22:01:51] <wired> lets give him a couple of minutes
[22:04:57] <Halcy0n> Want me to call Brian?
[22:05:12] <wired> could you please?
[22:05:33] <wired> or I will
[22:05:55] <Halcy0n> Its free for me to call if you want.  I just can't find his number.
[22:06:34] <wired> problem solved
[22:07:09] *** Quits: ali_bush (~alistair@gentoo/developer/alibush) (Read error: Connection reset by peer)
[22:07:39] <jmbsvicetto> Did we poke anyone to get an EAPI-4 status update?
[22:08:17] <Betelgeuse> jmbsvicetto: few_ said to look at the tracker bug
[22:08:29] <wired> ok so brian is not answering
[22:08:32] <wired> lets move on
[22:08:32] <Betelgeuse> 08:45 < few_> Betelgeuse: just in case nobody shows up to report about the portage eapi 4 status in the council meeting: it's all in bug 273620. whatever is marked as fixed can be made usable in the next release.
[22:08:34] <willikins> Betelgeuse: https://bugs.gentoo.org/273620 "[TRACKER] sys-apps/portage EAPI 4 implementation"; Portage Development, Core; NEW; SebastianLuther@gmx.de:dev-portage@g.o
[22:08:46] <wired> Betelgeuse: thanks, was about to paste that myself
[22:10:05] *** Joins: antarus (~antarus@gentoo/developer/antarus)
[22:10:13] <wired> it seems that the only open bugs there are bug 273642
[22:10:15] <willikins> wired: https://bugs.gentoo.org/273642 "USE is calculated differently (EAPI 4)"; Portage Development, Core; NEW; SebastianLuther@gmx.de:dev-portage@g.o
[22:10:18] <wired> and bug 273625
[22:10:20] <willikins> wired: https://bugs.gentoo.org/273625 "Slot operator dependencies (EAPI 4)"; Portage Development, Core; NEW; SebastianLuther@gmx.de:dev-portage@g.o
[22:10:47] <jmbsvicetto> few_ is around. Anyone has any questions?
[22:11:12] *** Quits: nirbheek (~nirbheek@gentoo/developer/flyingspaghettimonster/nirbheek) (Ping timeout: 240 seconds)
[22:11:23] <few_> there is a third: bug 273633
[22:11:25] <willikins> few_: https://bugs.gentoo.org/273633 "Controllable compression and docompress (EAPI 4)"; Portage Development, Core; NEW; SebastianLuther@gmx.de:dev-portage@g.o
[22:11:55] <wired> few_: right i missed it thanks
[22:11:56] <zmedico> all the stuff that's implemented should be listed in the tracker bug and here too: http://git.overlays.gentoo.org/gitweb/?p=proj/portage.git;a=blob_plain;f=doc/package/ebuild/eapi/4.docbook;hb=HEAD
[22:12:32] <Betelgeuse> I think we should roll that together with GLEP 55 if approved today and start getting EAPI 4 out.
[22:12:40] <Betelgeuse> zmedico said GLEP 55 already has code
[22:12:57] <Betelgeuse> zmedico: 54 does too?
[22:14:28] <zmedico> well, glep 54 isn't implemented but it would affect all the same places as the glep 55 patch
[22:15:01] <Betelgeuse> It would take some time to get PMS in order any way
[22:16:16] <Betelgeuse> zmedico: Also how's repoman wrt EAPI 4?
[22:17:16] <ssuominen> It would be nice to have special version string, called for example 'scm' or 'live' to replace 9999 a-like versioning. But .ebuild stops at .ebuild, extra suffix is just ugly and complicates workflow
[22:17:47] <zmedico> Betelgeuse: it's hard to say about repoman because we haven't enabled support for EAPI 4_pre1 yet
[22:18:12] <Betelgeuse> zmedico: I was thinking we might want to check some || die stuff but any way not really that relevant now
[22:18:16] <Betelgeuse> wired: Shouldn't we move on?
[22:18:19] <wired> yes
[22:18:57] <zmedico> ssuominen: a possible alternative to the -scm would be just to add a _scm suffix that's similar to the existing version suffixes
[22:19:07] <wired> lets talk about GLEP 54
[22:19:34] <jmbsvicetto> zmedico: would it be possible to have just ${PN}_scm ?
[22:19:39] <wired> zmedico: thats kind of what I was thinking
[22:19:54] <Betelgeuse> wired: shouldn't talk have happened at mailing lists?
[22:20:00] <zmedico> jmbsvicetto: we could allow that since we're changing version rules anyway
[22:20:06] <jmbsvicetto> ok
[22:20:28] <wired> Betelgeuse: well, it should, but i guess we can do some basic talk before we vote on it
[22:20:37] <Betelgeuse> Also we have the option to bikeshed before EAPI 4 is offically approved.
[22:20:46] <Betelgeuse> zmedico: I presume changing naming etc is quite trivial?
[22:20:56] <Betelgeuse> Once the approach is in.
[22:21:17] <ulm> Betelgeuse: the issue is about hyphen vs underscore, not live vs scm
[22:21:25] <wired> Im all for GLEP 54, but I strongly prefer _ and live
[22:21:33] <ulm> see http://www.mail-archive.com/gentoo-dev@lists.gentoo.org/msg34159.html
[22:21:47] <ulm> all this has been discussed ad nauseam one year ago
[22:22:32] <Chainsaw> ulm: You have a point, even versionator relies on those hyphens.
[22:22:33] <zmedico> Betelgeuse: changing naming of ebuilds? yeah pretty trivial, along the lines of the glep 55 patch.
[22:23:17] *** Joins: Ford_Prefect (~arun@gentoo/developer/ford-prefect)
[22:23:19] <ssuominen> GLEP-54, yes. GLEP-55, no.
[22:23:29] <wired> ^^ im with ssuominen 
[22:23:38] <Halcy0n> If its a well defined standard, it doesn't matter if its a - or a _.
[22:23:42] <jmbsvicetto> GELP-54, yes. GLEP-55, no.
[22:23:52] <jmbsvicetto> GLEP*
[22:24:06] <Betelgeuse> And how do you guys propose doing 54 without 55?
[22:24:12] <Halcy0n> That was my question :)
[22:24:32] <zmedico> you just use a new regex
[22:24:34] <Chainsaw> I don't believe 54 is workable without 55, and I don't like 55.
[22:24:38] <Chainsaw> So no on both.
[22:24:55] <zmedico> you scan the ebuild dir for valid ebuild names
[22:25:03] <zmedico> using a different regex for each format
[22:25:23] <Betelgeuse> Of course we can do it if we wait until people are expected to be on a new Portage version
[22:25:24] <zmedico> ebuid extension is irrelevant
[22:25:28] *** Joins: darkside_ (~darkside@gentoo/developer/darkside)
[22:25:36] <Chainsaw> Halcy0n: I'd prefer mandating the underscore though, to avoid violating any assumptions in current code.
[22:25:38] <Betelgeuse> Meaning the standard year and screw earlier versions.
[22:25:47] <jmbsvicetto> I'm open for a onetime change of the extension (.eb ?), if we use it to put up rules to ensure we can get changes in the future without requiring extension changes
[22:25:55] <Halcy0n> The standard year sucks.
[22:26:02] <Betelgeuse> Halcy0n: indeed
[22:26:33] <wired> zmedico: how would reasonably old (1yo) portage react to foo-live or foo-1_live?
[22:27:10] <zmedico> wired: it would just give a short warning message during dep calc and move on
[22:27:14] <jmbsvicetto> about the EAPI argument, I don't agree with the claims on GLEP-55, including the "limitation" in forcing EAPI to be set on a clear position of the file
[22:27:26] <wired> @council ^^ i don't see an issue with that
[22:27:45] <jmbsvicetto> s/of/in/
[22:27:57] <Chainsaw> Okay, so it seems we can do 54 without 55. What's the opinion on mandating the underscore?
[22:28:02] <wired> zmedico: for every {-,_}live ebuild in tree?
[22:28:17] <jmbsvicetto> wired: I'd rather see a new discussion taking the time to address the issue of supported features of a repo.
[22:28:33] <zmedico> wired: well, every one that's pulled into the dependency graph for whatever reason
[22:28:56] <wired> jmbsvicetto: sorry?
[22:28:59] <Chainsaw> zmedico: The main concern is that it wouldn't abort so that a newer python/portage can be merged.
[22:29:00] <Halcy0n> We really should have had these discussions on the mailing lists if you guys felt this way :)
[22:29:10] <zmedico> wired: one warning for each .ebuild file with unrecognized version part
[22:29:11] <jmbsvicetto> There have been talks about adding a file to every repo to specify features supported by the repo. It could be just at the root level, or it could also be used at the category or package level
[22:29:13] <Chainsaw> zmedico: Which I believe you've addressed :)
[22:29:25] <zmedico> Chainsaw: yeah, won't abort
[22:29:33] <Chainsaw> jmbsvicetto: That sounds as invasive as GLEP55, I'd like to have that punted back to the mailing list.
[22:30:00] <jmbsvicetto> Chainsaw: I'm not proposing to move forward with that. I'm proposing we take the time to discuss that (on mls)
[22:30:05] *** Joins: nirbheek (~nirbheek@gentoo/developer/flyingspaghettimonster/nirbheek)
[22:30:31] <wired> zmedico: that sounds good then, we could have GLEP 54 without severe consequences 
[22:30:45] <Chainsaw> wired: Agreed, revising my vote. GLEP 54 yes, GLEP 55 no.
[22:30:53] <zmedico> wired: sure
[22:30:55] <jmbsvicetto> Chainsaw: if we decide to do a major change in the file format (including a file extension change), we might take the chance to address as many issues as possible
[22:30:56] <Chainsaw> wired: Point about the underscores remains, but nobody seems willing to answer that.
[22:31:14] <wired> Chainsaw: +1 for underscore here
[22:31:22] <Betelgeuse> I voted yes to both previously and don't think anything has been presented since then.
[22:31:22] <jmbsvicetto> Chainsaw: I'm ok with forcing it to be '_'
[22:31:34] <wired> hyphen seems to make it harder for no apparent reason
[22:31:52] <Chainsaw> wired: Indeed. Principle of least astonishment.
[22:31:54] <Halcy0n> wired: _ has the same reprecussions anyway, since its a valid part of the version string right now as well.
[22:32:23] <wired> Halcy0n: true, but it doesn't split PN from PV
[22:32:35] <Betelgeuse> wired: I propose we vote on if this council thinks the problem of changing global scope should be addressed?
[22:32:45] <Halcy0n> Either way, I approve of GLEP 54, with my own minor bikeshedding of it be "live" vs "scm" which was brought up in one of the previous discussions.
[22:33:07] <jmbsvicetto> Betelgeuse: yes from me on both - we should vote and we should address it
[22:33:36] <jmbsvicetto> Halcy0n: no objection from me on live and no preference between live or scm
[22:33:38] *** Quits: nirbheek (~nirbheek@gentoo/developer/flyingspaghettimonster/nirbheek) (Excess Flood)
[22:33:54] <Halcy0n> Also, I like GLEP55, but I don't like the proposed solution, but like the second one with a static extension with the eapi as part of the version.
[22:34:07] <wired> Betelgeuse: can you be more specific, what issue are you refering to?
[22:34:19] <Betelgeuse> wired: The problem that GLEP 55 aims to solve.
[22:34:29] <Chainsaw> Halcy0n: I like the principle, but I don't like the implementation.
[22:34:38] <Betelgeuse> wired: As people vote no I want to know if they want to just ignore it for now.
[22:34:44] <wired> ah
[22:34:51] <jmbsvicetto> Betelgeuse: iirc, ferringb has disputed the claim in the GLEP
[22:35:18] <Betelgeuse> jmbsvicetto: The parsing EAPI part or the performance?
[22:35:49] <Betelgeuse> jmbsvicetto: I haven't heard there being anything wrong with the Problem statement in the GLEP (of course it's been a while)
[22:35:54] <wired> lets clear things out for a minute here
[22:35:54] <jmbsvicetto> Betelgeuse: and it has been discussed before that we can (should) split the ebuild sourcing from a "pre-processing" step
[22:35:59] <wired> hold your GLEP 55 thoughts
[22:36:16] <wired> GLEP 54, can I have yes/no votes please
[22:36:22] <jmbsvicetto> yes
[22:36:27] <Betelgeuse> wired: It really makes sense to do 55 first
[22:36:34] <jmbsvicetto> + '-' restriction
[22:36:42] <jmbsvicetto> bah, '_'
[22:36:42] <Chainsaw> wired: GLEP 54: yes.
[22:36:59] <Halcy0n> Doing 54 without 55 seems like we are just hoping to not have warnigns displayed to users.
[22:37:04] <Chainsaw> wired: And indeed, underscore, not hyphen.
[22:37:14] <Halcy0n> I'm not willing to vote on 54 until 55 is decided.
[22:37:29] <jmbsvicetto> 55 then?
[22:37:34] <wired> ok then
[22:38:40] <wired> i really don't like suffix
[22:38:55] <jmbsvicetto> 55: no - I don't agree with the premises nor with the proposed solution
[22:39:12] <Chainsaw> wired: GLEP 55: no.
[22:39:23] <ssuominen> 55: no
[22:39:24] <wired> GLEP 55: no.
[22:39:58] <wired> Halcy0n / Betelgeuse?
[22:40:21] <Betelgeuse> yes (and I don't have particular ties to any of the proposed naming schemes in the GLEP)
[22:40:49] <Halcy0n> Yes (and I prefer the second naming scheme)
[22:41:01] <wired> okay
[22:41:15] <wired> glep 55 was voted down 4 to 2
[22:41:31] <Halcy0n> glep 54 is a no for me then
[22:41:41] <Betelgeuse> If performance doesn't really matter (would need some numbers) it could also be a subdirectory if people are tied to .ebuild
[22:42:03] <Halcy0n> Betelgeuse: if we stick with .ebuild, we are tied to waiting and can't use it immediately.
[22:42:19] <Betelgeuse> Halcy0n: Not if the new EAPIs move to a pkg sub directory
[22:42:27] <Halcy0n> Oh, I see what you mean now.
[22:42:41] <Halcy0n> I personally don't like that solution, for reasons brought up in the glep.
[22:42:56] <ssuominen> need to stick with .ebuild, anything else is just extra layer of complexity, the benefits from this certainly doesn't outweight the unconvinience
[22:43:01] <wired> lets revisit our GLEP 54 vote now
[22:43:05] <jmbsvicetto> glep54 - yes with '_' restriction
[22:43:18] <Chainsaw> wired: GLEP54, yes, underscore not dash.
[22:43:22] <wired> i vote yes with _ and preferably "live"
[22:43:29] <Betelgeuse> I don't like showing warnings on users so no.
[22:44:00] <Halcy0n> ssuominen: a one time change of the extension to "eb" shouldn't be anything that's another layer of complexity, but the conversation has moved on I guess.
[22:44:03] <wired> i recommend voting between "live" and "scm" now so we can avoid bikeshedding later
[22:44:04] <ssuominen> yes
[22:44:07] <jmbsvicetto> ssuominen: I'd be willing to have a one time extension change (.eb?) if we use it to implement large changes
[22:44:39] <jmbsvicetto> I can live with live or scm.
[22:44:45] <Betelgeuse> wired: I would like that vote on if people are categorily against 55 or just the particular scheme it prefers
[22:44:54] <Halcy0n> jmbsvicetto: you don't like one of the proposed solutions of having it be foo-1.2.3-eapi_4.eb?
[22:45:07] <jmbsvicetto> no
[22:45:14] <jmbsvicetto> I don't want the EAPI exposed in the file name
[22:45:18] <ssuominen> me either
[22:45:27] <Betelgeuse> Halcy0n: It doesn't have to be pkg/eapiX/ could be just pkg/new-apis/foo-1.ebuild
[22:45:38] <ssuominen> btw, i'm fine with both live or scm, the naming doesn't really bother me, long as it's short and to the point
[22:45:41] <wired> Betelgeuse: i don't like any of the proposed "solutions"
[22:45:43] <Betelgeuse> Halcy0n: so only a single readdir more
[22:45:56] <wired> personally I believe we can live with some old portage warnings
[22:45:57] <Halcy0n> Even having it easily fetchable in the ebuild is fine.  I just don't see how you guys can go ahead with G54 when its clearly going to show warnings to users.
[22:46:15] <ssuominen> {package}-${version}.ebuild, profit. where ${version} can be scm or live or ...
[22:46:29] <wired> this thing has had so much bikeshedding its already pretty safe to apply glep 54 without breaking portage (when the glep was written it was not safe)
[22:46:59] <Betelgeuse> Halcy0n: yeah a feature for ricers should not show warnings to stable users
[22:47:29] <Chainsaw> There's a difference between warnings & breakage.
[22:47:35] <Betelgeuse> wired: s/old/current/
[22:47:53] <wired> Halcy0n: glep55 is about ebuild suffixes...
[22:47:55] <Chainsaw> Obviously it should not be phased in until a portage supporting the feature is marked stable.
[22:48:29] <Halcy0n> wired: g55 has other proposed solutions that we could talk about.
[22:48:29] <Chainsaw> A portage that warns until it is upgraded is acceptable. A portage that falls over so the situation is unrecoverable is not.
[22:48:52] <Chainsaw> (Ever tried upgrading an installation that is >6 months old? Warnings are the least of your problems)
[22:49:10] <jmbsvicetto> Halcy0n: GLEP-54 should be implemented on a new EAPI version
[22:49:42] <Betelgeuse> jmbsvicetto: o_O
[22:49:51] <Halcy0n> Its not going to understand the version no matter what you do at this point.
[22:50:03] <jmbsvicetto> Betelgeuse: ?
[22:50:32] <Betelgeuse> jmbsvicetto: just venting fustration a little
[22:50:41] <jmbsvicetto> ok
[22:51:03] <Betelgeuse> jmbsvicetto: as Halcy0n said you can't change version rules with our current way of naming files
[22:51:13] <Betelgeuse> Not tomorrow any way.
[22:51:19] <jmbsvicetto> sure
[22:51:26] <Chainsaw> You'd be about two months out before your first _live can go in, yes.
[22:51:40] <Halcy0n> Which is quite a crappy way to try and make the distribution move fowards.
[22:51:44] <Halcy0n> s/fowards/foward/
[22:51:55] * jmbsvicetto borrows an r to Halcy0n 
[22:51:57] <Betelgeuse> And what about the next feature like GLEP 54?
[22:52:02] <Betelgeuse> The same thing again?
[22:52:04] <Betelgeuse> And then again?
[22:52:05] <Betelgeuse> And again?
[22:52:28] <Chainsaw> 55 is down. If you feel 54 depends on 55, reflect it in your vote please.
[22:52:31] <peper> Halcy0n: to be fair the gleps are from 2007
[22:52:33] * peper hides
[22:52:43] <Halcy0n> peper: yea, but I'm trying to be foward thinking here :)
[22:52:44] <jmbsvicetto> Betelgeuse: That's why I want a new discussion including the idea of adding repo version files so that we can finally address the issue
[22:53:02] <Betelgeuse> Chainsaw: Already did. But I wanted that vote if the problem should be solved.
[22:53:09] <Betelgeuse> wired: I think you should control the meeting a little more.
[22:53:24] <jmbsvicetto> Betelgeuse: I'm even willing to have a one time extension change for that - assuming the not having to wait 1 year to implement rule
[22:53:41] <Betelgeuse> jmbsvicetto: That's an option in 55.
[22:53:53] <wired> well we are having a civilized discussion here and these are all our topics :)
[22:54:19] <jmbsvicetto> Betelgeuse: the problem is 55 dismisses the choice of having EAPI inside the file and has a few premisses I don't agree with
[22:54:49] <Betelgeuse> wired: The point was voting to end the discussion :)
[22:55:15] <peper> jmbsvicetto: dismisses? It just shows some weak points
[22:55:20] <jmbsvicetto> wired: Did we get a full count on 54?
[22:55:23] <wired> Betelgeuse: we voted on 55 but then you guys have objections on 54 cause of the 55 vote... so we talk :)
[22:55:29] <Betelgeuse> jmbsvicetto: "Easily fetchable EAPI inside the ebuild and one-time extension change" is an option there?
[22:55:41] <jmbsvicetto> peper: To get it to be considered took quite a large discussion in the mls ;)
[22:55:56] <jmbsvicetto> peper: the initial drafts ignored that option
[22:55:59] <Chainsaw> wired: They will vote no on 54 because 55 is down. Probably means 54 is down as well. Best take counts now.
[22:56:33] <wired> true
[22:57:07] <Betelgeuse> jmbsvicetto: We can consider the performance implications acceptable to get a solution that gets enough votes behind the GLEP but maybe not happening
[22:57:22] <Halcy0n> As I said, no to 54 without 55.  I need to go to a meeting in 4 minutes, so lets please wrap this up.
[22:57:32] <peper> jmbsvicetto: Yes, because I didn't thinkg it would cause such a reaction from some folks. The proposed solution seemed the best to me and I didn't see the point of considering other approaches at first
[22:57:33] <wired> well 54 passes the vote, 4 to 2
[22:57:58] <Chainsaw> I can make it a tie and let it die if that ends the meeting?
[22:58:04] <Betelgeuse> Also the benefits of 55 are not really apparent now as it enables further development.
[22:58:05] <wired> with the underscore instead of the hyphen
[22:58:08] <jmbsvicetto> peper: ok, understood
[22:58:27] <wired> Chainsaw: there's no need, it'll end anyway :)
[22:58:34] <wired> do we have a preference between live and scm?
[22:58:38] <Chainsaw> wired: live
[22:58:42] <wired> live+1
[22:59:03] <wired> jmbsvicetto: ssuominen (who voted for)?
[22:59:21] <jmbsvicetto> I'll accept either
[22:59:22] <Chainsaw> wired: No on 54. Then both fail.
[22:59:36] <wired> Chainsaw: sorry?
[22:59:38] <Chainsaw> wired: And we don't have a broken tree.
[22:59:54] <jmbsvicetto> Chainsaw: 54 doesn't break the tree
[23:00:20] <Halcy0n> Not if you wait the normal year, which is apparently our development cycle now O:)
[23:00:20] <Betelgeuse> Depends on your definition of "broken"
[23:00:50] <ssuominen> yes to 54, long as it can be implented without 55. waiting is fine.
[23:00:59] <wired> Chainsaw: did you change your vote to no?
[23:01:20] <Halcy0n> It doesn't matter since it passed anyway without him changing his vote.
[23:01:33] <tanderson> Halcy0n++
[23:01:53] <wired> thats not true, if he votes no we have a tie...
[23:02:10] <Betelgeuse> The summary defenitely needs to list who voted what.
[23:02:16] <Chainsaw> Playing devils advocate, it doesn't matter either way.
[23:02:54] <peper> wired: a tie with 7 people? impressive ;p
[23:03:02] <wired> peper: ferringb is not here
[23:03:05] <Chainsaw> peper: One of whom is not present.
[23:03:11] <Chainsaw> peper: You can do it with 6.
[23:03:26] <wired> Chainsaw: please make clear what you voted so we can move on :)
[23:03:29] *** Quits: lavajoe (~joe@gentoo/developer/lavajoe) (Ping timeout: 240 seconds)
[23:03:48] <Chainsaw> wired: GLEP54: yes
[23:03:54] <wired> thanks
[23:03:56] <peper> I thought ssuominen is proxying for ferringb but now I see scarabeus is not here as well. nvm
[23:04:20] <wired> peper: underscore, live, glep 54 is a go
[23:05:08] <peper> wired: with foo_live or foo-live?
[23:05:09] <jmbsvicetto> so, are we done with the votes?
[23:05:25] <Chainsaw> peper: underscore, not dash.
[23:05:31] <Chainsaw> jmbsvicetto: I believe so.
[23:05:34] <wired> jmbsvicetto: yes
[23:05:50] <jmbsvicetto> So, do we have any updates in the open bugs?
[23:05:54] <wired> yes
[23:05:55] <wired> bugs
[23:06:01] <wired> bug 256451 is done
[23:06:02] <peper> Chainsaw: well, no dash at all in PNV would be something new
[23:06:03] <willikins> wired: https://bugs.gentoo.org/256451 "Council meeting notes appear to be missing"; Doc Other, Other; RESO, FIXE; gentoo-bugs@allenjb.me.uk:council@g.o
[23:06:14] <wired> bug 256453 is done
[23:06:15] <Betelgeuse> wired: So I don't get my vote in this meeting?
[23:06:16] <willikins> wired: https://bugs.gentoo.org/256453 "Documentation on Gentoo Council meeting processes, particularly regarding agenda items"; Doc Other, Other; RESO, FIXE; gentoo-bugs@allenjb.me.uk:council@g.o
[23:06:28] <wired> Betelgeuse: you voted no?!
[23:06:46] <Betelgeuse> wired: The vote on if we want to solve the problems GLEP 55 was for.
[23:06:57] <jmbsvicetto> Betelgeuse: sorry, my bad
[23:07:07] <Betelgeuse> wired: I didn't ever see a clear vote count on that.
[23:07:12] <jmbsvicetto> Betelgeuse: I forgot your request
[23:07:14] <ulm> peper: foo_live doesn't make sense since you need to have ${PN}-${PV}
[23:07:18] <wired> Betelgeuse: you're right
[23:07:30] <peper> ulm: see scrollback..
[23:07:41] <jmbsvicetto> ulm: we talked about getting that
[23:07:57] <jmbsvicetto> ulm: having ${PN}_live
[23:08:25] <jmbsvicetto> wired: Yes from me in that we want to solve the issues raised by GLEP 55
[23:09:17] <wired> yes from me as well
[23:09:26] <Betelgeuse> yes
[23:09:26] <Halcy0n> yes
[23:10:01] <wired> Chainsaw: ssuominen?
[23:10:58] <Chainsaw> wired: I want to solve the issues in GLEP55, yes. Just not this way.
[23:11:20] <wired> ok. ssuominen?
[23:11:21] <Chainsaw> wired: Perhaps we should agree to shelve both, as it is unlikely we will agree on 54 now?
[23:11:57] <ssuominen> GLEP55 is a big no long as it talks about changing .ebuild or adding extra suffixes to it
[23:12:09] <Betelgeuse> ssuominen: That wasn't the question
[23:12:16] <wired> ssuominen: we're talking about the issues raised
[23:12:18] <wired> not the solution
[23:12:32] <ssuominen> I don't like the underscore, _
[23:12:50] <jmbsvicetto> ssuominen: please answer the question ;)
[23:12:52] <wired> i don't understand ;p
[23:12:54] <ssuominen> err wait
[23:13:12] <jmbsvicetto> 20:06 <@Betelgeuse> wired: The vote on if we want to solve the problems GLEP 55 was for.
[23:14:02] <ssuominen> no
[23:14:06] <ssuominen> shelve it
[23:14:18] <wired> ok
[23:14:19] <wired> thanks
[23:14:28] <jmbsvicetto> So, bugs again?
[23:14:28] <Betelgeuse> Can those people who voted yes now and no to GLEP 55 tell their preferred solution(s).
[23:15:00] <jmbsvicetto> Betelgeuse: That is something I'd like to talk in the mls. I don't have answers for now
[23:15:30] <Chainsaw> I would like to say shelve both for further discussion. And my apologies for being disruptive.
[23:16:03] <Chainsaw> (In the future my first vote is what goes, and nothing else. I am notoriously undecisive if I don't keep stop myself.)
[23:16:20] <Chainsaw> s/stop/stopping/
[23:16:56] <Betelgeuse> wired: your solution?
[23:18:00] <wired> Chainsaw is not making this easy
[23:18:07] <Chainsaw> I know.
[23:18:15] <wired> does anyone else feel we should discuss glep54 for two more weeks?
[23:18:23] <ssuominen> wired: o/
[23:18:47] <Chainsaw> Yes, let's go back to discussions.
[23:19:03] <Chainsaw> And we'll have a rule next meeting that I can't change my vote. Ever.
[23:19:55] <jmbsvicetto> Chainsaw: never say never
[23:20:19] <Chainsaw> jmbsvicetto: I certainly don't want a repeat of this.
[23:20:29] <antarus> Its like a gaggle of school girls in here
[23:20:36] <Chainsaw> Hi antarus.
[23:20:55] <wired> Chainsaw: lets say that glep 54 passed, but we will give it 2 weeks to see if we can find a better way to implement it (through a new solution to glep55)
[23:21:18] <peper> antarus: "A group of fat chicks; usually chit-chatting and usually snacking on fried foods.
[23:21:22] <peper> what? :D
[23:21:37] <Betelgeuse> wired: GLEP 54 should go in as part of an EAPI an ywa
[23:21:41] <Betelgeuse> wired: And that takes some time.
[23:21:54] <wired> Betelgeuse: shhh ;p
[23:22:00] <antarus> peper: I think urban dictionary failed you there ;p
[23:22:02] <wired> what im trying to say is
[23:22:25] <wired> we all want glep54, lets not vote for it again, its passed, lets just focus on finding the best way to implement it
[23:22:43] <Betelgeuse> wired: It was already passed before the meeting started :)
[23:22:46] <jmbsvicetto> can we move on to bugs?
[23:22:50] <wired> yes please
[23:22:52] <jmbsvicetto> We're almost on 90 minutes
[23:23:05] <wired> bug 234706
[23:23:09] <willikins> wired: https://bugs.gentoo.org/234706 "Slacker arches"; Gentoo Linux, Unspecified; NEW; dberkholz@g.o:council@g.o
[23:23:13] <antarus> you guys need to learn how to cut the meeting short; its ok to delay items to the next one
[23:23:25] <antarus> but ignore me and finish ;p
[23:23:29] *** Joins: jbergstroem (~johan@bergstroem.nu)
[23:23:30] <wired> antarus: the purpose here is not to "get done with it" :P
[23:23:57] <jmbsvicetto> wired: anything worthy to discuss about bugs, besides the 2 closed bugs?
[23:24:01] <wired> Halcy0n: ^^ bug, any progress?
[23:24:10] <jmbsvicetto> I don't think there were any relevant updates to the others
[23:24:11] <wired> jmbsvicetto: your bug, any progress?
[23:24:39] <jmbsvicetto> no, I said I was going to address it in 1 month, not 2 weeks :P
[23:24:45] <wired> you never know ;p
[23:25:11] <wired> ok
[23:25:21] <jmbsvicetto> next meeting?
[23:25:31] <wired> next meeting date: 2010-09-13
[23:25:36] <wired> jmbsvicetto: you'll chair? =]
[23:25:53] <Chainsaw> wired: That works for me. 1900UTC as usual?
[23:26:16] <wired> Chainsaw: yes
[23:26:54] <Betelgeuse> ok for me
[23:26:57] <jmbsvicetto> wired: I will
[23:27:01] <wired> great
[23:27:11] <wired> anyone from the community wants to discuss anything?
[23:27:22] <antarus> sure
[23:27:29] <antarus> when are we getting an expresso machine?
[23:27:33] <antarus> cause my work has one
[23:27:35] <antarus> and damn its good
[23:27:39] * jmbsvicetto points to the foundation lounge
[23:27:45] <peper> are you going to resurrect the g55 threads on the m/l?
[23:27:46] <jmbsvicetto> antarus: they have the money ;)
[23:27:52] <NeddySeagoon> antarus, the Foundation has one too
[23:27:59] <wired> antarus: here are the keys, please don't lose them
[23:28:04] <Philantrop> peper: 55 is dead.
[23:28:34] <jmbsvicetto> peper: Seems a few of us want to discuss the issues GLEP 55 tried to address
[23:28:38] <wired> peper: we'll be talking about a solution
[23:28:46] <NeddySeagoon> Philantrop, I thought the principle of 55 was agreed but not the solutions
[23:28:54] <jmbsvicetto> peper: tried as it wasn't approved - no other meaning
[23:29:01] <peper> Philantrop: it's been dead for 3 years now. It just has this funny feature of coming back every now and again
[23:29:47] <wired> tbh i voted no for ebuild naming changes but Im willing to discuss other options
[23:29:54] <jmbsvicetto> peper: if needed, we can look for some silver bullets
[23:30:25] <antarus> peper: like the herpes
[23:30:28] <Betelgeuse> We should just vote on individual implementation options.
[23:30:30] <jmbsvicetto> ok, if there's nothing else, I'm going to look around for dinner
[23:30:49] <wired> Betelgeuse: maybe. we'll have a more detailed agenda on this next time :)
[23:31:03] <wired> great
[23:31:05] <Chainsaw> Something tells me both will be back in some form.
[23:31:25] <wired> thanks everyone
[23:31:30] <wired> meeting is now over
