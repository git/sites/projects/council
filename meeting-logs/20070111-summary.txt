Summary of the Gentoo Council meeting held 11 January 2007
----------------------------------------------------------
(Summary prepared by robbat2).

Roll-call:
Present: flameeyes (proxied by UberLord), kingtaco, kloeri, kugelfang, robbat2, wolf31o2
Absent: vapier

No agenda items were raised ahead of time, so we just went in alphabetical
order.
- Kugelfang reported that the EAPI0 draft document is not quite complete. spb had
  hoped to have it ready before the meeting, but didn't manage. It has been
  restricted thus far to avoid 'large discussions about minor details' while
  the bigger picture is assembled. It will be open for all input and
  refinements later.
- Kugelfang visited the issue of the contents of /usr/libexec. Diego and
  Vapier had raised it previously, and Kugelfang is working on the document,
  in a style similar to FHS.
- Kugelfang wanted to know about the process for council members stepping
  down. This was prompted by solely by Flameeye's message to the council
  channel earlier in the day, which implied he was retiring. The point was
  made moot by Flameeye's later blog post that he was going to take a two week
  break instead.
- On the procedural side, both Kugelfang and KingTaco wanted to know what the
  what the process for a retiring council member was. Should it be the next
  person on the original ballot results, or should a further election be held?
  The spirit of the council GLEP was a further election, but some questions
  were had in this. The issue needs to be raised on the -dev mailing list, and
  revisited during the next council meeting.
- Robbat2 reported on the successful bugzilla migration, and the work for the
  new CVS server. The Bugzilla news was well recieved.
- Robbat2 brought up the status of the SPF documentation. Kloeri said that he
  has them in a nearly finished state, but hasn't had a chance lately to
  complete them. Kloeri will other complete them shortly, or upload the drafts
  to the bug in the meantime.
- Robbat2 requested that for helping to get an agenda together in future, all
  council members should just braindump their potential minor items to the
  council mailing list a few days ahead of each meeting. Large issues should
  still be raised on -dev/-core as needed, but the smaller stuff like
  followups can just be braindumped. Robbat2 promised to rig an automated
  reminder to the council members.
- A last call for vapier was made, and since he didn't show, he got his
  slacker mark for this meeting.
- Wolf31o2 inquired as to the status of the Reply-To documentation (bug
  154595). As there was absolutely no progress, Wolf31o2 was going to just
  write it up and convert it to GuideXML.
- Wolf31o2 proposed the concept of council-managed projects. These are to be
  Gentoo-specific projects where the council takes the initiative of defining
  creating software specifications and requirements, recruits people to
  work on them (not nessicarily developers), and helps manage the project
  (leaving people to actually work on it). Some past almost precedents were
  noted and the council was in favour of the general concept. Wolf31o2 was
  going to seek out some initial proposals for small projects to test the
  concept on.

The floor was opened at this point.

- KingTaco jokingly asked if the Gentoo Foundation could afford to buy
  Sealand, which lead into real queries about the current financial reports.
  Wolf31o2 located a November 2006 posting in the NFP archives and provided a
  link to it.
