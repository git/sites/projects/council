Roll Call:
===========
Betelgeuse: here 
Calchan: absent
dertobi123: here 
leio: here
lu_zero: here
solar: here
tanderson(secretary):  absent, but logged it
ulm: here

Update on LiveCD/DVD for Gentoo 10.0.
====================================
    
        Ned Ludd(solar) commmented that things were progressing fine. A new snapshot
    will be taken on September 20th and the cutoff date will be the 4th of
    October.

A Way to Modify the PMS such that it doesn't directly involve the EAPI Process.
===============================================================================

        Joshua Jackson(tsunam) requested a decision on a process to modify PMS
    without involving the EAPI Process.

    There was discussion about whether PMS is a documenting simply documenting
    the ebuild API or if it is a broader document covering the entire tree.
    Agenda Item[1] 3.1 was deferred until the next meeting to be discussed on
    mailing lists beforehand. 

Discussion of the Need for a PMS/EAPI committee outside of the council.
=======================================================================

        Of the three proposals(An external committee consisting of package manager
    representatives, a council member or two, and perhaps members of the PMS
    project; Using the existing PMS project; Something completely different),
    the council chose to do something complete different and what will be done
    will be discussed on list or next meeting.

[1]: http://archives.gentoo.org/gentoo-council/msg_96c702e85f79b8f5e22472ae2c961534.xml. 
