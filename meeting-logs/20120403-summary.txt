Summary of Gentoo council meeting 3 April 2012

Roll call
=========
Present: betelgeuse, chainsaw, dberkholz, grobian, hwoarang, ulm.
Missing: jmbsvicetto.

EAPI specification in ebuilds
=============================
Discuss/vote which of the proposals should be further pursued and
worked out in detail:
  A: Parse bash assignment statement
  B: Comment in first line of header
  C: Bash function
  D: Filename extension (GLEP 55)
  E: External metadata file
  F: None, keep status quo

The council members expressed their preference for option A.
Subsequently, a vote on option A was taken, which has been accepted
with 5 yes votes and 1 abstention.

Action: Work out exact specification until next meeting (ulm).

New udev and separate /usr partition
====================================
Decide on whether a separate /usr is still a supported configuration.
If it is, newer udev can not be stabled and alternatives should be
investigated. If it isn't, a lot of documentation will have to be
updated. (And an alternative should likely still be provided.)

The council has voted in favour of a separate /usr being supported
(5 yes, 1 no vote).

During the discussion, some concerns were raised that we might not be
able to provide a modified or forked udev version. Chainsaw assured
that if necessary, he will maintain a udev version that supports said
configuration.

It was remarked that a solution that comprises both the forked udev
version (separate /usr) and the latest versions is possible and
therefore should not block either way preferred by users.

herds.xml and mail aliases
==========================
Should devs be required to add themselves to herds.xml when adding to
mail alias?

This was rejected by the council (5 no votes).

Open bugs with council involvement
==================================
Bug 383467 "Council webpage lacks results for 2010 and 2011 elections":
Postponed, because jmbsvicetto isn't present.

Bug 408073: "Make the user choose a locale"
Action: ulm will ping the docs team.

Open floor
==========
No issues were brought up to the council by the community.

Next meeting date
=================
8 May 2012, 19:00 UTC.
