[15:00:07] <rich0> Ok, roll call :)
[15:00:10] <radhermit> here
[15:00:15] <WilliamH> here
[15:00:16] <dberkholz|mob> Sup
[15:00:49] -*- creffett|irssi here for ulm, unless ulm is here already
[15:01:02] <rich0> blueness, dilfridge, ulm?
[15:01:57] <rich0> Ok, let's get started.
[15:02:04] <rich0> First item, future of einstall
[15:02:14] <rich0> http://thread.gmane.org/gmane.linux.gentoo.devel/92713
[15:02:14] <rich0> http://thread.gmane.org/gmane.linux.gentoo.devel.announce/2212/focus=4025
[15:02:27] <rich0> Should einstall be banned in EAPI6.
[15:02:32] <rich0> Any comments beyond the lists?
[15:02:37] -*- creffett|irssi reviews his notes
[15:02:49] <creffett|irssi> no comments here
[15:02:54] <WilliamH> none here
[15:03:27] <radhermit> I don't have anything more to say
[15:03:32] <rich0> Ok, let's vote then.  "Einstall will be removed from EAPI6."
[15:04:07] -*- creffett|irssi yes
[15:04:12] <dberkholz|mob> Yep
[15:04:13] <radhermit> yes
[15:04:16] <WilliamH> yes
[15:04:31] -*- rich0 yes
[15:04:49] <rich0> Ok, that's all of us
[15:05:02] <rich0> Next item...
[15:05:11] <rich0> https://wiki.gentoo.org/wiki/User:Blueness/GLEP64
[15:05:20] <rich0> Blueness is requesting approval on this.
[15:05:24] <radhermit> someone want to text blueness?
[15:05:30] <rich0> good idea
[15:06:24] <dilfridge> sorry, here
[15:06:26] <rich0> I just texted him
[15:06:55] <rich0> Do we want to move on to git?
[15:07:05] <radhermit> sure
[15:07:06] <rich0> I'd prefer to give him the option to present.
[15:07:11] <blueness> here!!!
[15:07:11] <rich0> Ok, let's move on to git.
[15:07:16] <blueness> sorry thanks rich
[15:07:18] <rich0> never mind. :)
[15:07:22] <blueness> rich0,
[15:07:24] <rich0> let's do glep64 - I think it will be faster
[15:07:41] <rich0> blueness: do you have any comments you want to make?
[15:07:51] <blueness> rich0, just a few points
[15:07:58] <blueness> its was discussed on gentoo-dev@
[15:08:14] <blueness> it got feedback for ciarian and incorportated it
[15:08:28] <blueness> do you need me to repeate the motivation?
[15:08:39] <rich0> Nah - at least not for me.
[15:08:42] <rich0> I can read.  :)
[15:08:52] <rich0> My only real comment is that it is a bit vague - deliberately so.
[15:09:07] <blueness> this is the latest version -> https://wiki.gentoo.org/wiki/User:Blueness/GLEP64
[15:09:11] <rich0> I don't mind approving it per se, but do we think that it will go anywhere?
[15:09:37] <rich0> Ie are the various package managers behind it?
[15:09:37] <dberkholz|mob> Do we have agreement in theory from PM implementers?
[15:09:38] <blueness> rich0, i will try to work with ciarian and actually write code
[15:09:55] <WilliamH> blueness: what about pkgcore?
[15:09:56] <blueness> i'd like to hear from radhermit and package core
[15:09:56] -*- dberkholz|mob high-fives rich0
[15:10:32] <blueness> radhermit, ping ^^^
[15:10:45] <blueness> also zmedico was in support
[15:10:46] -*- radhermit is trying to skim through the mailing list thread :)
[15:10:53] <blueness> radhermit, okay
[15:10:58] <rich0> It sounds like most of this is in portage, it just needs the API to be written.
[15:11:08] <rich0> From what I know of portage, it won't be hard to do there.
[15:11:17] <rich0> Just needs commitment to the API.
[15:11:27] <radhermit> can we version the vdb or something if we start properly specifying it?
[15:11:40] <radhermit> maybe that's already in the glep
[15:11:53] <blueness> radhermit, i didn't mention a version to vdb
[15:12:00] <rich0> This GLEP doesn't really specify the VDB, so much as require an interface to it (without actually specifying it).
[15:12:17] <radhermit> so mainly it's about standardized file naming?
[15:12:17] <blueness> for the reason rich0 just mentioned ^^^
[15:12:24] <rich0> It might not hurt to incorporate some kind of VAPI versioning.
[15:12:35] <blueness> radhermit and standardizing what's exported
[15:12:40] <dberkholz|mob> Seems to me that vdb version would be a portage internal matter
[15:12:47] <rich0> It basically is a spec for the spec.
[15:12:48] <dberkholz|mob> What I care about is an API version on this
[15:13:23] <blueness> dberkholz|mob, i can add a sentence to that effect
[15:13:24] <rich0> dberkholz|mob: ++
[15:13:30] <dilfridge> good idea
[15:13:36] <rich0> That should be a part of the API when it is specified.
[15:13:58] <radhermit> basically what I meant
[15:14:20] <rich0> It feels a bit odd to approve this other than going along with the general sentiment that it is a good idea, but I have no objections to it.
[15:14:33] <rich0> It just feels a bit like approving a business case, vs a spec.
[15:14:51] <blueness> yeah, it turns out now there are not only several packages but also one eclass depending on vdb information from portage, none of which work with other pm's but could
[15:15:18] <rich0> SELinux and such sounded like a really good use case here.
[15:15:27] <rich0> You'd want that to work with any PM.
[15:15:48] <rich0> Or PaX in your example.
[15:15:58] <blueness> rich0, the way selinux eclass works now is it looks for reverse deps to do the markings
[15:15:59] <radhermit> mostly I'd like to quit having to read through portage code to make stuff like eix work :)
[15:16:04] <radhermit> with pkgcore-merged pkgs
[15:16:35] <rich0> Yeah, I have an EAPI hunter that depends on portage APIs, though to be fair this only pertains to installed packages I believel.
[15:16:45] <rich0> It might make sense to extend that API to installable packages as well.
[15:16:46] <blueness> yeah, i didn't even know about pkgcore until recently and it could benefit from this too
[15:17:06] <rich0> Ok, do we want to vote to approve this?
[15:17:13] <dilfridge> +
[15:17:48] <rich0> "We approve GLEP64 as documented at https://wiki.gentoo.org/wiki/User:Blueness/GLEP64 "
[15:17:56] <rich0> Does that work?
[15:18:00] <blueness> sure
[15:18:10] <rich0> You can promise not to change it too much.  :)
[15:18:13] <rich0> Ok, let's vote.
[15:18:17] <blueness> o
[15:18:21] -*- rich0 yes
[15:18:26] <dilfridge> yes
[15:18:27] -*- blueness yes
[15:18:32] -*- creffett|irssi abstain
[15:18:36] <dberkholz|mob> Yes + API version
[15:18:45] -*- WilliamH abstain
[15:19:09] <radhermit> yes with API version stuff
[15:19:28] <blueness> ulm, ?
[15:19:37] <radhermit> creffett|irssi is ulm
[15:19:49] <rich0> Ok, that's everybody - 5-0
[15:20:03] <rich0> And that includes the API version - I'll note that in the sumary.
[15:20:21] <blueness> rich0, and everyone, i think that just a one sentencer no?
[15:20:26] <rich0> I'll document it as: "We approve GLEP64 as documented at https://wiki.gentoo.org/wiki/User:Blueness/GLEP64 with API versioning added."
[15:20:33] <rich0> blueness: wfm
[15:20:52] <rich0> Ok, now the fun topic.
[15:20:54] <rich0> Git migration
[15:21:11] <dilfridge> wheee
[15:21:14] <blueness> shudder
[15:21:19] <rich0> My personal goal here would be to get opinions recorded anywhere we think they matter.
[15:21:25] <rich0> We're not going to bikeshed every detail.
[15:21:39] -*- mgorny is around to help :P
[15:21:41] <rich0> But, if there are things that we feel must be in place to do a migration, we should try to get them documented.
[15:21:44] <rich0> That is my sense of it.
[15:21:48] -*- WilliamH thinks we need to stop waiting for a perfect world and get it done ;-)
[15:22:05] <rich0> Any other comments before we dive in?
[15:22:34] <creffett|irssi> bring it on!
[15:22:44] <rich0> The first question in the agenda, is do we need to continue to create new ChangeLog entries once we're operating in git?
[15:22:54] -*- WilliamH no
[15:22:59] <dilfridge> no
[15:23:02] <rich0> no
[15:23:07] <creffett|irssi> nope.
[15:23:09] -*- blueness no
[15:23:18] <dberkholz|mob> hell no
[15:23:43] <radhermit> no
[15:23:51] <rich0> Ok, well, let's just call that a vote.  :)
[15:23:52] <dilfridge> !
[15:24:26] <rich0> Ok, let's skip "are we done yet" and move that to the end after we tackle all the specifics
[15:24:34] <rich0> Can yyyy/ prefix be dropped from gentoo-news?
[15:24:44] <rich0> We've been through that one once before.
[15:24:54] <rich0> http://archives.gentoo.org/gentoo-dev/msg_00f0a83b760b78c1baf32f118d1cb008.xml
[15:25:01] <rich0> https://bugs.gentoo.org/show_bug.cgi?id=523828
[15:25:23] <rich0> mgorny: will dropping this still make your life easier with metadata?
[15:25:38] <mgorny> rich0: a bit
[15:25:59] <mgorny> i just find it utterly stupid that 'reading' and 'writing' formats are different
[15:26:06] <rich0> ++
[15:26:11] <dilfridge> drop it
[15:26:27] <rich0> Any opposing commentary?
[15:26:38] <blueness> nah, no contraversy here
[15:26:46] <dberkholz|mob> Nope
[15:26:49] <blueness> who needs to implement this infra?
[15:27:07] <mgorny> someone commit to repo + infra change the script used for gen
[15:27:32] <rich0> Ok, let's vote "The yyyy/ prefix can be dropped from gentoo-news, timing to be determined by those implementing the change."
[15:27:44] <rich0> Does that work?
[15:27:44] -*- blueness yes
[15:27:47] -*- rich0 yes
[15:27:50] -*- creffett|irssi yes
[15:27:50] -*- WilliamH yes
[15:27:51] <dilfridge> yes
[15:27:59] <radhermit> yes
[15:28:04] <dberkholz|mob> Sure
[15:28:49] <rich0> ok
[15:29:10] <rich0> Ok, going in order of controversy...
[15:29:15] <rich0> Can we drop CVS headers post-migration?
[15:29:24] -*- WilliamH yes
[15:29:29] -*- rich0 burn with nuclear fire
[15:29:31] <dilfridge> yes please
[15:29:33] <creffett|irssi> KILL IT
[15:29:38] <blueness> heh
[15:29:41] <WilliamH> I don't think ghere is an equivalent to that in git.
[15:29:43] <creffett|irssi> er, I mean, yes
[15:30:24] <rich0> dberkholz|mob, radhermit - care to make it a vote?
[15:30:37] <rich0> blueness: also?
[15:30:39] <dberkholz|mob> Yes pls
[15:30:48] <radhermit> kill it of course
[15:31:06] <rich0> blueness: heh==yes?
[15:31:18] <blueness> yes
[15:31:23] <dilfridge> heh we're fast :)
[15:31:33] <rich0> ok
[15:31:39] <rich0> Now a bit more controversy.
[15:31:49] <rich0> Should we have separate git trees for historical vs current portage (with no parent commit reference from the one to the other)?  
[15:32:02] <rich0> http://thread.gmane.org/gmane.linux.gentoo.project/4030/
[15:32:37] <creffett|irssi> rich0: here's my question -- if someone did want to join the two trees locally, how much work would it be?
[15:32:43] <dilfridge> if we can arrangeit that they can be combined seamlessly into one, yes
[15:32:51] <dberkholz|mob> I would prefer a spliceable one
[15:32:55] <rich0> mgorny: you probably have more git replace experience than I
[15:33:19] <mgorny> git fetch history-remote; git replace ${first_commit_id} ${history_commit_id}
[15:33:23] <rich0> I'd think you could just fetch a second origin into another branch and then git replace the one into the history of the other
[15:33:29] <mgorny> (or teh other way around :P, easy to put on wiki)
[15:33:47] <radhermit> I'd vote for spliceable too
[15:33:55] <rich0> Yeah, you'd make the last commit in the history repo = the first commit in the active tree when doing a history
[15:33:55] <dilfridge> means?
[15:34:06] <radhermit> meaning you can graft the old tree onto the new one
[15:34:12] <rich0> radhermit: exactly
[15:34:14] <radhermit> if you want a giant, historical repo
[15:34:24] <WilliamH> Which ever one can get us up and running sooner. ;-)
[15:34:33] <rich0> Git will treat references to the first commit in the current tree as if it pointed to the last commit in the history tree.
[15:34:33] <creffett|irssi> so it's fairly simple to do the join if someone wants to?
[15:34:39] <radhermit> the old graft can technically be done later
[15:34:39] <rich0> So it would appear to have a continuous history.
[15:34:45] <mgorny> creffett|irssi: yes, only time consuming for fetch :)
[15:34:53] <creffett|irssi> mgorny: okay
[15:34:58] <creffett|irssi> then yes, separate is fine with me
[15:35:01] <dilfridge> radhermit: dberkholz|mob: I don't understand what your "spliceable" version does different
[15:35:06] <WilliamH> I have a question...
[15:35:06] <blueness> rich0, what's the gain on the divisionb between historical and current?
[15:35:14] <rich0> blueness: I outlined that in my post.
[15:35:22] <blueness> k
[15:35:25] <rich0> The current historical migrations have issues.
[15:35:43] <rich0> If we improve on them, then the original "official" migration turns into baggage.
[15:35:55] <dilfridge> blueness: we can start immediately and care about the exact history later
[15:35:56] <rich0> You could still splice a new migration over the old one.
[15:36:05] <WilliamH> So, if we have two trees: one would contain the history before the migration, and we would update the other from that point forward not worrying about the historical tree right?
[15:36:14] <mgorny> blueness: 1.5G
[15:36:24] <rich0> It also sidesteps arguments over whether the current migration is good enough, and makes the migration MUCH faster.
[15:36:25] <dilfridge> WilliamH: basically, yes. we start from a current point.
[15:36:26] <mgorny> 70M is 'current' afresh and grows
[15:36:29] <mgorny> 1.5G is historical and grows
[15:36:30] <blueness> so speed size and simplicity
[15:36:57] <blueness> what happens in the far future when it gets 1.5GB again, can it be sliced again?
[15:36:57] <dberkholz|mob> Ah I hadn't tracked the work on git replace. I'm fine with that
[15:36:57] <dilfridge> the conversion of the cvs history becomes a non-blocker, and non-critical project
[15:37:16] <rich0> exactly.  I'd still run the best migration that I could.
[15:37:32] <rich0> But, issues with it don't hold things up, and it could be improved on later.
[15:37:54] <rich0> Any other questions/concerns?
[15:38:10] <dilfridge> git question, if you end up pulling two separate histories, is there a way to prune the old, unused objects?
[15:38:16] <dilfridge> mgorny: ^
[15:38:36] <WilliamH> I think "git gc" will do that.
[15:38:37] <dilfridge> (not important now, just curiosity)
[15:38:42] <mgorny> dilfridge: there will be no unused objects if you merge them via replace
[15:38:55] <dilfridge> ok
[15:38:56] <mgorny> unless you mean after removing the history replace, then gc should catch them
[15:39:02] <dilfridge> ok
[15:39:03] <dilfridge> good
[15:39:25] <rich0> Yeah, the beauty of having separate repos is that you can easily get rid of the 750k bad commits if you have 750k better ones to replace them with.
[15:39:42] <dilfridge> hehe
[15:39:56] <rich0> The converted repository is pretty impressive, for all its faults.  :)
[15:40:04] <rich0> Something like 3M objects I think.
[15:40:27] <rich0> Ok, anything else before we vote?
[15:40:33] <blueness> i'm good
[15:40:48] <mgorny> if you mean having history1 repo and replacing part of it with history2, then objects from both repos will have to be kept
[15:40:53] <dberkholz|mob> I just want to make sure that the join gets tested, but I'm assuming that will happen
[15:41:07] <mgorny> dberkholz|mob: i've already tested it initially
[15:41:09] <rich0> "The git migration should produce a separate migrated and current repository, which can be spliced using git replace, but which are otherwise not connected."
[15:41:11] <dberkholz|mob> Awesome.
[15:41:20] <rich0> Any issues with the wording?
[15:41:31] <dberkholz|mob> What does "current" mean
[15:41:32] <rich0> The "which can be spliced with git replace" should cover the testing concerns.
[15:41:40] <dberkholz|mob> Last year, last X commits to each file, etch
[15:41:45] <rich0> Maybe historical and current ?
[15:41:47] <dberkholz|mob> etc*
[15:42:07] <rich0> Current means basically what you have in /usr/portage, really.
[15:42:10] <mgorny> newest version snapshot
[15:42:14] <rich0> Minus metadata/etc.
[15:42:14] <dberkholz|mob> And i'd go with s/migrated/historical/ , "full history" or something like that
[15:42:15] <mgorny> 'cvs up -dP'
[15:42:20] <rich0> Agree
[15:42:23] <mgorny> with some cleanup
[15:42:27] <dberkholz|mob> Oh a funtoo style thing with zero history
[15:42:35] <mgorny> that's the safe way of ensuring that we don't end up starting with broken repo
[15:42:35] <rich0> "The git migration should produce a separate historical and current repository, which can be spliced using git replace, but which are otherwise not connected."
[15:42:39] <mgorny> like current history migration causes
[15:43:15] <rich0> Well, we can at least get the CURRENT tree right with the migration.  It is identical now.
[15:43:25] <rich0> Go one commit back and it is less so.
[15:43:35] <rich0> Ok, if no issues with the wording...
[15:43:51] <rich0> Let's vote: "The git migration should produce a separate historical and current repository, which can be spliced using git replace, but which are otherwise not connected."
[15:43:55] -*- rich0 yes
[15:44:10] -*- blueness yes
[15:44:18] <radhermit> yes
[15:44:21] <creffett|irssi> yes
[15:44:30] -*- dilfridge yes
[15:44:31] -*- WilliamH yes
[15:44:44] <dberkholz|mob> k
[15:44:56] <rich0> ok, 7-0
[15:45:13] <rich0> That brings us to, "are we done yet?"
[15:45:37] <rich0> Are there any other high-level blockers we should consider, beyond just getting everything implemented and coordinated with infra, the migration team, etc?
[15:45:59] <dberkholz|mob> Beyond implementation. Like that's a minor issue. heh
[15:46:05] <dilfridge> mgorny: how's the status of whatever server-side hooks we need?
[15:46:07] <rich0> Also, what do we want the actual migration to look like?  Do we need to approve the final cutover, etc?
[15:46:23] <dberkholz|mob> It would be helpful if we could open up whatever backend code possible to enable more people to easily work on it
[15:46:37] <rich0> dberkholz|mob: ++ that is a problem with our current infra I think.
[15:46:42] <blueness> dberkholz|mob, yeah i'd like to see that
[15:46:46] <rich0> No reason the hooks/etc can't be FOSS.
[15:46:53] <mgorny> dilfridge: mostly done, i think infra will handle the remaining updates
[15:46:55] <rich0> Obviously passwords/configs/etc can be private.
[15:47:22] <dilfridge> is anyone from infra around who cares to comment? _robbat21irssi?
[15:47:45] <rich0> Making this FOSS would help a lot with anybody interested in "rolling your own Gentoo" 
[15:47:59] <mgorny> my code is on github, i think
[15:48:01] <mgorny> or bitbucket ;P
[15:48:11] <rich0> mgorny: I believe so.  
[15:48:49] <dilfridge> ok, let's consider a wurst-case scenario
[15:49:03] <rich0> dilfridge: systemd eats the repo?  :)
[15:49:04] <blueness> mgorny, email the community whree the hooks are so we can take a look at them
[15:49:07] <dilfridge> mgorny: if things fail badly, can we go back to cvs?
[15:49:25] -*- radhermit is going afk for a bit
[15:49:25] <dilfridge> (not that I want to, this is merely contingency planning)
[15:49:25] <mgorny> they were linked in my mails :P
[15:49:36] <rich0> dilfridge: that would be painful, at least if you wanted to preserve all the individual commits.
[15:49:44] <creffett|irssi> dilfridge: we would need a way to go git -> CVS to dump the history back into CVS
[15:49:44] <mgorny> dilfridge: i guess so though 'over dead commit access' of many people :)
[15:49:50] <rich0> If we want to do some kind of big test, better to do it first.
[15:50:12] <creffett|irssi> dilfridge: you could compromise, get a git->CVS bridge and keep the old CVS repo around for a little while until we're sure the bugs have been ironed out
[15:50:14] <dilfridge> preserving individual commits is second order problem, first priority would be to keep us functional.
[15:50:19] <dberkholz|mob> Can we stand up a beta, tell people to play with it for a week or so, then do the real cutover
[15:50:34] <dberkholz|mob> Or is our infra setup not able to cope with that kind of duplication
[15:50:44] <rich0> Well, having a read-only cvs for reference for a while makes sense.  We can keep a CVSROOT tarball forever, basically.
[15:50:48] <dilfridge> in this emergency case I'd be happy enough with seeing one big cvs commit "forward one week"
[15:51:06] <rich0> dberkholz|mob: we're basically doing the beta on github already.
[15:51:19] <rich0> I suppose it could be done on infra as well.
[15:51:22] <dberkholz|mob> Yeah but it's not full fledged
[15:51:27] <dberkholz|mob> That's a repo test, not a full distribution test
[15:51:43] <rich0> It certainly doesn't involve mirrors and all that.
[15:51:49] <mgorny> excelsior has all the git+rsync bits
[15:51:49] <rich0> Were you thinking full-scale end-to-end?
[15:51:53] <dilfridge> not sure how it could be full-fledged without switching e.g. the rsync mirror generation etc
[15:52:05] <dilfridge> and that would also affect our users, so no beta
[15:52:07] <dberkholz|mob> Don't need full scale, but at least full stack
[15:52:11] <rich0> We do generate all the way up to rsync trees though.
[15:52:24] <rich0> We can try to aggressively promote them.
[15:52:27] <dilfridge> sounds good.
[15:52:34] <WilliamH> So are we keeping rsync after the migration (I'm confused about that part)
[15:52:40] <mgorny> yes
[15:52:45] <mgorny> users can choose between git & rsync
[15:52:47] <rich0> Though all users can really do is sync them.  Unless we systematically sync all cvs commits it won't be the same as cvs.
[15:53:04] <rich0> mgorny: ++ - at least for now.
[15:53:14] <rich0> I think we should just generate the existing rsync, webrsync stuff.
[15:53:18] <rich0> Allow git as another option.
[15:53:24] <mgorny> this also means end users will not notice much of a difference
[15:53:24] <rich0> Then maybe consider more change down the road.
[15:53:38] <mgorny> except for disappearing changelogs and possibly resigned manifests
[15:53:45] <blueness> hmm ... will there be a delay between developer commits and staging to the mirrors like there currently is?
[15:54:23] <rich0> blueness: there would have to be some
[15:54:35] <rich0> mgorny: any idea what it would be?
[15:54:37] <mgorny> depends on exact implementation
[15:54:49] <rich0> It shouldn't be any worse than what we have now, at least.
[15:54:49] <blueness> we should try to keep one, just in case
[15:54:55] <mgorny> right now, there's ~3 minutes between dev git & master rsync, i think
[15:54:57] <rich0> I'd think that git will sync faster if nothing else.
[15:55:06] <mgorny> mirrors could fetch more often than rsync
[15:55:12] <mgorny> than with rsync*
[15:55:13] <dberkholz|mob> With git we could take a more push-driven approach
[15:55:20] <rich0> cvs syncing requires a full tree traversal.  git syncing is a lot smarter.
[15:55:21] <dberkholz|mob> Instead of 30 minute cron jobs or whatever
[15:55:37] <rich0> (you basically have COW at each level of the tree)
[15:56:20] <rich0> Ok, I have a hard stop in 4 mins.
[15:56:26] <rich0> Anything else on this?
[15:56:37] <rich0> I guess my question is, what next from us?
[15:56:43] <rich0> Do we need to approve some final cutover plan?
[15:56:51] <mgorny> 'd love to have games team decision today thouhg :P
[15:56:55] <rich0> Or do we just leave it up to infra and the migration team to just tell everybody what to do?
[15:57:21] <rich0> I don't necessarily mind if the rest continue on without me, but somebody else would have to chair that.
[15:57:30] <rich0> But, if we can wrap up git...
[15:57:45] <rich0> Does anybody feel that we need a final council vote on "all systems go?"
[15:57:48] -*- WilliamH thinks we really can't do anything more at this point.
[15:57:55] <rich0> Or can we just hand over the keys?
[15:58:10] <dilfridge> we need to take the step at some point.
[15:58:14] <dilfridge> so why not now.
[15:58:20] <rich0> Obviously we can step in off-schedule if we see cause to panic.  :)
[15:58:22] <blueness> i'd like to hear from infra about this
[15:58:27] <dilfridge> that said, *some* input from infra would be nice.
[15:58:30] <blueness> since they have to brunt the work
[15:58:40] <rich0> Well, nothing happens until they do something anyway.
[15:58:40] <blueness> dilfridge, collision!
[15:58:47] <dilfridge> :]
[15:58:58] <blueness> rich0, yeah but we really need to know if they're okay with this plan
[15:59:06] <rich0> I was thinking more in terms of whether we can just let infra and the git migration project run with the rest.
[15:59:24] -*- WilliamH doesn't see any reason not to
[15:59:40] -*- creffett|irssi needs to go shortly as well
[15:59:46] <rich0> Ok, I think we're basically all for moving forward, but we just want to make sure that infra is coordinated.
[15:59:49] <dilfridge> why not... we could just do a vote along "we don't see any big remaining obstacles and advise infra / the git migration project to proceed at their pace"
[16:00:05] <rich0> dilfridge: I'm fine with that.
[16:00:08] <rich0> Any strong objections?
[16:00:15] <blueness> not really
[16:00:27] <rich0> Ok. Let's vote, I have to RUN!  :)
[16:00:27] <creffett|irssi> no objections
[16:00:29] -*- rich0 yes
[16:00:31] -*- creffett|irssi yes
[16:00:34] -*- dilfridge yes
[16:00:34] -*- blueness yes
[16:00:38] -*- WilliamH yes
[16:00:44] <dberkholz|mob> ye
[16:00:45] <dberkholz|mob> s
[16:00:56] <dilfridge> rich0: shall I take over or do we postpone the rest?
[16:01:05] <dberkholz|mob> I've gotta run too, as did somebody else
[16:01:06] <rich0> radhermit ?
[16:01:13] <rich0> I suggest we adjourn.
[16:01:13] <dilfridge> ok then postpone I guess
[16:01:16] <rich0> Next week?
[16:01:20] <blueness> next week
[16:01:24] -*- WilliamH is fine with next week
[16:01:24] <dilfridge> next week
[16:01:49] <dberkholz|mob> wfm
[16:02:00] <rich0> Ok, we are adjourned until next week.  Radhermit, ping me with your vote on the last bit.  :)
[16:02:17] <rich0> I'll post log/summary
[16:02:20] <rich0> Thanks, all!
[16:02:32] <rich0> sorry, mgorny
[16:02:40] <rich0> games + herds next time
[16:02:42] <rich0> adios
