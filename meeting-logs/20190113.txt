2018-12-11 19:43:58-!- leio changed the topic of #gentoo-council to: 184th meeting: 2019-01-13 19:00 UTC | https://www.timeanddate.com/worldclock/fixedtime.html?iso=20190113T19 | https://wiki.gentoo.org/wiki/Project:Council | https://dev.gentoo.org/~dilfridge/decisions.html | "We're all individuals!" "No, I'm not...."
2019-01-13 19:56:00 * WilliamH is here
2019-01-13 20:00:29<@K_F> right, time is there then
2019-01-13 20:00:39<@K_F> Agenda: https://archives.gentoo.org/gentoo-project/message/782824290b81e455a8cac1e0457bbc8f
2019-01-13 20:00:47<@K_F> 1. Roll call
2019-01-13 20:00:48 * K_F here
2019-01-13 20:00:51 * slyfox is here
2019-01-13 20:00:55 * leio here
2019-01-13 20:02:03<@dilfridge> /&%)(/&=(/=()`
2019-01-13 20:02:05 * dilfridge here
2019-01-13 20:02:06 * WilliamH here
2019-01-13 20:02:31<@slyfox> !proj council
2019-01-13 20:02:32<+willikins> (council@gentoo.org) dilfridge, k_f, leio, slyfox, ulm, whissi, williamh
2019-01-13 20:02:42<@dilfridge> tamiko:
2019-01-13 20:03:00<@slyfox> ulm, Whissi ( tamiko ) ^
2019-01-13 20:03:04<@dilfridge> ulm and tamiko (for whissi) missing
2019-01-13 20:03:16<@K_F> lets give them a couple more minutes, light agenda today anyways
2019-01-13 20:03:43 * dilfridge gets a light snack for the light agenda and reads about light effects
2019-01-13 20:04:18<@K_F> sent an sms to ulm
2019-01-13 20:04:41<@K_F> but gives me time to light up a cigar anyways :)
2019-01-13 20:05:13<@ulm> K_F: thanks :)
2019-01-13 20:05:15 * ulm here
2019-01-13 20:06:57<@K_F> well, lets get started then, we'll see if tamiko shows up. For the record, whissi has notified of absence today and tamiko was appointed proxy.
2019-01-13 20:07:06<@K_F> 2. Updating Gentoo Repository Policy following introduction of AUTHORS
2019-01-13 20:07:06<@K_F> file (
2019-01-13 20:07:06<@K_F> https://archives.gentoo.org/gentoo-project/message/7f289a889e714e517db13a16250037c5
2019-01-13 20:07:16<@K_F> ulm: do you want to make a quick intro?
2019-01-13 20:07:36<@ulm> well, we have an AUTHORS file now
2019-01-13 20:08:09<@ulm> so looks like exceptions to the simplified attribution won't be needed any more
2019-01-13 20:08:36<@dilfridge> \o/
2019-01-13 20:08:58<@leio> I shall note that "SHALL" in there is equal to "MUST" as per rfc2119
2019-01-13 20:09:06<@dilfridge> ++
2019-01-13 20:09:10<@dilfridge> excellent
2019-01-13 20:09:11<@ulm> leio: yes
2019-01-13 20:09:19<@leio> (I think "MUST" would be clearer for people)
2019-01-13 20:09:20<@ulm> WilliamH: any comments from your side?
2019-01-13 20:09:29<@WilliamH> ulm: Not really.
2019-01-13 20:09:42<@dilfridge> I suppose "must" is clearer for non-native speakers.
2019-01-13 20:09:42<@WilliamH> leio++
2019-01-13 20:10:04 * ulm doesn't mind either way
2019-01-13 20:10:14<@WilliamH> I guess it's debatable whether profiles are copyrightable...
2019-01-13 20:10:15<@K_F> rfc2119-wise they are equal, I'm fine with MUST
2019-01-13 20:10:30<@WilliamH> porfiles just list things don't they?
2019-01-13 20:10:30<@dilfridge> both work for me
2019-01-13 20:10:33<@WilliamH> profiles *
2019-01-13 20:10:47<@slyfox> you can have quite big bashrc in a profiles/
2019-01-13 20:10:58<@WilliamH> Ah ok.
2019-01-13 20:11:21<@leio> then again "REQUIRED" would be even more clearer, eh
2019-01-13 20:11:50<@leio> any of the 3 work for me, but an alternative instead of "SHALL" might be clearer for the non-native speakers instead.
2019-01-13 20:11:56<@leio> indeed*
2019-01-13 20:12:12<@K_F> I'm writing up proposal now including MUST.. REQUIRED requires a bit more rewrite
2019-01-13 20:12:58<@ulm> do we need the RFC 2119 reference for "must" or "required"?
2019-01-13 20:13:11<@ulm> should be clear enough even without
2019-01-13 20:13:17<@K_F> I prefer keeping rfc reference
2019-01-13 20:13:33<+tamiko> dilfridge: pong
2019-01-13 20:13:43<+tamiko> K_F: Present. Sorry for the delay.
2019-01-13 20:13:44<@K_F> tamiko: hi there
2019-01-13 20:13:46<@dilfridge> good morning!
2019-01-13 20:13:58<@K_F> tamiko: will give you a min to post any questions before we open a vote then
2019-01-13 20:15:02<@K_F> Vote: The council votes to update the tree policy. The updated policy reads: "The simplified form of the copyright attribution according to GLEP 76 [2], i.e., "Copyright YEARS Gentoo Authors", MUST [3]  be used for ebuilds and profile files in the Gentoo repository."
2019-01-13 20:15:14 * slyfox votes yes
2019-01-13 20:15:22<@K_F> references are same as in original proposal linked in agenda
2019-01-13 20:15:24 * K_F yes
2019-01-13 20:15:35 * dilfridge yes
2019-01-13 20:15:39 * tamiko yes (substituting for Whissi)
2019-01-13 20:15:45 * ulm yes
2019-01-13 20:15:56 * leio yes
2019-01-13 20:16:06 * WilliamH yes
2019-01-13 20:16:14<@WilliamH> aside that's what we are doing at the office.
2019-01-13 20:16:22<@K_F> unanimous
2019-01-13 20:16:27<@ulm> repoman should be updated, I guess?
2019-01-13 20:16:45<@K_F> ulm: right, was going to be my next question :) What action items are required following this vote
2019-01-13 20:16:47<@ulm> or git hook?
2019-01-13 20:17:05<@K_F> I'm in favor of repoman
2019-01-13 20:17:11<@leio> there are ebuilds to fix too, I assume preferably by the claimed non-simplified copyright holder
2019-01-13 20:17:30<@K_F> well, don't we just update those as we go along?
2019-01-13 20:17:41<@K_F> i.e not change the previous ones in bulk
2019-01-13 20:17:47<@ulm> leio: we can do that, but they will disappear after some time
2019-01-13 20:17:50<@WilliamH> K_F++
2019-01-13 20:18:04<@WilliamH> We just let them disappear
2019-01-13 20:18:07<@leio> as long as the bump removes double notices, I guess.
2019-01-13 20:18:11<@ulm> same as for ebuilds with Foundation copyright
2019-01-13 20:18:40<@leio> I mainly meant due to the change for copyright holder in extra notice doing it, instead of a bump
2019-01-13 20:18:54<@leio> then less people can worry about "but copyright notices must not be touched", despite GPL allowing it
2019-01-13 20:19:36<@K_F> ulm: will you open bug for repoman update?
2019-01-13 20:19:39<@leio> as a relevant representative is doing the deed, which might not be the case on the next (rev)bump
2019-01-13 20:19:53<@ulm> K_F: can do
2019-01-13 20:19:54<@leio> as long as the next revisions comply, I'll be fine though.
2019-01-13 20:20:18<@K_F> leio: if that becomes an issue, lets revisit it at a later point
2019-01-13 20:20:30<@K_F> I don't expect it to be, and can be pointed at this discussion anyways
2019-01-13 20:20:31<@ulm> we can ask mgorny to add it to his CI
2019-01-13 20:20:34<@leio> then it's too late without massive reverts. But as said, I'm fine.
2019-01-13 20:20:39<@ulm> then it won't be missed
2019-01-13 20:22:48<@K_F> so, anyone believe we should vote on anything, or is the discussion here sufficient for people to do what they want in this matter :)
2019-01-13 20:23:33<+tamiko> I agree that revisiting the topic if problems emerge in the future is a good way to proceed.
2019-01-13 20:23:39<@ulm> +1
2019-01-13 20:24:20<@K_F> right, lets move on then
2019-01-13 20:24:26<@K_F> 3. Open bugs with council involvement
2019-01-13 20:24:27<@K_F> https://wiki.gentoo.org/wiki/Project:Council#Open_bugs_with_Council_participation
2019-01-13 20:24:43<@K_F> I don't really see any bug with action items for us
2019-01-13 20:25:05<@K_F> #637328 no update, but new year now and finally starting to catch up, so we hopefully can pick it up again 
2019-01-13 20:25:24<@K_F> #674608 and #642072 are more FYI
2019-01-13 20:25:48<@K_F> anyone have anything else before moving to open floor?
2019-01-13 20:26:14<@slyfox> nope
2019-01-13 20:26:20<+tamiko> nope
2019-01-13 20:26:31<@K_F> 4. Open floor
2019-01-13 20:26:53<@K_F> any comments from members?
2019-01-13 20:27:11<+tamiko> One quick comment: I do not see how #674608 is relevant to the council without a pgp web of trust policy in place.
2019-01-13 20:27:30<+tamiko> Maybe that's something we can work on in the future - but previous attempts over the last 15 years lead to nothing.
2019-01-13 20:27:33<@K_F> tamiko: I'd tend to agree
2019-01-13 20:28:27<@K_F> ok, reminder for FOSDEM , remember to sign up on https://wiki.gentoo.org/wiki/FOSDEM_2019 if coming
2019-01-13 20:28:30<@dilfridge> could we please use bugs in a way so willikins responds?
2019-01-13 20:28:44<@K_F> ulm: will you be there this year?
2019-01-13 20:29:01<+tamiko> dilfridge: bug #674608
2019-01-13 20:29:04<+willikins> tamiko: https://bugs.gentoo.org/674608 "Unjustified developer signatures on np-hardass' key"; Community Relations, Developer Relations; CONF; mgorny:council
2019-01-13 20:29:25<@dilfridge> I see absolutely no reason why the council should be involved there
2019-01-13 20:29:25<@ulm> K_F: unfortunately not, because our experiment will get beam time just then
2019-01-13 20:29:31<@WilliamH> I agree about that bug being irrelivent.
2019-01-13 20:29:53<@leio> I also don't see strong reasons why the signatures should be revoked
2019-01-13 20:29:55<@WilliamH> It isn't even a comrel issue because the distro doesn't have a web of trust policy.
2019-01-13 20:30:00<@K_F> ok, lets un-CC us on that one
2019-01-13 20:30:10<@leio> we are assignees.
2019-01-13 20:30:28<@WilliamH> resolved/invalid in that case?
2019-01-13 20:31:23<@K_F> yeah, can do RESOLVED INVALID, if reporter wants to persue it further can assign to someone else and reopen
2019-01-13 20:32:09<@WilliamH> Gentoo doesn't hav a Web of Trust policy at all, so the bug is completely irrelivent imo
2019-01-13 20:32:44<@K_F> ulm: too bad, but I'll likely be in mainz again later this year so lets find some time to meet up then :)
2019-01-13 20:33:31<@K_F> bug closed with comment; "As discussed in today's council meeting, as we do not have an OpenPGP WoT policy in place in Gentoo, the council does not have a role in this case."
2019-01-13 20:33:41<@K_F> anything else for open floor?
2019-01-13 20:35:39<@K_F> doesn't seem so
2019-01-13 20:36:00<@K_F> well, quick meeting today then, for once :)
2019-01-13 20:36:09 * K_F bangs the gavel, meeting closed
2019-01-13 20:36:35<@WilliamH> K_F: ;-)
2019-01-13 20:39:47<@slyfox> \o/
2019-01-13 20:40:58<+tamiko> Thanks all! :-)
