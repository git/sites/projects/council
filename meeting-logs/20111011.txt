[20:00:18] <hwoarang> time to start?
[20:00:18] <grobian> present
[20:00:20] <jmbsvicetto> here
[20:00:42] <dberkholz> hi
[20:00:48] <hwoarang> ulm: Betelgeuse ^
[20:00:52] <hwoarang> hmm missing Chainsaw?
[20:00:55] <ulm> here
[20:01:33] <hwoarang> lets wait a couple of minutes
[20:01:46] <jmbsvicetto> hwoarang: let me power up my laptop so I can get chainsaw's phone number
[20:01:55] <hwoarang> kk
[20:02:08] * hwoarang has changed topic for #gentoo-council to: "Next meeting: October 11, 1900UTC | http://www.gentoo.org/proj/en/council/utctolocal.html?time=1900 | http://www.gentoo.org/proj/en/council/ | Meeting Now! | agenda : http://dev.gentoo.org/~hwoarang/council-20111011-agenda.txt"
[20:02:15] <Betelgeuse> hwoarang: here
[20:03:44] * hwoarang has changed topic for #gentoo-council to: "Next meeting: Now | http://www.gentoo.org/proj/en/council/utctolocal.html?time=1900 | http://www.gentoo.org/proj/en/council/ | agenda : http://dev.gentoo.org/~hwoarang/council-20111011-agenda.txt"
[20:03:57] <jmbsvicetto> hwoarang: who's missing? Just Chainsaw?
[20:04:00] <hwoarang> yeah
[20:04:28] <hwoarang> will you call him?
[20:04:38] <jmbsvicetto> I'm calling
[20:05:22] <jmbsvicetto> He should be around soon
[20:05:38] <hwoarang> okey
[20:06:14] --> Chainsaw (~chainsaw@gentoo/developer/atheme.member.chainsaw) has joined #gentoo-council
[20:06:14] *** Mode #gentoo-council +o Chainsaw by ChanServ
[20:06:20] <Chainsaw> Sorry!
[20:06:30] -*- Chainsaw bows to jmbsvicetto and takes his seat
[20:06:39] <jmbsvicetto> Hi Tony :)
[20:06:50] <hwoarang> no prob :)
[20:07:04] <hwoarang> live summary here: http://dev.gentoo.org/~hwoarang/council-20111011-summary.txt
[20:07:08] <hwoarang> 1st Item
[20:07:20] <hwoarang> vote: Edit the generated changelogs
[20:07:34] <hwoarang> seems like the community wants to have this ability
[20:08:01] <grobian> ok, we're voting now?
[20:08:10] <hwoarang> http://archives.gentoo.org/gentoo-project/msg_c970aaa4a10a0c36f51e15fe0d1f72df.xml
[20:08:14] <hwoarang> for reference ^
[20:08:39] <hwoarang> I think we are good to vote here
[20:08:41] <grobian> yes, changelogs must be able to be changed always
[20:08:47] <dberkholz> i've kinda changed my mind since last time we discussed this, it's just a hassle to deal with.
[20:09:04] <hwoarang> dberkholz: depends on the implementation I guess
[20:09:11] <Betelgeuse> It's not a requirement but feel free to timplement.
[20:09:28] <Chainsaw> I want to be able to edit what I submit, in case I miss something.
[20:09:42] <Chainsaw> So: Yes to editing.
[20:09:44] <ulm> +1
[20:09:46] <hwoarang> +1
[20:10:07] <jmbsvicetto> My opinion is that our voting 2 (3?) months ago meant we would need to keep a file, so it would be possible to edit it
[20:10:10] <hwoarang> Betelgeuse: i don't agree that this is not a requirement
[20:10:19] <hwoarang> cause based on that the implementation changes
[20:10:22] <hwoarang> substantialy
[20:10:31] <hwoarang> *ll
[20:10:38] <grobian> jmbsvicetto: yes, so see my reply to the thread hwoarang just pointed to
[20:10:57] <dberkholz> i pretty much agree with vapier, i don't see this ability as a requirement either.
[20:11:05] <grobian> http://archives.gentoo.org/gentoo-project/msg_5dfed9997951547d2f08f933d84b97b2.xml
[20:11:42] <hwoarang> So I guess everyone is on board either as +1 or just do it if you want
[20:12:39] <hwoarang> any objections otherwise we are moving forward
[20:12:50] <jmbsvicetto> At this point I agree with Betelgeuse that this is not a requirement, but feel free to implement
[20:13:47] <hwoarang> the implementation is simple enough anyway. Has the user touched the changelog? Yes? ok so repoman wont write anything to it
[20:13:50] <dberkholz> this isn't consensus-based, it's majority-based ... objections don't matter if they're the minority.
[20:14:08] <grobian> hwoarang: that's wrong logic ;)
[20:14:22] <hwoarang> grobian: let me know why
[20:14:37] <grobian> if you touch changelog + ebuild, something is wrong
[20:15:09] <hwoarang> why? you probably want to write a different message to Changelog
[20:15:16] <hwoarang> and not the one you will use on the commit message
[20:15:32] <grobian> then the whole point of the changelog discussion is moot
[20:15:37] <hwoarang> err no
[20:15:44] <hwoarang> sometimes the changelog has to be verbose :)
[20:15:47] <grobian> we don't want people to write "^" in the changelog
[20:15:48] <hwoarang> very verbose :p
[20:15:55] <_AxS_> ..would it not be safer to allow edits but only secondarily?  ie, commit with repoman, and then you can edit the changelog after?
[20:15:57] <grobian> or just add a space to make them omit a a changelog entry
[20:16:19] <hwoarang> grobian: i think this is not the case anymore
[20:16:39] <grobian> hwoarang: true, but it was the reason this whole discussion started
[20:16:56] <hwoarang> ppl you did that wont bother editing changelog manually
[20:17:03] <hwoarang> will simply run repoman commit and let him do the job
[20:17:05] <jmbsvicetto> hmm, we also voted against "omitting" stuff from changelogs, correct?
[20:17:05] <grobian> if we decide now we all behave, we don't need to regulate anything, and we can just continue to do useful stuff
[20:17:27] <jmbsvicetto> so the commit message to repoman should be save in the changelog
[20:17:29] <hwoarang> grobian: the point is to get rid of the manual echangelog thingie
[20:17:33] <hwoarang> jmbsvicetto: yes
[20:17:33] <Chainsaw> grobian: Assuming common sense on the side of the developer has been tried.
[20:17:42] <grobian> hwoarang: that's a technical argument you should have with portage people
[20:17:45] <Chainsaw> grobian: It was not a success.
[20:18:04] <grobian> Chainsaw: sorry?
[20:18:10] <hwoarang> grobian: right now repoman will still need to run echangelog internally to write changelog
[20:18:35] <grobian> and how does that change?
[20:18:42] <hwoarang> change what?
[20:18:51] <grobian> if we decide anything?
[20:19:07] <grobian> we just decided 4-3 we won't auto-generate the log, but always store  it on disk in VCS somewhere
[20:19:09] <hwoarang> do you have another implementation in mind?
[20:19:16] <hwoarang> yes
[20:19:38] <grobian> so, poke zmedico to add echangelog code to repoman
[20:19:46] <hwoarang> correct
[20:19:52] <grobian> council only sets the policy that for each change a changelog entry should exist
[20:19:56] <hwoarang> that is why I am saying
[20:19:58] <hwoarang> *what
[20:20:01] <grobian> s/exist/be written/
[20:20:13] <ulm> we have a decision, right? so can we please move on and move further discussions to the ml?
[20:20:22] <ulm> otherwise this is a waste of time
[20:20:24] <_AxS_> ..would it help if the actual, exact issue which is being voted on is explicitly stated?
[20:20:34] <Chainsaw> _AxS_: It was, and a decision has been reached.
[20:20:41] -*- Chainsaw agrees with ulm, let's move on
[20:20:42] <hwoarang> ulm: we have nothing to push back do we?
[20:20:47] <hwoarang> this is now a task for the portage@ ppl
[20:21:05] <ulm> hwoarang: all the better then
[20:21:22] <grobian> hwoarang: yes, our request for them to do it, or any voluntyeer to work with them
[20:21:32] <grobian> ok, next topic please
[20:21:37] <jmbsvicetto> hwoarang: It's a task for any interested party and assigned to the portage people
[20:22:34] <jmbsvicetto> s/assigned to/we kindly ask the assistance of/
[20:22:48] <grobian> +1
[20:23:02] <hwoarang> sorry guys I have a huge lag
[20:23:08] <hwoarang> moving to task 2
[20:23:13] <hwoarang> EAPI1 in profiles
[20:23:32] <hwoarang> related discussion: http://archives.gentoo.org/gentoo-dev/msg_59c8c04883735f0b090f6e3f0525241e.xml
[20:24:20] <jmbsvicetto> Thanks to Donnie's poke I reviewed the thread and corrected my memory from it
[20:24:38] <grobian> imo the consensus of that thread is that eapi-1 is ok (nothing higher)
[20:24:41] <jmbsvicetto> so, +1 on setting EAPI=1 on profiles
[20:24:42] <ulm> I'd rather bump profiles to EAPI 2 immediately
[20:24:46] <hwoarang> from what I can tell it seems safe enough to do it
[20:24:47] <dberkholz> i'll summarize again.
[20:24:48] <dberkholz> 18:31 < dberkholz@> 17:15 < dberkholz@> in the "Call for items for September 13 council meeting" thread.
[20:24:51] <dberkholz> 18:32 < dberkholz@> basically, nobody really cared about EAPI=1, people had problems with USE deps in 2 and  not long enough support for EAPI=4
[20:25:10] <Chainsaw> EAPI 0 -> 1 has a lot of benefits and no downsides that I can see.
[20:25:13] <Chainsaw> I'm all for it.
[20:25:16] <hwoarang> ulm: there was a discussion about how to handle USE masks in >EAPI2
[20:25:19] <jmbsvicetto> ulm: I'd prefer we stick to EAPI-1
[20:25:22] <hwoarang> >=
[20:25:30] <hwoarang> EAPI1 seems better to me
[20:25:31] <grobian> I'm ok with EAPI-1
[20:25:40] <Chainsaw> 2 and higher have pitfalls.
[20:25:47] <ulm> hwoarang: that USE masks are possible doesn't imply that they must be used ;)
[20:25:51] <jmbsvicetto> ulm: I'd be interested in getting some experimental profiles where we could try a bump to a more recent EAPI version
[20:26:06] <hwoarang> ulm: afaik there is no provision for app-foo/bar[test] in profiles
[20:26:09] <hwoarang> is it?
[20:26:11] <dberkholz> i'd prefer to take the incremental step forward and approve 1 right now.
[20:26:18] <dberkholz> we can then talk about 2 separately.
[20:26:20] <ulm> but of course, I'm o.k. with EAPI 1
[20:26:26] <hwoarang> good
[20:26:37] <ulm> only we'll have the same discussion at a later point then :p
[20:26:39] <Betelgeuse> EAPI 1 ok
[20:26:56] <hwoarang> baby steps
[20:27:12] <hwoarang> moving to item 3
[20:27:16] <hwoarang> open bugs
[20:27:18] <jmbsvicetto> ulm: we can start the work on experimental profiles after we approve EAPI-1
[20:27:33] <ulm> jmbsvicetto: yeah, let's do that
[20:28:06] <hwoarang> ok
[20:28:24] <hwoarang> bug #316401
[20:28:27] <willikins> hwoarang: https://bugs.gentoo.org/316401 "Add resolution OBSOLETE"; Bugzilla, General Bugs; IN_P; betelgeuse:bugzilla
[20:28:53] <Chainsaw> !seen idl0r
[20:28:53] <willikins> Chainsaw: idl0r was last seen 31 minutes and 36 seconds ago, saying ":D" in #gentoo.de
[20:28:59] <hwoarang> no progress since last month. It seems to me it is fixed
[20:29:03] <Betelgeuse> hwoarang: https://bugs.gentoo.org/page.cgi?id=fields.html#status
[20:29:06] <Betelgeuse> hwoarang: it's not there yet
[20:29:15] <hwoarang> oh right
[20:29:17] <hwoarang> sorry 
[20:29:23] <Chainsaw> * You've invited idl0r to #gentoo-council (leguin.freenode.net)
[20:29:25] <Chainsaw> Summons issued.
[20:29:52] <hwoarang> in case he does not show up by the end of the meeting
[20:29:57] <a3li> poked him
[20:29:59] <hwoarang> I will try to talk to him in #infra
[20:30:34] <hwoarang> bug #331987
[20:30:37] <willikins> hwoarang: https://bugs.gentoo.org/331987 "Merge -council and -project mailing lists"; Gentoo Infrastructure, Mailing Lists; UNCO; scarabeus:infra-bugs
[20:30:40] <hwoarang> no progress as well
[20:31:05] --> idl0r (~idl0r@gentoo/developer/idl0r) has joined #gentoo-council
[20:31:08] <idl0r> hey
[20:31:17] <Chainsaw> Thank you for joining us idl0r.
[20:31:32] <Chainsaw> Could you add the description in bugzilla please, for bug #316401
[20:31:32] <willikins> Chainsaw: https://bugs.gentoo.org/316401 "Add resolution OBSOLETE"; Bugzilla, General Bugs; IN_P; betelgeuse:bugzilla
[20:31:43] <Chainsaw> Or do you require further assistance from us?
[20:32:03] <idl0r> sec.
[20:33:02] <idl0r> ah that one
[20:33:17] <ulm> hm, I cannot find the word "obsoleted" in my dictionary
[20:33:43] <jmbsvicetto> ulm: obsoleted or obsolete ?
[20:33:43] <ulm> should be "obsolete" IMHO (but I'm not a native speaker)
[20:33:45] <idl0r> i'll need to fight me through the templates again and add all resolutions and so on
[20:33:59] <Betelgeuse> ulm: it should be obsolete
[20:34:01] <grobian> obsolete is in the dictionary
[20:34:16] <Betelgeuse> ulm: Where did you see "obsoleted"?
[20:34:24] <ulm> Betelgeuse: in your comment #0
[20:34:30] <ulm> "The bug has become obsoleted."
[20:34:43] <dberkholz> in that use, obsoleted would be bad grammar anyway.
[20:35:06] <Chainsaw> Has become obsolete, or has been obsoleted by X.
[20:35:19] <Chainsaw> But "made obsolete" would be preferred.
[20:35:29] <Betelgeuse> Feel free to go and add the better and as the comment calls for :)
[20:35:55] --> idella4 (~idella4@gentoo/contributor/idella4) has joined #gentoo-council
[20:35:56] <idl0r> Chainsaw: so it'll take some time again, it was/is more low priority on my TODO
[20:36:16] <idl0r> or if a3li want to do that.. :)
[20:37:58] <hwoarang> moving on
[20:38:11] <hwoarang> bug #331987
[20:38:14] <willikins> hwoarang: https://bugs.gentoo.org/331987 "Merge -council and -project mailing lists"; Gentoo Infrastructure, Mailing Lists; UNCO; scarabeus:infra-bugs
[20:38:19] <hwoarang> is someone willing to talk to robbat2 about that?
[20:38:35] <hwoarang> no progress since last month
[20:39:05] <jmbsvicetto> hwoarang: we're waiting on the numbers, correct?
[20:39:11] <hwoarang> yeah
[20:39:34] <hwoarang> should we push this forward in case @infra forgotten about his?
[20:39:36] <hwoarang> *this
[20:39:39] <hwoarang> or just wait
[20:40:39] <hwoarang> ok lets just wait then 
[20:41:03] <hwoarang> bug #331987
[20:41:04] <willikins> hwoarang: https://bugs.gentoo.org/331987 "Merge -council and -project mailing lists"; Gentoo Infrastructure, Mailing Lists; UNCO; scarabeus:infra-bugs
[20:41:20] <hwoarang> i will talk to tove in IRC soon. Bugzilla talking did not work out
[20:41:42] <hwoarang> bug #383467
[20:41:45] <willikins> https://bugs.gentoo.org/383467 "Council webpage lacks results for 2010 and 2011 elections"; Website www.gentoo.org, Projects; CONF; hwoarang:jmbsvicetto
[20:41:52] <jmbsvicetto> I've taken this bug for myself
[20:41:58] <jmbsvicetto> I'll take care of it asap
[20:42:21] <hwoarang> jmbsvicetto: thanks
[20:42:48] <hwoarang> Last item: Open floor 
[20:43:02] <hwoarang> @community. Now it is time to jump in
[20:43:23] <Chainsaw> a3li: Are you able to assist with bug #316401 please?
[20:43:25] <willikins> Chainsaw: https://bugs.gentoo.org/316401 "Add resolution OBSOLETE"; Bugzilla, General Bugs; IN_P; betelgeuse:bugzilla
[20:43:34] <Chainsaw> a3li: I would prefer to be able to close it off.
[20:44:14] <a3li> Chainsaw: when idl0r provides me with bugzilla access, then yes
[20:44:30] <Chainsaw> idl0r: Are you able to do that please?
[20:44:44] <a3li> not right now, that is. but tomorrow/this week
[20:44:47] <idl0r> sure
[20:45:00] <Chainsaw> idl0r: Many thanks. Then I will consider this to be sorted.
[20:45:15] <idl0r> you're welcome
[20:45:38] <hwoarang> seems like we finished :)
[20:46:04] <idl0r> done (access)
[20:46:09] <Chainsaw> idl0r: Cheers.
[20:46:13] <grobian> thanks
[20:46:21] <-- idella4 (~idella4@gentoo/contributor/idella4) has left #gentoo-council ("Once you know what it is you want to be true, instinct is a very useful device for enabling you to know that it is")
[20:46:22] <Chainsaw> jmbsvicetto: Thanks again :)
[20:46:30] <hwoarang> the summary is live here
[20:46:33] <hwoarang> http://dev.gentoo.org/~hwoarang/council-20111011-summary.txt
[20:46:52] <hwoarang> if you want to comment on that. I will review it and send it for review before announcing it
[20:47:01] <jmbsvicetto> Chainsaw: np :)
[20:47:04] <grobian> hwoarang: deceded -> decided
[20:47:07] <hwoarang> right
[20:47:18] <hwoarang> next meeting is on November 8th
[20:47:20] <grobian> electiosns -> elections
[20:47:30] <jmbsvicetto> Markos, thanks for chairing the meeting
[20:47:35] <dberkholz> good deal. thanks hwoarang
[20:48:21] <hwoarang> ok i will send the review to the alias tonight and tomorrow to -dev-announce
[20:48:24] <jmbsvicetto> hwoarang: I'd suggest s/EAPI1/EAPI-1/ just to make it more readable
[20:48:28] <hwoarang> ok jmbsvicetto
[20:49:05] <jmbsvicetto> hwoarang: I'm fine with this summary. Feel free to submit / commit it from my part
[20:49:13] <grobian> +1
[20:49:33] <hwoarang> @all: Once I upload everything I will open the bug for repoman
[20:49:35] <ulm> summary is o.k., thanks hwoarang
[20:50:01] <Chainsaw> Thumbs up for summary.
[20:50:03] <grobian> hwoarang: there already is one by diego
[20:50:38] <hwoarang> there are plenty
[20:50:47] --> NeddySeagoon (~NeddySeag@gentoo/developer/NeddySeagoon) has joined #gentoo-council
[20:50:57] <grobian> hwoarang: https://bugs.gentoo.org/show_bug.cgi?id=337853
[20:51:23] <hwoarang> Betelgeuse has also opened a number of bugs about changelog entires on removal and addition
[20:51:30] <grobian> hwoarang: want me to comment on that bug with council hat?
[20:51:32] <hwoarang> we should probably merge them to a single one
[20:51:48] <hwoarang> grobian: i'd prefer first to make summary public and then poke them
[20:51:57] <grobian> hwoarang: fine, your call
[20:52:40] <Betelgeuse> hwoarang: I have?
[20:52:51] <grobian> Betelgeuse: yeap :D
[20:52:58] <hwoarang> Betelgeuse: mind if you mark 365361 and 177290 as duplicates?
[20:53:06] <hwoarang> those two at least ^
[20:53:50] <grobian> hwoarang: I think once you push out the summary, you should comment on those bugs referring to the summary
[20:54:02] <hwoarang> yeah
[20:54:10] <hwoarang> probably add a comment and close them
[20:54:14] <hwoarang> move everything to a single bug
[20:54:16] <grobian> although 365361 has already summary defined, but now also the generation thing
[20:54:22] <grobian> sth like that
[20:54:31] <Betelgeuse> hwoarang: not exactly the same thing but as long as both get impelmented I don't midn
[20:54:57] <grobian> cool antarus uses his new wings
[20:55:13] <hwoarang> Betelgeuse: ok i will make that clear when I add the comment
[20:56:13] <grobian> now we have the stats for -council and -project, can we make a decision in 5 minutes?
[20:56:42] <hwoarang> we could if everyone is still here ;p
[20:57:04] <grobian> jmbsvicetto, Chainsaw, dberkholz, ulm, Betelgeuse ping?
[20:57:10] <jmbsvicetto> pong
[20:57:13] <Chainsaw> Yes grobian?
[20:57:27] <grobian> can we decide on bug 331987 now?
[20:57:29] <willikins> grobian: https://bugs.gentoo.org/331987 "Merge -council and -project mailing lists"; Gentoo Infrastructure, Mailing Lists; UNCO; scarabeus:infra-bugs
[20:57:44] <jmbsvicetto> grobian: I poked infra about the numbers ;)
[20:57:52] <grobian> jmbsvicetto: thx
[20:57:56] <jmbsvicetto> let me read the numbers
[20:58:22] <Chainsaw> 54 users affected.
[20:58:33] <hwoarang> probably stale entries?
[20:58:40] <Chainsaw> I would subscribe to project without a personal approach. If it was <10, a more personal approach would be warranted.
[20:58:41] <grobian> could we just move them?
[20:58:50] <dberkholz> just move 'em.
[20:59:06] <hwoarang> yeah move them and close the list
[20:59:08] <dberkholz> and let them know while you're at it, mentioning in the note how to unsubscribe
[20:59:21] <dberkholz> since they'll suddenly be on a list with more traffic
[20:59:24] <grobian> move + post-move msg on project both are merged
[20:59:35] <Chainsaw> grobian: Yes, that sounds sensible.
[20:59:52] <jmbsvicetto> move + close + note about how to unsubscribe (if possible)
[21:00:00] <hwoarang> good
[21:00:06] <ulm> jmbsvicetto: sounds good
[21:00:17] <grobian> ok, so do we have a majority for just moving them in the first place?
[21:00:23] <jmbsvicetto> +1
[21:00:25] <hwoarang> +1
[21:00:28] <grobian> +1
[21:00:41] <Chainsaw> Agreed.
[21:00:48] <grobian> ok, so yes
[21:01:27] <jmbsvicetto> I don't think we need to vote on closing the ml - we already did it
[21:01:28] <grobian> next thing is probably if we want to notify them
[21:01:48] <grobian> if the ml software can do it, or we need to send an additional answer is just an implementation
[21:02:36] <jmbsvicetto> I'd ask infra if they can send an automated message about unsubscribing. If not, we can send the message grobian talked about and add a note about how to unsubscribe from that list
[21:02:47] <grobian> +1
[21:02:48] <hwoarang> jmbsvicetto: thanks
[21:02:56] <hwoarang> i will add that bit to the summary
[21:05:03] <grobian> ok, thanks once again
[21:05:47] <Chainsaw> bonsaikitten: I'm taking those xen bugs from idella4 if that's okay.
[21:06:15] <bonsaikitten> Chainsaw: yes please
[21:07:06] <-- grobian (~grobian@gentoo/developer/grobian) has quit (Quit: Zzzzz)
[21:07:07] <jmbsvicetto> hwoarang: If you don't anything else from me, I'll take care of a few things at work before I leave
[21:07:18] <hwoarang> no everything is good
[21:07:32] <jmbsvicetto> ok, thanks for chairing
[21:07:35] <jmbsvicetto> later
[21:08:22] * hwoarang has changed topic for #gentoo-council to: "Next meeting: November 8, 1900UTC | http://www.gentoo.org/proj/en/council/utctolocal.html?time=1900 | http://www.gentoo.org/proj/en/council/"
