Council 2011/05/10 meeting summary
==================================


Agenda
------

 * roll call

 * vote/discuss: ChangeLogs must be used in all situations

 * open bugs with council involvement
 * next meeting date / chair
 * open floor - listen to the community

 [1] http://www.gentoo.org/proj/en/council/utctolocal.html?time=2000
 [2] http://www.gentoo.org/


Meeting
-------

 * roll call

    here:

    Betelgeuse
    bonsaikitten
    ferringb (late)
    jmbsvicetto
    scarabeus
    wired
    chainsaw

 * vote/discuss: ChangeLogs must be used in all situations
    After some discussion the strictest rule was selected where ChangeLogs need
    to be updated for all changes. The devmanual was already updated by Peteri
    with the new wording [1,2] during the meeting.

[1] - http://git.overlays.gentoo.org/gitweb/?p=proj/devmanual.git;a=commitdiff;h=eee81246369d1124501528a289adbbb77a5e3cd1
[2] - http://git.overlays.gentoo.org/gitweb/?p=proj/devmanual.git;a=commitdiff;h=05923dc0acae7624116407b08cd9bb734e542826

 * Open bugs

    Nothing changed since last meeting for the currently open bugs.

 * Next meeting chair

    Jorge (jmbsvicetto)

 * Next meeting date

    Wednesday, 20110608 1900 UTC

 * Open floor - listen to the community

	No issue was brought forward to the council.