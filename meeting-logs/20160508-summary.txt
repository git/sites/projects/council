Summary of Gentoo council meeting 8 May 2016

Agenda
======
1. Roll call
2. Open Bugs with Council Involvement
3. Open Floor

Roll call
=========
Present: blueness, dilfridge, jlec, k_f, rich0, ulm, williamh

Open bugs with council involvement
==================================

These bugs have to do with games issues:

 - Bug 566498 - Use of games group needs to be removed
    No action from council

 - Bug 574080 - path customization needs to be removed
 No action from council

 - Bug 574952 - comrel complaint w/ council in cc wrt the games team.
   no visible progress

 - Bug 579460 - make repoman ignore missing # $Id$ line:
   CC only for tracking purposes.

The following bugs are about missing meeting summaries, so the cc's are
for tracking.

 - Bug 569914
 - Bug 571490

Open floor
==========

Williamh brought up the gtk use flag situation; it was referred to the qa team.

Dilfridge brought up the idea of writing council meeting summaries in
latex and publishing them in pdf. There wasn't any interest in doing so.

