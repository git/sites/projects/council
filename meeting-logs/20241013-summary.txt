Summary of the Gentoo council meeting 2024-10-13


Agenda [1]
==========

1. Roll call
2. Plans regarding the time64 transition [2]
3. Open floor


Roll call
=========

Present: arthurzam, dilfridge, mgorny, robbat2, sam, soap,  ulm


Plans regarding the time64 transition [2]
=========================================

mgorny brought up the question of appending a *t64 suffix to CHOST on 
32bit installations migrated to 64bit time_t, in preparation for
submitting corresponding patches to clang upstream. Several technical
and non-technical details were raised.

* Regarding collaboration with other distributions, we tried to work
  with Debian but they weren't interested.

* Obviously all changes are limited to 32bit architectures.

* The format of utmp is outside the scope of the current discussion.

* CHOST only affects build time; so far issues were found only with
  ncurses and clang, both of which can be fixed.

In the end the consensus was that we go ahead with the addition of a 
t64 suffix to CHOST values to signify 32bit installations migrated to
64bit time_t. The libdir is not changed; only during conversion a
temporary libt32 is used.


Open floor
==========

* arthurzam brought up the matter of missing council meeting summaries.

* robbat2 reports that there has been no progress with SPI's treasurer 
  in getting updated paypal links created (that directly assign funds
  to Gentoo). He will raise it at the next SPI board meeting.


References
==========

[1] https://public-inbox.gentoo.org/gentoo-project/12528360.O9o76ZdvQC@pinacolada/
[2] https://public-inbox.gentoo.org/gentoo-project/5cb9eebf074de55a12017115335408d617c6ec04.camel@gentoo.org/
