Summary of Gentoo council meeting 2021-12-12



Agenda [1]
==========

1) Roll call
2) Disabling APNG patches to libpng by default [2,3]
3) Infra shopping list, request for proposals [4,5]
4) Minor financial issues [6]
5) Open bugs with Council participation
6) Open floor



Roll call
=========

Present: 
dilfridge, gyakovlev, marecki, mgorny, sam, soap (proxy for mattst88), ulm



Disabling APNG patches to libpng by default [2,3]
=================================================

After detailed explanation of the technical situation and available data and
subsequent discussion the following motion was put to vote:
 1) mozilla products get a system-png useflag where applicable, 
 2) apng useflag enabling is removed from desktop profiles

This motion was passed with 6 yes, 1 abstention, 0 no



Infra shopping list, request for proposals [4,5]
================================================

There was not much actionable here apart from a general sentiment of 
"go ahead with figuring out what you want". 

Several council members expressed support for consolidating many old machines
on few strong new ones.



Minor financial issues [6]
==========================

The item brought up [6] is shipping of a HPPA machine from Europe to OSUOSL.

After some discussion whether hosting the box in Europe would be possible the
following motion was brought up: 
Approve [the requested funds] with the proviso that it should be housed in the
.eu if feasible within the next few months, but otherwise ship to US.

This motion was carried with 7 yes, 0 no, 0 abstentions.



Open bugs with council participation
====================================

bug 821553, "Document upgrade path policy" [7]
  Awaiting implementation by the documentation writers.

bug 824018, "media-libs/libpng: dropping "apng" patches" [2]
  This has already been dealt with (see above).

bug 823762, "[TRACKER] ~ only candidate arches" [8]
  ppc has seen some selective dekeywording.
  The open bug graphs still look good.

bug 828113, "Proposal to ship donated HPPA development system from Europe to
  OSUOSL" [6]
  This has already been dealt with (see above).



Open floor
==========

No items were brought up.



[1] https://archives.gentoo.org/gentoo-project/message/c40ae223363f5f92e98fa997fe642a92
[2] https://bugs.gentoo.org/824018
[3] https://archives.gentoo.org/gentoo-project/message/0dd4a4a92a99c1aba7101b9b38437552
[4] https://archives.gentoo.org/gentoo-project/message/d65e2b67bb97dbb5de5ff4c1fcaab857
[5] https://wiki.gentoo.org/wiki/Project:Infrastructure/Shopping_list
[6] https://bugs.gentoo.org/828113
[7] https://bugs.gentoo.org/821553
[8] https://bugs.gentoo.org/823762
