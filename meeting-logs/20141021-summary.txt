Roll call
=========

Present: blueness, dilfridge, radhermit, rich0, ulm, williamh 
Absent: dberkholz


Deprecating and killing the concept of herds
============================================
"The council is in favor of retiring herds, allowing non-maintainer
aliases to exist, and having a way to distinguish between individuals,
projects, and non-maintainer aliases in metadata.xml.  The details of
how to implement this will be worked out in the lists before the next
meeting."

Aye: blueness, dilfridge, radhermit, rich0, ulm, williamh 


Status of Games Team
====================
"Council deferrs to radhermit to continue working with the games team on
the organization issues for another month.  Council will reach out to
QA/Treecleaners and support their reviewing games packages as any other
as far as bugs/security/QA/etc goes."

Aye: blueness, dilfridge, radhermit, rich0, ulm, williamh 



Status of Projects
==================
1) the multilib porting and subsequent disposal of emul-... packages
2) the migration of project web pages to our wiki

http://thread.gmane.org/gmane.linux.gentoo.devel.announce/2212/

See meeting log for further details.  No actions by council.


Bugs assigned to Council
========================

(5 minutes)

Bug #503382 - Missing summaries for 20131210, 20140114, and 20140225 council meetings

dberkholz is reminded to follow-up...  


Open floor
==========

(5 minutes)




