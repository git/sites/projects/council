Council 2011/06/08 meeting summary
==================================


Agenda
------

 * roll call

 * ChangeLog policy
 * GLEP48 review
 * Removal of support for old-style virtuals (ulm)
 * Review of the ending council term
 * Council web app

 * open bugs with council involvement
 * open floor - listen to the community


Meeting
-------

 * roll call

    here:

    Betelgeuse
    bonsaikitten
    chainsaw
    jmbsvicetto
    scarabeus
    wired

    missing:

    ferringb


 * items for discussion / vote

    * ChangeLog policy

    The policy approved by the council in the last meeting gave rise to some incidents and sparked much
    debate. In the fallout Fabian made a request[1] for the council to discuss the ChangeLog generation.
    Jorge suggested the council should divide this topic in 2 issues: the result of the policy approval
    and the request to review the policy.
    Jorge made a point of thanking both the QA lead and deputy lead for addressing the ChangeLog policy
    and enforcing it inside QA, as well as thank DevRel (which Jorge and Petteri are part of) for their
    resolution of the bug. Tony argued that the ChangeLog policy is working as intended. Petteri argued
    that the policy is strict but that it does show how the problematic people react to policies in
    general.
    After some discussion regarding the "strictness" of the policy and whether to accept the request to
    review the policy, the council decided to not review the policy with 5 no votes and 1 abstention.
    The council issued a clarification note about the policy stating that as one removes the ChangeLog
    while dropping a package from the tree, there is no need to update it while commiting a package
    removal. 
    The discussion then turned to the point about automatic generation of the ChangeLog. The following
    debate mentioned having repoman -m call echangelog, having the ChangeLog files generated server
    side and completing the move to the git tree.
    A vote was called stating that the decision of the council about automated changelog messages is
    that repoman be updated to add the commit message to changelog, until such time as changelogs can
    be created server side. The council also urges individual developers to join the effort to move
    the tree to git. The vote was carried with 5 yes votes and 1 abstention. 2 council members each
    approved one part and abstained on the other.
    Tony made a point of recalling that this decision does in no way affect the decision not to update
    the changelog policy, so developers are still required to update changelogs for every commit.
    Tomas[2] and Alex[3] provided some commit wrappers to call echangelog.

    [1] - http://archives.gentoo.org/gentoo-dev/msg_2ff02d6910d797045af3659fb21c712f.xml
    [2] - http://dpaste.com/552036/
    [3] - http://paste.pocoo.org/show/403011/


    * GLEP48 review

    Jorge submitted a proposal to the ml to update GLEP48[4].
    After some initial debate over the power granted to the QA team, the timeline in case
    of an escalation to DevRel, the relation with DevRel and whether QA should only enforce
    policies or also take part in creating policies, after the request by Patrick, Jorge
    suggested pushing this back to the mls.
    Petteri then asked the council to at least vote to commit the non suspension related
    parts of the proposal. The diff[5] was approved with 6 yes votes.
    Alec during this discussion presented some thoughts about the QA team[6].

    [4] - http://archives.gentoo.org/gentoo-project/msg_ac161677a6e06a8647e16420eeae8d47.xml
    [5] - http://sources.gentoo.org/cgi-bin/viewvc.cgi/gentoo/xml/htdocs/proj/en/glep/glep-0048.txt?r1=1.3&r2=1.4
    [6] - http://pastebin.com/C1jGF1DJ


    * Removal of support for old-style virtuals (ulm)

    Ulrich made a request to the council to remove the support for old-style virtuals[7].
    Jorge asked if there could be any compatibility issue with old installations and Petteri
    asked what would happen to the vdb for old packages. Ulrich replied there should be no
    compatiblity issues as it should be comparable to a package removal. So if the PM can't
    resolve the old-style virtual, the package depending on it would pull the new-style
    virtual.
    The proposal was approved with 6 yes votes.

    [7] - http://archives.gentoo.org/gentoo-project/msg_7986aa2d9651c600f7302a8d84cc3721.xml


    * Review of the ending council term

    Several council members took this chance to look back at the ending term and to make their
    evaluation of what was accomplished. Most agreed it was fun and that it was nice working
    with the other elected members. The members also wished a good time for the next council.
    There was some talk about the move to git, including that council wasn't able to get it done,
    a mention about not having moved forward with the GLEP 39 reform and a listing of some ideas
    for next council.


    * Council web app

    Joachim Bartosik, a GSoC student at Gentoo, who is working on a RoR app for council[8],
    did a short demo for council members. The council was able to see the current state of
    the app and the state of the meeting bot.

    [8] - http://www.google-melange.com/gsoc/project/google/gsoc2011/jbartosik/19001



 * open bugs with council involvement

    The only bugs mentioned during the meeting were:

    bug 237381 - "Document appeals process"
    Jorge said he failed to present a proposal for this bug.

    bug 341959
    Alex said he still needs to fix this bug and that he will do that soon.

    bug 362803
    Jorge commited the update approved in the council meeting to the text version
    of the GLEP before the meeting ended. He promised to work in the html version
    later in the day.


 * Open floor - listen to the community

	No issue was brought forward to the council.
