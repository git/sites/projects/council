<ulm> Agenda is here:
      http://archives.gentoo.org/gentoo-project/msg_ac95bed78694852cd09f20a07437b805.xml
								        [21:01]
<ulm> Roll call
<dberkholz> hi
<grobian> here
<Chainsaw> Present.
<dberkholz> fwiw, i've always got logs if nobody else does.
<hwoarang> hi							        [21:02]
*** nimiux (~nimiux@gentoo/developer/nimiux) has joined channel
    #gentoo-council
<ulm> Betelgeuse, jmbsvicetto?
<Chainsaw> Do we have phone numbers for the gentlemen in question?      [21:03]
<ulm> yes							        [21:04]
<ulm> I'll call them
<Chainsaw> Thank you.
<ulm> seems I don't have the right number of betelgeuse		        [21:06]
<ulm> "number is incomplete"
<dberkholz> hmm, i might have it. lemme check. it could've been on my stolen
	    phone						        [21:07]
<ulm> and only mailbox for jmbsvicetto 
<hwoarang> the phone numbers should be available in the council@ alias from
	   June
<dberkholz> just texted Betelgeuse				        [21:08]
<Betelgeuse> dberkholz: hello
<Betelgeuse> dberkholz: thanks for the reminder
<dberkholz> we're still short by one jorge
<Betelgeuse> I have a bunch of numbers for him
<Chainsaw> The coverage at his hospital is supposed to be spotty, so a text is
	   probably best.					        [21:09]
*** AdmiralNemo (~dustin.ha@vpn.softekinc.com) has joined channel
    #gentoo-council
<Betelgeuse> I got through to one number but a woman responded in Portuguese
								        [21:11]
<grobian> lol
<ulm> we've got a long agenda today, so I suggest that we start	        [21:12]
<hwoarang> it's been 12' can we start?
<grobian> Betelgeuse: is that the 4153 one?
<ulm> EAPI specification in ebuilds
<Betelgeuse> grobian: xxx xxx 250
<dberkholz> indeed we do. if i'd noticed the times on that agenda, i would've
	    said something.
<Betelgeuse> grobian: 4153 did not connect			        [21:13]
<Betelgeuse> ulm: agreed
<grobian> I texted that number
<ulm> discussion on -dev and the wiki page are linked from the agenda
*** thrice` (thrice@unaffiliated/thrice/x-000000001) has joined channel
    #gentoo-council						        [21:14]
<grobian> ulm: my reponse/input:
	  http://dev.gentoo.org/~grobian/altEAPImechs.txt
<dberkholz> so, of methods A-F, i'd be ok with A, B, or E.	        [21:15]
<hwoarang> A or F for me
<grobian> F A C							        [21:16]
<ulm> I'd much prefer B, but A would be o.k. too
<Chainsaw> As long as we do not attempt D, which has been voted out in the
	   past...
<Chainsaw> And with the understanding that F is the preferred option...
								        [21:17]
<dberkholz> i suppose F too, but i'd rather see some forward motion even if
	    the chosen solution has some cons.
<Chainsaw> Most of the others look good.
<_AxS_> for clarification -- F is do nothing and ensure that portage depends
	on an appropriate bash for EAPI=0 ?
*** thrice` (thrice@unaffiliated/thrice/x-000000001) has left channel
    #gentoo-council: #gentoo-council
<ulm> right, F is to do nothing
<dberkholz> F is basically say this problem isn't important because we haven't
	    needed it for the past 5 years.
<grobian> … or we silently broke PMS' restriction of bash version because we
	  didn't care enough					        [21:18]
<ulm> Chainsaw: any preferences?
<Chainsaw> ulm: F please!
<ulm> Betelgeuse?						        [21:19]
<Betelgeuse> ulm: D A B C E					        [21:20]
<grobian> lol
<Betelgeuse> F
<Betelgeuse> as in fail
<Betelgeuse> Anyone know what the status of xattrs is?
<Betelgeuse> If it's in any way feasible.			        [21:21]
<grobian> yeah, won't work everywhere
<dberkholz> ha. that's an idea i was talking about years ago.
<dberkholz> we'd hate to stop supporting gentoo on aix though. =P
*** johu (~johu@gentoo/developer/johu) has quit: Quit: No Ping reply in 180
    seconds.
<ulm> so, there seems to be preference for options A and F
*** johu (~johu@gentoo/developer/johu) has joined channel #gentoo-council
								        [21:22]
<dberkholz> B also seemed fairly popular
<grobian> any reason people dislike C?
<Betelgeuse> dberkholz: it could come in an external file for people without
	     xattr support
<ulm> grobian: it's summarised on the wiki page already
<Betelgeuse> so they pay a small performance penalty
<grobian> ulm: yeah, but see my link
<grobian> metadata influences Portage's "ugly die behaviour": users won't see
	  it							        [21:23]
<dberkholz> Betelgeuse: yep, that had occurred to me. it is a fun idea, isn't
	    it?
<ulm> so, how should we proceed?
<Chainsaw> I could live with A. B assign meaning to comments without a
	   shebang.						        [21:24]
<Chainsaw> And that seems very, very wrong to me.
<ulm> I could work out a more detailed proposal for option A for the next
      meeting
<Chainsaw> But, if I could have F, that would be even better.
<hwoarang> it seems everyone understand the proposals so I think we are safe
	   to vote?
<grobian> the only concensus at the moment is F?
<hwoarang> seems like most of us have more than one items of preference
								        [21:25]
<ulm> A is slightly preferred
<grobian> I prefer to put the portage team in the loop for their input on some
	  candidates for the best match (performance, ease of implementation)
<dberkholz> A seems to be the only one everybody is OK with
<grobian> say those that would get a council majority vote
<hwoarang> so basically A					        [21:26]
<dberkholz> assuming i got this right: http://pastebin.com/35HS216n
<ulm> grobian: I believe zmedico prefers A
<Chainsaw> dberkholz: That's helpful, thanks. A looks like a winner then.
<_AxS_> ulm: by vote (ranking in order), A is the winner with 22, followed by
	F with 16.						        [21:27]
<grobian> betelgeuse also was ok with F
<ulm> _AxS_: yes, I've put it into a Condorcet/Schulze calculator too ;)
<Chainsaw> So let's move forward. Proposal A, yes/no?
<_AxS_> ulm: ah.  (i didn't know there was a name for that, i just wrote it
	out)
<ulm> A, then F, then B
* Chainsaw votes yes						        [21:28]
<ulm> Chainsaw: that's A against F?
<Chainsaw> ulm: We can only do yes/no votes last I checked.
<ulm> o.k., let's vote yes/no on A
<ulm> yes from me
* Chainsaw votes yes (again)					        [21:29]
<grobian> A: yes
<Betelgeuse> Chainsaw: Well we just need to reach a majority in some fashion
<hwoarang> yes
<ulm> Betelgeuse: your vote?
<Betelgeuse> So are we voting on the exact specifics now or just the approach?
<dberkholz> are we just re-doing everything we just did?
<Chainsaw> Betelgeuse: Option A. Yes/no?			        [21:30]
<dberkholz> i would've sworn we just finished saying all the ones we'd be fine
	    with, and now we seem to be doing it again one at a time
<Chainsaw> dberkholz: Yes, it is most tedious.
<ulm> Betelgeuse: exact specifics will follow, but as outlined on the wiki
      page as of today
<_AxS_> i think this vote is for official concensus on A
<Chainsaw> _AxS_: Yes.
* grobian nods
<dberkholz> ok, i'm yes on A/B/F, and ping me in 20 minutes when the rest of
	    you are ready =P
<Betelgeuse> Chainsaw: neutral					        [21:31]
<Chainsaw> Betelgeuse: There is no abstaining from a yes or no vote.
<ulm> I count 5 yes and one abstention
<Chainsaw> ulm: Not unanimous but sufficient?
<grobian> ok, so we're on schedule now
<Chainsaw> grobian: Yes, that agenda is bang on.		        [21:32]
<ulm> that's sufficient I guess
<hwoarang> yes it is
<ulm> so I hope we'll have the exact specifics (PMS patch) ready for the next
      meeting
<Betelgeuse> Chainsaw: I guess depending on how you look at it could be
	     counted as no.
<Betelgeuse> Chainsaw: But I don't think you must say anything.	        [21:33]
<grobian> ulm: make it an action point
<ulm> grobian: k
<ulm> next topic
<ulm> New udev and separate /usr partition
<ulm> Chainsaw: Do you want to comment on this?
<Chainsaw> Indeed. In my opinion, a separate /usr partition has been a
	   supported configuration for a very long time and should remain so.
<Chainsaw> You will have seen a statement by Jaco Kroon, who admins an order
	   of magnitude more Gentoo servers than me.		        [21:34]
<Chainsaw> We are both in the same predicament, in that we have LVM2 systems
	   with /usr separate, as per the official guide.
<Chainsaw> I do not feel that the new udev should be allowed to have stable
	   keywords unless it is capable of handling a separate /usr in a
	   graceful fashion.
<Betelgeuse> Chainsaw: So to clarify a universal initramfs is not enough?
								        [21:35]
<Chainsaw> For the avoidance of doubt, I do not contest the unmasking, nor the
	   ~amd64 keyword on it.
<_AxS_> ... point of order on the agenda, regarding the conclusion about what
	would occur if /usr will remain supported ....  udev can go stable if
	a means of supporting /usr-on-separate partition (via initramfs or
	other) is a possibility, no?
<ulm> Chainsaw: separate /usr, as in separate /usr without an initramfs?
<Chainsaw> Betelgeuse: No. That is additional work for a clearly broken
	   package.
<dberkholz> here's the thing
<dberkholz> who's going to either "port" udev as necessary, or maintain an old
	    version forever?					        [21:36]
<dberkholz> we can't proclaim things like this from on high without a route
	    forward
<Chainsaw> I will keep an old version going until the end of time.
<hwoarang> if udev is moving that way, we can't stay 'old' forever
<dberkholz> what happens when kernel 3.6 no longer supports it?
<Chainsaw> Then dev(tmp)fs will win.
<ulm> or static /dev as in the old times ;)			        [21:37]
<dleverton_> As a point of interest, does anyone know exactly what changed in
	     udev to stop /usr from working?
<hwoarang> given that udev is going to be merged to systemd, this discussion
	   may render moot
<dberkholz> i was just about to mention that.
<ulm> dleverton_: IIUC it depends on programs in /usr now	        [21:38]
<dleverton_> Unconditionally?
<Chainsaw> I never thought I'd speak out in favour of openrc...
<Chainsaw> But I will use openrc over systemd any day.
<_AxS_> ...there's a fair bit of talk on this issue, and from more than just
	gentoo.  If gentoo took the stance of definitely not sticking with
	upstream (and probably forking udev), i think that there would be
	support for this.
<ulm> dleverton_: /usr/bin/udevadm
<Chainsaw> And to confirm, if that means "Chainsaw maintains udev in Gentoo"
	   and "point at Chainsaw if you disagree", I can live with that.
								        [21:39]
<dleverton_> OK
<grobian> Chainsaw: that makes a change, and probably you can find people that
	  support you
<Chainsaw> grobian: I have jkroon on my team. We can handle Asterisk.   [21:40]
<Chainsaw> grobian: I am sure we can handle udev too.
<ulm> Chainsaw++						        [21:41]
<hwoarang> what do udev maintainers think about this?
<grobian> Chainsaw: I mean, it's hard to say something must remain supported
	  if there is no people doing that, so if you back it, I'm happy to
	  support you
<Chainsaw> grobian: I will do what it takes, on company time.	        [21:42]
<Chainsaw> grobian: Expect an Asterisk-style patchset on top of what upstream
	   does to tame it into reasonable behaviour.
<grobian> Yes, separate /usr should remain supported
<ulm> so, should we have a vote on it in this meeting?		        [21:43]
<grobian> ^^^ my vote
<Chainsaw> ulm: I think we should.
<ulm> or postpone it?
<hwoarang> postpone it for what?
<Chainsaw> ulm: Because if we postpone it, there is time for udev to be
	   stabled under "it's been 30 days!" conditions.
<_AxS_> the discussion we just have about udev is somewhat aside from the
	issue.  It would be pertinent, i think to vote on continuing to
	support separate /usr or not
<ulm> o.k., then please vote yes/no
<Betelgeuse> yeah we should at least delay udev going stable
<grobian> could either flesh out building the "separate /usr" team, or vote
	  now assuming it will happen
<Betelgeuse> As long as there is no automated help for people to automatically
	     get initramfs I vote yes				        [21:44]
<Chainsaw> Separate /usr is a supported configuration. I vote yes.
<dberkholz> umm
<hwoarang> i vote no, because diverting from upstream is not an ideal option
	   for me						        [21:45]
<dberkholz> i'm fine with supporting that in the sense that older udevs could
	    be maintained forever
<dberkholz> but i'm not ok with never stabling new versions
<Chainsaw> That isn't the question though dberkholz.
<grobian> this is not about stabling packages
<Chainsaw> The question is whether separate /usr is a supported configuration.
<ulm> The question is: "Decide on whether a separate /usr is still a supported
      configuration."
<Chainsaw> If it is, then stabling udev and "the hell with you LVM2 weirdos"
	   is no longer an option.				        [21:46]
<ulm> as in the agenda
<dberkholz> and my vote is conditional on this not blocking stabilization of
	    new udev
<dberkholz> as long as the other configuration is somehow supported
<Chainsaw> dberkholz: Whilst you can't have it both ways...
<grobian> you could make a profile				        [21:47]
<Chainsaw> dberkholz: My plan is to patch reasonable behaviour back into udev,
	   and going with the upstream releases as long as it is feasible.
<dberkholz> sure i can. maintain old udev-XXX forever, put an elog in new udev
	    that says "if you want separate /usr without initramfs, block old
	    udev or whatever
<dberkholz> err, install old udev, mask new
<grobian> do it's masked/unmasked there
<Chainsaw> dberkholz: That is the python 3 disaster all over again. *NO*
<dberkholz> well, i want new crap, and i don't want to wait around for custom
	    gentoo ports all day. that is why we do vanilla.
<dberkholz> fork udev into a different package name then	        [21:48]
<Chainsaw> dberkholz: Then put it back under package.mask and go nuts.
<zmedico> yeah, use a virtual
<dberkholz> we already have a device-manager virtual
<zmedico> problem solved :)
<dberkholz> dev-manager
<ulm> anyway, including another yes from me, I count 4 yes and 1 no     [21:49]
<grobian> so this seems to be able to be solved, apart from the question
	  whether or not separate /usr  is supported
<ulm> which is a majority
<_AxS_> there are ways to provide support for /usr with a newer udev; again,
	the stabilization of udev (in the long term) should not really be an
	issue with this vote, should it?
<Chainsaw> _AxS_: It does not block newer udev from being stabled in the long
	   term.
<Chainsaw> _AxS_: It just blocks stabling with a some armwaving about
	   initramfs generators over in the corner there.	        [21:50]
<Chainsaw> (Which is going to happen. Sorry for my lack of trust in people,
	   but it will)
<_AxS_> *nod*.  right.  ok, well ulm closed voting so i'll go back to my
	corner.
<ulm> _AxS_: we should make sure that there's reasonable documentation about
      setting up an initramfs though
<_AxS_> ulm: oh yes -- i was figuring the solution on how to support separate
	/usr with new udev would be tabled at the mext meeting (if necessary)
<grobian> ok, next?						        [21:51]
<ulm> dberkholz: should I count you as an abstention for the last vote?
<Chainsaw> _AxS_: I would recommend raising the implementation details for
	   next meeting, yes.
<dberkholz> ulm: nah, i'm for it.				        [21:52]
<dberkholz> the implementation details are my concern, but that's not the
	    vote.
<ulm> o.k., that's 5 yes 1 no then
<ulm> has anyone further comments on this topic?
<Chainsaw> dberkholz: My immediate concern is the prevention of stabling by
	   stealth, which is now done. We can agree further work offline.
								        [21:53]
<Chainsaw> As in, outside of this meeting. Let's move on.
<ulm> next topic
<ulm> herds.xml and mail aliases
<grobian> answer: no						        [21:54]
<hwoarang> no
<ulm> I think that has been more or less answered on the ml already
<dberkholz> if this is even a question, that means it should be automated.
<ulm> no from me, too
<Chainsaw> No.
<Betelgeuse> no
<Chainsaw> (It's a laudable goal, but it will not work this way.)       [21:55]
<ulm> so obviously no majority, at least not for the way it has been proposed
								        [21:56]
<ulm> I guess we can move on
<ulm> Open bugs with council involvement
<ulm> bug 383467						        [21:57]
<willikins> ulm: https://bugs.gentoo.org/383467 "Council webpage lacks results
	    for 2010 and 2011 elections"; Website www.gentoo.org, Projects;
	    CONF; hwoarang:jmbsvicetto
<ulm> jmbsvicetto isn't present, so I guess we won't get an answer on this one
<grobian> answer is probably: ETOOBUSY :)
<ulm> anyone has comments about this bug?
<ulm> seems not to be the case					        [21:58]
<ulm> bug 408073
<willikins> ulm: https://bugs.gentoo.org/408073 "Make the user choose a
	    locale"; Documentation, Installation Handbook; CONF;
	    betelgeuse:docs-team
<dberkholz> sounds like we need to heckle cam.
<dberkholz> ulm, wanna ping on the bug?				        [21:59]
<ulm> can do
<Chainsaw> Okay, so when we will we meet again?
<hwoarang> Betelgeuse: can we please have the logs + summary from the last
	   meeting? It's been a month and it does not look good	        [22:00]
<dberkholz> well, we finally did something interesting this time, so it's
	    worth posting the logs more speedily =)
<ulm> Chainsaw: May 8th?
<dberkholz> may 8 is more or less fine for me, with the caveat that my next
	    baby's due date is may 7.				        [22:01]
<Betelgeuse> hwoarang: yeah I will do it when this meeting wraps up.
<Chainsaw> May 8 works well for me, yes please.
<hwoarang> thanks a lot
<Betelgeuse> hwoarang: sorry about it and thanks for reminding me
<grobian> ulm: May 8th ok
<ulm> who's chair for next month?
<grobian> jmbsvicetto:
<grobian> I don't mind doing it for him				        [22:02]
<ulm> k
<grobian> I suspect he'll be busy still
<hwoarang> grobian: you probably want to get in touch with him
<ulm> grobian: let him comment by e-mail
<hwoarang> in case he wont make it, lets be prepared
<grobian> yup
<grobian> I'll be just his stand in
<grobian> send him an email and such
<ulm> tahnks							        [22:03]
<ulm> so, open floor
<Chainsaw> The mics are on guys & gals.
<Chainsaw> Share your thoughts.
<grobian> deafening silence					        [22:04]
<_AxS_> ... not much to say, all the big stuff was covered already      [22:05]
<ulm> then let's close the meeting
<ulm> thanks to all of you, it was very efficient this time
<Chainsaw> *big hammer* *BANG*
<Chainsaw> Very civilised as well.
<grobian> thanks for chairing ulm				        [22:06]
<Chainsaw> Even got the results I wanted. Thank you all.
<dberkholz> thx
*** ulm (~ulm@gentoo/developer/ulm) has set the topic for #gentoo-council:
    "Next meeting: May 8th, 19:00 UTC | Meeting chairs:
    http://www.gentoo.org/proj/en/council/#doc_chap5 |
    http://www.gentoo.org/proj/en/council/utctolocal.html?time=1900 |
    http://www.gentoo.org/proj/en/council/"			        [22:07]
