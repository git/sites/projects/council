Summary of the Gentoo Council meeting 10 January 2016

Roll call
=================
Present:
blueness, dilfridge, jlec, k_f, rich0, ulm, williamh


1. Further discussion on GLEP 67
=================
https://wiki.gentoo.org/wiki/GLEP:67
https://wiki.gentoo.org/wiki/User:MGorny/GLEP:67
https://archives.gentoo.org/gentoo-project/message/637270936c9f07e3bd2f10ee45264a42

Michał Górny reported on the progress regarding GLEP 67. He has cleaned the 
spec to make it more informative and updated the reference implementation 
section for the most common software in use.

Nearly all herds have been merged into existing or new projects, or have been
abandoned. 

Automated generation of projects.xml is already active on Gentoo Infrastructure.

A short discussion about the mapping between projects and subprojects has 
happened. It has been agreed on not definitely specifying the mapping being 1:n
or m:n in the GLEP right now. 
The general consensus was that m:n mapping resembles the reality best and 
missing support of that map style in tools like mediawiki is not sufficient 
to nail the connectivity to 1:n.

The Council approved GLEP 67 with wording from https://wiki.gentoo.org/wiki/User:M
Gorny/GLEP:67 as of 08:14, 10 January 2016 unanimously.

The Council voted on the final deadline for the herds -> project migration
to be the 24th January 2016. All remaining herds will be dropped after that and
the remaining packages will be assigned to maintainer-needed. The vote passed 
unanimously.


2. Banning of EAPI 0 & 3 for new ebuilds
=================
https://archives.gentoo.org/gentoo-project/message/bc0a1b7498c389bdbb0b0d52feb43391
https://archives.gentoo.org/gentoo-dev/message/6904e810caedf66d889458e6fd1cc552

The Council voted on banning EAPI 0 & 3 for new ebuilds and EAPI updates
in existing ebuilds. After some discussion an exception was made allowing
the security team and only the security team to do minor patching using 
those EAPI levels. The decision was made unanimously.

Further the Council decided unanimously to amend the ban of EAPI
1 & 2 made in the Council meeting 2014-03-11. To this effect, the
exception of updating EAPI=0 ebuilds to EAPI=1 is dropped.


3. Bugs with Council participation
=================
The Council briefly discussed bug 503382 again: "Missing summaries for 
20131210, 20140114, and 20140225 Council meetings". Ulm had written the
final missing summary. The bug is fixed now.

The Council briefly discussed bug 569914 & 571490: Missing meeting logs
for 20151025 and 20140225. Dilfridge is working on them.

The Council voted on the NEWS item format 1.1, bug 568068. Format 1.1 allows
EAPI=5 style package dependency specification. Rich0 questioned the amount
of discussion with the community, but the general consensus was that the
public transparency for the size of the change has been enough. The Council 
unanimously approved the new format.


4. Open floor
=================
A short information exchange about the presence of Gentoo Linux at the 2016 
FOSDEM has happened but further discussion has been moved to #gentoo-fosdem 
@ freenode.
