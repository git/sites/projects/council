Roll call
=========
betelgeuse here
dberkholz  here
dertobi123 here
flameeyes  here [cardoe]
halcy0n    here
jokey      here
lu_zero    here


Old topics
==========

Reactions to dev banned from freenode
-------------------------------------
Update: none. Assume lack of interest.


Moving meetings to a location we control
----------------------------------------
Update: none. Assume lack of interest.


Favor irc.gentoo.org alias in docs, etc
---------------------------------------
Update: Freenode acknowledgments page thanks people for doing this, so 
the potential issue with confusion apparently isn't a large problem.

Goal: Can we decide today?

Decision: Update all our pointers to IRC to use irc.gentoo.org. (But 
please mention FreeNode is our provider.)


Why aren't fired developers banned from the channels where they
displayed this behavior?
---------------------------------------------------------------
Update:

For banning from those channels: halcy0n, dertobi123 (on gentoo-dev)
No opinions from the rest of us

Goal: Get yes or no on banning from the same channels. If no, ask for 
alternate suggestions if there are any. (Example: let devrel decide)

Summary: halcy0n, dertobi123, lu_zero think fired devs should be banned 
from the places where they behaved in the way that got them fired. 
dberkholz and cardoe think that this should be handled by devrel and 
council shouldn't set policy on it. halcy0n later agreed with letting 
devrel address it, as did lu_zero and betelgeuse.


PMS as a draft standard of EAPI 0
---------------------------------
What changes are required before this is true?

Update: The main thing that needs to get figured out is conflict 
resolution.

Idea: Ask portage devs & PMS authors to develop a process that both 
groups will respect, then present it to the council for approval. 
Options include a "neutral" third party as PMS czar, having council 
decide, just trying harder to come to agreement, deciding that e.g. 
portage's choice always wins, random, etc.

spb and ciaranm agree that a third party or council would work well. 
Since such a third party would probably be better invested in actually 
working on the spec, the council seems reasonable if PMS editors & PM 
developers can't work it out.

20:46 < dberkholz@> zmedico, ferringb, ciaranm, spb: so you'll all agree to 
                    follow council decisions on conflicts you aren't able to 
                    resolve otherwise?
20:46 <   zmedico > dberkholz: I agree
20:47 <  ferringb > dberkholz: either way, game to attempt something different- 
                    what's in place doesn't particularly work imo
20:47 <   ciaranm > dberkholz: so long as the council's prepared to follow 
                    through with its resolutions
20:49 <  ferringb > either way, council as arbitrator flies.

Decision: Council will vote to resolve conflicts that the PMS editors 
and PM developers weren't able to resolve.

zmedico, ferringb & ciaranm (developers of each PM) all agree that 
having a written specification is worthwhile.

Next meeting is Sept 11, and we request that everyone involved with PM 
development or the spec email gentoo-dev about any issues with it. 
Otherwise, it's likely to be approved as a draft standard.

