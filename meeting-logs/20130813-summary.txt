Summary of Gentoo council meeting 13 August 2013

Agenda
======
1. Introduction and roll call

2. Support for separate /usr partition - williamh [1]

3. Locale for build logs in bugzilla - dilfridge [2]

4. Open bugs with council involvement
   - Bug 477030 - Missing summary for 20130611 council meeting [3]
   - Bug 480408 - Autobuilds go to /experimental and to /releases only
     when actually tested [4]

5. Open floor


Roll call
=========
Present:
blueness, dberkholz, dilfridge, rich0, scarabeus, ulm, williamh

Support for separate /usr partition
===================================
williamh asked for clarification of the vote about separate /usr
support taken in the April 2012 meeting [5]. Newer developments in
some upstream packages make supporting a proper barrier between root
and /usr filesystems increasingly difficult. A long discussion
followed. Taking a decision about the question if we should do the
/usr merge was generally regarded as too early. It was noted the
latest FHS standard [6] still requires that a system must be able to
boot off root without /usr, and that we should not remove support
where it already exists. In setups with an initramfs, some of the boot
and repair functionality can be moved from the root filesystem to the
initramfs. It was also observed that we need official documentation on
how to install a system with an initramfs, although some documentation
is already in place [7,8].

Two votes were taken:

- "Since that particular setup may already be subtly broken today
  depending on the installed software, Council recommends using an
  early boot mount mechanism, e.g. initramfs, to mount /usr if /usr
  is on a separate partition."
  Accepted unanimously.

- "The intention is to eventually not require maintainers to support
  a separate /usr without an early boot mechanism once the Council
  agrees that the necessary docs/migration path is in place."
  Accepted with 4 yes votes, 1 no vote, 2 abstentions.

Locale for build logs in bugzilla
=================================
dilfridge suggested that we should make build logs English by default,
by setting an appropriate locale in our profiles. From the previous
mailing list discussion the conclusion was that LC_MESSAGES=C would be
an appropriate setting.

Vote:

- "Make a news item, then add LC_MESSAGES=C to the base profile
  make.defaults."
  Accepted unanimously.

Open bugs with council involvement
==================================
- Bug 477030 "Missing summary for 20130611 council meeting"
  No progress since last meeting.
  Action: scarabeus will talk to betelgeuse (who was chairman of the
  June meeting).

- Bug 480408 "Autobuilds go to /experimental and to /releases only
  when actually tested"
  No action to be taken by the council. This should be sorted out
  within the releng team.

Open floor
==========
Q: Does the council favour git migration? (xmw)
A: The council is in favour of the migration to git, and has expressed
   its support already in a previous meeting [9].

Next meeting date
=================
10 September 2013, 19:00 UTC


[1] Message-ID: <20130801003614.GA29807@linux1> (not in archives)
[2] http://article.gmane.org/gmane.linux.gentoo.project/2926
[3] https://bugs.gentoo.org/show_bug.cgi?id=477030
[4] https://bugs.gentoo.org/show_bug.cgi?id=480408
[5] http://www.gentoo.org/proj/en/council/meeting-logs/20120403.txt
[6] http://www.linuxbase.org/betaspecs/fhs/fhs.html
[7] http://wiki.gentoo.org/wiki/Early_Userspace_Mounting
[8] http://www.gentoo.org/doc/en/initramfs-guide.xml
[9] http://www.gentoo.org/proj/en/council/meeting-logs/20110608-summary.txt
