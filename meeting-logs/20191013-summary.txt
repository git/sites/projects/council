Agenda
======

1. Roll call
2. Allow pkgcheck as repoman replacement [1]
3. Valid forms of activity for recruitment/retirement [2]
4. Open bugs with council participation
5. Open floor


Roll call
=========
Present: gyakovlev, mgorny (proxy for dilfridge), patrick, slyfox,
         ulm, whissi, williamh

Allow pkgcheck as repoman replacement
=====================================
There was consensus to delegate this item to the QA team.

Valid forms of activity for recruitment/retirement
==================================================
It was noted that current recruitment policy, which includes passing
of quizzes, originated in the recruiters project. It was suggested
during discussion that any changes to this policy (e.g., for proxied
maintainers) should be initiated by recruiters, followed by a
discussion on mailing lists.

Regarding retirement, council members noted that the decision in the
2019-05-12 meeting was on a by-case basis for one single developer,
and doesn't establish any general rule.

Open bugs with council participation
====================================
- Bug 637328 "GLEP 14 needs to be updated":
  No news.

- Bug 642072 "[Tracker] Copyright policy":
  Tracker bug, no action.

- Bug 662982 "[TRACKER] New default locations for the Gentoo
  repository, distfiles, and binary packages":
  Last open blocker is about renaming of the snapshot tarball.

- Bug 695172 "Please clarify QA policy regarding USE flags with
  underscores":

  - Motion: "Underscores should be considered reserved for USE_EXPAND.
    They must not be used in any new 'normal' USE flags. Existing
    flags are technically valid; phasing them out has low priority."
    Accepted with 4 yes votes and 3 abstentions.

- Bug 696882 "Register /EFI/Gentoo namespace in UEFI Subdirectory
  Registry":
  No council action. Antarus has sent a reminder mail to the USWG.

Open floor
==========
No items were raised.


[1] https://archives.gentoo.org/gentoo-project/message/915d843e638656fc037dc21aa0e7243f
[2] https://archives.gentoo.org/gentoo-project/message/6bfdb844b9a9abfaf8ae20c386308ba2
