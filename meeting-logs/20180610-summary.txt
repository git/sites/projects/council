Summary of Gentoo council meeting 10 June 2018

Agenda
======

1. Roll call
2. Proposal for procedure to appoint SPI liaison by dilfridge@
3. Open bugs with council involvement
4. Open floor

Roll call
=========
6 attendees: WilliamH, slyfox, Whissi (proxies K_F), dilfridge, Soap (proxies mgorny), ulm
1 missing: tamiko

Proposal for procedure to appoint SPI liaison by dilfridge@
===========================================================
The Gentoo council shall directly contact "Software in the Public Interest
Inc." (SPI), with the intention of Gentoo becoming a SPI Associated Project.
The intention is for SPI to become an *additional* service provider of the
Gentoo developer community for Accepting Donations, Holding Funds, and Holding
Assets. The SPI project liaison shall be appointed by the Gentoo council.
No transfer of funds or assets of any kind between SPI and the Gentoo
Foundation is stipulated (it would be the trustees' responsibility anyway), so
any (dys)function of the Gentoo Foundation has no impact on this new business
relationship. Equally, the business relationship with SPI shall have no impact
on the current function of the Gentoo Foundation. Essentially, the proposal is
that we start with an empty account at SPI.

- Votes: 4 yes, 1 no, 1 abstain

Passed.

References:
    https://archives.gentoo.org/gentoo-project/message/70f054aac20e5c80f14590cc5cbf4418

Open bugs with council involvement
==================================

- bug #637328: GLEP 14 needs to be updated 

  Update by Whissi: Security project is making progress. Hopefully we will be
  able to show you something next meeting.

- bug #642072: Joint venture to deal with copyright issues 

  Update by ulm: Nicely progressing. Have settled on a policy with a Gentoo
  DCO and without a FLA/CLA: https://www.gentoo.org/glep/glep-0076.html

- bug #650964: Implement council decision on user whitelisting 

  No infra@ presence. Asked for update in the bug.

- bug #655734: Access/status of gentoo-managers@l.g.o

  """Give access to the archives to the people working on copyright project for
  search purposes.  This would include those of (Council, Trustees, robbat2,
  rich0) who explicitly ask for this.  At the same time, they will be asked not
  to use, disclose or publish anything they found there without explicit permission
  from the poster.  That said, we'll probably prepare a list of all mails we'd like
  to use and send a single combined request for permission."""

  Votes: [y/n/a] Council: 5/0/1 Trustees: 3/0/0

  Passed.

References:
    https://wiki.gentoo.org/wiki/Project:Council#Open_bugs_with_Council_participation
    https://bugs.gentoo.org/637328
    https://bugs.gentoo.org/642072
    https://www.gentoo.org/glep/glep-0076.html
    https://bugs.gentoo.org/650964
    https://bugs.gentoo.org/655734

Open floor
==========

No activity.
