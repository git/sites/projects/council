[20:00:42] <dberkholz> ok it's 1900, let's get started. who's here?
[20:00:50] -*- dilfridge here
[20:01:14] <dberkholz> blueness scarabeus ulm WilliamH phajdan-jr (for rich0): speak up
[20:01:19] -*- WilliamH here
[20:01:21] -*- blueness  here
[20:01:39] <dilfridge> blueness: not to my knowledge, I did not see anything new on the glep either
[20:02:46] <WilliamH> I didn't see anything either.
[20:03:11] <dberkholz> scarabeus ulm phajdan-jr: ping
[20:03:22] <blueness> ulm said he'd be a bit late
[20:03:51] <phajdan-jr> here
[20:04:23] <blueness> where's our opensuse liason?
[20:04:38] <dberkholz> alright, we've got 5 folks so let's get started with the 1st item and we can wait if we can't make it past a vote
[20:04:48] <dberkholz> Modernization/adaption of the Code of Conduct
[20:05:20] <dberkholz> i propose that we vote on ulm's version of the changes: http://article.gmane.org/gmane.linux.gentoo.project/3205 — any objections?
[20:05:21] <ulm> here
[20:05:23] <dilfridge> for what it's worth, I'm fine with ulm's comment about the "future" sentence 
[20:05:34] <dilfridge> we can leave that out for now
[20:05:41] <dilfridge> the background of the sentence is, 
[20:06:11] <dilfridge> in comrel we discussed a lot about the restructuring, and having a separate subproject for CoC enforcement was the original plan
[20:06:27] <dilfridge> but it was deferred to later, because 
[20:06:36] <dilfridge> of basically not having enough people at hand
[20:06:40] <dberkholz> sure.
[20:06:46] <blueness> okay
[20:06:46] <dilfridge> I'm not sure how current the plan still is though.
[20:06:48] <dberkholz> no point in talking about hypotheticals in the CoC
[20:06:53] <dilfridge> indeed.
[20:06:55] <blueness> agreed
[20:07:05] <dberkholz> so let's vote on ulm's version of the CoC changes
[20:07:07] <WilliamH> agreed
[20:07:13] <dberkholz> +1 from me
[20:07:14] -*- dilfridge yes ulm's version
[20:07:19] <phajdan-jr> +1
[20:07:22] -*- WilliamH yes ulm's version
[20:07:26] <ulm> +1
[20:07:47] -*- blueness yes
[20:07:55] <dberkholz> great, it's approved
[20:08:00] <dberkholz> now for the fun stuff!
[20:08:07] <dberkholz> next item: GLEP 48: QA Team's Role and Purpose
[20:08:10] <dilfridge> hehe
[20:08:32] <dberkholz> we have 3 proposals to revise the glep, described here: http://thread.gmane.org/gmane.linux.gentoo.project/3162
[20:09:04] <dberkholz> do people feel comfortable voting on one of them, or is there something we're missing?
[20:09:16] <dilfridge> I like it, in particular 3
[20:09:25] <phajdan-jr> same here
[20:09:42] <phajdan-jr> I think not that much discussion happend on the MLs, so it's not really controversial
[20:09:47] <ulm> I dislike approving the qa lead
[20:10:04] <ulm> as it potentially will result in stalemates
[20:10:12] <dilfridge> hmm
[20:10:22] <phajdan-jr> I think I pointed that out
[20:10:22] <blueness> dilfridge, what is it about 3 that you like, they are all pretty close
[20:10:23] <dilfridge> and how is that worse than current?
[20:10:27] <ulm> what are we going to do if the qa team and the council disagree
[20:10:33] <ulm> ?
[20:10:33] <phajdan-jr> and rich0 replied that in a stalemate council can appoint a lead
[20:10:47] <blueness> ulm, the qa team has to try again
[20:10:51] <phajdan-jr> I'm fine with amending the text to be more explicit about that
[20:11:09] <blueness> or better yet appoint a lead
[20:11:16] <dilfridge> 1 is the basics... 2 makes clear what happens if things go wrong, 3 is correcting some typos
[20:11:31] <blueness> because QA has disciplinary powers, that power must be legitimized by a voted body, aka the council
[20:11:36] <blueness> QA is not any ordinary team
[20:11:42] <ulm> it is against the principles that projects organise themselves
[20:11:47] <dilfridge> 1 alone can lead to a mess, since there is no definition what happens if the lead is not confirmed
[20:11:48] <dberkholz> phajdan-jr: options 2-3 already mention the council appointing an interim lead.
[20:12:17] <blueness> ah okay, i like 3 then
[20:12:35] <dilfridge> ulm: yes, but that rule makes sense for non-exclusive projects... now if qa were purely advisory, that would be fine
[20:12:35] <phajdan-jr> correct - just in case it's not clear, we could even more clearly say (if people want that) that this means in case of stalemate/disagreement, council appoints a lead resolving the stalemate
[20:12:44] <blueness> ulm, "it is against the principles that projects organise themselves" -> but QA is not any old project, its special
[20:13:01] --> mrueg_ (~mrueg@gentoo/developer/mrueg) hat #gentoo-council betreten
[20:13:02] <phajdan-jr> qa is special, similar to e.g. devrel, recuiters, council
[20:13:12] <dilfridge> same as, I can't just go and found comrel2
[20:13:21] <blueness> right
[20:13:24] <WilliamH> blueness: I understand what ulm is saying. All projects are accountable to the council.
[20:13:31] <dilfridge> (or infra2, that's already taken by patrick)
[20:13:32] <blueness> haha! i don't like QA so i'll form QA2
[20:14:11] <WilliamH> blueness: The way I seeit the council can block qa2.
[20:14:36] <blueness> dilfridge, how shall we proceed with a 3 way vote?
[20:14:57] <dilfridge> tbh, it makes no sense to split between 2 and 3
[20:15:05] <blueness> agreed
[20:15:06] <dilfridge> let me make a diff, just a moment
[20:16:08] <dilfridge> arrgh
[20:16:09] <Zero_Chaos> WilliamH: the council has no ability to block creation of a new project. They could refuse that project any power of course.
[20:16:50] <dilfridge> dosnt work that quickly
[20:17:03] <WilliamH> Zero_Chaos: I guess that's what I mean. if someone wanted to make qa2, qa2 wouldn't have any tree-wide "power" without the council giving them that.
[20:17:22] <dilfridge> but as foar as I can see, 
[20:17:27] <dilfridge> 3 adds
[20:17:37] <dilfridge> * sentence about revision
[20:17:53] <dilfridge> * typo fix percieved
[20:18:17] <Zero_Chaos> WilliamH: well they would have the power they assign themselves until the council removes it :-)
[20:18:17] <dilfridge> * devrel -> comrel
[20:18:32] <dilfridge> and no other changes that I can see now
[20:18:48] <dilfridge> so the question is more 1+typo fixes, or 3
[20:19:00] <dberkholz> Zero_Chaos: insofar as that power is basically identical to that of any individual developer, sure
[20:19:24] <blueness> dilfridge, okay
[20:19:25] <phajdan-jr> Zero_Chaos: I don't think that's true; it'd be hard to claim powers of e.g. managing the infrastructure, recruiting/retiring developers and so on; in the tree there are also pretty clear rules for developers that are also quite flexible
[20:19:35] <Zero_Chaos> dberkholz: happy to discuss later, I don't want to derail the meeting with OT banter.
[20:19:56] <Zero_Chaos> phajdan-jr: see above :-)
[20:20:18] <phajdan-jr> right, dberkholz's answer is even shorter and I fully agree
[20:21:42] <blueness> dilfridge, can you formulate what we are/will be voting on then
[20:21:47] <blueness> language wise
[20:22:33] <dberkholz> we also need an option of "minor typographical changes only"
[20:23:11] <dberkholz> so really the choices are 1) typo fixes, 2) lead confirmation, 3) filling in the holes in (2) to avoid stalemates etc
[20:23:24] <ulm> we really don't have to vote about spelling fixes
[20:23:28] <dilfridge> what we can do is vote yes no on each of those
[20:23:28] <ulm> just do it
[20:23:41] <dilfridge> http://bpaste.net/show/157371/
[20:23:56] <dilfridge> here's the difference between 2 and 3 (the "typo fixes")
[20:24:32] <ulm> lines 7 to 9 don't make sense in this context
[20:24:43] <dberkholz> the 1st point belongs in a version that has substantive changes
[20:24:47] <dilfridge> yes
[20:25:12] <dberkholz> i agree with ulm, the rest should just happen without any vote required
[20:25:35] <ulm> e after i except after c ;)
[20:26:11] <dberkholz> but my initial point was really that we need an option for "keep it the same as it was"
[20:26:21] <dilfridge> how about we vote on lead confirmation, i.e. 1, first (yes/no), then we vote on the difference 1-2 (yes/no)?
[20:26:24] <dberkholz> including any minor updates
[20:26:30] <dilfridge> if the first goes no, well it's over
[20:27:14] <dberkholz> seems reasonable, although i really hope we don't end up stuck in the middle
[20:27:22] <blueness> dilfridge, i'm okay with your voting scheme
[20:27:37] <dberkholz> so let's vote on lead confirmation
[20:27:41] <dberkholz> +1 from me
[20:27:44] -*- blueness yes
[20:27:48] -*- dilfridge yes
[20:27:52] -*- ulm no
[20:27:55] <phajdan-jr> yes for lead confirmation
[20:27:57] -*- WilliamH no
[20:28:26] <dberkholz> ok, that one's approved.
[20:28:49] <dberkholz> given that we will confirm leads, do you want to have the version that fixes some of the issues there to enable council to deal with any stalemates?
[20:29:10] <blueness> let's vote!
[20:29:22] -*- dilfridge yes for 2 (and 3)
[20:29:34] -*- ulm no
[20:29:37] -*- blueness yes
[20:29:40] <phajdan-jr> yes for resolving stalemates (council appointing lead)
[20:29:59] <dberkholz> +1 again
[20:30:09] -*- WilliamH no
[20:30:23] <dberkholz> same again, approved by a 4–2 vote.
[20:30:57] <dberkholz> good, we've got that dealt with
[20:31:18] <dberkholz> next topic
[20:31:19] <dberkholz> Open bugs/issues with council involvement
[20:31:40] <dberkholz> i put a bunch of time for this mainly because of the key glep
[20:32:21] <dberkholz> robbat2: i noticed you didn't request a vote for this meeting, and i didn't get a response from my irc ping the other day
[20:32:38] <dberkholz> robbat2: any chance you're around to provide a quick update? is this ready to vote on?
[20:33:28] <dberkholz> the other issue i wanted to suggest, which i put in the agenda, is whether this should be an actual glep at all
[20:33:33] <dberkholz> vs a policy owned by QA
[20:33:44] <blueness> dberkholz, glep is fine
[20:33:55] <blueness> its both QA and infra really
[20:34:03] <phajdan-jr> note that for a GLEP vote, it'd be good to have a unified final version of the GLEP
[20:34:20] <phajdan-jr> I think it makes sense as a GLEP btw
[20:34:33] <dilfridge> glep is good, more official
[20:34:51] <blueness> should we table this issue seeing as robbat2 is not here and none of us saw a final glep
[20:34:53] <dberkholz> phajdan-jr: what do you think is missing from http://permalink.gmane.org/gmane.linux.gentoo.project/3155
[20:35:28] -*- WilliamH agrees with blueness for now
[20:35:36] <phajdan-jr> dberkholz: there is some discussion after initial e-mail
[20:35:47] <ulm> we agreed last month that it should be a GLEP
[20:35:52] <ulm> with a number
[20:36:17] <ulm> so that any updates can be tracked
[20:36:24] <blueness> uep
[20:36:25] <blueness> yep
[20:36:42] <dilfridge> yeah
[20:37:04] <ulm> apart from that, the text seems to be fine mostly
[20:37:15] <phajdan-jr> +1
[20:37:15] <ulm> but again, we said that last month already
[20:37:19] <dberkholz> phajdan-jr: there were dol-sen's follow-ups, in which he basically said (1) ignore the first point, i was wrong and (2) +1
[20:37:32] <blueness> ulm, i thought the ideas were all fine, but that it would be a glep, so i'd like to see that language
[20:37:38] <dberkholz> phajdan-jr: did i miss something?
[20:38:07] <dberkholz> anyone want to take responsibility for making sure this thing gets a number?
[20:38:09] -*- phajdan-jr checks quickly
[20:38:32] <dberkholz> we need someone to find and chase the glep editor
[20:38:46] <dilfridge> actually
[20:38:55] <dilfridge> we had an agenda topic some months ago
[20:39:01] <dilfridge> about re-forming the glep team
[20:39:11] <dilfridge> does anyone know what became of that?
[20:39:12] <jmbsvicetto> iirc, the glep will get a number when the glep editor is fine with it and thinks it's ready to be submitted to council for a vote
[20:39:37] <WilliamH> jmbsvicetto: who is the glep editor?
[20:39:43] <jmbsvicetto> antarus
[20:39:44] <phajdan-jr> dberkholz: I see dol-sen's "Sounds good to me" to "This should probably make it into the glep."
[20:40:05] <phajdan-jr> which refers to keyrings = line in metadata/layout.conf
[20:40:12] <dilfridge> robbat2 wanted to check at least one more reference document, so maybe he still has changes to integrate
[20:40:41] <phajdan-jr> and the issue of key distribution doesn't seem to be fully resolved to me
[20:40:53] <phajdan-jr> I'd be fine with making that a part of seperate GLEP if people would find it useful
[20:41:04] <phajdan-jr> and accepting the developer key guidelines first
[20:41:05] <dilfridge> better separate
[20:41:22] <blueness> let's vote to table then
[20:41:35] <dilfridge> table means defer to next meeting?
[20:41:37] <dberkholz> yeah i just went and checked history for gleps (http://sources.gentoo.org/cgi-bin/viewvc.cgi/gentoo/xml/htdocs/proj/en/glep/) and it's mostly antarus as editor recently.
[20:41:47] <blueness> yes defer the vote because its not ready
[20:41:56] <phajdan-jr> if we defer let's specify what action is required
[20:42:23] <phajdan-jr> my suggestions: split per-developer key/gpg guidelines from key distribution mechanism/policy;
[20:42:32] <blueness> phajdan-jr, we could say by next meeting but it really depends on robbat2
[20:42:42] <blueness> we already approved the principle
[20:42:46] <phajdan-jr> and having a text on MLs not raising significant questions/objections
[20:42:53] <blueness> we just need to approve the final language
[20:42:59] <dilfridge> indeed
[20:43:09] <dilfridge> so final language is what we need
[20:43:14] <phajdan-jr> +1
[20:43:16] <blueness> yeah
[20:43:20] <ulm> +1
[20:43:39] <blueness> (it'd be wierd to approve again in principle)
[20:44:02] <ulm> "We appreciate the GLEP draft submitted by robbat2 in above mailing list post, and look forward to approving a final version with additional minor changes merged in." <-- this we had last month
[20:44:21] <dberkholz> i just pinged antarus about getting it a glep #
[20:44:37] <blueness> yep
[20:44:45] <dberkholz> so we can get stop worrying about the "paperwork" side of things
[20:45:00] <phajdan-jr> sounds good
[20:45:10] <blueness> btw, nothing is stopping the new QA team from beginning to implement the gpg guidelines
[20:45:26] -*- dilfridge hears a "hint hint" there :D
[20:45:35] <blueness> there's probably a lot of work to look at all the dev's gpg keys to make sure they fit
[20:45:41] <blueness> yeah hint hint!
[20:45:43] <phajdan-jr> please correct me if I miss something, but I think the key distribution part of the GLEP is not fully resolved; that'
[20:45:49] <phajdan-jr> s the only issue I see
[20:46:00] <Zero_Chaos> blueness: that's mostly been done already
[20:46:00] <ulm> blueness: last time I looked at them it was a total mess
[20:46:00] <dberkholz> yeah, this is purely key policies, not verification
[20:46:01] <dilfridge> phajdan-jr: talk to dol-sen about that, he's working on it
[20:46:03] <phajdan-jr> and, yeah, +1 to QA doing things
[20:46:13] <Zero_Chaos> blueness: details from dol-sen
[20:46:20] -*- phajdan-jr sends a reply on gentoo-dev about that
[20:46:22] <ulm> blueness: keys in ldap being different from what was used for signing etc
[20:46:27] <phajdan-jr> oh, gentoo-project actually
[20:46:36] <dberkholz> alright, can we move on to the next issue?
[20:46:41] <blueness> ulm, you'll need some automation there, i'm sure, and produce a checklist in a chart with green arrows and red x's
[20:46:50] <dberkholz> just a couple more things then open floor for whatever else
[20:46:58] <dilfridge> ook
[20:47:03] <dilfridge> betelgeuse
[20:47:04] <blueness> Zero_Chaos, that's right, i remember dol-sen doing some of that work
[20:47:22] <blueness> he pinged me about my gpg key for some reason, so he was looking at them
[20:47:22] --> TomJBE (~tb@gentoo/developer/tomjbe) hat #gentoo-council betreten
[20:47:42] <dberkholz> let's keep going, and slide this discussion over to #-dev if you want to continue it
[20:47:57] <dberkholz> since we're not going to get anywhere on it today
[20:48:06] <dberkholz> anything happen on archives.g.o in the past couple weeks (bug #424647)? doubt it but we should keep thinking about it
[20:48:08] <willikins> dberkholz: https://bugs.gentoo.org/424647 "archives.gentoo.org: Broken URLs for e.g. gentoo-dev-announce and others"; Gentoo Infrastructure, Other; CONF; darkside:infra-bugs
[20:48:48] <dilfridge> not that I know
[20:49:55] <dberkholz> boo
[20:50:08] <dberkholz> maybe we can get a gsoc student to do something with it, if it's still busted then.
[20:50:31] <dberkholz> last open issue is bug #477030
[20:50:33] <willikins> dberkholz: https://bugs.gentoo.org/477030 "Missing summary for 20130611 council meeting"; Doc Other, Project-specific documentation; CONF; ulm:council
[20:50:41] <jmbsvicetto> dberkholz: robbat2 explained again the issue with archives
[20:51:09] <jmbsvicetto> dberkholz: anyone wanting to fix archives needs to read robbat2's email and address the issues he raised
[20:51:57] <dberkholz> jmbsvicetto: could you add a pointer to the email in that bug?
[20:52:06] <phajdan-jr> yeah, looks more like infra issue thatn council
[20:52:11] <phajdan-jr> +1 to e-mail reference
[20:52:44] <dberkholz> i just pinged betelgeuse re that summary
[20:52:49] <dberkholz> for the last open bug
[20:53:11] <dberkholz> any volunteers who were here last year if he doesn't?
[20:53:44] <ulm> dberkholz: I can take a look at it
[20:54:07] <ulm> although I would dislike Petteri getting away like that ;)
[20:54:26] <phajdan-jr> isn';t the link http://thread.gmane.org/gmane.linux.gentoo.project/3149/focus=3153 ? :)
[20:54:30] <phajdan-jr> maybe we could add it now
[20:55:03] <dberkholz> looks good enough
[20:55:15] -*- dilfridge would even be happy with a mailman-like archive
[20:55:26] <dberkholz> jmbsvicetto: could you stick that in the bug? http://article.gmane.org/gmane.linux.gentoo.project/3153
[20:55:31] <dberkholz> don't need 5 people to post dupes of it...
[20:55:35] <ulm> dilfridge: any archive is better than no archive ;)
[20:56:40] <dberkholz> alright
[20:56:44] <dberkholz> that's it for open issues
[20:56:48] <dberkholz> now for the open floor
[20:56:56] <dberkholz> anyone, council or otherwise, have something to say?
[20:57:59] <blueness> nothing from me
[20:58:05] <WilliamH> here's my thing about us confirming the qa lead.
[20:58:30] <WilliamH> Why do we need to confirm the lead since we can step in and remove a lead of any project at any time?
[20:59:08] <phajdan-jr> good question
[20:59:21] <ulm> WilliamH: I'm all with you there
[20:59:28] <phajdan-jr> I think historically council didn't step in that much in pretty obvious (arguably) QA "crisises"
[20:59:40] <ulm> I'd have been happier with the council approving qa members instead of the lead
[20:59:43] <jmbsvicetto> back, I had to relocate
[20:59:47] <phajdan-jr> and I think an explicit confirmation would address that by _requiring_ council action
[21:00:03] <dberkholz> the problem is that people have a really hard time taking any sort of negative action
[21:00:04] <dilfridge> phajdan-jr+
[21:00:10] <blueness> a pro-active confirmation is also a vote of confidence which is important message to the community
[21:00:11] <phajdan-jr> I'd prefer the QA lead to have enough autonomy to approve team members
[21:00:12] <dberkholz> a removal is seen as far more severe than a confirmation
[21:00:17] <phajdan-jr> +1
[21:00:20] <dilfridge> +1
[21:00:22] <dberkholz> so we wait way too long to do anything
[21:00:24] <phajdan-jr> to both blueness and dberkholz
[21:00:35] <blueness> dberkholz, +1
[21:00:54] <blueness> look at how messy it was to "remove" the previous QA situation
[21:00:59] <blueness> hurt feelings etc
[21:01:09] <floppym> It's like Google+ in here with all these +1's :P
[21:01:15] <WilliamH> blueness: it really wasn't, we just stepped in and did it.
[21:01:18] <Zero_Chaos> the council absolutely should not have authority to dictate individual team members for a team.
[21:01:22] <blueness> floppym++
[21:01:22] <Zero_Chaos> that's nonsense
[21:01:33] <blueness> Zero_Chaos, then QA is not a team
[21:02:05] <Zero_Chaos> blueness: team/project/herd/orgy I don't care what it's called. Council shouldn't have the authority to dictate team members, that's not their job.
[21:02:08] <blueness> it is something more akin to the council with over-arching powers
[21:02:26] <Zero_Chaos> blueness: totalitarian is the word you are looking for.
[21:02:29] <blueness> there is a distinction: a team eg hardened does not have overarching powers
[21:02:42] <dberkholz> Zero_Chaos: enabling formation of a cliquey in-group with too much authority and no external oversight is an entirely different problem
[21:02:56] <blueness> Zero_Chaos, that's a straw man argument, the point is that over-arching powers need legitimation by vote
[21:03:07] <blueness> people vote for the council which has overarching powers
[21:03:11] <blueness> this makes it legitimate
[21:03:20] <blueness> people do not vote for QA, and yet it has overarchin powers
[21:03:28] <Zero_Chaos> blueness: the lead already has to be approved. just like in government. did you elect the desk clerk at the dmv?
[21:03:28] <jmbsvicetto> dberkholz: I've added the project ml thread link to the bug
[21:03:29] <blueness> so how is its legitimacy entrenched
[21:03:31] <blueness> ?
[21:03:40] <dberkholz> thx jmbsvicetto
[21:03:42] <WilliamH> blueness: qa is accountable to the council though just like any of us are.
[21:03:50] <ulm> blueness: abuse of power was not a problem at all with qa
[21:03:58] <ulm> but rather inactivity
[21:04:12] <phajdan-jr> I remember some concerns about qa's actions from the past though
[21:04:17] <phajdan-jr> there were some conflicts
[21:04:17] <blueness> ulm, but legitimacy is because people will say, who is QA to make these decisions on my package
[21:04:26] <blueness> rich0, had a nice write-up on it
[21:04:48] <dberkholz> alright, we're not bringing up any new issues here but rehashing old discussions
[21:04:54] <blueness> eg if the council were not elected, people would say, who are we to make such over-arching decisions
[21:04:58] <dberkholz> i'm going to close the formal meeting but feel free to keep talking about this
[21:05:00] <blueness> dberkholz, indeed
[21:05:02] <WilliamH> blueness: that's what an appeal to the council takes care of.
[21:05:16] <blueness> okay guys nice meeting until next time!
