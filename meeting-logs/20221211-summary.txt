Summary of Gentoo council meeting 11 December 2022

Agenda
======

1. Roll call
2. Architecture status review
3. GLEP76 update
4. Open bugs with council participation
5. Open floor

Roll Call
=========

Present: ajak, dilfridge, gyakovlev, mattst88, mgorny, sam, ulm

Architecture status review
==========================
Council agrees that architecture testing is going well. No changes needed.

mgorny notes that our profiles for LoongArch are no longer experimental.

GLEP76 update
=============
robbat2 proposed changes to GLEP76's "name policy" [1]. In the previous
council meeting, the Council voted to send the topic back to the mailing list
for more discussion after various concerns were raised.

Some further discussion and concerns were raised, despite no replies to the
mailing list discussion since the previous council meeting. ajak gave multiple
reminders to reply to the mailing list thread with concerns in the time since
the last meeting.

Given no responses to the mailing list discussion, sam and ajak say they would
like to vote on what was proposed before. mgorny says he'd like to re-table
the issue until there's a new patch to vote on.

There's further discussion of whether "records" refers to "government
records". mgorny says that the GLEP author has had a month to send a new patch
with that amendment.

14:10 <@  dilfridge> | the author was against the clarification "government records"
14:10 <@        ulm> | dilfridge: that's not what I remember

Motion: Vote to approve GLEP76 changes, with the understanding that there may
        be further small changes soon
Accepted 5-1-1 (see below)

In writing the summary, mattst88 noticed that dilfridge's vote was given as

  * dilfridge yes with "government records", abstain otherwise

and realized that there was apparently a difference of opinion between robbat2
and CatSwarm in the November meeting as to whether "records" means "government
records":

  <@ulm> so if the intention is "government" then it should say so
  <@ulm> and if it's google/twitter/whatever then it should say so as well
  < CatSwarm> I never intentionally tried to, or considered that I had
              unintentionally implied government, so no.

and separately:

  <+robbat2> records search in the context of the GLEP means to me searching
             government records, not public records like (twitter.com/jesus)

so I (mattst88) am reconsidering dilfridge's vote to be an abstention (which
is what dilfridge assumed I had done to begin with because he didn't realize
gyakovlev had also abstained).

Accepted: 4-1-2

[1] https://gitweb.gentoo.org/data/glep.git/commit/?h=glep76&id=139198d2e8560f8dfb32c8f4c34a3e49d628b184

Open bugs with council involvement
==================================
- Bug 729062 "Services and Software which is critical for Gentoo should be developed/run in the Gentoo namespace"
  This was recently reassigned to council@ due to Whissi's retirement.

  Whissi assigned the bug to himself 2021-05-09 in order to remove it from
  future Council discussions until he followed up with jstein@.

  No updates.

- Bug 882643 "Approve econf --disable-static change retroactively for EAPI 8"
  Votes cast on bugzilla. Accepted 7-0.

- Bug 883715 "(new) Developers who wish to stay anonymous"
  No updates, but the bug is dependent on GLEP76 changes which were just
  approved.

Open floor
==========
arthurzam notes that there are missing Council meeting logs and summaries.

This work is licensed under the CC BY-SA 4.0 License.
