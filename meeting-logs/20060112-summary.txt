Present:
  Koon (Thierry Carrez)
  Swift (Sven Vermeulen)
  agriffis (Aron Griffis)
  seemant (Seemant Kulleen)
  solar (Ned Ludd)
  vapier (Mike Frysinger)

Absent: 
  azarah (Martin Schlemmer)
  Where abouts unknown for the last 30 days.

Attendance by non council members appeared to be rather low.

Agenda:

 * GLEP 45 - GLEP date format
 * Disallowing council members to act as proxies for other council members.
* Global gentoo goals for 2006


Outcome:

* GLEP 45:
As stated on the mailing lists minor changes to GLEP's and the GLEP 
process is best left in the hands of the GLEP editors. Everybody 
supported g2boojum's ability to decide. Otherwise we would of voted 
yes.

* Disallowing council members to act as proxies for other council members.
Approved.

* Global gentoo goals for 2006
This was mostly a brainstorming session covering such topics as should 
the council even be setting global goals, enterprise support, GRP, 
ebuild/profile/eclass and binpkg signing.

