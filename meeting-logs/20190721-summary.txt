Summary of Gentoo council meeting 21 July 2019

Agenda
======
1. Roll call
2. Constitute the new council
   - Decide on time of meetings. The previous council had its meetings
     on the 2nd Sunday of every month at 19:00 UTC
   - Vote for continuing last council's workflow considering sending
     call for agenda items (2 weeks in advance), sending the agenda
     (1 week in advance) and have the meeting focussed, i.e., have
     major discussions on -project ML prior to the meeting
   - Appoint chairmen for this term's meetings
3. GLEP 81 approval [1]
4. Unrestrict gentoo-dev mailing list [2]
5. Real name requirement [3]
6. Proctors policy [4]
7. Open bugs with council involvement
8. Open floor


Roll call
=========
Present: dilfridge, gyakovlev, patrick, slyfox, ulm, whissi, williamh

Constitute the new council
==========================
- The council will hold its meetings on the 2nd Sunday of every month
  at 19:00 UTC.
- There is consensus on continuing last council's workflow.
- Meeting chairs:
    Jul:      ulm
    Aug/Sep:  slyfox
    Oct:      ulm
    Nov/Dec:  dilfridge
    Jan/Feb:  williamh
    Mar/Apr:  gyakovlev
    May/Jun:  whissi

GLEP 81 approval
================
It was briefly discussed if re-enabling a user could lead to security
problems, and if installing users to an alternative ROOT is possible.

- Vote: Accept GLEP 81.
  Accepted unanimously.

Unrestrict gentoo-dev mailing list
==================================
- Vote: Remove posting restrictions from gentoo-dev mailing list.
  Accepted unanimously.

Real name requirement
=====================
The point was raised that we do not verify names, and therefore cannot
be sure if an identity is real.

- Vote: No changes to existing policy.
  Accepted with 4 yes votes, 2 no votes, and 1 abstention.

Proctors policy
===============
The decision making process of proctors was discussed, especially the
merits of a quick decision by two persons or a slower decision by the
whole team. No motion was brought forward.

Open bugs with council involvement
==================================
- Bug 637328 "GLEP 14 needs to be updated":
  No news.

- Bug 642072 "[Tracker] Copyright policy":
  Tracker bug, no action.

- Bug 662982 "[TRACKER] New default locations for the Gentoo
  repository, distfiles, and binary packages":
  Some progress, stages are using the new /var/db/repos location.

- Bug 677824 "Deferred decision: Forums (specifically OTW)":
  So far, no actionable result from mailing list discussion.
  Resolving as "needinfo" for now.

- Bug 687938 "QA lead approval 2019: soap edition":
  Vote from previous council term (6 yes, 1 missing vote). Closing.

Open floor
==========
- Ban evasion on mailing lists and its consequences were briefly
  discussed.

- Whissi proposed a change of meeting procedure: Agenda items must be
  submitted with specific motions, council will vote only on motions
  published before the meeting. No consensus was reached, and in any
  case the council would not vote during open floor.

- Recruitment process: Amynka suggested to drop quizzes and focus on
  the candidate's contributions and the interview instead. Also it was
  pointed out that (as of the meeting's date) the recruiters team has
  only one member. Amynka and zlogene eventually agreed to discuss
  this topic and possibly bring it up on mailing lists.


[1] https://archives.gentoo.org/gentoo-project/message/5d649766eebc4b8550555a66df8c700c
[2] https://archives.gentoo.org/gentoo-project/message/3883fd8a106a0655f412e7c770dfce4e
[3] https://archives.gentoo.org/gentoo-project/message/85de6190bd27693bed07744e04855911
[4] https://archives.gentoo.org/gentoo-project/message/854d484eca8664e7ee6678bf79d63976

This work is licensed under the CC BY-SA 4.0 License.
