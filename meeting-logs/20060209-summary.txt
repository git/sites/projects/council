Present:
  azarah (Martin Schlemmer)
  Koon (Thierry Carrez)
  Swift (Sven Vermeulen)
  agriffis (Aron Griffis)
  seemant (Seemant Kulleen)
  solar (Ned Ludd)
  vapier (Mike Frysinger)

Agenda:
 * GLEP 44 - Manifest2 format

Outcome:
 * Council members were generally in agreement that GLEP 44 is a good idea, but
   without genone present to answer questions, the council was unwilling to
   approve or deny it.  Postponed to next meeting, with the expectation that
   genone or a proxy will attend then.
