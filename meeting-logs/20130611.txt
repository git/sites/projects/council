<11.06.2013 19:02> -!- Betelgeuse changed the topic of #gentoo-council to: Meeting going on: http://thread.gmane.org/gmane.linux.gentoo.project/2495 | http://www.gentoo.org/proj/en/council/utctolocal.html?time=1900 | http://www.gentoo.org/proj/en/council/
<11.06.2013 19:02> <@Betelgeuse> Rollcall!
<11.06.2013 19:02> <@ulm> here
<11.06.2013 19:02>  * scarabeus up
<11.06.2013 19:02> <@grobian> here
<11.06.2013 19:02> <+dberkholz> hi
<11.06.2013 19:02>  * WilliamH is here
<11.06.2013 19:03>  * Chainsaw is present
<11.06.2013 19:03> <@Betelgeuse> good
<11.06.2013 19:03> <@Betelgeuse> anyone object to the summary I sent?
<11.06.2013 19:03> <@grobian> nope
<11.06.2013 19:04> <@Chainsaw> None here.
<11.06.2013 19:04> <@Betelgeuse> ulm: I will add your link
<11.06.2013 19:04> <@Betelgeuse> ok thanks
<11.06.2013 19:04> < scarabeus> nope it reflects what it should
<11.06.2013 19:04> <@Betelgeuse> First item
<11.06.2013 19:04> <@Betelgeuse> http://article.gmane.org/gmane.linux.gentoo.project/2487
<11.06.2013 19:04> < scarabeus> btw guys i have 2secs lag so dont get suprised with my shifted replies
<11.06.2013 19:04> <@Betelgeuse> rich0: around?
<11.06.2013 19:05> <@ulm> actually we have a policy for non-maintainer commits
<11.06.2013 19:05> <@ulm> http://devmanual.gentoo.org/ebuild-writing/ebuild-maintenance/index.html#touching-other-developers-ebuilds
<11.06.2013 19:05> < scarabeus> ulm: it was other herd member who added the patch to my knowledge no?
<11.06.2013 19:05> <+dberkholz> that's ignored issues, not disagreements
<11.06.2013 19:06> < Zero_Chaos> in the specifically complained about case it was a co-maintainer commit.
<11.06.2013 19:06> <@Chainsaw> They were both maintainers on the package in this case.
<11.06.2013 19:06> < WilliamH> It was a co-maintainer iirc that added thefiles.
<11.06.2013 19:06> < scarabeus> the problem is that too much people are in "kill it its systemd" mood", if you don't have to care about the bugs, there should not be problem if they add pink piggies
<11.06.2013 19:06> <+dberkholz> assuming that introducing new code doesn't create bugs elsewhere that are hard to diagnose
<11.06.2013 19:07> < scarabeus> yea but even that falls under the good old "you broke it, fix your shit" which we do with nm-commits and everything else
<11.06.2013 19:07> <@ulm> I'm strongly opposed against any special rules for systemd
<11.06.2013 19:07> < WilliamH> ulm: same here.
<11.06.2013 19:07> <+dberkholz> my philosophy is that maintainer rules, unless superceded by a higher authority
<11.06.2013 19:07> <@ulm> the current policies are good enough to cover such cases
<11.06.2013 19:08> <@Betelgeuse> dberkholz: But isn't that what the request is? Asking the higher authority.
<11.06.2013 19:08> <+dberkholz> it may be that we should actually have a lead maintainer concept vs a committee
<11.06.2013 19:08> < Zero_Chaos> the issue in this case is one maintainer didn't want systemd support the other one did. no policy covers this afaik aside from general conflict resolution which was ignored in favor of crying on -dev.
<11.06.2013 19:08> <@Betelgeuse> I can see the point that things like systemd could not never get added if any maintainer could block it
<11.06.2013 19:09>  * WilliamH tends to agree with Zero_Chaos  on this specific case.
<11.06.2013 19:10> < WilliamH> It wasn't a case where adding the file would break something.
<11.06.2013 19:10> <@Betelgeuse> Still I don't think policies necessary need adjusting
<11.06.2013 19:10> <@Betelgeuse> If you need something system wide with opposition then ask council on a case by case basis
<11.06.2013 19:10> <@Chainsaw> The existing rules make sense. No change required.
<11.06.2013 19:11> <@ulm> Chainsaw: +1
<11.06.2013 19:11> < WilliamH> +1
<11.06.2013 19:11> <@Chainsaw> And no, even *I* won't advocate for systemd-specific policy. Please give it a rest.
<11.06.2013 19:11> < scarabeus> i agree policies dont need to change
<11.06.2013 19:12> <@Betelgeuse> ok that seems to be the majority
<11.06.2013 19:12> <@grobian> Chainsaw: +1
<11.06.2013 19:12> <@Betelgeuse> moving on to the next item
<11.06.2013 19:12> <@Betelgeuse> http://article.gmane.org/gmane.linux.gentoo.project/2470
<11.06.2013 19:13> <@Betelgeuse> scarabeus: your thread
<11.06.2013 19:13> < scarabeus> okey guys
<11.06.2013 19:13> < scarabeus> i hope you guys red the mail and the thread
<11.06.2013 19:14> <+dberkholz> 5:1 not 3:1 =)
<11.06.2013 19:14> < Zero_Chaos> I specifically don't like "whenever the
<11.06.2013 19:14> < Zero_Chaos> project will be judged by our actions"
<11.06.2013 19:14> < Zero_Chaos> this is an interpersonal code of conduct not just a "lets try not to make gentoo look stupid" policy
<11.06.2013 19:15> <+dberkholz> scarabeus: can you describe the source of your material or is it all original?
<11.06.2013 19:15> <@Chainsaw> decission -> decision
<11.06.2013 19:15> <+dberkholz> scarabeus: i didn't take the time to read through all of your reference to see which was the "biggest" contrib
<11.06.2013 19:15> < scarabeus> dberkholz: at the bottom, ubuntu coc/suse coc and kde coc
<11.06.2013 19:15> <+dberkholz> vs which you did yourself
<11.06.2013 19:15> < scarabeus> possibly kde coc is greatest contrib
<11.06.2013 19:16> <+dberkholz> and really if we're gonna base on some other one, i might just take the whole thing wholesale instead of a mishmash
<11.06.2013 19:16> <@Betelgeuse> scarabeus: The thing I am missing is an update based on the thread
<11.06.2013 19:16> < scarabeus> it was updated in the link
<11.06.2013 19:16> < scarabeus> http://dev.gentooexperimental.org/~scarabeus/gentoo-coc.txt
<11.06.2013 19:17> <@ulm> scarabeus: section "examples of inappropriate behavior", could you clarify what is considered as "public accessible areas"? would gentoo mirrors count as such?
<11.06.2013 19:18> < scarabeus> oh yes, those do count
<11.06.2013 19:19> <@Betelgeuse> scarabeus: it still lists the language on council that people raised in the thread
<11.06.2013 19:19> <@ulm> then it's censorship on package content
<11.06.2013 19:19> <@Betelgeuse> scarabeus: So my point was if it still needs rounds on ml
<11.06.2013 19:19> < scarabeus> Betelgeuse: yes and i explained why i kept it there
<11.06.2013 19:19> <@Betelgeuse> scarabeus: true
<11.06.2013 19:20> < scarabeus> btw typos, i think devrel should be allowed to do minor modifications without council approval
<11.06.2013 19:21> < scarabeus> rewording for clarity/typos/spells/whateverifuckedupthere :-)
<11.06.2013 19:21> <@grobian> define minor ;)
<11.06.2013 19:21> < scarabeus> ^
<11.06.2013 19:21> <@grobian> clarity is vague
<11.06.2013 19:21> <@grobian> typos/spells are ok
<11.06.2013 19:21> <@Chainsaw> I would consider it more like a legal document, and that would require votes to update it.
<11.06.2013 19:21> <@grobian> what's the problem with getting a little ack on a small diff from council@g.o?
<11.06.2013 19:22> <+dberkholz> may seem a little weird but i don't like the term enforcers
<11.06.2013 19:22> <@Chainsaw> You cannot derive much meaning from a document that remains in flux.
<11.06.2013 19:22> <@Betelgeuse> scarabeus: I agree with rich though
<11.06.2013 19:22> <@Chainsaw> (And if you want to continue tweaking it that much, why did you submit it for approval?)
<11.06.2013 19:22> < scarabeus> Betelgeuse: i told there it should be on devrel  to pick what they want didn't I, so whatever you and markos decide :P
<11.06.2013 19:23> < scarabeus> grobian: okey thats a good point, i just didnt want one ' addition to take 30 days, but to be fair who cares it lacks some '
<11.06.2013 19:23> <+dberkholz> i also agree with rich on the non-conflict, and with roy on removing specific examples or clarifying that as "some examples" so it doesn't imply that's everything
<11.06.2013 19:24> < scarabeus> ok removing the non-conflict part
<11.06.2013 19:24> < scarabeus> also fixed the decisions
<11.06.2013 19:24> <+dberkholz> and i can come up with references that bad-to-good is more like 5:1 than 3:1 if you like =)
<11.06.2013 19:24> < scarabeus> ok refresh
<11.06.2013 19:24> <@Chainsaw> "including but not limited to" is the accepted language for non-exhaustive examples/lists.
<11.06.2013 19:25> < scarabeus> dberkholz: agree it can be even 5:1
<11.06.2013 19:26> < scarabeus> dberkholz: i will drop the sentence as whole or just adjust it?
<11.06.2013 19:26> < scarabeus> 3:1 is minimum as i think
<11.06.2013 19:26> <+dberkholz> where did you come up with that number?
<11.06.2013 19:26> < scarabeus> marketing stuff i got from uni, nifty book 4 euros :P
<11.06.2013 19:26> < scarabeus> scriptum for exam :P
<11.06.2013 19:27> < scarabeus> Chainsaw: the addition should be to topic?
<11.06.2013 19:27> <@Chainsaw> scarabeus: Or in a lead-in sentence.
<11.06.2013 19:28> <@Chainsaw> scarabeus: You could shorten the heading to bad behaviour, that'll make it easier to slot in.
<11.06.2013 19:29> < scarabeus> shorten?
<11.06.2013 19:29> < scarabeus> not get it
<11.06.2013 19:30> <+dberkholz> back in a few, family issue.
<11.06.2013 19:30> < scarabeus> dberkholz: dropped the line as whole, i was actually not wanting to add it there in the first draft, but others at suse convinced me it sounds good :-)
<11.06.2013 19:31> <@Betelgeuse> Ok I think we need to see what gets the majority 1) accept 2) reject 3) revisit later
<11.06.2013 19:31>  * WilliamH votes revisit later
<11.06.2013 19:31> <@grobian> 3)
<11.06.2013 19:31> <@ulm> 3
<11.06.2013 19:32> < scarabeus> you know you could've replied on the list
<11.06.2013 19:33> < scarabeus> i sent it quite in advance even to -council
<11.06.2013 19:33>  * grobian feels guilty
<11.06.2013 19:33> <@Chainsaw> Even if it makes me feel bad, 3.
<11.06.2013 19:34> <@Chainsaw> (If I'd have had an agenda earlier I would have looked at it)
<11.06.2013 19:34> <@Betelgeuse> yes, sorry about that
<11.06.2013 19:35> < scarabeus> Chainsaw: ok i kinda updated the topic for that section for further reference, cant think up better wording
<11.06.2013 19:35> <@Betelgeuse> The next item is wrapping up the term.
<11.06.2013 19:35> <@Betelgeuse> I think we can handle rich's request for the elections too
<11.06.2013 19:36> <@Chainsaw> Are we seconds from netsplit? I am experiencing horrendous lag...
<11.06.2013 19:37> <@grobian> Betelgeuse: ok
<11.06.2013 19:37> <@Betelgeuse> So how does everyone think the year went?
<11.06.2013 19:38> <@Chainsaw> It wasn't perfect, but I think we did rather well in voting on what was actionable and pushing back on what wasn't.
<11.06.2013 19:38> <@Chainsaw> (The latter is a definite change from the year before, and an important one.)
<11.06.2013 19:38> < WilliamH> I would agree, I think things have gone pretty well this year.
<11.06.2013 19:39> <@grobian> overall I'm happy
<11.06.2013 19:39> < scarabeus> could've been much worse :-)
<11.06.2013 19:39> <@Chainsaw> scarabeus: Oh, easily :)
<11.06.2013 19:39> < scarabeus> and we approved eapi! :P (as usual)
<11.06.2013 19:39> <@Chainsaw> I don't take credit for any EAPI work. That's all ulm.
<11.06.2013 19:40> <@Chainsaw> All I did was say yes a few times.
<11.06.2013 19:40> < WilliamH> Chainsaw: heh I know what you mean there.
<11.06.2013 19:40> < Hello71> when's the log going up?
<11.06.2013 19:40> <@Chainsaw> Hello71: For tonight's council meeting? When we're done!
<11.06.2013 19:41> < scarabeus> when the meeting ends as always? :-)
<11.06.2013 19:41> < Hello71> hehe
<11.06.2013 19:41> < Hello71> that would make sense
<11.06.2013 19:41> <@Betelgeuse> scarabeus: minus last time :)
<11.06.2013 19:41> <@ulm> Chainsaw: thanks, but it's not true that it's all me ;)
<11.06.2013 19:41>  * Chainsaw escorts Hello71 to the visitors area
<11.06.2013 19:41> <@ulm> I'm mainly collecting contributions of others
<11.06.2013 19:41> < scarabeus> ulm: yea but you are mostly knees deep in it so you deserve the credit
<11.06.2013 19:42> <@Chainsaw> ulm: I rely heavily on your summaries.
<11.06.2013 19:42> < scarabeus> we just say "yay" "nay"
<11.06.2013 19:42> < scarabeus> based on what Chainsaw said :P
<11.06.2013 19:42> < scarabeus> i even read the pms so i dont feel dumb tho
<11.06.2013 19:42> < scarabeus> but your summaries are great "don't try to sneak somethink evil next time now you know it"
<11.06.2013 19:42> < mgorny> scarabeus: did you feel dumb afterwards? :P
<11.06.2013 19:43> <@Betelgeuse> Any practical tips we could leave to the next council?
<11.06.2013 19:43> < scarabeus> no just numb and could not feel my tongue
<11.06.2013 19:44> <@grobian> Make sure you (council member) have enough time in your life to properly prepare ;)
<11.06.2013 19:44> < scarabeus> don't wait for the darn agenda and read up in advance ;-)
<11.06.2013 19:45> <@Betelgeuse> Something else that most councils wouldn't have said? :D
<11.06.2013 19:45> <@Betelgeuse> It seems like the format has become quite steady
<11.06.2013 19:46> < scarabeus> yeah use hardened, its the most coolest thing we have in gentoo right now, and we should promote it
<11.06.2013 19:46> < scarabeus> :-)
<11.06.2013 19:47> <@Betelgeuse> Do people plan on running again?
<11.06.2013 19:47> <@Chainsaw> I will, yes.
<11.06.2013 19:47>  * WilliamH would like to, yes
<11.06.2013 19:47> <@ulm> me too
<11.06.2013 19:48> < scarabeus> might, dunno about time
<11.06.2013 19:48> <@grobian> in doubt
<11.06.2013 19:48> <@grobian> I really lack the time
<11.06.2013 19:49> <@Betelgeuse> If I do, I should aim to chair more. Makes me pay more attention.
<11.06.2013 19:50> <@Betelgeuse> Hmm it's been fix years.
<11.06.2013 19:50> <@Betelgeuse> s/fix/six/
<11.06.2013 19:50> <@Betelgeuse> No wonder much of it blurs together
<11.06.2013 19:51> <@Betelgeuse> ulm: I committed the summary
<11.06.2013 19:52> <@Chainsaw> Betelgeuse: Twice as long as me.
<11.06.2013 19:52> <@Chainsaw> Betelgeuse: This is my third council year.
<11.06.2013 19:53> <@Betelgeuse> If people decide to not run, the elections people could probably use some help.
<11.06.2013 19:55> <@Betelgeuse> Let's open the floor the audience
<11.06.2013 19:55> < mgorny> finally!
<11.06.2013 19:55> <@Betelgeuse> You can let all your fustrations out now
<11.06.2013 19:55> < mgorny> while you're still here, i wanted to get a bit of your opinion on our little multilib effort
<11.06.2013 19:56> <+dberkholz> i'm considering it
<11.06.2013 19:56> < mgorny> we've switched whole xlibs to the new eclasses, a few soundlibs and i get quite frustrated with base-system
<11.06.2013 19:56> <+dberkholz> my goal is basically "maintain the gentoo philosophy" vs trying to do anything earth-shatting
<11.06.2013 19:56> <+dberkholz> shattering, too
<11.06.2013 19:56> < scarabeus> mgorny: whats with base?
<11.06.2013 19:56> < scarabeus> mgorny: no new eapi?
<11.06.2013 19:56> < mgorny> i feel like it's a team of people who intentionally ignore bugs they don't like
<11.06.2013 19:57> < mgorny> it's irritating to report a bug, ping, query and not get a word of reply for a few months
<11.06.2013 19:57> < mgorny> that's my frustration
<11.06.2013 19:57> < scarabeus> !herd base
<11.06.2013 19:57> < willikins> scarabeus: No such herd base
<11.06.2013 19:57> < scarabeus> !herd base-system
<11.06.2013 19:57> < willikins> scarabeus: (base-system) cardoe, chainsaw, flameeyes, hwoarang, radhermit, robbat2, ssuominen, vapier, williamh
<11.06.2013 19:57> -!- mode/#gentoo-council [+o hwoarang] by ChanServ
<11.06.2013 19:57> < scarabeus> thats quite few people
<11.06.2013 19:58> < Zero_Chaos> some of them in this room....
<11.06.2013 19:58> < mgorny> i think vapier is the person most appropriate for the particular packages
<11.06.2013 19:58> < mgorny> and he's the person frustrating me most, to be honest
<11.06.2013 19:59> <@grobian> so where's this going?
<11.06.2013 19:59> < mgorny> i've already had a bug with him which took him one year to reply
<11.06.2013 19:59> <@Chainsaw> grobian: Character assassination?
<11.06.2013 20:00> < mgorny> me? sorry, i focused on frustration :P
<11.06.2013 20:00> < mgorny> i'm asking for general tips/opinion
<11.06.2013 20:00> < floppym> It sounds like someone needs to poke the other base-system members to pick up the slack.
<11.06.2013 20:00> <@grobian> or find a way to progress
<11.06.2013 20:00> <@Chainsaw> Most of this is about toolchain, not base-system.
<11.06.2013 20:00> <@Chainsaw> And that set of developers is smaller.
<11.06.2013 20:00> <@grobian> because it's so easy to say "this works for me", but you don't know what else you break
<11.06.2013 20:01> < mgorny> well, we intend not to touch toolchain
<11.06.2013 20:01> <+dberkholz> test suites, test boxen
<11.06.2013 20:01> <@Chainsaw> mgorny: Generally, the moment you end up faced with vapier, you're well within the toolchain boundary.
<11.06.2013 20:01> < scarabeus> also in base-system we have quite prob with stabilisations, debian has newer pkgs than us sometimes :P
<11.06.2013 20:01> < mgorny> i mean, gcc, glibc are built for multilib already and those need not to be fixed
<11.06.2013 20:01> < mgorny> the packages in question were zlib and bzip2
<11.06.2013 20:02> <+dberkholz> i would buy lots of beer for anyone who set up pre-mirror CI, at least for stuff in a stage3
<11.06.2013 20:02> <@grobian> ok, but apart from that those packages aren't to be taken lightly
<11.06.2013 20:02> <@ulm> mgorny: xlibs should be well tested by now
<11.06.2013 20:02> <@ulm> so why don't we stabilise them, and then see about the rest
<11.06.2013 20:02> < WilliamH> scarabeus: afaik anyone can file a stabilization request, you just can't add the arch's.
<11.06.2013 20:02> < mgorny> ulm: some of them may be stable already
<11.06.2013 20:03> < mgorny> at least people asked me if it's fine to stabilize them and i said yes
<11.06.2013 20:03> < scarabeus> WilliamH: i had bug opened for 6 months like that -> motivation to fill more goes to drain
<11.06.2013 20:03> < mgorny> this is generally something like devrel/userrel issue
<11.06.2013 20:04> < mgorny> i can understand developers being angry, unwilling and so on with people
<11.06.2013 20:04> < mgorny> but people reporting bugs and not getting a single answer for one year is something making us look really bad
<11.06.2013 20:04> <@ulm> mgorny: hm, I see it more as a case where consensus on -dev ml should be reached
<11.06.2013 20:04> <@hwoarang> you can't just force people to reply can you?
<11.06.2013 20:04> <@ulm> and if that's not possible, a council decision
<11.06.2013 20:05> < TomWij> mgorny: Exactly, I'm planning to set up a project on that; but I'm kind of afraid that not enough people are going to chime in to reach the goal.
<11.06.2013 20:05>  * WilliamH agrees with hwoarang on that, you can't force people to reply to you...
<11.06.2013 20:05> <@grobian> mgorny: I'm affraid I've ignored some bugs for a long time too, they just disappear in the pile… unfortunately
<11.06.2013 20:05>  * WilliamH is guilty of the same too
<11.06.2013 20:05> < TomWij> The goal being dealing with the oldest bugs until the oldest bugs are less than "some acceptable amount of time" old.
<11.06.2013 20:06> < scarabeus> i have rule to at least reply to the bug when its filled
<11.06.2013 20:06> < scarabeus> so it should have first reply within a month
<11.06.2013 20:06> < scarabeus> and then god knows but at least i explain there what is needed if it has not patch
<11.06.2013 20:06> < mgorny> grobian: me too. but when i get a 'ping' on the bug, i try at least to tell that i can't do anything right now
<11.06.2013 20:07> < mgorny> ulm: do you mean consensus on multilib or on bugs without reply? because this got a quite sidetracked
<11.06.2013 20:07> <@ulm> mgorny: multilib
<11.06.2013 20:07> <@grobian> mgorny: all I try to say, I get those too, and I'm not always in the position to open bugzie and reply (work/firewalls and all that crap)
<11.06.2013 20:08> <@grobian> even though I want
<11.06.2013 20:08> <@grobian> when I come home, I forgot
<11.06.2013 20:08> < mgorny> ulm: i'm not sure if the ml is the place to decide what to do with base-system packages but it's some way
<11.06.2013 20:08> <@grobian> not that that is an excuse or something
<11.06.2013 20:08> < mgorny> i didn't really want to set deadlines like 'one month or we are committing it'
<11.06.2013 20:09> < TomWij> mgorny: But what if that's a bug of 2005, it wouldn't be acceptable to just ditch that away with "no time now" or "later" or "..."; that would've been a bug that has been around for 8 years or so, you can't call that "no time".
<11.06.2013 20:09> < mgorny> grobian: would be probably useful if bugzie had some kind of default 'unreplied bugs' search
<11.06.2013 20:09> < TomWij> Relevant bug #88596
<11.06.2013 20:09> < willikins> TomWij: https://bugs.gentoo.org/88596 "sys-devel/libtool stores gcc-path internally.  sometimes a bad thing due to gcc update"; Gentoo Linux, Core system; IN_P; spider:base-system
<11.06.2013 20:09> < mgorny> TomWij: ^ that's probably first task for you
<11.06.2013 20:10> < TomWij> mgorny: Yeah, I'm planning to categorize old bugs; and links to useful searches like that are probably nice too, thanks for the idea.
<11.06.2013 20:10> < TomWij> One tracker I for instance started is bug #472746
<11.06.2013 20:10> < willikins> TomWij: https://bugs.gentoo.org/472746 "[TRACKER] Old interesting conceptual, abstract and unimplemented ideas and feature requests."; Gentoo Linux, Unspecified; CONF; tomwij:tomwij
<11.06.2013 20:11> <@Betelgeuse> I am heading out to bed for the day.
<11.06.2013 20:11> <@grobian> same here
<11.06.2013 20:11> <@Betelgeuse> I am calling the meeting officially over. Thanks to everyone!
