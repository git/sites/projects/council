Summary of Gentoo council meeting 13 November 2012


Roll Call
=========
betelgeuse
Chainsaw
rich0 (proxy for dberkholz)
graaff (proxy for ulm)
grobian
scarabeus
WilliamH


Handling separate /usr support
==============================
WilliamH requested approval for two methods to support separate /usr
systems[2].  The discussion is closely related to recent opinons on udev, such
as e.g. [1], because the main reason to force a system without separate /usr
during boot is to allow newer versions of udev to be used.
The originally announced item of discussing the removal of gen_usr_ldscript
has been retracted[4].
- approve/disapprove plan (forcing everyone to take action, and
  implement one of the two "supported" solutions)

WilliamH requests a council vote to allow migrating everyone after bugs
[5,6,7] are resolved.  He proposes a news item to announce this that allows to
assume after a given period of time that everyone who is using split /usr is
using a method to mount /usr before boot.  The focus is purely on this topic.

rich0 prefers to move on until suport for separate /usr becomes a
barrier, and handle things from there.  This allows for alternative
solutions to be developed and put forward.  He favours waiting somewhat
to see developments of the udev fork.

Chainsaw is a strong proponent for waiting a month and see how the new
udev fork develops itself.  If within a month no solution is provided by
the udev fork, things need to be moved forward in WilliamH's proposed
way.

scarabeus approves the plan.

betelgeuse likes to ensure users won't be caught off guard, but has no
preference for any direction taken in particular.

graaff's main concern is how the problem is tied to udev, or not.  A fork of
udev may not change the situation regarding separate /usr, hence delaying a
decision now is not sensical.  Opt-in system for people to ensure they can
boot is pre-requisite.  If this cannot be ensured, we have to wait.

grobian disapproves the plan, since there will be systems that cannot easily
be changed to ensure /usr being mounted at boot, and it is no good to expel
users of (security) updates just because of that.  With the use of a special
profile (masks/unmasks, variables and/or use-flags), users that want to move
on, can opt-in to getting packages that require non separate /usr.



Policy on "<" versioned dependencies
====================================
chithahn requested the council to clear up confusion around "<" versioned
dependencies[3].  This issue seems to combine:
1) notorious behaviour from the usual suspects
2) QA policies whether or not they are properly documented/advertised
3) the technical problem of "<" dependencies causing downgrades

The council sees no rule that makes it illegal to use < dependencies, but
strongly discourages their use.  It must be noted that for some
packages, a downgrade is very undesirable.  This has triggered package
removals in the past.  However, the council requests the teams responsible for
that removal to act reasonably and in good cooperation with the maintainers of
the packages in question.


Open bugs with council involvement
==================================
Bug 383467 "Council webpage lacks results for 2010 and 2011 elections"
- ulm has done the work here, waiting for a confirmation that we can really
  close the bug
Bug 438338 "Please update devmanual with EAPI5 info"
- no progress and/or actions planned for this


Open Floor
==========
No issues were brought up to the council.


Next meeting date
=================
11 December 2012, 20:00 UTC


[1] https://lkml.org/lkml/2012/10/2/303
[2] http://thread.gmane.org/gmane.linux.gentoo.project/2208
[3] http://thread.gmane.org/gmane.linux.gentoo.project/2213
[4] http://article.gmane.org/gmane.linux.gentoo.project/2235
[5] https://bugs.gentoo.org/411627
[6] https://bugs.gentoo.org/435756
[7] https://bugs.gentoo.org/441004
