-:- Topic (#gentoo-council): changed by vapier: Meeting at 1900 UTC (1400 EST) || anybody seen azarah in the last month?
06:54PM <SwifT> where am I?
06:54PM <SwifT> :p
06:55PM <vapier> batman
<solar> anybody want to step up and chair this or open session today?
06:57PM <vapier> i would but like i said, i'm prob gonna have to pop out early
06:59PM <vapier> welp, by my clock, it's about that time eh
07:00PM <vapier> i'm pretty sure az isnt going to show up
-:- tove [n=tove@p54A60D69.dip0.t-ipconnect.de] has joined #gentoo-council
07:00PM <vapier> Koon / seemant / solar / seemant / vapier
07:00PM <vapier> who is awake ?
<solar> check
07:01PM <SwifT> mate
07:01PM <seemant> I am awake
07:02PM <vapier> i'll assume Koon will wake shortly
-:- kloeri_ [n=kloeri@gentoo/developer/kloeri] has joined #gentoo-council
<solar> * GLEP 45 - GLEP date format <-- ok this one. This is not even for us to decide on is it? The glep editors got that one covered right?
07:02PM <vapier> yes, g2boojum is for it
07:02PM * SwifT/#gentoo-council doesn't mind at all
07:02PM <vapier> and i'm pretty sure we'd all vote yes
-:- NetSplit: irc.freenode.net split from zelazny.freenode.net [07:02pm]
-:- BitchX: Press ^F to see who left ^E to change to [irc.freenode.net]
07:02PM <seemant> it's not for us to decide, I should think
07:02PM <vapier> stupid freenode
-:- [Users(#gentoo-council:10)] 
[ kloeri_   ] [ tove      ] [@seemant   ] [@vapier    ] [ kallamej  ] 
[vg2boojum  ] [ FuzzyRay  ] [@SwifT     ] [ spb       ] [@solar     ] 
-:- Topic (#gentoo-council): Meeting at 1900 UTC (1400 EST) || anybody seen azarah in the last month?
-:- Topic (#gentoo-council): set by vapier at Thu Jan 12 18:54:06 2006
07:02PM <SwifT> regarding chairing, sorry, just saw the e-mail. Perhaps next meeting
<solar> who did we lose on that split?
07:03PM <seemant> I support g2boojum's ability to decide that :)
07:03PM <SwifT> Koon
-:- Koon [n=koon@gentoo/developer/Koon] has joined #gentoo-council
-:- mode/#gentoo-council [+o Koon] by ChanServ
07:03PM <SwifT> here he is
<solar> ok next item is.
<solar> * disallow multiple votes per person (from ciaranm)
<solar>  http://marc.theaimsgroup.com/?t=113467833000002&r=1&w=2
07:04PM <vapier> WFM
07:04PM <SwifT> wfm?
07:04PM <seemant> the reasoning in that email is sound
07:04PM <Koon> yes too
07:04PM <seemant> so yes, wfm2
07:04PM <SwifT> isn't it Windows Meta Format or something?
07:04PM <seemant> SwifT: works for me
07:04PM <vapier> emerge wtf && wtf wfm
07:04PM <SwifT> oh, that
07:04PM <SwifT> yes, wfmaw
07:04PM <vapier> pfft that isnt real
07:05PM <vapier> you're just making s**t up
07:05PM <Koon> brb phone
07:05PM <seemant> have we all voted for that?
07:05PM <seemant> (or a majority anyway)
07:05PM <vapier> solar hasnt
<solar> I dont see the point personally and what brought it up was I'm guessing me trying to cover for az last month. 
<solar> but if the majority want to put into effect thats ok.
<solar> so yes
07:06PM <vapier> you're so accommodating
07:06PM !alindeman:*! Hi all .. Regional server looks to be having some trouble; we're working to resolve it now
<solar> next item is
<solar> * global gentoo goals for 2006
07:06PM <vapier> whee
07:06PM <vapier> that thread went to garbage fast eh
<solar> that we talked about a short bit in mail and said that global goals is not something that the council should decide. 
<solar> anybody have any input towards that subject?
07:08PM <vapier> i dont think that's it
07:08PM <seemant> so my followup question is: who's role is that to play?
<solar> explain please
07:09PM !lilo:*! Problem with a temporary main rotation server; however, we'd removed it from the main rotation as soon as we could manage, and about 800 users were affected
07:09PM <vapier> we cant just defer it, if we do we're saying "what Gentoo is now is good enough"
07:10PM <SwifT> no, but some people would like to see us 7 give the global direction where Gentoo is heading at
07:10PM <SwifT> which, frankly, will probably upset at least 7 other people :p
07:10PM <SwifT> however, if the council could work on keeping track of Gentoo's shortcomings and possible interesting areas, we can give a boost to new proposals
07:10PM <SwifT> well, not really "council" job, but someone has to :)
07:11PM <vapier> that seems feasible
07:11PM <SwifT> previously, the gentoo managers did that a bit
07:11PM <seemant> honestly, that seems more like a gentoo council job than deciding date formats
07:11PM <vapier> picking out the top "must have" goals and tracking them ?
07:11PM <SwifT> =)
07:11PM <SwifT> no, not "must have", a bit more abstract
07:11PM <seemant> just my opinion, of course, but shouldn't we focus on high level stuff rather than low level stuff?
07:11PM <seemant> as a council I mean
07:12PM <SwifT> for instance, if lots of people want Gentoo to focus more to enterprises, the council can attempt to get the enterprise gentoo project up and running
07:12PM <SwifT> without saying what we (the council) would like to see happen
07:12PM <SwifT> no?
07:14PM <seemant> I don't know
07:14PM <Koon> back
07:14PM <seemant> I'm honestly unclear on this
07:14PM <SwifT> or, if people are too upset about lacking QA, we might want to make an analysis of that and start discussions about it
07:15PM <vapier> theres the conflict of some devs disliking the lack of cohesiveness, singular purpose, while others are specifically looking for that
07:15PM <SwifT> yet, saying that the gentoo web site is too slow is purely infrastructure project
07:15PM <SwifT> true, but that doesn't mean we need to stand on opposite sides ourselves
07:15PM <Koon> I think we can elect the top best cross-project ideas and hope (pray) that some team will form and do it... but not much more
07:16PM <Koon> we lack people to do things, not people to have ideas
07:16PM <Koon> see the enterprise stuff
<solar> enterprise itself is tricky. Some have strong feelings that any enterprise should not contain bins. where I would think enterprise is the exact place we should be providing binpkgs
07:16PM <SwifT> I'm in the middle, enterprise should inform the users how to create their own, coherent binplg set :)
07:17PM <Koon> enterprise = freezed tree + reference bins
07:17PM <SwifT> s/plg/pkg/
07:17PM <vapier> eh, enterprise/GRP could be unified
07:17PM <vapier> have some dedicated autobuild boxes ala-Debian and keep a tree of current stable binpkgs
07:18PM <SwifT> which means freezing USE, C{,XX}FLAGS, make.profile, /etc/portage
<solar> I'm attempting todo that now for x86
07:18PM <vapier> there's no issue of freezing
07:18PM <vapier> expand/automate GRP
07:18PM <seemant> wait wait wait
07:18PM <SwifT> sleep(10)
07:18PM <Koon> vapier: once again, the best solution will probably be the one that a team forms around, not the one some other group decides
07:18PM <seemant> are we as a council discussing enterprise as a future direction for 2006?
07:19PM <seemant> or was the above just an example of something or the other?
07:19PM <Koon> an example, I'd say :)
07:19PM <SwifT> more of an example how we don't lack ideas
-:- fox2mike [n=fox2mike@gentoo/developer/fox2mike] has joined #gentoo-council
07:19PM <vapier> i'm pretty sure if we produce such a list and enterprise was not on it, that'd be quite a failing on our part
-:- MetalGOD [n=DevNull@gentoo/developer/MetalGOD] has joined #gentoo-council
07:19PM <vapier> it'd basically be us saying "Gentoo is not for use in corporations, toss off"
07:20PM <SwifT> I don't think we'll be giving a list right now
07:20PM <vapier> we're brainstorming now
07:20PM <vapier> and i'm simply saying that enterprise/binpkg is a no-brainer imho
-:- nattfodd [n=nattfodd@gentoo/developer/nattfodd] has joined #gentoo-council
07:20PM <Koon> vapier: it just needs some people to work on it
<solar> for the sake of anybody reading and willing to test here are my repos. ftp://tinderbox.x86.dev.gentoo.org/default-linux/x86/2005.1/All/Packages
          ftp://tinderbox.x86.dev.gentoo.org/hardened/x86/Packages
07:21PM <Koon> that we cannot force in any way
07:21PM <vapier> i know
07:21PM <Koon> but we can say "here are what we think would be good directions for Gentoo, if one of them interests you, pick it up"
07:22PM <vapier> as i'm sure solar knows, i tend to be scatter brained in terms of what i'm working on at any one moment
07:22PM <SwifT> I'm kinda wondering why, if there would be a good goal for gentoo, it wouldn't be part of a single project... we already have a good separation of duties for all non-pkg
          development stuff
07:22PM <vapier> having anyone dictate a specific project would make me angry
07:23PM <Koon> maybe we should just talk about cross-project goals, not those requiring a whole new project team
-:- agriffis [n=agriffis@gentoo/developer/agriffis] has joined #gentoo-council
-:- mode/#gentoo-council [+o agriffis] by ChanServ
07:24PM <vapier> chatting about global gentoo direction atm agriffis
07:24PM <Koon> enterprise is more a project thing than a cross-project thing, so not sure it's our role to say "hey guys, here is a good one, now would you be so kind to form a team"
07:24PM <agriffis> vapier: ok, thanks.  somebody email me the log so I can catch up?
07:24PM <SwifT> isn't coaching people with good ideas on who to contact for what and how a good approach?
07:24PM <Koon> but it might be our role to say, hey, portage and enterprise teams, please play nice
07:25PM <SwifT> yeah, don't spank each other unless they like it
07:25PM <Koon> anyway, we'll lack time at this meeting to define the goal list
07:26PM <vapier> i think of it as us deciding whats most important in terms of greatest benefit to long term Gentoo
07:26PM <Koon> was there anything else to discuss, 'coz this one can take a long time...
07:26PM <agriffis> I understand that GLEP 45 and Ciaran's no-multiple-vote proposal have already been discussed.  Regarding the first one, it's fine with me.  Regarding the second, I wish I
          hadn't missed the discussion, but the otherwise I don't think it's a bad idea anyway, so I'm fine with it.
07:26PM <vapier> this was last item, but it was more of a brain storm session
07:26PM <Koon> ok
07:26PM <SwifT> or attempt to have new stuff well prepared for release... instead of just committing, making sure a press release is made with screenshots and articles, a nice news item, good,
          solid documentation, ...
<solar> that would be nice swift.
07:27PM <Koon> vapier: we should also track progress in portage signing, since last month
07:27PM <SwifT> that's theoretical :)
<solar> I still like it
07:27PM * SwifT/#gentoo-council for instance is really hoping axxo's java repo hits portage with a good release information, updated documentation, ...
07:27PM <SwifT> :)
07:28PM <SwifT> same with new features that gentoo/hardened supports
07:28PM <vapier> well the manifest stuff is in portage now, so next step is handling of keys
<solar> hardened is and probably will remain in maintenance mode. My goal is of course to see more hardened by default for gentoo
07:29PM <vapier> hardened i see as being a slightly tweaked flavor
07:29PM <vapier> that if all the other pieces fall into place, hardened is an extra pass
07:30PM <SwifT> or what about a knowledge base for gentoo/linux? A place where errors are stored, the circumstances when that error will occur and how to resolve it together with information as
          to why it happens...
07:30PM <Koon> vapier: should we follow robbat2 solution or the old simple "keychain-stored-in-portage, validated once using master key, then at each emerge sync" solution ?
07:30PM * SwifT/#gentoo-council stops brainstorming
07:30PM <vapier> SwifT: that sounds like hopping onto irc and pasting an error into #gentoo :)
07:31PM <SwifT> =)
<solar> that is what wikis are for. But you the formep GDP lead did not want those if I recall
07:31PM <SwifT> unable to mount root fs, probably the most frequent question on the forums
07:31PM <agriffis> Koon: is the robbat2 solution published somewhere?
07:31PM <SwifT> true, I don't like any  documentation way where no qa is involved
07:31PM <SwifT> wikipedia is great because *many* eyes are watching
07:31PM <Koon> agriffis: gentoo-dev archive I suppose...
07:31PM <SwifT> gentoo-wiki fails there for most articles, for instance
07:31PM <Koon> agriffis: hm. maybe was posted to -core
<solar> I think there is a problem with robbat2's solution. portage devs seem to want it to go another direction. 
<solar> sadly none of them are here to speak up
07:32PM <agriffis> Koon: ok, I'll look for it.  Generally I'm impressed by anything that robbat2 suggests regarding gnupg, signing, etc.  I'd be hesitant to disregard it without careful
          consideration.
07:32PM <Koon> agriffis: history on the subject is : we had a painful discussion on gentoo-dev about 18 months ago and the solution most agreed on was the "simple but doable" one
07:33PM !lilo:*! orphaned group contact for Novell, Inc....that group registration has been inactivated
07:33PM <agriffis> Koon: ok, I'll have to look it up, I guess.  I'm sure I read it then, but it's slipped away at this point.
07:33PM <Koon> then robbat2 said he would handle it and designed a nice solution... but apparently not taht easy to implement
07:33PM <Koon> especially when its author disappears
<solar> I trust robbat2 also, but the direction he wants to go in requires devs getting together for key signing parties to form big chains of trust.
07:34PM <vapier> solar: i dont see how it can be done any other way
<solar> that is clearly going to be a problem when we have a dev tucked away in some corner of the world
07:34PM <Koon> I prefer the "simple but now" solution to the "unbreakable but tomorrow" one
07:34PM <vapier> LWE and such help a lot with that
07:35PM !lilo:*! Okay, deactivated if you prefer. Sheesh. Grammar wonks. *grin*
07:35PM <SwifT> or, "simple now and unbreakable tomorrow"?
07:35PM <agriffis> well, clearly whatever is implemented, we don't want to present it as something it isn't.
-:- mpagano [n=mike@70.105.167.111] has joined #gentoo-council
<solar> anybody ever met azarah or me in person?
07:36PM <vapier> i'll fly down for butt sex with you solar
07:36PM <SwifT> not sure I want that ;p
07:36PM <agriffis> so if we go with a simple solution with flaws, we need to present it as that, and our reasons (meaning gentoo's not the council's) for doing so.
<solar> I've say it should be simple to begin with and made stronger and better over time
07:36PM <SwifT> j/k
07:36PM <vapier> i'm gonna try to fly to fl this year, could make a stop over :p
<solar> hush you
07:37PM <Koon> I would just do a flat keychain for starters
07:37PM <Koon> signed by a master key, verified by release media or public www servers
07:37PM <vapier> well the level of trust would be up to the user
07:37PM <Koon> always time to do chain of trust things to replace that
07:37PM <vapier> same as with gpg, how did you verify the key/person
07:39PM <Koon> the chain of trust thing adds identification to the process, that's not very useful... people trust "solar" more than "Ned Ludd"
07:39PM <agriffis> What about a hybrid solution, where the level of trust in a key is reported somehow to the user on request?  (i.e. this package has a chain of trust from the master key vs.
          this package has no chain of trust, or however robbat2's solution works)
07:39PM <Koon> we just need authentication
07:39PM <agriffis> perhaps I'm just demonstrating my lack of knowledge on the topic.
07:39PM <vapier> agriffis: gpg already tracks that
07:40PM <agriffis> vapier: good, but it needs to be wrapped in a portage interface so emerge can report easily
07:41PM <vapier> i gotta jet, time for doctors appt
<solar> cya vapier 
07:42PM <agriffis> btw, since I arrived late... who is chairing?
07:42PM <seemant> solar is chair
<solar> I am?
07:43PM <agriffis> ha, strike that from the log, quick!
07:43PM <SwifT> you are pointing out the agenda subjects :)
<solar> sorry I just wanted to get this thing going today.
07:43PM <SwifT> :)
<solar> anyway I guess the formal meeting was over when agriffis added his votes in
<solar> everything else remains brainstorming?
07:44PM <Koon> yes
07:44PM <Koon> we should require proper glepping of robbat2's solution
<solar> ok then back to portage signing. 
<solar> another blocker I see is the whole eclass+profiles 
07:45PM <Koon> I thought that was taken care of.
<solar> repoman has no support for those and probably wont for some time. But it's clear we will need a wrapper around commiting to those
-lilo(i=levin@freenode/staff/pdpc.levin)- [Server Notice] Hi all. Just a reminder: we're aiming to restart the server you're on, zelazny.freenode.net, this weekend. Please help us get it done
                                          by disconnecting and reconnecting to chat.freenode.net at your first opportunity. Thanks in advance for your help, and thank you for using freenode!
<solar> I'm not away that was taken care of by any tool
<solar> what have you heard on the subject which makes you think that?
07:46PM <Koon> maybe discussion on new eclasses a few months ago
07:46PM <Koon> I don't remember
07:47PM <agriffis> another thing I'd like to know about is interaction between signing and binary packages.  just wondering how/if that would work.
07:48PM <SwifT> makes me think about disec
<solar> we are going to have to come up with a detached method for signatures
<solar> disec was the in ELF signatures?
07:48PM <SwifT> yup
<solar> if so that wont work. think shell scripts
<solar> that entire idea was flawed. We picked it apart for two days in hardned
07:49PM <SwifT> ah
<solar> brb
07:49PM <Koon> binary packages are tarballs... so I guess they could include signature info
07:50PM <Koon> but that definitely needs some more work in design
07:50PM <Koon> hence the "show me your GLEP" thing
07:51PM * SwifT/#gentoo-council needs to go, catch my train 
07:51PM <SwifT> sorry folks
07:52PM <Koon> hm. we should definitely brainstorm some more
07:52PM <Koon> but no need to do it in-session
07:55PM <Koon> looks like the meeting is dead -- solar if you have the log you can cvs it... that will earn you your chairman turn :)
<solar> I do not have a log
07:56PM <Koon> me neither :)
<solar> however my scrollback buffer does go back till last meeting
<solar> I'll do it the hard copy+paste way
07:57PM <tove> so why not start a tree signing project to gather all problems, questions, solutions, ideas. taking the discussion away from -dev or -core for some time
