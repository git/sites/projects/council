Summary of Gentoo council meeting 11 November 2018

Agenda
======

1. Roll call
2. Open bugs with council involvement
   - bug #637328: GLEP 14 needs to be updated
   - bug #642072: [Tracker] Copyright policy
   - bug #670702: sys-apps/util-linux-2.33: Header does not comply with GLEP 76
3. Open floor

Roll call
=========

7 attendees: dilfridge, K_F, leio, slyfox, ulm, Whissi, WilliamH

Open bugs with council involvement
==================================

- bug #637328: GLEP 14 needs to be updated

  No progress from security@ this month.

- bug #642072: [Tracker] Copyright policy

  FYI. A tracker with work to do.

- bug #670702: sys-apps/util-linux-2.33: Header does not comply with GLEP 76

  The majority of the council agreed that the header of
  sys-apps/util-linux-2.33.ebuild (commit c3517916d940cebd8ae58994a6c799ec8e34fcc3)
  violates GLEP 76. However, it was decided to postpone any council action
  until next meeting.

References:
    https://wiki.gentoo.org/wiki/Project:Council#Open_bugs_with_Council_participation
    https://bugs.gentoo.org/637328
    https://bugs.gentoo.org/642072
    https://bugs.gentoo.org/670702

Open floor
==========

No items were raised during the open floor
