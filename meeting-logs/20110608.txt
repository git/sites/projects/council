19:00 <@wired> here we go :)
19:01 <@jmbsvicetto> roll call
19:01 <@jmbsvicetto> here
19:01 <@Chainsaw> Present.
19:02 <@wired> here
19:02 <@Betelgeuse> here
19:02 <@scarabeus> well we can say here
19:03 <@jmbsvicetto> bonsaikitten / ferringb ^^
19:03 <@jmbsvicetto> in the mean time, the agenda is on topic, but here is it again: http://archives.gentoo.org/gentoo-project/msg_0a7935a1097ba0aa41c3370a20679f9a.xml
19:03 <@bonsaikitten> present and arming bears
19:03 -!- jmbsvicetto changed the topic of #gentoo-council to: Next meeting: *now* | http://www.gentoo.org/proj/en/council/utctolocal.html?time=1900 | http://www.gentoo.org/proj/en/council/ | agenda - http://archives.gentoo.org/gentoo-project/msg_0a7935a1097ba0aa41c3370a20679f9a.xml
19:04 <@Chainsaw> Ah yes, the Changelog policy.
19:04 <@Chainsaw> Which I believe is working as intended.
19:04 <@bonsaikitten> well, it might be a bit too strict as a response to the constant lawyering
19:04 <@wired> more like the changelog massacre
19:05 <@jmbsvicetto> even thought he issue started before, I forgot to mention Fabian and http://archives.gentoo.org/gentoo-dev/msg_2ff02d6910d797045af3659fb21c712f.xml
19:05 <@Chainsaw> bonsaikitten: If people show malicious creativity, the ruleset will be firmed up in response.
19:05 <@bonsaikitten> Chainsaw: I would apply malicious creativity to their metacarpals
19:05 <@jmbsvicetto> For my part, I'd suggest we have 2 things to talk about here
19:06 <@jmbsvicetto> 1. the result of the policy we applied
19:06 <@jmbsvicetto> 2. the proposal to review it
19:06 <@jmbsvicetto> give me a minute while I try to phone Brian
19:06 <@Chainsaw> Okay.
19:08 <@jmbsvicetto> no answer, so let's move on
19:08 <@jmbsvicetto> Does anyone want to talk about 1 or just focus on 2?
19:08 <@wired> imo the policy itself is not the issue here. sure, it's a bit too strict, but thats easily fixed. my primary concern is the whole attitude surrounding this issue.
19:08 <@jmbsvicetto> I have a few words about 1, myself
19:08 <@Betelgeuse> At least 1 let's you find out things.
19:09 <@Chainsaw> jmbsvicetto: The result is that two developers have ended up in hot water.
19:09 <@jmbsvicetto> Chainsaw: In my view there's a bit more about 1, that I want to mention
19:09 <@Chainsaw> jmbsvicetto: I'm listening.
19:10 <@Betelgeuse> My previous line was probably ambiguous. I mean the policy is strict but it does show how the problematic people react to policies in general.
19:10 <@jmbsvicetto> First I want to thank both the QA lead and deputy lead for addressing the policy and enforcing inside QA
19:10 <@jmbsvicetto> Then I also want to thank DevRel for their resolution for the bug
19:11 <@Betelgeuse> jmbsvicetto: For the sake of the log let's say our resolution :)
19:12 <@Chainsaw> Betelgeuse: Removal of CVS commit privileges I seem to recall.
19:12 <@jmbsvicetto> Finally I'm very sad to see the length and extreme some of our developers are ready to go to boycot and refuse to follow a simple rule
19:12 <@Chainsaw> jmbsvicetto: Which vindicates the decision to go with the firm & unambiguous policy.
19:12 <@Betelgeuse> Chainsaw: I was referring to me and jmbsvicetto being part of DevRel.
19:12 <@jmbsvicetto> Chainsaw: DevRel sustained QA lead decision to suspend commit privileges
19:12 <@jmbsvicetto> Betelgeuse: and you're right :)
19:13 <@Betelgeuse> jmbsvicetto: sustain?
19:13 <@Betelgeuse> jmbsvicetto: I wrote the resolution to not take any direct stance on the suspension.
19:13 <@jmbsvicetto> Betelgeuse: the devrel resolution agreed with the commit privilege suspension
19:14 <@jmbsvicetto> Betelgeuse: that's what I mean. I'm sorry if I gave it any other meaning
19:14 <@bonsaikitten> so, how do we fix this situation?
19:14 <@Betelgeuse> jmbsvicetto: I was supposed to say that enough disciplinary action has already been taken.
19:14 <@bonsaikitten> people seem to disagree with any rules ;)
19:15 <@jmbsvicetto> so by my part moving on to 2
19:15 <@Betelgeuse> s/I/It/
19:15 <@jmbsvicetto> bonsaikitten / Chainsaw: I think we need a clear rule, but I also agree we should add 2 obvious exceptions: removing a package from the tree (it seems some don't consider this to be clear) and typo / comments fix
19:16 <@Chainsaw> jmbsvicetto: No, I strongly disagree.
19:16 <@Chainsaw> jmbsvicetto: Common sense & leniency is applied in *reading* of policies, not in *writing*.
19:16 <@bonsaikitten> in a perfect world we wouldn't need the rules as people would just agree on common sense
19:17 <@Chainsaw> jmbsvicetto: We have existing commit review practices that solve all this by concensus.
19:17 <@jmbsvicetto> Chainsaw: can you please elaborate further? (I'm trying to determine if my comment wasn't clear or gave any wrong impressions)
19:18 <@Chainsaw> jmbsvicetto: gentoo-dev@ <dev_a> Hey Chainsaw, you didn't update the Changelog for your commit X on foo-bar/baz-3.21 <Chainsaw> I didn't feel it was necessary, because blah blah blah.
19:18 <@Chainsaw> jmbsvicetto: Two possible outcomes. (1) <dev_a> Oh, okay then. (2) <dev_a> I disagree, and I would like to hear what others have to say on this.
19:18 <@jmbsvicetto> Chainsaw: ok, let me get back to 1 because I missed on crucial point: no matter how much one dislikes an approved policy, one has to respect it even while working on reviewing or removing it
19:18 <@Chainsaw> jmbsvicetto: The leniency, the common sense, the view of the group as a whole... we have all this.
19:19 <@bonsaikitten> Chainsaw: "stop attacking me you muppet" ;)
19:19 <@Betelgeuse> jmbsvicetto: The first exception is probably clearer with something like "The ChangeLog must be updated with each commit until the removal of the whole package."
19:19 <@jmbsvicetto> Betelgeuse: sure
19:19 <@Chainsaw> Betelgeuse: Which will promptly get abused as "but I was removing something, it says that's okay. let me be."
19:20 <@Betelgeuse> Chainsaw: If they abuse that I would just argue that they are malicious that should have a talk with QA.
19:20 <@Betelgeuse> +and
19:20 <@jmbsvicetto> All I'm saing is that I think it should be obvious that you don't run echangelog and commit saying I'm going to drop this from the tree and then commit the removal. However, some people are trying to use that absurd argument on their "campaign" against the echangelog policy
19:20 <@wired> changing the wording will probably change nothing
19:20 <@Chainsaw> jmbsvicetto: The correct response to an absurd argument is to reject it and keep the status quo.
19:20 <@jmbsvicetto> that's why I'm saying we might want to "clear" that
19:21 <@Chainsaw> jmbsvicetto: I am strongly opposed to making any further changes to the policy in question. It is unambiguous, it is producing inevitable but ultimately necessary & welcome results.
19:21 <@scarabeus> how about we keep the strict policy, and ask infra to implement Fabians patch, if we decide that we want only generated changelogs it will save us some time
19:22 <@jmbsvicetto> The second part was about people fixing a typo and comments - as long as it doesn't change the "code"
19:22 <@Chainsaw> jmbsvicetto: And if the community feels that my hard line on this is wrong, I strongly encourage them to not vote me in for the next term.
19:22 <@jmbsvicetto> Chainsaw: ok, I see your point
19:22 <@jmbsvicetto> Chainsaw: to be clear, I'm open to make it a bit more flexible, but only about commits that don't change the code
19:23 <@wired> I agree with Chainsaw here
19:23 <@scarabeus> jmbsvicetto: all commits change code, even your whitespace sed can screwup
19:23 <@jmbsvicetto> Should we have a vote on updating the policy or should we move on to other points, like getting automatic changelogs?
19:23 <@Chainsaw> jmbsvicetto: The only safe commits are commits that do not change an ebuild.
19:23 <@wired> I'd rather have exceptions to the written rule than open holes to the rule that will allow abuse
19:23 <@jmbsvicetto> scarabeus: yes, but I'm talking about comment lines, not code lines
19:23 <@bonsaikitten> automated changelogs are a good idea
19:23 <@bonsaikitten> what needs to be done to get them?
19:23 <@jmbsvicetto> bonsaikitten: just one second please
19:24 <@Chainsaw> jmbsvicetto: Let me change this #!/bin/sh comment for a second.
19:24 <@scarabeus> jmbsvicetto: well yeah but shit can happen you press enter while saving the file and do not notice and just commit the "tiny comment change" that breaks the ebuild :)
19:24 <@jmbsvicetto> just to confirm, the majority isn't willing to discuss an update to the policy, correct? So no need for a vote and we can move forward
19:24 <@Chainsaw> jmbsvicetto: It's okay, the rule says that anything with a leading hash is free game.
19:24 <@wired> imo most people complaining about the strict rules are just using that to avoid doing something (removals) *they* think is wrong
19:24 <@jmbsvicetto> Chainsaw: I understand, but that isn't what I'm talking about ;)
19:25 <@Chainsaw> jmbsvicetto: But that's what I can turn your modification into with little thought.
19:25 <@Betelgeuse> jmbsvicetto: To be sure you want to explicitly collect opinions.
19:25 <@wired> in the end this whole deal is stupid, we are wasting tons of time arguing about something extremely easy to fix in a number of ways (other than changing the policy)
19:25 <@jmbsvicetto> ok, so let's take care of this in a minute
19:26 <@Chainsaw> wired: Refusal to delete ebuilds until the policy is changed, yes. I saw that. Where I come from we call that blackmail.
19:26 <@Betelgeuse> I don't mind improving the documentation to be clearer on what happens when a package is removed.
19:26 <@jmbsvicetto> Who votes about discussing changes to the policy? An yes vote means we will talk about updating it, a no vote means it stays as was approved and we move forward
19:26 <@wired> Chainsaw: agreed. I find it hilariously stupid.
19:26  * Chainsaw votes no; the policy is perfect, move on
19:26 <@Betelgeuse> We could just add a new sentence: When the package is removed the ChangeLog is removed along with the rest of the files.
19:26  * bonsaikitten abstains, indifferent either way as there are multiple solutions
19:26 <@Betelgeuse> Or just note this in the summary
19:27 <@wired> the whole changelog argument sucks. tons of email, bug comments, council time for something that's fixed with a 3 line script and an ssh master connection
19:27 <@Betelgeuse> The exact text could just be left to people who usually maintain the document any way :)
19:27 <@jmbsvicetto> Betelgeuse: I'll add a note in the summary about it - let's call if a "clarification of the policy"
19:27 <@Betelgeuse> jmbsvicetto: Yeah and Recruiters/QA I think can improve the wording without council any way.
19:27 <@jmbsvicetto> I'm still waiting for the votes so we can move forward
19:28 <@jmbsvicetto> up till now we have: 1 no and 1 abstain
19:28 <@wired> jmbsvicetto: I say we leave it as is.
19:28 <@scarabeus> no keep the policy, the removal of the package is done without repoman and there is no possibility to run changelog in the category where you are doing the commit
19:28 <@Betelgeuse> jmbsvicetto: no and let people who maintain document what happens with package removals
19:29 <@jmbsvicetto> ok, so a no from me as well
19:29 <@jmbsvicetto> so 5 no and 1 abstain vote
19:29 <@wired> just a note
19:29 <@wired> a strict policy doesn't mean we can't have exceptions ( Chainsaw's example was nice )
19:30 <@jmbsvicetto> moving on, do we want to automate the process? How?
19:30 <@jmbsvicetto> wired: sure
19:30 <@Chainsaw> repoman -m could call echangelog & remanifest.
19:30 <@wired> +1
19:31 <@wired> I would also consider a server-side restriction if people get stubborn
19:31 <@bonsaikitten> acceptable proposal
19:31 <@scarabeus> i would preffer to run the thing on the server side only for rsync
19:31 <@Chainsaw> wired: That's a technical solution to a social problem. Don't go there.
19:31 <@scarabeus> so i just repoman commit
19:31 <@jmbsvicetto> There was an argument about droping ChangeLogs altogether and get them create at cvs host. Do we want to address that?
19:31 <@scarabeus> and then the server generatest the changelog on the server side from vcs
19:31 <@wired> Chainsaw: agreed. infact I hate policies. but if we have to have them we have to make sure they are followed
19:32 <@jmbsvicetto> Then there was also the argument about using a different scm altogether
19:32 <@Chainsaw> jmbsvicetto: I believe the newer SCM would make adding autogeneration to the workflow easier.
19:32 <@bonsaikitten> Chainsaw: future ideas
19:32 <@Chainsaw> jmbsvicetto: So if anything, I would like to postpone automatic generation until that scm is in place.
19:32 <@wired> jmbsvicetto: I'd welcome any better solution in the long run, but the repoman -m patch would be quicker until that happens
19:32 <@Betelgeuse> I support server side generation and git but there's little concrete we can do besides join helping the efforts individually.
19:32 <@Chainsaw> jmbsvicetto: What I see on the GIT migration worries me. It has a name.
19:32 <@Chainsaw> jmbsvicetto: "Second system syndrome"
19:33 <@jmbsvicetto> So, repoman -m calls echangelog for now and we strongly urge people to join the work to get the job done server side and moving to git?
19:34 <@jmbsvicetto> shall we have a vote on that?
19:34 <@scarabeus> Chainsaw: is it really that hard to do it on cvs side?
19:34 <@scarabeus> Chainsaw: from what i see the script works on cvs quite fine
19:35 <@scarabeus> ftr i expect git migration to take loong time unless somebody really adopt and work on it a lot
19:35 <@Chainsaw> scarabeus: If you can easily do it on CVS, then I'm all for it.
19:35 <@Chainsaw> scarabeus: What I don't want to see is it being used as another nail in the coffin of the CVS->GIT migration by adding more to the spec sheet.
19:35 <@bonsaikitten> so if we can easily do it from CVS I'm all for it
19:35 <@scarabeus> Chainsaw: nah the changelog generation on server side was always on "required for git migration"
19:36 <@wired> it would probably make it simpler
19:36 <@scarabeus> Chainsaw: it is harder to track changelog in git anyway :)
19:36 <@scarabeus> (collisions)
19:36 <@jmbsvicetto> ok, what about us voting the following:
19:36 <@Chainsaw> scarabeus: *nod* That eases my reservations then.
19:37 -!- angelos [angelos@gentoo/developer/angelos] has joined #gentoo-council
19:38 <@jmbsvicetto> The council decision about automating changelog messages is that repoman be updated to add the commit message to changelog, until such time as changelogs can be created server side. The council also urges individual developers to join the effort to move the tree to git.
19:38 < nirbheek> If I recall correctly, Fabian's analysis of server-side ChangeLog generation for CVS had numerous problems
19:38  * Chainsaw votes YES
19:38 <@wired> yes please
19:38 <@scarabeus> yes
19:39 <@jmbsvicetto> yes
19:40 <@Chainsaw> (Please note that this in no way invalidates the vote on the unchanged policy; developers can commit to CVS without repoman and may try to do so)
19:40 <@jmbsvicetto> yes from me as well
19:40 <@bonsaikitten> I don't see the point of tacking on that git rider
19:40 <@jmbsvicetto> who are we missing?
19:40 <@bonsaikitten> yes to the first part, irrelevant to the second
19:40 <@Betelgeuse> Chainsaw: I don't think committing without repoman in package directories is allowed
19:40 <@jmbsvicetto> bonsaikitten: I'll not that on the summary
19:41 <@jmbsvicetto> Betelgeuse: how do you vote?
19:41 <@Chainsaw> Betelgeuse: I've seen it done without alarm bells ringing, so I just wanted to get that out there.
19:41 <@Betelgeuse> Chainsaw: If you don't cause a problem you probably get away with it but doesn't make it allowed
19:42 <@scarabeus> Chainsaw: that HAS a policy :) so QA should handle that :)
19:42 <@Betelgeuse> jmbsvicetto: indifferent on the first as you could just as well wrap repoman as I do and yes and the second
19:42 <@Betelgeuse> jmbsvicetto: so I don't see a need to mandate it
19:42 <@Betelgeuse> if people find it easier then it can be added sure
19:43 -!- tampakrap [~tampakrap@gentoo/developer/tampakrap] has joined #gentoo-council
19:43 <@jmbsvicetto> Betelgeuse: sorry, I missed your 2nd point
19:43 -!- antarus [~antarus@gentoo/developer/antarus] has joined #gentoo-council
19:43 <@jmbsvicetto> Betelgeuse: "and yes and the second"? Yes to the 2nd point?
19:43 <@scarabeus> just to be exact it is damn easy to dumb write wrapper: http://dpaste.com/552036/
19:43 <@scarabeus> *write dumb
19:44 <@Betelgeuse> scarabeus: indeed
19:44 <@wired> scarabeus: I use this: http://paste.pocoo.org/show/403011/
19:44 <@jmbsvicetto> Betelgeuse: mind clearing the above for the summary?
19:45 <@Betelgeuse> jmbsvicetto: I support putting effort in to the git migration
19:45 <@jmbsvicetto> ok, thanks
19:45 <@jmbsvicetto> so let's move on
19:45 <@jmbsvicetto> Have you read my proposal for the GLEP48 update?
19:45 <@scarabeus> jmbsvicetto: post it for log purposes :)
19:46 <@jmbsvicetto> yes, sorry I forgot to add the link
19:46 <@jmbsvicetto> http://archives.gentoo.org/gentoo-project/msg_ac161677a6e06a8647e16420eeae8d47.xml
19:48 <@bonsaikitten> jmbsvicetto: read it, looks sane and harmless at first glance
19:48 < antarus> bonsaikitten: no it doesn't
19:48 <@bonsaikitten> at first glance :)
19:48 <@Betelgeuse> jmbsvicetto: The last point is problematic as it will take quite a while: 15 days QA + 17 days DevRel is a month to handle
19:49 <@jmbsvicetto> Betelgeuse: 15 days and 17 days are the limits
19:49 <@jmbsvicetto> nothing prevents QA and devrel from taking less time
19:49 <@Betelgeuse> DevRel usually takes the full time.
19:49 <@Betelgeuse> (or more when we fail)
19:49 <@jmbsvicetto> and this is about the extreme case
19:49 <@jmbsvicetto> sure, but we will have to take the blame for that
19:50 <+dberkholz> i don't see why the QA team should be different from anyone else when escalating an issue to devrel
19:50 < antarus> not to be a dick to flameeyes, but when the QA team is lead by crazy people I don't really feel happy giving them that kind of power
19:50 <+dberkholz> since it's specifically about nontechnical issues
19:50 <+dberkholz> so i'm not sure why that needs to be in the glep
19:50 <@jmbsvicetto> One thing I didn't write was that the last time we talked about this, the idea was that if QA failed to comply with the 15 days then the suspension would be immediately reversed
19:51 <@jmbsvicetto> Should we reduce that time window? 1 week?
19:52 <@Betelgeuse> jmbsvicetto: If we go as dberkholz suggested they get the DevRel 3 days
19:52 <@scarabeus> 3 days are enough
19:52 <@scarabeus> remember qa fill the bug
19:52 <@scarabeus> so they already have some evidence
19:52 <@scarabeus> so they just need to put it up and post
19:52 < c1pher> scarabeus: +1
19:52 <@jmbsvicetto> I have no problem with taking that 15 days part out
19:52 < antarus> jmbsvicetto: I am writing a short writeup, although I doubt anyone will  heed it ;p
19:53 <@Betelgeuse> jmbsvicetto: The other problem is that the text says nothing what happens after someone is suspended.
19:53 <@jmbsvicetto> given the discussion, let me replace the following:
19:53 <@jmbsvicetto>  Whenever the QA team escalates an issue to DevRel, it shall have 15 days to
19:53 <@jmbsvicetto> +  present the case and provide support evidence. DevRel shall then follow its
19:53 <@jmbsvicetto> +  established policies to evaluate it.
19:53 <+dberkholz> scarabeus: it's talking about escalating nontechnical issues only there. the bug is normally filed about technical ones...
19:53 <@Betelgeuse> How do they get access restored or revoked permanently.
19:53 <@Betelgeuse> jmbsvicetto: Just remove the point fully?
19:54 <@Betelgeuse> jmbsvicetto: As they are the same as any other DevRel bug reporter.
19:54 <@scarabeus> antarus: about abusing powers, did really diego over used them? or the QA resolution for samuli's bug for example is bad and offending (the attachment at the bug)?
19:54 <@jmbsvicetto> with "Whenever the QA team escalates an issue to DevRel, it shall follow DevRel's established policies, meaning it has 3 days to present evidence about the case."
19:55 <@scarabeus> jmbsvicetto: Whenever the QA team escalates an issue to the DevRel, it follows the Devrel's established policies.
19:55 <@jmbsvicetto> Betelgeuse: My proposal in this update is to give the decision about suspension to QA as long as the issue remains techincal
19:55 -!- NeddySeagoon [~NeddySeag@gentoo/developer/NeddySeagoon] has joined #gentoo-council
19:56 <@jmbsvicetto> So developers that won't fix the issue with QA, can appeal to the council or open a bug to devrel if they think QA is abusing powers
19:56 < antarus> scarabeus: to get away from Diego for a moment, it is not clear to me how new QA policies are added
19:56 <+dberkholz> so the idea is that QA can determine on its own how long suspensions should last, rather than it being set in stone by a glep. right?
19:56 <@Betelgeuse> dberkholz: that's already the case with DevRel too
19:56 <@jmbsvicetto> brb
19:56 <@scarabeus> antarus: policies are added by council or by dicussion on -dev/qa mls and should be voted by qa members
19:57 <@scarabeus> antarus: also there are not MUCH new policies as we are quite swamped by current ones anyway :D
19:57 <+dberkholz> Betelgeuse: i agree with it, i was just trying to make sure that's what was intended
19:57 <@Betelgeuse> Before QA fully handling the issues they really should have established practises on how they process issues.
19:57 <@jmbsvicetto> dberkholz: yes
19:57 <@scarabeus> s/we/QA/
19:57 < antarus> scarabeus: I dislike that QA both makes and enforces policy
19:58 <@bonsaikitten> good point
19:58 <@wired> council should always have the final word on new global policies
19:58 <@scarabeus> antarus: there is council above them if they accept weird policy council can smash their fingers
19:58 < ssuominen> Half of the QA team doesn't even agree adding or removing a ChangeLog entry belongs to the team -> Unjustified removal of commit access
19:58 < antarus> scarabeus: good I like finger smashing ;p
19:59 <@bonsaikitten> so I think we won't reach a consensus there yet. Push back to ML for further discussion?
19:59 <@jmbsvicetto> antarus: my point is that QA should enforce policies and provide input on creating policies
19:59 <@Betelgeuse> ssuominen: council > QA
19:59 <@jmbsvicetto> antarus: they shouldn't "approve" policies
19:59 <+dberkholz> i prefer that QA both makes and enforces policy
19:59 < antarus> dberkholz: I would be happier if policies were a bit more squishy
19:59 <@jmbsvicetto> bonsaikitten: push back my proposal?
19:59 < antarus> like debian, ironically ;)
20:00 <@wired> qa suggests policy, council accepts policy, qa enforces them
20:00 <@jmbsvicetto> bonsaikitten: I don't mind, but seeing as no one bothered to comment on it in the last week, I don't see that changing :P
20:00 <+dberkholz> so would i. i also think there are too many policies for one-time occurrences because we don't let "enforcers" do anything without a policy for it
20:00 <@bonsaikitten> jmbsvicetto: we're taking much time for a discussion that seems to have no clear goal yet
20:01 <@jmbsvicetto> ok, proposal pushed back to the ml
20:01 <@scarabeus> poor next council...
20:01 <@jmbsvicetto> removal of old-style virtuals
20:01 <@wired> scarabeus: heh
20:01 <@scarabeus> wired: just joking :)
20:01 <@jmbsvicetto> bonsaikitten: I'll be sponsoring this to the new council
20:01 <@jmbsvicetto> (my GLEP48 update proposal)
20:01 <@Betelgeuse> jmbsvicetto: Can we at least vote to commit the non suspension related parts?
20:01 <@Betelgeuse> We should get at least the agreed parts committed.
20:02 <@jmbsvicetto> I'm fine with that
20:02 <@Betelgeuse> http://www.gentoo.org/proj/en/glep/glep-0048.html
20:02 <@Betelgeuse> The official GLEP 48 can't be some diff somewhere
20:02 <@jmbsvicetto> Betelgeuse: what part do you suggest we use?
20:02 <@jmbsvicetto> Betelgeuse: do you mind pasting the diff so we can vote on it?
20:03 <@jmbsvicetto> If it gets approved I pledge to commit it to the glep space or get someone to do it (me looks at antarus)
20:03 <@scarabeus> jmbsvicetto: just ommit last for bullets from it and it is the previously approved update so lets just push that
20:04 <@scarabeus> s/for/four/
20:04 <@Betelgeuse> jmbsvicetto: http://paste.pocoo.org/show/403024/
20:04 <@jmbsvicetto> scarabeus: I added an initial point as well
20:04 < antarus> jmbsvicetto: http://pastebin.com/C1jGF1DJ
20:04 < antarus> jmbsvicetto: is my short ditty
20:04 <@scarabeus> jmbsvicetto: but that one is not questionable :)
20:04 < antarus> I think suspending someone over missing Changelog entries is somewhat laughable
20:05 < ssuominen> antarus: I call it power mongering.
20:05 <@jmbsvicetto> I'm trying to open both here
20:05 < antarus> I do appreciate that Gentoo had the balls to do it though ;)
20:05 < antarus> ssuominen: death by a thousand cuts is a difficult problem to fix :/
20:05 <@Betelgeuse> antarus: missing and explicitly refusing are different
20:05 <@bonsaikitten> antarus: slippery slope ... what other rules are ignored?
20:05 <@scarabeus> antarus: it was not over missing changelog, but over not willing to stop doing that
20:06 < antarus> bonsaikitten: the proposal details severity bits attached to violations
20:06 <@jmbsvicetto> antarus: skimming over your paste, that seems most appropriate to the QA team page than to GLEP48
20:06 < antarus> bonsaikitten: I don't think missing changelogs are 'dangerous' to end users
20:06 <@scarabeus> eg it was to be expected the policy will be further ignored by the developer if the access is kept
20:06 < antarus> the proposal = my ditty
20:06 <@jmbsvicetto> so, shall we commit the diff Betelgeuse pasted?
20:06 <@jmbsvicetto> I say yes
20:06 <@bonsaikitten> antarus: but disrespect of rules can be
20:06 < antarus> jmbsvicetto: perhaps I should run for QA lead then ;p
20:06  * Chainsaw votes YES for the Betelgeuse diff
20:06 < ssuominen> antarus: yes, please
20:06 < antarus> bonsaikitten: true enough ;)
20:07 <@bonsaikitten> consider it a vote of confidence ;)
20:07 < antarus> bonsaikitten: as I said, I appreciate the tenacity
20:07  * scarabeus vote yes for the diff
20:07 <+dberkholz> antarus: ever heard that story about broken windows and murders in new york city?
20:07 <@Betelgeuse> yes
20:07 <@wired> yes
20:07 < nirbheek> antarus, I think it's overcompensation :p
20:07 <@jmbsvicetto> so, 6 yes votes
20:07  * bonsaikitten votes yes for the Betelgeuse diff
20:07 < antarus> dberkholz: are you talking about the famous 'fail to assist' case?
20:08 <@jmbsvicetto> antarus: If I can't commit that diff to the glep space, I'm going to poke you to do it
20:08 < antarus> dberkholz: or something else?
20:08 < antarus> jmbsvicetto: ok
20:08 < antarus> jmbsvicetto: I'm pretty sure anyone can commit gleps though
20:08 <+dberkholz> antarus: nope. telling the policy to go enforce people who break windows and put graffiti on walls lowered the murder rate by 72%
20:08 <@jmbsvicetto> moving on, removal of old-style virtuals
20:08 <+dberkholz> police*
20:08 <@jmbsvicetto> ulm_ / zmedico: Do you guys have anything more to say about it?
20:08 < antarus> dberkholz: I take your point
20:09 < ulm> jmbsvicetto: should be all said in the posting to the ml and in the respective bug
20:09 < zmedico> jmbsvicetto: no :)
20:09 < antarus> I propose we move Gentoo to Singapore, I hear there is very little crime there
20:09 <@jmbsvicetto> ulm: I only have one question: is there any compatibility issue for people with old installs?
20:10 < ulm> I don't think so, it should be comparable to a package removal
20:10 <@jmbsvicetto> ok, thanks
20:10 < zmedico> yeah, roughtly
20:10 <@jmbsvicetto> Can we vote on this proposal?
20:10 < ulm> i.e. if portage doesn't resolve the old-style virtual any more, the package depending on it would pull in the new-style virtual
20:11 <@Betelgeuse> What happens with vdb for old packages?
20:11 <@Betelgeuse> ulm: hmm true
20:12 <@jmbsvicetto> bonsaikitten / Chainsaw / scarabeus / wired: ^^
20:12 <@scarabeus> i agree with the ban, it should not cause issues from what i see :)
20:12 <@Chainsaw> jmbsvicetto: Please state the proposal, for the record.
20:12 <@scarabeus> or should i say removal rather than ban? well anyway ack
20:12 <@bonsaikitten> removing old-style virtuals, I'm in favour of that idea.
20:12  * Chainsaw votes YES on removing old-style virtuals
20:13 <@wired> yes
20:13 <@jmbsvicetto> ok, let me add the link - http://archives.gentoo.org/gentoo-project/msg_7986aa2d9651c600f7302a8d84cc3721.xml
20:13 <@Betelgeuse> yes
20:13 <@jmbsvicetto> yes
20:14 <@jmbsvicetto> so 6 votes for the removal
20:14 <@jmbsvicetto> moving on
20:14 < ulm> thanks
20:14 <@jmbsvicetto> Do we want to look back at this term and share some thought and or make a balance?
20:15 <@scarabeus> It was quite fun :) and i think we didn't do really bad job at this :)
20:15 <@jmbsvicetto> thoughts*
20:15 <@scarabeus> but we still didn't get GIT done :D
20:15 -!- toralf [~toralf@g224126229.adsl.alicedsl.de] has joined #gentoo-council
20:16 <@scarabeus> just look on nirbheeks sad face, he must be disappointed by this council
20:17 <@bonsaikitten> so I resubmit for the next council to discuss and agree on signing keys and key distribution mechanisms
20:17  * bonsaikitten is a persistent bugger :)
20:17 <+dberkholz> if only we could take all the time spent in meetings and have the 7 of you work on git instead
20:17 <@scarabeus> dberkholz: didn't i work on git? :D more on the eclass side but i did :D
20:18 < nirbheek> scarabeus, I am disappoint.
20:18 <+dberkholz> i propose that all council candidates not elected set aside time to do this, since they clearly had the time to spare =P
20:18 <@jmbsvicetto> hehe
20:18  * wired had substantially less time than what he wanted to spend on gentoo + the council this term
20:19 <@scarabeus> btw bit offtopic how would you feel if council elect qa lead from the nominees generated by the qa team? (it is just question, not something i would plan as i am no longer even qa member)
20:19 <@jmbsvicetto> Looking at my manifesto for the election, I clearly left GLEP 39 reform behind. I had a talk on FOSDEM with Petteri and Roy that touched the issue, but I didn't got to write any proposals yet
20:19 <@bonsaikitten> well, better luck next time ;)
20:20 <@jmbsvicetto> scarabeus: I believe council should not elect team leads
20:20 <@scarabeus> i can say i improved the qa interaction :D or at least we spent almost all time talking about qa :D
20:20 <@bonsaikitten> things never go as planned, but I think we had a good time here
20:20 <@jmbsvicetto> scarabeus: and that's something for GLEP 39 ;)
20:20 <@Chainsaw> And in the case of bonsaikitten, short term, big waves. Watch that space :)
20:21 <@jmbsvicetto> It was a pleasure working with all of you on this term and I think we tried to get a few things rolling :)
20:21 <@bonsaikitten> Chainsaw: and one does wonder if someone is crazy enough to vote me back in
20:21 <@Chainsaw> bonsaikitten: You never know :)
20:21 < antarus> bonsaikitten: nothing burned down :)
20:21 <@bonsaikitten> antarus: I can change that
20:21 <@scarabeus> bonsaikitten: why not, you didn't burn anything out :)
20:21 <@Chainsaw> jmbsvicetto: Thank you for the various phone calls, which made a real difference for me.
20:21 <@jmbsvicetto> I hope to also be able to get the arch teams and automation stuff moving
20:21  * NeddySeagoon wonders if any of the current council are going to accept their nominations 
20:21 <@Chainsaw> What nominations?
20:22 <@wired> jmbsvicetto nominated us all
20:22 <@bonsaikitten> NeddySeagoon: yes, as soon as I can think of a manifesto-like thingy
20:22 <@bonsaikitten> more than "yes", in essence ;)
20:22 < NeddySeagoon> wired, except one
20:22 <@jmbsvicetto> NeddySeagoon: dilfridge fixed that ;)
20:22 < NeddySeagoon> jmbsvicetto, yes
20:22 <@jmbsvicetto> dilfridge: thanks again for that
20:23 < NeddySeagoon> Chainsaw, read the projects ml
20:23 <@jmbsvicetto> So, any more comments or should we move on?
20:24 <@wired> yeah, here's to an even better term for the next council :)
20:24 <@wired> nice working with y'all
20:24 <@Betelgeuse> Thanks all.
20:24 <@bonsaikitten> was nice to finally not be The Other Choice :)
20:24 <@bonsaikitten> although I guess it kept people honest
20:25 < NeddySeagoon> you don't get to retire until the new council is elected
20:25 <@Chainsaw> NeddySeagoon: Oh shoot, you mean I have to cancel my ticket to the Bahamas?
20:25 <@Chainsaw> NeddySeagoon: My plane leaves tomorrow morning!
20:25 <@jmbsvicetto> One last thing I think we should do is quickly go over the current open bugs and give a short status update so the next council can have an idea where things stand
20:25 < NeddySeagoon> Chainsaw, you can serve from there
20:25 <@Betelgeuse> jmbsvicetto: has there been a change?
20:26 <@jmbsvicetto> Chainsaw: no running away ;)
20:26 <@Betelgeuse> jmbsvicetto: you around?
20:26 <@Betelgeuse> jbartosik: ^
20:26 < jbartosik> Yes :)
20:26 <@wired> jmbsvicetto: I have one bug to still take care of before the elections
20:26 <@jmbsvicetto> Betelgeuse: not by my part.
20:26  * jbartosik begins talking about webapp
20:26 <@Betelgeuse> jmbsvicetto: maybe we could look at the web app status before then
20:26 <@jmbsvicetto> I failed to take care of bug 237381
20:26 < willikins> https://bugs.gentoo.org/237381 "Document appeals process"; Gentoo Linux, Unspecified; CONF; dberkholz:jmbsvicetto
20:27 <@jmbsvicetto> Betelgeuse: ok
20:27 <@jmbsvicetto> jbartosik: the floor is yours :)
20:27 < jbartosik> You can see a working demo of the application on http://morning-winter-26.heroku.com/
20:27 < jbartosik> Unfortunately there is a problem when application runs on heroku, and pages that give data to IRC bot
20:27 < jbartosik> So I will need nicks of those who would like to talk to the bot later (if you want to, please say it now)
20:28 <@bonsaikitten> so I shall disappear now. Talk to y'all soon / tomorrow :)
20:29 < jbartosik> Allright looks like I'll just demo bot myself
20:29 < jbartosik> As a guest you can view old & current agendas
20:29 < jbartosik> If you want to suggest agenda items you need to register
20:29 < jbartosik> If you do register, please use fake email (application doesn't need your real email).
20:30 < jbartosik> When you're done looking on the application as a regular user please tell me
20:30 < jbartosik> And give me a link to your profile ("Logged in as ..." in top-right corner)
20:30 < jbartosik> Then I'll give you council member role
20:30 <@Betelgeuse> jbartosik: http://morning-winter-26.heroku.com/users/3-betelgeuse
20:31 <@jmbsvicetto> jbartosik: http://morning-winter-26.heroku.com/users/4-jorge-manuel-b-s-vicetto
20:31 < jbartosik> Betelgeuse: Given
20:31 <@scarabeus> http://morning-winter-26.heroku.com/users/5-scarabeus
20:31 <@jmbsvicetto> jbartosik: what's the bot name?
20:31 < jbartosik> jmbsvicetto:  I gave you council_member role
20:32 < jbartosik>  jmbsvicetto: bot is not here, it's on #gentoo-council-webapp-testing
20:32 < jbartosik> As council member when you view current agenda you can change it's state
20:32 < jbartosik> To do this go to "Agendas" tab, then follow link to current agenda and click links on the bottom of the page
20:32 < jbartosik> Note that when agenda becomes "old" (archived) a new agenda is created
20:32 < jbartosik> As council members you can also add suggested items to current agenda, reject them and add voting options for items
20:32 <@wired> http://morning-winter-26.heroku.com/users/6-wired/account
20:32 < jbartosik> To do this view current agenda then click one of items and use buttons at the bottom of the page
20:33 < jbartosik> wired: added role
20:33 < jbartosik> Please let me know when you'd like to take a look at the bot
20:35  * wired wants to see how voting works
20:36 <@jmbsvicetto> jbartosik: I'm ready
20:36 <@jmbsvicetto> wired: me too
20:36 <+dberkholz> i'm curious about the weirder stuff
20:36 <+dberkholz> like, stopping a vote in the middle, restarting it, changing the text of the vote, etc.
20:36 < jbartosik> Please join #gentoo-council-webapp-testing
20:36 <@wired> can someone vote through the interface later?
20:36 < jbartosik> dberkholz: you can resume voting as many times as you want
20:37 <@Betelgeuse> wired: not yet but it's been a couply weeks now
20:37 <@Betelgeuse> +only
20:37 <+dberkholz> jbartosik: can you reset the count?
20:37 < jbartosik> dberkholz:  also you can vote many times for one item
20:37 < jbartosik> dberkholz: oonly newest vote will be counted
20:37 < jbartosik> dberkholz: no, ou can't reset vote now
20:37 <+dberkholz> that mostly solves the problem, except when someone votes and then goes afk before the 2nd one
20:40 < nirbheek> Wouldn't that set his/her vote to "abstain"?
20:40 <@jmbsvicetto> jbartosik: ^^
20:41 -!- ulm [~ulm@gentoo/developer/ulm] has quit [Quit: ERC Version 5.3 (IRC client for Emacs)]
20:45 -!- hypertux [~hypertux@vps1.joelegasse.com] has left #gentoo-council []
20:52 -!- tampakrap [~tampakrap@gentoo/developer/tampakrap] has left #gentoo-council ["http://quassel-irc.org - Chat comfortably. Anywhere."]
20:53 <@Betelgeuse> jmbsvicetto: ok let's move on
20:53 <@jmbsvicetto> So does anyone have anything to say about the currently open bugs?
20:54 <@jmbsvicetto> I just commited the update to GLEP48, so we can probably close bug 362803
20:54 < willikins> jmbsvicetto: https://bugs.gentoo.org/362803 "GLEP 48 (QA Team's Role and Purpose) update per council decision"; Gentoo Linux, Unspecified; CONF; tove:qa
20:54 <@jmbsvicetto> I'll work on the guideXML update later tonight
20:54 <@scarabeus> jmbsvicetto: awesome :)
20:55 <@wired> i still have to fix bug 341959 which I failed to take care of until now, I'll fix it soon
20:55 < willikins> wired: https://bugs.gentoo.org/341959 "council changed the waiting period in "eclass removal policy""; Doc Other, Devmanual; CONF; tove:qa
20:56 <@jmbsvicetto> If there is anything else, then we should open the floor to the community
20:57 <@jmbsvicetto> This is the last chance to get anything in this council term, so do it now or ... ;)
20:57 < jbartosik> nirbheek, jmbsvicetto: Not resuming vote doesn't change anyvotes already given.
20:57 <@wired> jmbsvicetto: flip the switch
20:57 <@wired> :)
20:58 <@jmbsvicetto> ok, once more thank you for being here and for this last year
20:58 -!- jmbsvicetto changed the topic of #gentoo-council to: Next meeting: up to the new council | http://www.gentoo.org/proj/en/council/utctolocal.html?time=1900 | http://www.gentoo.org/proj/en/council/ | agenda - http://archives.gentoo.org/gentoo-project/msg_0a7935a1097ba0aa41c3370a20679f9a.xml
20:58  * jmbsvicetto closes the meeting
