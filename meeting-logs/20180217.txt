<mgorny> ok, it is time                                                 [22:00]
<mgorny> !proj council
<willikins> (council@gentoo.org) dilfridge, k_f, mgorny, slyfox, tamiko, ulm,
            williamh
<mgorny> !proj trustees
<willikins> mgorny: (trustees@gentoo.org) alicef, dabbott, jmbsvicetto,
            kensington, klondike, maffblaster, prometheanfire, robbat2
<mgorny> roll call!
* ulm here
* mgorny here
<prometheanfire> here                                                   [22:01]
* dilfridge here
* K_F here
<kensington> Here
<prometheanfire> robbat2: stated he had other business to attend to, but I'll
                 give his (short) update
<dilfridge> welcome to the president's hour!                            [22:02]
<mgorny> anyone else we should expect/wait for?
<prometheanfire> I think we can move on                                 [22:03]
<mgorny> hmm, wait a sec
<mgorny> i'll try to ping people who are not on the channel
<prometheanfire> not sure if alicef is awake yet (given the notice was sent
                 out a couple of hours ago)
<prometheanfire> k
<mgorny> prometheanfire: could you /invite slyfox and tamiko?           [22:04]
<prometheanfire> this channel shouldn't be private
<prometheanfire> are their nicks not registered?
<prometheanfire> (also, invited)                                        [22:05]
<mgorny> prometheanfire: was asking in case they have auto-invite-join  [22:06]
<mgorny> anyway, let's proceed
<mgorny> 1. Copyright policy
<mgorny> ulm, alicef                                                    [22:07]
<ulm> some progress there
<mgorny> i think the main items in TODO are updating for the FLA, then sending
         it for wider review                                            [22:08]
<ulm> DCO has been updated, as well as some of the policies
<ulm> yes, we should create a FLA based on the FSFE's new version
*** tamiko (~tamiko@gentoo/developer/tamiko) has joined channel
    #gentoo-trustees
*** ChanServ (ChanServ@services.) has changed mode for #gentoo-trustees to +v
    tamiko
<prometheanfire> is that the only remaining item before sending for wider
                 review?
* prometheanfire would like a positive ack                              [22:09]
<ulm> what I wanted to ask is if formatting this as a GLEP is fine with
      trustees?
<prometheanfire> rather than a passive one :D
<prometheanfire> I suppose it'd still need our signoff, but the formatting
                 itself shouldn't matter
<prometheanfire> (need our signoff because copyright/legal)             [22:10]
<ulm> sure, GLEP format doesn't necessarily imply GLEP workflow for approval
      in this case
* tamiko reporting in (sorry for the delay)
<ulm> so I'll go ahead with what is in https://github.com/ulm/copyrightpolicy
      and obtain a GLEP number for it                                   [22:11]
<mgorny> i'd really like to see it applying ASAP
<mgorny> i feel bad about every pull request without proper copyright policy
         in place
<mgorny> anyone have any questions, suggestions, requests or should we move
         on?                                                            [22:12]
<prometheanfire> ulm: sgtm
<ulm> still not entirely clear what we should put in ebuild headers, that may
      need another iteration
<ulm> but we've already narrowed it down
<prometheanfire> ya, it's getting more straigforward as we go           [22:13]
<ulm> mgorny: nothing else from my side
<mgorny> ok, let's move on                                              [22:14]
<mgorny> - Financial status of the foundation
<mgorny> prometheanfire: you wanted to say sth
<prometheanfire> sure
<prometheanfire> just repeating what robbat2 said earlier               [22:15]
<prometheanfire> there hasn't been progress made on that front (tax wise)
<prometheanfire> the tax guy he'd been seeing in person moved across the
                 country, they can still work together, but not in person
<prometheanfire> we (the trustees) have also been looking into someone to
                 manage the books (and that alone)                      [22:16]
<prometheanfire> that's it, any questions?
<K_F> thanks for update, nothing specific from my side
<mgorny> i've seen some discussion about using proprietary software for that
         but i think that's out of the question given the SC            [22:18]
<prometheanfire> ya, rich0 suggested it iirc, but robbat2 pointed him to the
                 SC
<ulm> "Gentoo will never depend upon ..." is rather clear               [22:19]
<veremitz> is Gentoo actually depending on it? or just Using it?
<NeddySeagoon> Just using it.
* dilfridge heard some rumors about debian and gitlab
<mgorny> which makes me wonder why infra is using Amazon AWS but well..
                                                                        [22:20]
<mgorny> we're straying from topic
<prometheanfire> dilfridge: they use the open source gitlab iirc
<NeddySeagoon> There are OS alternatives ... but it looks like CPAs don't use
               them
<prometheanfire> mgorny: or any cloud provider, or cdn, etc :P
<mgorny> unless anyone has any comments regarding financials, let's move on
<prometheanfire> ack
<mgorny> - Purpose of the Foundation Council split                      [22:21]
<mgorny> is there anything to add here or can we scratch it from future
         agendas?
<K_F> can likely scratch it                                             [22:22]
<prometheanfire> scratch
<ulm> +1
<mgorny> - Legal protection for the foundation
<mgorny> anything more here or scratch as well?
<K_F> from my side any question got cleared up last time                [22:23]
<prometheanfire> scratch                                                [22:24]
<mgorny> ok, next
<mgorny> - Criteria for accepting members to the foundation
<mgorny> did anything happen here?
<prometheanfire> not sure, I don't think so
<prometheanfire> there was never a clear outcome there
<mgorny> lemme quote the last summary                                   [22:25]
<mgorny> *result* Foundation was willing to tighten this, something like
<mgorny>         the staffer quiz to be given to non-devs (and judged by the
<mgorny>         trustees and/or officers), it'd take a bylaw change and
         someone
<mgorny>         to 'champion' it.
<mgorny> so, does anyone want to do it?
<NeddySeagoon> It does not need a bylaw change.  Just a trustees vote.
<prometheanfire> over the next month I do not (openstack packaging and travel)
<klondike> Hi, sorry for the delay                                      [22:26]
<prometheanfire> NeddySeagoon: woudln't it modify
                 https://wiki.gentoo.org/wiki/Foundation:Bylaws#Section_4.3._Admission_of_Members
<K_F> prometheanfire: no, it already requires a trustee vote to become a
      member
<K_F> so it'd just be a policy change on requirements for such an approval,
      which wouldn't require bylaw change per se                        [22:27]
<mgorny> anyone volunteering or wanting to add something, or should we move
         on?
<K_F> since the list of acceptance criteria is non-exhausive to begin with
<prometheanfire> K_F: k, makes sense
<mgorny> (sorry, got a big of lag here)
<mgorny> bit*
<NeddySeagoon> prometheanfire: The trustees would use the quiz to fulfil "cite
               verifiable evidence of contributing to Gentoo"
<mgorny> K_F: want to oversee this on Council end?                      [22:28]
<prometheanfire> I'd be open to giving feedback to any proposal, but don't
                 have time to do it myself
<prometheanfire> NeddySeagoon: ack
<K_F> mgorny: I can do that, but someone from trustees should be main
      contributor of it
<NeddySeagoon> Put it on the March Trustees agenda for a vote
<NeddySeagoon> as quoted by mgorny above                                [22:29]
<mgorny> ok, let's let Trustees decide later and move on
<mgorny> - Funding for travel and meetups
<dabbott> o/ sorry for being late                                       [22:30]
<mgorny> anything to add here?
<prometheanfire> mgorny: don't think so, nothing changed since the last time
* Shentino is present
<dilfridge> for whom?                                                   [22:31]
<prometheanfire> the two trustee items can likely be changed into reporting
                 and proctors
<mgorny> yeah, let's take both at the same time                         [22:32]
<mgorny> - CoC enforcement + Comrel
<dilfridge> well
<mgorny> dilfridge, prometheanfire
* Shentino is present for himself as a foundation member observing the
  meeting, but did want to cite bug 645192 in relation to the foundation
  membership criteria just mentioned
<willikins> Shentino: https://bugs.gentoo.org/645192 "Staff quiz and gpg
            competence should be required for foundation membership"; Gentoo
            Foundation, Proposals; CONF; shentino:trustees
<dilfridge> as far as the e-mails got exchanged, I think prometheanfire
            basically agrees with my plan for the proctors
<prometheanfire> dilfridge: ya, generally, the main thing is that I want it to
                 be independant                                         [22:33]
<dilfridge> there may be some small details, but I offhand dont remember any
            big issues with the proposed policies
<prometheanfire> ya, the policies themselves seemed fine
<mgorny> independent of whom? of gentoo completely?
<dilfridge> I dont care how the project structure looks like as long as the
            escalation path is fine
<dilfridge> of comrel
<dilfridge> the question was "subproject of comrel or not"              [22:34]
<NeddySeagoon> It would be good to launch proctors with a CoC review and
               endorsemeth by both council and trustees.
<prometheanfire> NeddySeagoon: I imagine that's fine
<mgorny> i should point out that 'subproject or not' problem is generally a
         bit silly, given that gnetoo project structure is a bit silly and
         hysterical by design
<dilfridge> mgorny: exactly
<mgorny> as for escalation, is it proctors -> comrel -> council?        [22:35]
* prometheanfire generally doesn't have a problem with the CoC as it exists
<dilfridge> yes
<NeddySeagoon> prometheanfire: Review <> change
<dilfridge> as for the other thing, reporting, well what prometheanfire
            requested (notifying trustees when a comrel action is taken)
            should be no problem either
<prometheanfire> we just need to get that process codified is all       [22:36]
<mgorny> as i've mentioned before, i don't think providing details to trustees
         would be a problem as long as confidentiality of appropriate private
         information is preserved                                       [22:37]
<dilfridge> that's ok, I think, but
<prometheanfire> I can ask during our meeting, but I don't think that's a
                 problem
<mgorny> one question would be whether we're proactively passing all
         information or only when there is a need to
<dilfridge> what I would like to avoid is that it messes up the escalation
            path again
<mgorny> dilfridge: if we only give it for informational purposes only, i
         don't think so                                                 [22:38]
<dilfridge> yes, exactly
<prometheanfire> I'd like it to be proactive, so we know of potential problems
                 ahead of time
<NeddySeagoon> Trustees are informed.  There is no escalation path
<mgorny> i.e. escalation still works the same, trustees don't need to
         intervene unless something really illegal happens
<prometheanfire> mgorny: yep, that's basically it
<antarus> its not an escalation, just an audit                          [22:39]
<antarus> (trail)
<mgorny> prometheanfire: lemme rephrase. do you need just information that an
         action was taken, or access to all evidence proactively?
<dilfridge> sounds ok to me. I need to pass it by the team.
<antarus> (or maybe notification is a better term)
<mgorny> what i'm aiming for is spreading the private details as little as
         possible
<prometheanfire> mgorny: I think after action is taken would be sufficient
                                                                        [22:40]
<mgorny> ok, we have an agreement here, i presume prometheanfire and dilfridge
         will work on it further
<mgorny> anyone else have comments on this topic?
<prometheanfire> sure
<dilfridge> as I said I need to run this past the team                  [22:41]
<mgorny> prometheanfire: on related topic, any news on moderation?
<antarus> (I think there is an intersting argument on what actions the
          trustees might take when notice is given, but I'll follow up with
          prometheanfire ;)
*** jstein (~jstein@gentoo/developer/jstein) has joined channel
    #gentoo-trustees                                                    [22:42]
* dilfridge brb, spaghetti transfer
<prometheanfire> re: moderation, we can use mailman, and if we are all happy
                 with it, it can be moved to a more permament install
                                                                        [22:43]
<kensington> Moderation--
<mgorny> prometheanfire: did you establish if it's generally 'better' than
         mlmmj?                                                         [22:44]
<dilfridge> back
<prometheanfire> I think so
<prometheanfire> easier to manage                                       [22:45]
<mgorny> any ETA on bringing it to production?
* prometheanfire prefers to just manage it in a venv
<prometheanfire> lets say next month, next combined meeting time at the
                 earliest (given other constraints)
<mgorny> ok                                                             [22:46]
<mgorny> anything else to say or should we switch to open floor?
<prometheanfire> it is simple to set up though, for anyone with python
                 knowlege
<prometheanfire> open floor is fine with me
<mgorny> so, let the floor be open                                      [22:47]
<mgorny> anyone has anything to discuss?                                [22:48]
<dabbott> I would like opinions on accepting drobbins as a foundation member
<NeddySeagoon> dabbott: He needs to do his quiz
<prometheanfire> his contributions are generally far in the past, but he's
                 doing his quiz (last I heard)
<prometheanfire> does gentoo get good reciprocity from funtoo?          [22:49]
<mgorny> didn't he have some recent contributions to Portage though?    [22:50]
<mgorny> (i don't recall if those were actual patches)
<antarus> mgorny: he send some patches that Zac merged
<antarus> I'd appreciate it if he had a sponsor who could make a statement
                                                                        [22:51]
<antarus> there are a lot of ideas (and thoughts on leadership)
<antarus> https://gitweb.gentoo.org/proj/portage.git/log/?qt=author&q=drobbins
<mgorny> well, *if* he is really doing quizzes and intending to return as a
         developer, i don't think there is a need to separately consider
         foundation membership
<antarus> (are his portage patches)                                     [22:52]
<mgorny> because that would be rather implicit then
<prometheanfire> yep
<dabbott> ok
<Shentino> mgorny: I cited bug 645192 in relation to your issue regarding
           foundation membership criteria
<willikins> Shentino: https://bugs.gentoo.org/645192 "Staff quiz and gpg
            competence should be required for foundation membership"; Gentoo
            Foundation, Proposals; CONF; shentino:trustees
<antarus> yeah I'm trying to ascertain dabbott's feeling here ;)
<prometheanfire> antarus: that gets an ack from me then
<prometheanfire> K_F: might want to look at  https://bugs.gentoo.org/645192 if
                 you are leading the council side of that               [22:53]
<prometheanfire> 5 min til trustee meeting
<klondike> yay
<Shentino> I'm...not sure if I brought it up at the correct time during the
           meeting but I do think it's a worthy issue
<dabbott> I just asked him if he wanted to get more involved and he said yes,
          was just checking on what everyone thoughts were
<antarus> dabbott: I wasn't around for the politics of the last attempt by
          drobbins
<K_F> prometheanfire: I don't see anything there relevant for today's meeting,
      but sure, the foundation quiz can be a superset of staff quiz
<antarus> but IMHO there is no need to avoid admittance provided we think he
          returns in good faith and I've been fairly happy with his
          contributions thus far                                        [22:54]
<antarus> no worse than others we have admitted ;)
<mgorny> well, if there is no other topic to be included, then we might finish
         and give trustees a 5                                          [22:55]
<mgorny> they might want to visit their happy place before the meeting ;-)
<klondike> antarus: people change over time and based on the context
<ulm> mgorny: thank you for chairing
<prometheanfire> :D
<prometheanfire> yep, good meeting
<dilfridge> ++
<mgorny> thanks to everyone present
<mgorny> next meeting March, same day, same hour?                       [22:56]
<prometheanfire> ya, febuary being 28 days makes scheduling even easier :D
<dabbott> sounds good                                                   [22:57]
* mgorny bangs the gavel
