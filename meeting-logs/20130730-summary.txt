Summary of Gentoo council meeting 30 July 2013


Agenda
======
1) Vote for meeting schedule
2) Vote for continuing last council's workflow
3) Appoint chairman for this term's meetings
4) Vote on meeting format 1: "the open floor is the mailing list discussion", 
   i.e. no open floor during the meeting anymore
5) Vote on meeting format 2: "shift council votes to mail instead of IRC"
6) Open floor to council members to introduce themselves
7) General discussion on the introduction of a "Bikeshed of the month" 
8) Open bugs with council involvement (currently only #477030)
9) Open floor


Roll call
=========
Present:
blueness, dberkholz, dilfridge, rich0, scarabeus, ulm, williamh


1) Vote for meeting schedule
============================
Council meetings will be 2nd tuesdays of the month at 19:00 UTC.
(carried unanimous)


2) Vote for continuing last council's workflow
==============================================
We will send out a call for agenda items (2 weeks in advance), send the agenda 
(1 week in advance) and aim to have the meeting focussed, e.g., have major 
discussions on the -project mailing list prior to the meeting.
(carried unanimous)


3) Appoint chairman for this term's meetings
============================================
By general consensus a provisionary list was made: 
  July - dilfridge
  August - ulm
  September - ulm
  October - dilfridge
  November - dilfridge
  December - dberkholz
  January - dberkholz
  February - dberkholz
  March - blueness
  April - blueness
Later dates will be decided next year.


4) Vote on meeting format 1: "the open floor is the mailing list discussion" 
============================================================================
Vote: "Should we discontinue open floor at the end of the meeting?"
2 yes, 5 no - not carried


5) Vote on meeting format 2: "shift council votes to mail instead of IRC"
=========================================================================
Vote: "Shift council votes to mail instead of IRC"
5 no - not carried

Vote: "Allow voting via leaving comments YES/NO/ABSTAIN on a bug for urgent
issues, with prior notice (at least 3 days earlier) to -project whenever 
practical"
4 yes, 1 no, 1 abstention - motion carried


6) Open floor to council members to introduce themselves
========================================================
See the full log for the detailed statements.


7) General discussion on the introduction of a "Bikeshed of the month" 
======================================================================
The idea is to pick topics where a decision clearly makes sense, but people 
could not agree during bikeshedding, put them on the agenda and try to settle 
things. General agreement was that this does not need to be formalized but can 
be handled via established Council procedures. 


8) Open bugs with council involvement (currently only #477030)
==============================================================
We agree to kick Betelgeuse to complete the missing meeting summary 
whenever we see him. 


9) Open Floor
=============
williamh asks how having bugzilla involved in Council matters (see point 5) 
changes how to bring an issue to Council's attention. After some discussion, 
the following is brought up for vote:

Vote: "For adding an item to the agenda of the next council meeting, please 1)
assign a bug to council and 2) link to it in a post on -project; the discussion
should take place on -project"
A decision was postponed, also for discussion on the mailing lists.
